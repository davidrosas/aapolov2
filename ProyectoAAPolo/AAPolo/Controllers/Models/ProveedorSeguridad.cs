﻿using AAPolo.Aplication;
using AAPolo.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

namespace AAPolo.Controllers.Models
{
    public class ProveedorSeguridad : MembershipProvider
    {
        public async Task<MembershipUser> GetUser(string DocumentoIdentidad, string Password, int Cod_Persona, bool userIsOnline)
        {
            PersonaSM Seg = new PersonaSM();
            Persona per = new Persona() { Codigo = Cod_Persona, DocumentoIdentidad = DocumentoIdentidad, Password = Password };
            Persona m = new Persona();
            per = Seg.BuscarUsuarioLogin(per);

            if (per != null && per.Codigo > 0)
            {
                MembershipUser memUser = new MembershipUser("ProveedorSeguridad",
                                                            per.DocumentoIdentidad,
                                                            per.Codigo,
                                                            DocumentoIdentidad, string.Empty,
                                                            string.Empty,
                                                            true,
                                                            false,
                                                            DateTime.MinValue,
                                                            DateTime.MinValue,
                                                            DateTime.MinValue,
                                                            DateTime.Now,
                                                            DateTime.Now);
                return memUser;

            }

            return null;
        }

        public MembershipUser GetUser(UsuarioLogin obj, bool userIsOnline)
        {

            if (obj != null && obj.Codigo > 0)
            {
                try
                {
                    MembershipUser memUser = new MembershipUser("ProveedorSeguridad",
                                                                obj.DocumentoIdentidad,
                                                                JsonConvert.SerializeObject(obj),
                                                                obj.DocumentoIdentidad,
                                                                string.Empty,
                                                                string.Empty,
                                                                true,
                                                                false,
                                                                DateTime.MinValue,
                                                                DateTime.MinValue,
                                                                DateTime.MinValue,
                                                                DateTime.Now,
                                                                DateTime.Now);

                    return memUser;
                }
                catch (Exception)
                {
                    throw;
                }

            }

            return null;
        }

        public override MembershipUser GetUser(string Correo, bool userIsOnline)
        {
            //SeguridadSM Seg = new SeguridadSM();

            ////string[] cadena = username.Split('@');

            //Persona per = Seg.BuscarUsuarioLogin(Correo, Password);
            ////usr = svcSeguridad.BuscarUsuario("", cadena[0]);

            //if (per != null && per.Cod_Persona > 0)
            //{
            //    MembershipUser memUser = new MembershipUser("ProveedorSeguridad",
            //                                                per.Correo,
            //                                                per.Cod_Persona,
            //                                                Correo, string.Empty,
            //                                                string.Empty,
            //                                                true,
            //                                                false,
            //                                                DateTime.MinValue,
            //                                                DateTime.MinValue,
            //                                                DateTime.MinValue,
            //                                                DateTime.Now,
            //                                                DateTime.Now);
            //    return memUser;

            //}

            return null;
        }


        public override bool ValidateUser(string username, string password)
        {
            //    svcHISSEG.IsvcHISSEGClient svcSeguridad = new svcHISSEG.IsvcHISSEGClient();
            //    string[] cadena = username.Split('@'); /* Cadena[0] : almacena el usuario || Cadena[1] : almacena el dominio del Usuario -- estos datos seran verificados si existen en el ActiveDirectory y posteriormente en la BD */
            //    /* Validacion del Dominio de Usuario */
            //    mDominio mDom = svcSeguridad.ValidarDominio(username, 0);

            //    if (mDom.PathActiveDirectory == null)
            //    {
            //        return false;
            //    }
            //    else
            //    {
            //        /* Verificacion de los datos del Usuario en el PathActiveDirectory */
            //        if (svcSeguridad.EstaAutenticado(cadena[0], password, cadena[1], mDom.PathActiveDirectory))/
            //        {
            //            /* Busqueda de Usuarios activos en el PathActiveDirectory*/
            //            try
            //            {
            //                /* Obtencion de los datos de Usuario solicitado */
            //                var mUsu = svcSeguridad.BuscarUsuario("", cadena[0]);

            //                /* Se verifica que el usuario se encuentre activo*/
            //                if (mUsu.Cod_Estatus == 1)//Activo
            //                {
            //                    return true;
            //                }


            //            }
            //            catch (Exception ex)
            //            {
            //                return false;
            //            }
            //        }
            //        else
            //        {
            //            return false;

            //        }
            //    }

            return false;
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }


        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }
    }
}