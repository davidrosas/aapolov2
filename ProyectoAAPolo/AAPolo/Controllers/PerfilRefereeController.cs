﻿using AAPolo.Aplication;
using AAPolo.Controllers.Helper;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Controllers
{
    [Authorize]
    public class PerfilRefereeController : ClassHelper
    {
        // GET: PerfilReferee
        public async Task<ActionResult> Index()
        {
            if (DatosUsuario().Codigo == 0 || DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Jugador)
                return RedirectToAction("Index", "Home");

            if (!DatosUsuario().EsReferee)
                return RedirectToAction("Index", "PerfilJugador");

            Referee objRef = new Referee()
            {
                Persona = DatosUsuario()
            };

            var vRefPersona = new PersonaSM().BuscaJugadorAsync(objRef.Persona);
            //var vPerAux = new PersonaSM().BuscaJugadorPagoHandicapAsync(objRef.Codigo, "", 0, 0, 0);

            var vObj = new RefereeSM().BuscaRefereeAsync("", 0, DatosUsuario().Codigo, 0);

            await Task.WhenAll(vRefPersona, /*vPerAux,*/ vObj);

            objRef.Persona = vRefPersona.Result.FirstOrDefault();
            Persona perAux = vRefPersona.Result.FirstOrDefault();
            Referee obj = vObj.Result.FirstOrDefault();

            //return View(obj);
            //------------------------------------------------------------------------------------------------------

            objRef.Persona.HandicapPago = perAux.HandicapPago;

            if (objRef.Persona.HandicapPersona != null)
            {
                objRef.Persona.HandicapPersona.strFechaHasta = perAux.HandicapPersona.strFechaHasta;
                objRef.Persona.HandicapPersona.DescripcionHandicapUsuario = perAux.HandicapPersona.DescripcionHandicapUsuario;
            }
            else
            {
                objRef.Persona.HandicapPersona = new HandicapPersona()
                {
                    strFechaHasta = perAux.HandicapPersona.strFechaHasta,
                    DescripcionHandicapUsuario = perAux.HandicapPersona.DescripcionHandicapUsuario
                };
            }

            if (objRef.Persona.HandicapPago && objRef.Persona.lClubUsuarioSecundario.Where(x => x.EsPrincipal && x.Codigo == ConfiguracionAPP.Clud_NoInformado).Count() > 0)
            {
                ViewBag.lstClub = new ClubSM().BuscaClub(0, 0, "", 0).Where(x => x.EsActivo).ToList();
            }

            objRef.EsActivo = obj.EsActivo;
            objRef.CantidadEvaluados = obj.CantidadEvaluados;
            objRef.PromedioPuntaje = obj.PromedioPuntaje;
            objRef.SumaPuntaje = obj.SumaPuntaje;
            objRef.Ranking = obj.Ranking;
            objRef.Persona.TipoReferee = obj.Persona.TipoReferee;

            return View(objRef);

        }

        //[OutputCache(Duration = 1200/*, Location = System.Web.UI.OutputCacheLocation.Any*/)] //setear en cerrar sesion
        public ActionResult _RankingReferee()
        {
            try
            {
                return PartialView(new RefereeSM().RankingReferee("", 0, DatosUsuario().Codigo).FirstOrDefault());
            }
            catch (Exception)
            {
                return PartialView(new List<Referee>());
            }
        }

        public PartialViewResult _Listado(int Cod_Torneo = 0, bool SoloPartidoEfectuado = false)
        {
            Persona p = DatosUsuario();
            List<Referee> lRef = new List<Referee>();
            try
            {
                List<Referee> lReferee = Task.Run(async () =>
                {
                    Task<List<Referee>> vlReferee = new RefereeSM().BuscarEvaluacionRefereeAsync(DatosUsuario().Codigo);
                    /* --- REACTIVAR CUANDO SE CONTINUE DESARROLLO DE FICHA DE PARTIDO
                    Task<List<Referee>> vlReferee = new RefereeSM().BuscarPartidoEvaluacionRefereeAsync((SoloPartidoEfectuado? p.Codigo:0), Cod_Torneo, SoloPartidoEfectuado);
                    if (SoloPartidoEfectuado)
                    {
                        Task<List<Referee>> vlReferee2 = new RefereeSM().BuscarEvaluacionRefereeAsync(DatosUsuario().Codigo);
                        await Task.WhenAll(vlReferee, vlReferee2);
                        lRef = vlReferee.Result;
                        if (SoloPartidoEfectuado)
                        {
                            lRef.AddRange(vlReferee2.Result);
                        }
                    }
                    else {
                        await Task.WhenAll(vlReferee);
                        lRef = vlReferee.Result;
                    }
                    
                    lRef = lRef.OrderBy(x => x.Evaluacion.FechaPartido).ToList();
                    */
                    await Task.WhenAll(vlReferee); //QUITAR AL CONTINUAR DESARROLLO DE FICHA DE PARTIDO
                    lRef = vlReferee.Result.OrderBy(x => x.Evaluacion.FechaPartido).ToList(); //QUITAR AL CONTINUAR DESARROLLO DE FICHA DE PARTIDO
                    return lRef;
                }).Result;

                return PartialView(lReferee);
            }
            catch (Exception e)
            {
                return PartialView(new List<Referee>());
            }
        }

        public PartialViewResult _SelectTorneoActivo()
        {
            try
            {
                return PartialView(new TorneoSM().BuscarTorneo(0, 0, 0, true));
            }
            catch (Exception)
            {
                return PartialView(new List<Torneo>());
            }
        }

        public PartialViewResult _ModalDatosEvaluacionReferee(int Cod_Evaluacion)
        {

            try
            {
                return PartialView(new RefereeSM().BuscarEvaluacionReferee(DatosUsuario().Codigo, Cod_Evaluacion));
            }
            catch (Exception)
            {
                return PartialView(new Evaluacion());
            }

        }

        public PartialViewResult _ModalResultadoPartido(int Cod_Evaluacion = 0, int Cod_Partido = 0) {

            Partido objPart = new Partido();
            Evaluacion objEval = new Evaluacion();
            if (Cod_Partido > 0)
            {
                objPart = new TorneoSM().BuscarPartidoInstancia(0, 0, Cod_Partido, 2).FirstOrDefault();
            }
            else {
                objEval = new RefereeSM().BuscarEvaluacionReferee(DatosUsuario().Codigo, Cod_Evaluacion);
                objPart = objEval.PartidoEvaluado;
            }
            if (objPart != null) {
                Tuple<Referee,Persona> t = Task.Run(async () => { 

                    Task<List<Referee>> vlReferee = new RefereeSM().BuscaRefereeAsync("", 0, 0, objPart.Referee.Codigo);
                    if (objPart.Veedor.Codigo > 0)
                    {
                        Task<List<Persona>> vlPersona = objPart.Veedor.Codigo > 0 ? new PersonaSM().BuscaJugadorAsync(objPart.Veedor) : null;
                        await Task.WhenAll(vlReferee, vlPersona);
                        return new Tuple<Referee, Persona>(vlReferee.Result.FirstOrDefault(), (vlPersona!= null? vlPersona.Result.FirstOrDefault(): new Persona()));
                    }
                    else {
                        await Task.WhenAll(vlReferee);
                        return new Tuple<Referee, Persona>(vlReferee.Result.FirstOrDefault(), new Persona());
                    }
                }).Result;

                objPart.Referee = t.Item1;
                objPart.Veedor = t.Item2;
            }

            return PartialView(objPart);
        }

        public PartialViewResult _SelectEquipoPartido(int Cod_Partido, int Cod_Equipo = 0)
        {

            ViewBag.Cod_Equipo = Cod_Equipo;
            List<Equipo> lEquipo = new TorneoSM().BuscarEquipoPartidoInstancia(Cod_Partido);
            return PartialView(lEquipo);
        }

        public PartialViewResult _ListaJugadorEquipo(int Codigo = 0)
        {
            try
            {
                Equipo Eq = new Equipo();
                if (Codigo > 0)
                {
                    Eq = new TorneoSM().BuscarEquipoZona(0, 0, Codigo).FirstOrDefault();
                    if (Eq != null)
                        Eq.lPersona = new TorneoSM().ListadoPersonaEquipoPartido(Codigo, 0);
                }
                return PartialView(Eq);
            }
            catch (Exception)
            {
                return PartialView(new List<Persona>());
            }
        }

        public PartialViewResult _ListaJugadorEquipoIncidencia(int Codigo = 0)
        {
            try
            {
                Equipo Eq = new Equipo();
                if (Codigo > 0)
                {
                    Eq = new TorneoSM().BuscarEquipoZona(0, 0, Codigo).FirstOrDefault();
                    if (Eq != null)
                        Eq.lPersona = new TorneoSM().ListadoPersonaEquipoPartido(Codigo, 0);
                }
                return PartialView(Eq);
            }
            catch (Exception)
            {
                return PartialView(new List<Persona>());
            }
        }

    }
}