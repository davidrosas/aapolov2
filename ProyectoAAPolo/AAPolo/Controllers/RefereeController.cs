﻿using AAPolo.Aplication;
using AAPolo.Controllers.Helper;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Controllers
{
    [Authorize]
    public class RefereeController : ClassHelper
    {
        // GET: Referee
        public ActionResult Index()
        {
            if(DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            return View();
        }
        
        public PartialViewResult _Listado(string DocumentoNombre = "", int Cod_tipoReferee = 0) {
            try
            {
                return PartialView(new RefereeSM().BuscaReferee(DocumentoNombre, Cod_tipoReferee, 0, 0));
            }
            catch (Exception)
            {
                return PartialView(new List<Referee>());
            }
        }

        public PartialViewResult _BuscarTipoReferee() {
            try
            {
                return PartialView(new RefereeSM().BuscaTipoReferee());
            }
            catch (Exception)
            {
                return PartialView(new List<TipoReferee>());
            }
        }

        public PartialViewResult _BuscarPosibleReferee(int Cod_Usuario = 0, string DocumentoNombre = "")
        {
            try
            {
                return PartialView(new RefereeSM().BuscarPosibleReferee(Cod_Usuario, DocumentoNombre));
            }
            catch (Exception e)
            {
                return PartialView(new List<Persona>());
            }
        }

        public JsonResult GuardarReferee(int Cod_Usuario = 0, int Cod_TipoReferee = 0) {
            ResultadoTransaccion result = new ResultadoTransaccion() {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos."
            };

            if (Cod_Usuario > 0 && Cod_TipoReferee > 0)
                result = new RefereeSM().GuardarReferee(Cod_Usuario, Cod_TipoReferee);

            if (result.EsSatisfactorio)
                result.Vista = RenderRazorViewToString(this.ControllerContext, "_Listado", new RefereeSM().BuscaReferee("", 0, 0, 0));

            return Json(result,JsonRequestBehavior.AllowGet);
        }

        public JsonResult CambiarEstadoReferee(int Codigo, int EstadoReferee)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };

            try
            {
                resul = new RefereeSM().CambiarEstadoReferee(Codigo, EstadoReferee);
            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

    }
}