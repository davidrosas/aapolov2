﻿using AAPolo.Aplication;
using AAPolo.Controllers.Helper;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Controllers
{
    [Authorize]
    public class SancionController : ClassHelper
    {
        // GET: Sancion
        public ActionResult Index()
        {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            return View();
        }

        public PartialViewResult _Listado()
        {

            try
            {
                return PartialView(new SancionSM().BuscarSancion(0, 0, "", ""));
            }
            catch (Exception)
            {
                return PartialView(new List<Sancion>());
            }
        }

        public ActionResult CrearSancion()
        {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            return View();
        }

        public ActionResult EditarSancion(int Codigo = 0)
        {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            if (Codigo == 0)
                return RedirectToAction("Index", "Sancion");

            Sancion san = new SancionSM().BuscarSancion(Codigo, 0, "", "").FirstOrDefault();

            try
            {

                return View(san);

            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Sancion");
            }
        }

        public JsonResult GuardarSancion(Sancion obj) {
            ResultadoTransaccion resul = new ResultadoTransaccion() {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos."
            };

            if (obj.Persona.Codigo < 1)
            {
                resul.Mensaje = "Disculpe, debe selecionar un jugador.";
                return Json(resul, JsonRequestBehavior.AllowGet);
            }
            else if (String.IsNullOrEmpty(obj.DescripcionSancion)) {
                resul.Mensaje = "Disculpe, debe ingresar descripción de la sanción.";
                return Json(resul, JsonRequestBehavior.AllowGet);
            }else if (String.IsNullOrEmpty(obj.CodigoAritculo))
            {
                resul.Mensaje = "Disculpe, debe ingresar artículo de la sanción.";
                return Json(resul, JsonRequestBehavior.AllowGet);
            }

            try
            {
                resul = new SancionSM().GuardarSancion(obj);
            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult _BuscarUsuario(string Cadena)
        {
            try
            {
                return PartialView(new PersonaSM().BuscaJugadorResumen(0, Cadena, 0, 0, -1, 0));
            }
            catch (Exception e)
            {
                ViewBag.MensajeError = e.Message;
                return PartialView(new List<Persona>());
            }
        }

        public JsonResult CambiarEstadoSancion(int Codigo, int EstadoSancion)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };

            try
            {
                resul = new SancionSM().CambiarEstadoSancion(Codigo, EstadoSancion);
            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

    }
}