﻿using AAPolo.Aplication;
using AAPolo.Controllers.Helper;
using AAPolo.Entities;
using AAPolo.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Controllers
{
    [Authorize]
    public class PagoHandicapController : ClassHelper
    {
        // GET: Pago
        public async Task<ActionResult> Index()
        {
            Persona p = DatosUsuario();
            if (p.Rol.Codigo != ConfiguracionAPP.Rol_Jugador)
                return RedirectToAction("Index", "Home");

            if (p.Rol.Codigo == ConfiguracionAPP.Rol_Jugador && p.EsMember) 
                return RedirectToAction("Index", "PerfilMember");


            try
            {
                ViewBag.MostrarMsj = Convert.ToBoolean(TempData["MostrarMsj"]);
                ViewBag.Mensaje = TempData["Mensaje"].ToString();
                ViewBag.EsSatisfactorio = Convert.ToBoolean(TempData["EsSatisfactorio"]);

            }
            catch (Exception)
            {
                ViewBag.MostrarMsj = false;
            }

            /*try
            {
                string rootApp = obtenerTagDelWebConfig("rootApp_AutomaticoPagosEnEspera");
                if (!String.IsNullOrEmpty(rootApp)) {
                    await Task.Run(() => 
                    {
                        Process proc = new Process();
                        proc.StartInfo.FileName = rootApp;
                        proc.Start();
                    });
                }
            }
            catch (Exception)
            {
                throw;
            }*/

            p = new PersonaSM().BuscaJugador(p).FirstOrDefault();
            Persona perAux = new PersonaSM().BuscaJugadorPagoHandicap(p.Codigo, "", 0, 0, 0).FirstOrDefault();
            p.HandicapPago = perAux.HandicapPago;
            p.PermitePase = perAux.PermitePase;
            if (p.HandicapPersona != null)
            {
                p.HandicapPersona.strFechaHasta = perAux.HandicapPersona.strFechaHasta;
                p.HandicapPersona.DescripcionHandicapUsuario = perAux.HandicapPersona.DescripcionHandicapUsuario;
            }
            else
            {
                p.HandicapPersona = new HandicapPersona()
                {
                    strFechaHasta = perAux.HandicapPersona.strFechaHasta,
                    DescripcionHandicapUsuario = perAux.HandicapPersona.DescripcionHandicapUsuario
                };
            }
            return View(p);
        }

        public PartialViewResult _ListadoPagoHandicap()
        {
            return PartialView(new List<Persona>());
        }

        public PartialViewResult _PagoJugador(Persona obj)
        {
            obj = new PersonaSM().BuscaJugador(obj).FirstOrDefault();

            List<TipoJugador> lJugador = new PersonaSM().BuscarTipoJugador();
            if (obj.EdadTipoJugador > 0)
                lJugador = lJugador.Where(x => obj.EdadTipoJugador >= x.EdadDesde && obj.EdadTipoJugador <= x.EdadHasta).ToList();

            ViewBag.lJugador = lJugador;
            return PartialView(obj);
        }

        //[OutputCache(Duration = 6000)]
        public PartialViewResult _DuracionHandicap(string NombreElemento)
        {
            try
            {
                ViewBag.NombreElemento = NombreElemento;
                return PartialView(new PagoSM().BuscarVigencia());
            }
            catch (Exception)
            {
                return PartialView(new List<Vigencia>());
            }
        }

        //[OutputCache(Duration = 6000, VaryByParam = "Cod_TipoJugador")]
        public PartialViewResult _BuscarCategoriaHandicap(int Cod_TipoJugador, string NombreElemento)
        {
            try
            {
                ViewBag.NombreElemento = NombreElemento;
                return PartialView(new PagoSM().BuscarCategoriaHandicap(Cod_TipoJugador));
            }
            catch (Exception e)
            {
                return PartialView(new List<CategoriaHandicap>());
            }
        }

        [OutputCache(Duration = 240, VaryByParam = "CodClub")]
        public PartialViewResult _SelectClub(int CodClub)
        {
            try
            {
                ViewBag.CodClub = CodClub;
                return PartialView(new ClubSM().BuscaClub(0, 0, "", 0));
            }
            catch (Exception e)
            {
                return PartialView(new List<Club>());
            }
        }

        public PartialViewResult _BuscarUsuario(string Cadena)
        {
            try
            {
                List<Persona> lPer = new PersonaSM().BuscaJugadorPagoHandicap(0, Cadena, 0, 0, 0);
                lPer = lPer.Where(x => !x.HandicapPago && (x.HandicapPersona.detallePago.estado.Codigo == 0 || x.HandicapPersona.detallePago.estado.Codigo == 0)).ToList();
                return PartialView(lPer);
            }
            catch (Exception e)
            {
                ViewBag.MensajeError = e.Message;
                return PartialView(new List<Persona>());
            }
        }

        public JsonResult BuscarCostoHandicap(int Cod_TipoJugador, int Cod_Vigencia, int Cod_CategoriaHandicap)
        {
            try
            {
                Handicap han = new PagoSM().BuscarCostoHandicap(Cod_TipoJugador, Cod_Vigencia, Cod_CategoriaHandicap).FirstOrDefault();
                if (han == null)
                    han = new Handicap();
                return Json(han, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public JsonResult ProcesarPago(List<Persona> lPersona)
        {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, los datos ingresados son inválidos."
            };

            try
            {
                string idReference = "AAPolo" + "-user-" + DatosUsuario().Codigo + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss.fff");

                for (int i = 0; i < lPersona.Count; i++)
                {
                    lPersona[i].HandicapPersona = new HandicapPersona() { Codigo = lPersona[i].Cod_Hand };
                }

                if (lPersona != null && lPersona.Count() > 0)
                    result = new PagoSM().ProcesarPagoHandicap(lPersona, idReference, DatosUsuario().Codigo);
                if (result.EsSatisfactorio)
                {
                    result.Cadena = obtenerTagDelWebConfig("UrlPasarelaDePago") + HttpUtility.UrlEncode(obtenerDatosPasarelaMercadoPago(lPersona, idReference));
                }
            }
            catch (Exception e)
            {
                result.MensajeError = e.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult _BuscarUsuarioPase(string Cadena)
        {
            List<Persona> lPerAux = new List<Persona>();
            try
            {
                List<Persona> lPer = new PersonaSM().BuscaJugadorPagoHandicap(0, Cadena, 0, 0, 0, 0, true).Where(x => x.HandicapPago == true && x.PermitePase == true).ToList();
                List<Handicap> lHandicap = new PagoSM().BuscarHandicapTransicion(0);

                foreach (var item in lPer)
                {
                    if (lHandicap.Where(a => a.CodigoHandicapDesde == item.HandicapPersona.Codigo).Count() > 0)
                    {
                        lPerAux.Add(item);
                    }
                }

                return PartialView(lPerAux);
            }
            catch (Exception e)
            {
                ViewBag.MensajeError = e.Message;
                return PartialView(new List<Persona>());
            }
        }

        public PartialViewResult _PagoJugadorPase(Persona obj)
        {

            obj = new PersonaSM().BuscaJugador(obj).FirstOrDefault();

            List<TipoJugador> lJugador = new PersonaSM().BuscarTipoJugador();
            if (obj.EdadTipoJugador > 0)
                lJugador = lJugador.Where(x => obj.EdadTipoJugador >= x.EdadDesde && obj.EdadTipoJugador <= x.EdadHasta).ToList();

            ViewBag.lJugador = lJugador;

            List<Handicap> lHandicap = new PagoSM().BuscarHandicapTransicion(obj.HandicapPersona.Codigo);
            ViewBag.lHandicap = lHandicap;
            return PartialView(obj);

        }

        public JsonResult ProcesarPagoPase(List<Persona> lPersona)
        {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, los datos ingresados son inválidos."
            };

            try
            {
                string idReference = "AAPolo" + "-user-" + DatosUsuario().Codigo + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss.fff");

                for (int i = 0; i < lPersona.Count; i++)
                {
                    lPersona[i].HandicapPersona = new HandicapPersona()
                    {
                        Codigo = lPersona[i].Cod_Hand,
                        CodigoHandicapTransicion = lPersona[i].Cod_HandTransicion
                    };
                }

                if (lPersona != null && lPersona.Count() > 0)
                    result = new PagoSM().ProcesarPagoHandicapPase(lPersona, idReference, DatosUsuario().Codigo);
                if (result.EsSatisfactorio)
                {
                    result.Cadena = obtenerTagDelWebConfig("UrlPasarelaDePago") + HttpUtility.UrlEncode(obtenerDatosPasarelaMercadoPagoPaseHandicap(lPersona, idReference));
                }
            }
            catch (Exception e)
            {
                result.MensajeError = e.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        #region Actualizacion Pago Handicap
        public ActionResult DetalleDePago(string external_reference, string collection_id = "")
        {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, ocurrio un error al guardar los resultados de su operación, por favor contacte con los administradores."
            };

            var pReq = Request;

            try
            {
                result = new PagoSM().ActualizarTransaccionHandicap(external_reference, ConfiguracionAPP.Pago_Satisfactorio, collection_id, DatosUsuario().Codigo);
                TempData["Mensaje"] = result.Mensaje;
                TempData["EsSatisfactorio"] = result.EsSatisfactorio;
                TempData["MostrarMsj"] = true;
            }
            catch (Exception)
            {

                throw;
            }
            return RedirectToAction("Index", "HistorialPagos");
        }

        public ActionResult DetalleDePagoPending(string external_reference, string collection_id = "")
        {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, ocurrio un error al guardar los resultados de su operación, por favor contacte con los administradores."
            };
            try
            {
                result = new PagoSM().ActualizarTransaccionHandicap(external_reference, ConfiguracionAPP.Pago_EnEspera, collection_id, DatosUsuario().Codigo);
                TempData["Mensaje"] = result.Mensaje;
                TempData["EsSatisfactorio"] = result.EsSatisfactorio;
                TempData["MostrarMsj"] = true;
            }
            catch (Exception)
            {

                throw;
            }
            return RedirectToAction("Index", "HistorialPagos");
        }

        public ActionResult DetalleDePagoError(string external_reference, string collection_id = "")
        {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, ocurrio un error al guardar los resultados de su operación, por favor contacte con los administradores."
            };
            try
            {
                result = new PagoSM().ActualizarTransaccionHandicap(external_reference, ConfiguracionAPP.Pago_Cancelado, collection_id, DatosUsuario().Codigo);
                TempData["Mensaje"] = result.Mensaje;
                TempData["EsSatisfactorio"] = result.EsSatisfactorio;
                TempData["MostrarMsj"] = true;
            }
            catch (Exception)
            {

                throw;
            }
            return RedirectToAction("Index", "HistorialPagos");
        }
        #endregion
        string obtenerDatosPasarelaMercadoPago(List<Persona> lPersona, string idReference)
        {
            List<Handicap> lHandicaps = new PagoSM().BuscarCostoHandicap(0, 0, 0);

            DatosMercadoPago mpg;

            mpg = new DatosMercadoPago()
            {
                ID_Credencial = obtenerTagDelWebConfig("MercadoPagoClientID"),// "3906782221688425", 
                Clave_Credencial = obtenerTagDelWebConfig("MercadoPagoClientSecret"),//"8QCz3xC2W2KEzG51Qk37jP10LKMXWC2J" ,
                Referencia_Externa = idReference,
                UrlAPP = Request.Url.Authority,
                UrlSuccess = Request.Url.Authority + "/" + Url.Action("DetalleDePago", "PagoHandicap", new { external_reference = idReference }).ToString(),
                UrlPending = Request.Url.Authority + "/" + Url.Action("DetalleDePagoPending", "PagoHandicap", new { external_reference = idReference }).ToString(),
                UrlFailure = Request.Url.Authority + "/" + Url.Action("DetalleDePagoError", "PagoHandicap", new { external_reference = idReference }).ToString(),
                UrlNotificacion = "",
            };

            List<Item> lItem = new List<Item>();
            foreach (var item in lPersona)
            {
                lItem.Add(new Item()
                {
                    title = "Handicap: " + item.NombreCompleto,
                    currency_id = obtenerTagDelWebConfig("MercadoPagoMoneda"),
                    quantity = 1 /*un paquete*/,
                    unit_price = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator == "," && !lHandicaps.Find(f => f.Codigo == item.HandicapPersona.Codigo).strImporte.Contains(",") ? Convert.ToDouble(lHandicaps.Find(f => f.Codigo == item.HandicapPersona.Codigo).strImporte.Replace('.', ',')) : Convert.ToDouble(lHandicaps.Find(f => f.Codigo == item.HandicapPersona.Codigo).strImporte)
                });
            }

            mpg.lstItem = lItem;

            mpg.payer = new payer()
            {
                name = DatosUsuario().Nombre,
                surname = DatosUsuario().Apellido,
                email = "",
                date_created = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"),
                identification = new identification()
                {
                    type = "DNI",
                    number = DatosUsuario().DocumentoIdentidad
                }
            };

            return JsonConvert.SerializeObject(mpg);


        }

        string obtenerDatosPasarelaMercadoPagoPaseHandicap(List<Persona> lPersona, string idReference)
        {
            List<Handicap> lHandicaps = new PagoSM().BuscarHandicapTransicion(0);
            
            DatosMercadoPago mpg;

            mpg = new DatosMercadoPago()
            {
                ID_Credencial = obtenerTagDelWebConfig("MercadoPagoClientID"),
                Clave_Credencial = obtenerTagDelWebConfig("MercadoPagoClientSecret"),
                Referencia_Externa = idReference,
                UrlAPP = Request.Url.Authority,
                UrlSuccess = Request.Url.Authority + "/" + Url.Action("DetalleDePago", "PagoHandicap", new { external_reference = idReference }).ToString(),
                UrlPending = Request.Url.Authority + "/" + Url.Action("DetalleDePagoPending", "PagoHandicap", new { external_reference = idReference }).ToString(),
                UrlFailure = Request.Url.Authority + "/" + Url.Action("DetalleDePagoError", "PagoHandicap", new { external_reference = idReference }).ToString(),
                UrlNotificacion = "",
            };

            List<Item> lItem = new List<Item>();
            string precio = "";
            foreach (var item in lPersona)
            {
                precio = lHandicaps.Find(f => f.CodigoHandicapTransicion == item.HandicapPersona.CodigoHandicapTransicion).strImporte;

                lItem.Add(new Item()
                {
                    title = "Handicap: " + item.NombreCompleto,
                    currency_id = obtenerTagDelWebConfig("MercadoPagoMoneda"),
                    quantity = 1 /*un paquete*/,
                    unit_price = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator == "," && !precio.Contains(",") ? Convert.ToDouble(precio.Replace('.', ',')) : Convert.ToDouble(precio)
                });
            }

            mpg.lstItem = lItem;

            mpg.payer = new payer()
            {
                name = DatosUsuario().Nombre,
                surname = DatosUsuario().Apellido,
                email = "",
                date_created = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"),
                identification = new identification()
                {
                    type = "DNI",
                    number = DatosUsuario().DocumentoIdentidad
                }
            };

            return JsonConvert.SerializeObject(mpg);

        }

    }
}