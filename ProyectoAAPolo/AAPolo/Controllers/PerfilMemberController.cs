﻿using AAPolo.Aplication;
using AAPolo.Controllers.Helper;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Controllers
{
    public class PerfilMemberController : ClassHelper
    {
        // GET: PerfilJugador
        public ActionResult Index()
        {
            Persona p = DatosUsuario();
            if (p.Codigo == 0 || p.Rol.Codigo != ConfiguracionAPP.Rol_Jugador)
                return RedirectToAction("Index", "Home");

            p = new PersonaSM().BuscaJugador(p).FirstOrDefault();
            //Persona perAux = new PersonaSM().BuscaJugadorPagoHandicap(p.Codigo, "", 0, 0, 0).FirstOrDefault();
            //p.HandicapPago = perAux.HandicapPago;

            /*if (p.HandicapPersona != null)
            {
                p.HandicapPersona.strFechaHasta = perAux.HandicapPersona.strFechaHasta;
                p.HandicapPersona.DescripcionHandicapUsuario = perAux.HandicapPersona.DescripcionHandicapUsuario;
            }
            else
            {
                p.HandicapPersona = new HandicapPersona()
                {
                    strFechaHasta = perAux.HandicapPersona.strFechaHasta,
                    DescripcionHandicapUsuario = perAux.HandicapPersona.DescripcionHandicapUsuario
                };
            }

            if (p.HandicapPago && p.lClubUsuarioSecundario.Where(x => x.EsPrincipal && x.Codigo == ConfiguracionAPP.Clud_NoInformado).Count() > 0)
            {
                ViewBag.lstClub = new ClubSM().BuscaClub(0, 0, "", 0).Where(x => x.EsActivo).ToList();
            }*/

            return View(p);
        }
    }
}