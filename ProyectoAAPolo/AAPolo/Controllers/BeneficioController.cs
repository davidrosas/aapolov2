﻿using AAPolo.Aplication;
using AAPolo.Controllers.Helper;
using AAPolo.Entities;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Controllers
{
    [Authorize]
    public class BeneficioController : ClassHelper
    {
        // GET: Beneficio
        public ActionResult Index()
        {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            return View(new BeneficioSM().BuscarEstadisticasCommunity());
        }

        public PartialViewResult _Listado() {
            try
            {
                return PartialView(new BeneficioSM().BuscarBeneficio(0,"",0,0, false));
            }
            catch (Exception e)
            {
                return PartialView(new List<Beneficio>());
            }
        }

        public JsonResult GuardarBeneficio(Beneficio obj, string PrecioPoloPlayers = "", string PrecioPoloMembers = "") {

            ResultadoTransaccion result = new ResultadoTransaccion() {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos."
            };

            if (obj.Imagen != null)
            {
                obj.UrlImagen = GuardarImagen(obj.Imagen);
            }

            if (obj.Imagen == null && obj.Codigo == 0)
            {
                result.Mensaje = "Disculpe, debe cargar imagen para el beneficio.";
            }
            else if (String.IsNullOrEmpty(obj.Nombre)) {
                result.Mensaje = "Disculpe, debe ingresar un nombre válido para el beneficio.";
            }
            else if (obj.CategoriaBeneficio == null || obj.CategoriaBeneficio.Codigo == 0) {
                result.Mensaje = "Disculpe, debe seleccionar categoría de beneficio.";
            }
            else if (String.IsNullOrEmpty(obj.UrlMarca)) {
                result.Mensaje = "Disculpe, debe ingresar url de la marca.";
            }
            else if (String.IsNullOrEmpty(obj.Condicion)) {
                result.Mensaje = "Disculpe, debe ingresar condición del beneficio.";
            }
            else if (PrecioPoloPlayers != "" || PrecioPoloMembers != "")
            {
                obj.lstPrecioXBeneficios = new List<PrecioXBeneficio>();
                if (obj.TipoBeneficiario.Codigo == ConfiguracionAPP.Cod_TipoBeneficiario_PoloTodos && PrecioPoloPlayers != "" && PrecioPoloMembers != "")
                {
                    obj.lstPrecioXBeneficios.Add(new PrecioXBeneficio()
                    {
                        TipoBeneficiario = new TipoBeneficiario() { Codigo = ConfiguracionAPP.Cod_TipoBeneficiario_PoloMembers },
                        Precio = PrecioPoloMembers
                    });
                    obj.lstPrecioXBeneficios.Add(new PrecioXBeneficio()
                    {
                        TipoBeneficiario = new TipoBeneficiario() { Codigo = ConfiguracionAPP.Cod_TipoBeneficiario_PoloPlayers },
                        Precio = PrecioPoloPlayers
                    });
                }
                else if (obj.TipoBeneficiario.Codigo == ConfiguracionAPP.Cod_TipoBeneficiario_PoloMembers && PrecioPoloMembers != "")
                {
                    obj.lstPrecioXBeneficios.Add(new PrecioXBeneficio()
                    {
                        TipoBeneficiario = new TipoBeneficiario() { Codigo = ConfiguracionAPP.Cod_TipoBeneficiario_PoloMembers },
                        Precio = PrecioPoloMembers
                    });
                }
                else if (obj.TipoBeneficiario.Codigo == ConfiguracionAPP.Cod_TipoBeneficiario_PoloPlayers && PrecioPoloPlayers != "")
                {
                    obj.lstPrecioXBeneficios.Add(new PrecioXBeneficio()
                    {
                        TipoBeneficiario = new TipoBeneficiario() { Codigo = ConfiguracionAPP.Cod_TipoBeneficiario_PoloPlayers },
                        Precio = PrecioPoloPlayers
                    });
                }

                if (obj.lstPrecioXBeneficios != null && obj.lstPrecioXBeneficios.Count() > 0)
                {
                    try
                    {
                        int Dia = Convert.ToInt32(obj.strFechaInicio.Split('/')[0]),
                            Mes = Convert.ToInt32(obj.strFechaInicio.Split('/')[1]),
                            Anio = Convert.ToInt32(obj.strFechaInicio.Split('/')[2]);
                        obj.FechaInicio = new DateTime(Anio, Mes, Dia);

                        Dia = Convert.ToInt32(obj.strFechaFin.Split('/')[0]);
                        Mes = Convert.ToInt32(obj.strFechaFin.Split('/')[1]);
                        Anio = Convert.ToInt32(obj.strFechaFin.Split('/')[2]);
                        obj.FechaFin = new DateTime(Anio, Mes, Dia);
                        try
                        {
                            result = new BeneficioSM().GuardarBeneficio(obj);
                        }
                        catch (Exception e)
                        {
                            result.MensajeError = e.Message;
                        }
                    }
                    catch (Exception)
                    {
                        result.Mensaje = "Disculpe, debe ingresar fechas válidas para inicio y fin del beneficio.";
                    }


                }
                else {
                    result.Mensaje = "Disculpe, debe ingresar precio/descuento.";
                }
            }
            else {
                result.Mensaje = "Disculpe, debe ingresar precio/descuento.";
            }


            return Json(result,JsonRequestBehavior.AllowGet);
        }

        public ActionResult CrearBeneficio() {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            return View();
        }

        public ActionResult EditarBeneficio(int Codigo = 0)
        {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");
            
            if (Codigo == 0)
                return RedirectToAction("Index", "Beneficio");
            try
            {
                return PartialView(new BeneficioSM().BuscarBeneficio(Codigo, "", 0, 0, true).First());
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Beneficio");
            }

        }

        public PartialViewResult _SelectCategoriaBeneficiario(int Codigo = 0) {
            try
            {
                ViewBag.Codigo = Codigo;
                return PartialView(new BeneficioSM().BuscarCategoriaBeneficio());
            }
            catch (Exception e)
            {
                return PartialView(new List<CategoriaBeneficio>());
            }
        }

        public ActionResult _SelectTipoBeneficiario(int Codigo = 1)
        {
            try
            {
                ViewBag.Codigo = Codigo;
                return PartialView(new BeneficioSM().BuscarTipoBeneficiario());
            }
            catch (Exception e)
            {
                return PartialView(new List<TipoBeneficiario>());
            }
        }

        public JsonResult CambiarEstadoBeneficio(int Codigo, int EstadoBeneficio)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };

            try
            {
                resul = new BeneficioSM().CambiarEstadoBeneficio(Codigo, EstadoBeneficio);
            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CambiarEstadoDestacadoBeneficio(int Codigo, int EstadoDestacadoBeneficio)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };

            try
            {
                resul = new BeneficioSM().CambiarEstadoDestacadoBeneficio(Codigo, EstadoDestacadoBeneficio);
            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public void ReporteMiembro()
        {
            var libro = new XLWorkbook(Server.MapPath(@"~/img/Excels/TemplateMembers.xlsx"));
            IXLWorksheet hoja = libro.Worksheet(1);
            var miembros = new BeneficioSM().BuscarMiembro(0,"");
           
            int fila = 2;

            foreach (var miembro in miembros)
            {
                hoja.Cells("A" + (fila).ToString()).Value = miembro.Nombre;
                hoja.Cells("B" + (fila).ToString()).Value = miembro.Apellido;
                hoja.Cells("C" + (fila).ToString()).Value = miembro.TipoDocumento.Nombre;
                hoja.Cells("D" + (fila).ToString()).Value = miembro.DocumentoIdentidad;
                hoja.Cell("E" + (fila).ToString()).SetValue<string>(miembro.strFechaAlta);
                hoja.Cells("F" + (fila).ToString()).Value = miembro.lDomicilio.FirstOrDefault().DireccionDomicilio;
                hoja.Cells("G" + (fila).ToString()).Value = miembro.lDomicilio.FirstOrDefault().pais.Nombre;
                hoja.Cells("H" + (fila).ToString()).Value = miembro.lDomicilio.FirstOrDefault().provincia.Nombre;
                hoja.Cells("I" + (fila).ToString()).Value = miembro.lDomicilio.FirstOrDefault().municipio.Nombre;
                hoja.Cells("J" + (fila).ToString()).Value = miembro.lDomicilio.FirstOrDefault().NroDomicilio;
                hoja.Cells("K" + (fila).ToString()).Value = miembro.lDomicilio.FirstOrDefault().NroPiso;
                hoja.Cells("L" + (fila).ToString()).Value = miembro.lDomicilio.FirstOrDefault().Departamento;
                hoja.Cells("M" + (fila).ToString()).Value = miembro.lDomicilio.FirstOrDefault().CodigoPostal;
                hoja.Cell("N" + (fila).ToString()).SetValue<string>(miembro.strFechaNacimiento);
                hoja.Cells("O" + (fila).ToString()).Value = miembro.Genero.Nombre;
                hoja.Cells("P" + (fila).ToString()).Value = miembro.EsActivo?"Sí":"No";
                hoja.Cells("Q" + (fila).ToString()).Value = miembro.Pais.Nombre;
                hoja.Cell("R" + (fila).ToString()).SetValue<string>(miembro.NumeroTarjeta);
                hoja.Cells("S" + (fila).ToString()).Value = miembro.clubUsuarioPrincipal.NombreClub;
                hoja.Cells("T" + (fila).ToString()).Value = miembro.Correo;
                hoja.Cell("U" + (fila).ToString()).SetValue<string>(miembro.TelefonoMiembro);
                hoja.Cells("V" + (fila).ToString()).Value = miembro.TipoMiembro;
                hoja.Cell("W" + (fila).ToString()).SetValue<string>((String.IsNullOrEmpty(miembro.NumeroTarjeta) ? "No" : "Sí"));

                fila++;
            }


            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=\"Reporte miembros.xlsx\"");

            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }

            Response.End();
        }

    }
}