﻿using AAPolo.Aplication;
using AAPolo.Controllers.Helper;
using AAPolo.Entities;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Controllers
{
    [Authorize]
    public class ReporteController : ClassHelper
    {
        // GET: Reporte
        public ActionResult Index()
        {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            return RedirectToAction("ReporteClub", "Reporte");
        }

        public ActionResult ReporteClub()
        {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");
            return View();
        }

        public PartialViewResult _ListadoReporteClub(string Nombre = "", int Cod_CategoriaClub = 0, int Cod_EstadoPago = 0)
        {
            List<Club> lClub = new List<Club>();
            try
            {
                lClub = new ReporteSM().BuscarReporteClub(0, 0, 0);
                ViewBag.CantClub = lClub.Count();
                ViewBag.CantClubPagadosEsteAnio = lClub.Where(x => x.detallePago.PagadoEsteAnio).Count();
                ViewBag.CantClubNoPagadosEsteAnio = lClub.Where(x => !x.detallePago.PagadoEsteAnio).Count();
                if (!String.IsNullOrEmpty(Nombre) || Cod_CategoriaClub > 0)
                    lClub = lClub.Where(x => x.NombreClub.ToLower().Contains((!String.IsNullOrEmpty(Nombre) ? Nombre.ToLower() : x.NombreClub.ToLower())) && x.Categoria.Codigo == (Cod_CategoriaClub > 0 ? Cod_CategoriaClub : x.Categoria.Codigo)).ToList();

                if (Cod_EstadoPago > 0)
                {
                    if (Cod_EstadoPago == 2)
                    {
                        lClub = lClub.Where(x => x.detallePago.estado.Codigo == ConfiguracionAPP.Pago_PorOtroMedio || x.detallePago.estado.Codigo == ConfiguracionAPP.Pago_EnEspera).ToList();
                    }
                    else if (Cod_EstadoPago == 3)
                    {
                        lClub = lClub.Where(x => x.AfiliacionVenciada).ToList();
                    }
                    else if (Cod_EstadoPago == 1)
                    {
                        lClub = lClub.Where(x => !x.AfiliacionVenciada && !(x.detallePago.estado.Codigo == ConfiguracionAPP.Pago_PorOtroMedio || x.detallePago.estado.Codigo == ConfiguracionAPP.Pago_EnEspera)).ToList();
                    }

                }

                return PartialView(lClub);
            }
            catch (Exception)
            {
                return PartialView(lClub);
            }
        }

        public ActionResult ReporteHandicap()
        {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");
            return View();
        }

        public PartialViewResult _ListadoReporteHandicap(int Cod_Usuario = 0, string Cadena = "", int Cod_TipoJugador = 0, int Cod_Club = 0, int Nro_Handicap = -1, int Cod_EstadoPago = 0, int Cod_Vigencia = 0)
        {
            try
            {
                List<Persona> lObj = new ReporteSM().BuscarReportePagoHandicap(Cod_Usuario, Cadena, Cod_TipoJugador, Cod_Club, Nro_Handicap);
                if (Cod_EstadoPago > 0)
                {
                    if (Cod_EstadoPago == 2)
                    {
                        lObj = lObj.Where(x => x.HandicapPersona.detallePago.estado.Codigo == ConfiguracionAPP.Pago_PorOtroMedio || x.HandicapPersona.detallePago.estado.Codigo == ConfiguracionAPP.Pago_EnEspera).ToList();
                    }
                    else if (Cod_EstadoPago == 3)
                    {
                        lObj = lObj.Where(x => x.HandicapPersona.EsVencido).ToList();
                    }
                    else if (Cod_EstadoPago == 1)
                    {
                        lObj = lObj.Where(x => !x.HandicapPersona.EsVencido && !(x.HandicapPersona.detallePago.estado.Codigo == ConfiguracionAPP.Pago_PorOtroMedio || x.HandicapPersona.detallePago.estado.Codigo == ConfiguracionAPP.Pago_EnEspera)).ToList();
                    }
                }
                if (Cod_Vigencia > 0) {
                    lObj = lObj.Where(x => x.HandicapPersona.vigencia.Codigo == Cod_Vigencia).ToList();
                }

                return PartialView(lObj);
            }
            catch (Exception)
            {
                return PartialView(new List<Persona>());
            }
        }
        
        public PartialViewResult _ReporteEstadisticaHandicap()
        {
            try
            {
                return PartialView(new ReporteSM().BuscarReporteEstadisticaHandicap());
            }
            catch (Exception e)
            {
                return PartialView(new ReporteEstadisticaHandicap());
            }
        }

        public PartialViewResult _CargarPago(int Cod_Persona)
        {
            Persona obj = new Persona();
            List<TipoJugador> lJugador = new List<TipoJugador>();
            List<Handicap> lHandicap = new List<Handicap>();
            List<MedioDePago> lMedioDePagos = new List<MedioDePago>();
            List<TipoDocumento> lTipoDocumento = new List<TipoDocumento>();
            List<Pais> lPais = new List<Pais>();
            List<Club> lClub = new List<Club>();
            Pais pais = new Pais();
            TipoDocumento tDoc = new TipoDocumento();
            try
            {
                obj = new ReporteSM().BuscarReportePagoHandicap(Cod_Persona, "", 0, 0, -1).FirstOrDefault();
                if (obj != null && obj.EsActivo)
                {
                    pais = obj.Pais;
                    tDoc = obj.TipoDocumento;
                    lMedioDePagos = new ReporteSM().BuscarMedioDePago();
                    lTipoDocumento = new PersonaSM().BuscarTipoDocumento();
                    lPais = new PersonaSM().BuscarPais();
                    lClub = new ClubSM().BuscaClub(0, 0, "", 0);

                    if (!(obj.HandicapPersona.detallePago.estado.Codigo == ConfiguracionAPP.Pago_EnEspera || obj.HandicapPersona.detallePago.estado.Codigo == ConfiguracionAPP.Pago_EnEspera))
                    {
                        obj = new PersonaSM().BuscaJugadorPagoHandicap(Cod_Persona, "", 0, 0, 0, 0).FirstOrDefault();
                        obj.EsActivo = true;
                        obj.Pais = pais;
                        obj.TipoDocumento = tDoc;
                        obj.HandicapPersona.EsPase = obj.HandicapPago;
                        if (!obj.HandicapPago)
                        {
                            lJugador = new PersonaSM().BuscarTipoJugador();
                            if (obj.EdadTipoJugador > 0)
                                lJugador = lJugador.Where(x => obj.EdadTipoJugador >= x.EdadDesde && obj.EdadTipoJugador <= x.EdadHasta).ToList();
                            lHandicap = new JugadorSM().BuscarJugadorHandicap(0, 0, 0).Where(x => lJugador.Any(a => a.Codigo == x.tipoJugador.Codigo)).Select(s => s.handicap).ToList();
                        }
                        else if (obj.HandicapPago && obj.PermitePase)
                        {
                            lHandicap = new PagoSM().BuscarHandicapTransicion(obj.HandicapPersona.Codigo);
                        }

                    }
                }

            }
            catch (Exception e)
            {

            }

            ViewBag.lClub = lClub;
            ViewBag.lPais = lPais;
            ViewBag.lTipoDocumento = lTipoDocumento;
            ViewBag.lMedioDePagos = lMedioDePagos;
            ViewBag.lHandicap = lHandicap;
            ViewBag.lJugador = lJugador;
            return PartialView(obj);

        }

        public PartialViewResult _CargarPagoClub(int Codigo, bool PagoEnProceso = false)
        {
            Club obj = new Club();
            List<MedioDePago> lMedioDePagos = new List<MedioDePago>();
            List<CategoriaClub> lCategoria = new List<CategoriaClub>();
            try
            {
                if (PagoEnProceso)
                {
                    obj = new ReporteSM().BuscarReporteClub(0, 0, Codigo).FirstOrDefault();
                }
                else
                {
                    obj = new ClubSM().BuscaClub(0, Codigo, "", 0).FirstOrDefault();
                }

                lCategoria = new ClubSM().BuscarCategoriaClub();
                lMedioDePagos = new ReporteSM().BuscarMedioDePago();
            }
            catch (Exception e)
            {

            }

            ViewBag.lCategoria = lCategoria;
            ViewBag.lMedioDePagos = lMedioDePagos;
            return PartialView(obj);

        }

        public JsonResult ConfirmarPagoHandicap(Persona obj, HttpPostedFileBase Imagen, int Cod_Pago = 0, int EsPase = 0, int Cod_Medio = 0, int Cod_Handicap = 0, string collection_id = "", int Cod_TipoDocumento = 0, int Cod_Pais = 0, int Cod_ClubPrincipal = 0)
        {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos."
            };

            try
            {

                if (Cod_Pago == 0 && Cod_Handicap == 0)
                {
                    result.Mensaje = "Por favor complete todos los campos.";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else if (Cod_Pais == 0)
                {
                    result.Mensaje = "Por favor seleccione país.";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else if (Cod_TipoDocumento == 0)
                {
                    result.Mensaje = "Por favor seleccione tipo de documento.";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else if (Cod_Medio == 0)
                {
                    result.Mensaje = "Por favor seleccione medio de pago.";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else if (Cod_ClubPrincipal == 0)
                {
                    result.Mensaje = "Por favor seleccione club.";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else if (collection_id == "")
                {
                    result.Mensaje = "Por favor ingrese codigo de comprobante.";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

                obj.TipoDocumento = new TipoDocumento() { Codigo = Cod_TipoDocumento };

                obj.Pais = new Pais() { Codigo = Cod_Pais };

                obj.clubUsuarioPrincipal = new ClubUsuario() { Codigo = Cod_ClubPrincipal };

                obj.HandicapPersona = new HandicapPersona()
                {
                    Codigo = Cod_Handicap,
                    detallePago = new DetallePago()
                    {
                        Codigo = Cod_Pago,
                        medioDePago = new MedioDePago() { Codigo = Cod_Medio },
                        collection_id = collection_id,
                        Imagen = Imagen
                    },
                    EsPase = EsPase == 1 ? true : false,
                };

                int Dia = Convert.ToInt32(obj.strFechaNacimiento.Split('/')[0]),
                    Mes = Convert.ToInt32(obj.strFechaNacimiento.Split('/')[1]),
                    Anio = Convert.ToInt32(obj.strFechaNacimiento.Split('/')[2]);

                DateTime fecha = DateTime.Now;
                try
                {
                    fecha = new DateTime(Anio, Mes, Dia);
                }
                catch (Exception)
                {
                    result.Mensaje = "Disculpe, debe ingresar una fecha válida.";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                obj.FechaNacimiento = fecha;
                if (obj.HandicapPersona != null && obj.HandicapPersona.detallePago != null && obj.HandicapPersona.detallePago.Imagen != null)
                {
                    obj.HandicapPersona.detallePago.Url_Imagen = GuardarImagen(obj.HandicapPersona.detallePago.Imagen);
                    result = new ReporteSM().ConfirmarPagoHandicap(obj, DatosUsuario().Codigo);
                    //if (result.EsSatisfactorio)
                    //    result.Vista = RenderRazorViewToString(this.ControllerContext, "_ListadoReporteHandicap", new ReporteSM().BuscarReportePagoHandicap(0));
                }
                else
                {
                    result.Mensaje = "Por favor, cargue el comprobante.";
                }

            }
            catch (Exception e)
            {
                result.MensajeError = e.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ConfirmarPagoClub(Club obj, HttpPostedFileBase Imagen, int Cod_Pago = 0, int Cod_Medio = 0, string collection_id = "", bool EsPagoEnProceso = false)
        {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos."
            };

            try
            {

                if (Cod_Pago == 0 && EsPagoEnProceso)
                {
                    result.Mensaje = "Por favor complete todos los campos.";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else if (Cod_Medio == 0)
                {
                    result.Mensaje = "Por favor seleccione medio de pago.";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else if (collection_id == "")
                {
                    result.Mensaje = "Por favor ingrese codigo de comprobante.";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

                obj.detallePago = new DetallePago()
                {
                    Codigo = Cod_Pago,
                    medioDePago = new MedioDePago() { Codigo = Cod_Medio },
                    collection_id = collection_id,
                    Imagen = Imagen,
                    EsPagoEnProceso = EsPagoEnProceso,
                };

                if (obj.detallePago != null && obj.detallePago.Imagen != null)
                {
                    obj.detallePago.Url_Imagen = GuardarImagen(obj.detallePago.Imagen);
                    result = new ReporteSM().ConfirmarPagoClub(obj, DatosUsuario().Codigo);

                }
                else
                {
                    result.Mensaje = "Por favor, cargue el comprobante.";
                }

            }
            catch (Exception e)
            {
                result.MensajeError = e.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReporteEquipo(int Cod_Torneo, int EsIndividual, string NombreTorneo)
        {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            ViewBag.Cod_Torneo = Cod_Torneo;
            ViewBag.EsIndividual = EsIndividual;
            ViewBag.NombreTorneo = NombreTorneo;
            return View();
        }

        public PartialViewResult _ListadoReporteEquipo(int Cod_Torneo, string NombreTorneo = "")
        {
            try
            {
                ViewBag.Cod_Torneo = Cod_Torneo;
                ViewBag.NombreTorneo = NombreTorneo;
                return PartialView(new ReporteSM().BuscarReporteEquipo(0, Cod_Torneo, "", 0));
            }
            catch (Exception)
            {
                return PartialView(new List<Equipo>());
            }
        }

        public PartialViewResult _ListadoReporteEquipoIndividual(int Cod_Torneo, string NombreTorneo = "")
        {
            try
            {
                ViewBag.Cod_Torneo = Cod_Torneo;
                ViewBag.NombreTorneo = NombreTorneo;
                return PartialView(new ReporteSM().BuscarReporteEquipo(0, Cod_Torneo, "", 0));
            }
            catch (Exception)
            {
                return PartialView(new List<Equipo>());
            }
        }

        public ActionResult ReporteGeneralInscripcionTorneo()
        {

            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            return View();
        }

        public PartialViewResult _ListadoReporteGeneralInscripcionTorneo(int Cod_Torneo = 0, string Cadena = "", int Cod_EstadoPago = 0)
        {
            try
            {
                return PartialView(new ReporteSM().BuscarReporteEquipo(0, Cod_Torneo, Cadena, Cod_EstadoPago));
            }
            catch (Exception)
            {
                return PartialView(new List<Equipo>());
            }
        }

        public PartialViewResult _CargarPagoEquipo(int Codigo, bool PagoEnProceso = false)
        {
            Equipo obj = new Equipo();
            List<MedioDePago> lMedioDePagos = new List<MedioDePago>();
            try
            {
                if (PagoEnProceso)
                {
                    obj = new ReporteSM().BuscarReporteEquipo(Codigo, 0, "", 0).FirstOrDefault();
                }

                lMedioDePagos = new ReporteSM().BuscarMedioDePago();
            }
            catch (Exception e)
            {

            }

            ViewBag.lMedioDePagos = lMedioDePagos;
            return PartialView(obj);
        }

        [HttpPost]
        public PartialViewResult _DeclinarInscripcion(int Codigo, string NombreEquipo, string NombreTorneo) {
            ViewBag.Cod_Pago = Codigo;
            ViewBag.NombreEquipo = NombreEquipo;
            ViewBag.NombreTorneo = NombreTorneo;
            return PartialView();
        }

        public JsonResult DeclinarPagoEquipo(int Cod_Pago) {

            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos."
            };

            try
            {
                result = new ReporteSM().DeclinarPagoEquipo(Cod_Pago);
                result.CodigoResultado = Cod_Pago;
            }
            catch (Exception e)
            {
                result.MensajeError = e.Message;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ConfirmarPagoEquipo(Equipo obj, HttpPostedFileBase Imagen, int Cod_Pago = 0, int Cod_Medio = 0, string collection_id = "", bool EsPagoEnProceso = false)
        {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos."
            };

            try
            {

                if (Cod_Pago == 0 && EsPagoEnProceso)
                {
                    result.Mensaje = "Por favor complete todos los campos.";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else if (Cod_Medio == 0)
                {
                    result.Mensaje = "Por favor seleccione medio de pago.";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else if (collection_id == "")
                {
                    result.Mensaje = "Por favor ingrese codigo de comprobante.";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

                obj.detallePago = new DetallePago()
                {
                    Codigo = Cod_Pago,
                    medioDePago = new MedioDePago() { Codigo = Cod_Medio },
                    collection_id = collection_id,
                    Imagen = Imagen,
                    EsPagoEnProceso = EsPagoEnProceso,
                };

                if (obj.detallePago != null && obj.detallePago.Imagen != null)
                {
                    obj.detallePago.Url_Imagen = GuardarImagen(obj.detallePago.Imagen);
                    result = new ReporteSM().ConfirmarPagoEquipo(obj, DatosUsuario().Codigo);
                    if (result.EsSatisfactorio)
                        result.CodigoResultado = obj.Codigo;
                }
                else
                {
                    result.Mensaje = "Por favor, cargue el comprobante.";
                }

            }
            catch (Exception e)
            {
                result.MensajeError = e.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #region Descargables

        public void GetReporteCuentasCorrientes()
        {
            var libro = new XLWorkbook(Server.MapPath(@"~/img/Excels/TemplateReporteCuentasCorrientes.xlsx"));
            IXLWorksheet hoja = libro.Worksheet(1);
            var clubes = new ReporteSM().GetReporteClub();
            int fila1 = 4;
            int fila2 = 8;

            hoja.Cells("C" + (fila1).ToString()).Value = clubes.Count();
            hoja.Cells("D" + (fila1).ToString()).Value = clubes.Count(x => x.importe > 0);
            hoja.Cells("E" + (fila1).ToString()).Value = clubes.Count(x => x.importe == 0);
            foreach (var miClub in clubes)
            {
                hoja.Cells("C" + (fila2).ToString()).Value = miClub.Club;
                hoja.Cells("D" + (fila2).ToString()).Value = miClub.NombreAdmin + " " + miClub.ApellidoAdmin;
                hoja.Cells("E" + (fila2).ToString()).Value = miClub.CategoriaClub;
                hoja.Cells("F" + (fila2).ToString()).Value = miClub.fec_ult_pago.ToString();
                hoja.Cells("G" + (fila2).ToString()).Value = miClub.importe == null ? "" : ((int)miClub.importe).ToString("0.00");
                hoja.Cells("H" + (fila2).ToString()).Value = miClub.sn_pago == 1 ? "Al día" : "Vencido";
                fila2++;
            }


            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=\"ReporteCuentaCorriente.xlsx\"");

            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }

            Response.End();

        }

        public void GetReporteDatosClub()
        {
            var libro = new XLWorkbook(System.Web.Hosting.HostingEnvironment.MapPath(@"~/img/Excels/TemplateReporteDatoClubes.xlsx"));
            IXLWorksheet hoja = libro.Worksheet(1);
            var Clubes = new ReporteSM().GetReporteDatosClub();
            int fila = 2; int columna = 1;
            foreach (var miClub in Clubes)
            {
                columna = 1;
                hoja.Cell(fila, columna++).Value = miClub.ClubID;
                hoja.Cell(fila, columna++).Value = miClub.Club;
                hoja.Cell(fila, columna++).Value = miClub.CategoriaClub;
                hoja.Cell(fila, columna++).Value = miClub.NombreAdmin;
                hoja.Cell(fila, columna++).Value = miClub.ApellidoAdmin;
                hoja.Cell(fila, columna++).Value = miClub.NroCanchas;
                hoja.Cell(fila, columna++).Value = miClub.Presidente;
                hoja.Cell(fila, columna++).Value = miClub.VicePresidente;
                //hoja.Cell(fila, columna++).Value = miClub.FecAlta;
                hoja.Cell(fila, columna++).Value = miClub.Vocal1;
                hoja.Cell(fila, columna++).Value = miClub.Vocal2;
                hoja.Cell(fila, columna++).Value = miClub.Vocal3;
                hoja.Cell(fila, columna++).Value = miClub.Tesorero;
                hoja.Cell(fila, columna++).Value = miClub.FecFundacion;
                hoja.Cell(fila, columna++).Value = miClub.Historia;
                hoja.Cell(fila, columna++).Value = miClub.CantSocios;
                hoja.Cell(fila, columna++).Value = miClub.BoolActivo;
                hoja.Cell(fila, columna++).Value = miClub.Mail;
                hoja.Cell(fila, columna++).Value = miClub.Teléfono;
                hoja.Cell(fila, columna++).Value = miClub.TeléfonoClub;
                hoja.Cell(fila, columna++).Value = miClub.Calle;
                hoja.Cell(fila, columna++).Value = miClub.Numero;
                hoja.Cell(fila, columna++).Value = miClub.Pais;
                hoja.Cell(fila, columna++).Value = miClub.Provincia;
                hoja.Cell(fila, columna++).Value = miClub.Ciudad;
                hoja.Cell(fila, columna++).Value = miClub.CodPostal;
                hoja.Cell(fila, columna++).Value = miClub.Facebook;
                hoja.Cell(fila, columna++).Value = miClub.Twitter;
                hoja.Cell(fila, columna++).Value = miClub.Instagram;

                fila++;
            }

            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=\"Reporte de datos de clubes.xlsx\"");

            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }

            Response.End();
        }

        public void GetReporteCuentasCorrientesClubes()
        {
            var libro = new XLWorkbook(Server.MapPath(@"~/img/Excels/TemplateReporteCuentasCorrientes.xlsx"));
            IXLWorksheet hoja = libro.Worksheet(1);
            var clubes = new ReporteSM().GetReporteClub();
            int fila1 = 4;
            int fila2 = 8;

            hoja.Cells("C" + (fila1).ToString()).Value = clubes.Count();
            hoja.Cells("D" + (fila1).ToString()).Value = clubes.Count(x => x.importe > 0);
            hoja.Cells("E" + (fila1).ToString()).Value = clubes.Count(x => x.importe == 0);
            foreach (var miClub in clubes)
            {
                hoja.Cells("C" + (fila2).ToString()).Value = miClub.ClubID;
                hoja.Cells("D" + (fila2).ToString()).Value = miClub.Club;
                hoja.Cells("E" + (fila2).ToString()).Value = miClub.NombreAdmin + " " + miClub.ApellidoAdmin;
                hoja.Cells("F" + (fila2).ToString()).Value = miClub.CategoriaClub;
                hoja.Cells("G" + (fila2).ToString()).Value = miClub.fec_ult_pago.ToString();
                hoja.Cells("H" + (fila2).ToString()).Value = miClub.importe == null ? "" : ((int)miClub.importe).ToString("0.00");
                hoja.Cells("I" + (fila2).ToString()).Value = miClub.sn_pago == 1 ? "Al día" : (miClub.sn_pago == 2 ? "En espera" : "Vencido");
                hoja.Cells("J" + (fila2).ToString()).Value = miClub.TipoPago;
                hoja.Cells("K" + (fila2).ToString()).Value = miClub.txt_url_comprobante;

                fila2++;
            }


            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=\"Reporte de pago de afiliaciones.xlsx\"");

            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }

            Response.End();
        }

        public void GetReporteCuentasCorrientesJugadores()
        {
            var libro = new XLWorkbook(Server.MapPath(@"~/img/Excels/TemplateReporteCuentasCorrientesJugadores.xlsx"));
            IXLWorksheet hoja = libro.Worksheet(1);
            var Jugadores = new ReporteSM().GetReporteCuentaJugadores();
            int fila1 = 4;
            int fila2 = 8;

            hoja.Cells("C" + (fila1).ToString()).Value = Jugadores.Count();
            hoja.Cells("D" + (fila1).ToString()).Value = Jugadores.Count(x => x.importe > 0);
            hoja.Cells("E" + (fila1).ToString()).Value = Jugadores.Count(x => x.importe == 0);
            foreach (var miJugador in Jugadores)
            {
                hoja.Cells("C" + (fila2).ToString()).Value = miJugador.Nombre + " " + miJugador.Apellido;
                hoja.Cells("D" + (fila2).ToString()).Value = miJugador.Documento;
                hoja.Cells("E" + (fila2).ToString()).Value = miJugador.TipoJugador.ToString();
                hoja.Cells("F" + (fila2).ToString()).Value = miJugador.Pais;
                hoja.Cells("G" + (fila2).ToString()).Value = miJugador.CategoriaHandicap.ToString();
                hoja.Cells("H" + (fila2).ToString()).Value = miJugador.Handicap.ToString();
                hoja.Cells("I" + (fila2).ToString()).Value = miJugador.NroHandicap == -1 ? "" : miJugador.NroHandicap.ToString();
                hoja.Cells("J" + (fila2).ToString()).Value = miJugador.sn_pago == 1 ? "Al día" : (miJugador.sn_pago == 2 ? "En espera" : "Vencido");
                hoja.Cells("K" + (fila2).ToString()).Value = miJugador.fec_ult_pago.ToString();
                hoja.Cells("L" + (fila2).ToString()).Value = miJugador.importe == null ? "" : ((int)miJugador.importe).ToString("0.00");
                hoja.Cells("M" + (fila2).ToString()).Value = miJugador.txt_url_comprobante;
                hoja.Cells("N" + (fila2).ToString()).Value = miJugador.JugadorID;
                hoja.Cells("O" + (fila2).ToString()).Value = miJugador.FechaAlta;
                hoja.Cells("P" + (fila2).ToString()).Value = miJugador.TipoPago;
                hoja.Cells("Q" + (fila2).ToString()).Value = miJugador.Club;
                hoja.Cells("R" + (fila2).ToString()).Value = miJugador.Vigencia;
                fila2++;
            }

            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=\"Reporte de pago de Handicaps.xlsx\"");

            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }

            Response.End();
        }

        public void GetReporteJugadores()
        {
            var libro = new XLWorkbook(Server.MapPath(@"~/img/Excels/TemplateReporteJugadores.xlsx"));
            IXLWorksheet hoja = libro.Worksheet(1);
            var Jugadores = new ReporteSM().GetReporteJugadores();
            int fila = 2; int columna = 1;
            foreach (var miJugador in Jugadores)
            {
                columna = 1;

                hoja.Cell(fila, columna++).Value = miJugador.JugadorID;
                hoja.Cell(fila, columna++).Value = miJugador.Nombre;
                hoja.Cell(fila, columna++).Value = miJugador.Apellido;
                hoja.Cell(fila, columna++).Value = miJugador.TipoDoc;
                hoja.Cell(fila, columna++).Value = miJugador.Documento;
                hoja.Cell(fila, columna++).Value = miJugador.TipoJugador;
                hoja.Cell(fila, columna++).Value = miJugador.FecNac;
                hoja.Cell(fila, columna++).Value = miJugador.Nacionalidad;
                hoja.Cell(fila, columna++).Value = miJugador.Genero;
                hoja.Cell(fila, columna++).Value = miJugador.Activo;
                hoja.Cell(fila, columna++).Value = miJugador.Mail;
                hoja.Cell(fila, columna++).Value = miJugador.Telefono;
                hoja.Cell(fila, columna++).Value = miJugador.Club;
                hoja.Cell(fila, columna++).Value = miJugador.ClubRepublica;
                hoja.Cell(fila, columna++).Value = miJugador.ClubSec1;
                hoja.Cell(fila, columna++).Value = miJugador.ClubSec2;
                hoja.Cell(fila, columna++).Value = miJugador.ClubSec3;
                hoja.Cell(fila, columna++).Value = miJugador.ClubSec4;
                hoja.Cell(fila, columna++).Value = miJugador.TipoHandicapPrincipal;
                hoja.Cell(fila, columna++).Value = miJugador.CategoriaHandicapPrincipal;
                hoja.Cell(fila, columna++).Value = miJugador.HandicapPrincipal;
                hoja.Cell(fila, columna++).Value = miJugador.TipoHandicapSecundario;
                hoja.Cell(fila, columna++).Value = miJugador.CategoriaHandicapSecundario;
                hoja.Cell(fila, columna++).Value = miJugador.HandicapSecundario;
                hoja.Cell(fila, columna++).Value = miJugador.Domicilio;
                hoja.Cell(fila, columna++).Value = miJugador.NroDomicilio;
                //hoja.Cell(fila, columna++).Value = miJugador.TipoDomicilio;
                hoja.Cell(fila, columna++).Value = miJugador.Piso;
                hoja.Cell(fila, columna++).Value = miJugador.Dpto;
                hoja.Cell(fila, columna++).Value = miJugador.CodPostal;
                hoja.Cell(fila, columna++).Value = miJugador.PaisDomicilio;
                hoja.Cell(fila, columna++).Value = miJugador.ProvinciaDomicilio;
                hoja.Cell(fila, columna++).Value = miJugador.MunicipioDomicilio;
                hoja.Cell(fila, columna++).Value = miJugador.Veedor;
                hoja.Cell(fila, columna++).Value = miJugador.AdminClub;
                hoja.Cell(fila, columna++).Value = miJugador.Instagram;
                hoja.Cell(fila, columna++).Value = miJugador.Facebook;
                hoja.Cell(fila, columna++).Value = miJugador.Twitter;
                hoja.Cell(fila, columna++).Value = miJugador.sn_pago == 1 ? "Al Día" : "Vencido";
                hoja.Cell(fila, columna++).Value = miJugador.TipoPago;
                hoja.Cell(fila, columna++).Value = miJugador.txt_url_comprobante;
                hoja.Cell(fila, columna++).Value = miJugador.fec_ult_pago;

                fila++;
            }

            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=\"ReporteJugadores.xlsx\"");

            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }

            Response.End();
        }//Listo

        public void GetReporteAllCuentasCorrientesJugadores()
        {
            try
            {
                var libro = new XLWorkbook(Server.MapPath(@"~/img/Excels/TemplateReporteCuentasCorrientesJugadores.xlsx"));
                IXLWorksheet hoja = libro.Worksheet(1);
                var Jugadores = new ReporteSM().GetReporteAllCuentasCorrientesJugadores();
                int fila1 = 4;
                int fila2 = 8;

                hoja.Cells("C" + (fila1).ToString()).Value = Jugadores.Count();
                hoja.Cells("D" + (fila1).ToString()).Value = Jugadores.Count(x => x.importe > 0);
                hoja.Cells("E" + (fila1).ToString()).Value = Jugadores.Count(x => x.importe == 0);
                foreach (var miJugador in Jugadores)
                {
                    hoja.Cells("C" + (fila2).ToString()).Value = miJugador.Nombre + " " + miJugador.Apellido;
                    hoja.Cells("D" + (fila2).ToString()).Value = miJugador.Documento;
                    hoja.Cells("E" + (fila2).ToString()).Value = miJugador.TipoJugador.ToString();
                    hoja.Cells("F" + (fila2).ToString()).Value = miJugador.Pais;
                    hoja.Cells("G" + (fila2).ToString()).Value = miJugador.CategoriaHandicap.ToString();
                    hoja.Cells("H" + (fila2).ToString()).Value = miJugador.Handicap.ToString();
                    hoja.Cells("I" + (fila2).ToString()).Value = miJugador.NroHandicap;
                    hoja.Cells("J" + (fila2).ToString()).Value = miJugador.sn_pago == 1 ? "Pago" : (miJugador.sn_pago == 2 ? "En espera" : "Vencido"); //"No pago"
                    hoja.Cells("K" + (fila2).ToString()).Value = miJugador.fec_ult_pago.ToString();
                    hoja.Cells("L" + (fila2).ToString()).Value = miJugador.importe == null ? "" : ((int)miJugador.importe).ToString("0.00");
                    hoja.Cells("M" + (fila2).ToString()).Value = miJugador.txt_url_comprobante;
                    hoja.Cells("N" + (fila2).ToString()).Value = miJugador.JugadorID;
                    hoja.Cells("O" + (fila2).ToString()).Value = miJugador.FechaAlta;
                    hoja.Cells("P" + (fila2).ToString()).Value = miJugador.TipoPago;
                    fila2++;
                }

                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=\"Reporte de pago de Handicaps completo.xlsx\"");

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    libro.SaveAs(memoryStream);
                    memoryStream.WriteTo(Response.OutputStream);
                    memoryStream.Close();
                }

                Response.End();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void GetReporteInscripcionesEquiposExcel()
        {
            var equipos = new ReporteSM().GetReporteInscripcionesEquiposExcel();

            var libro = new XLWorkbook(System.Web.Hosting.HostingEnvironment.MapPath(@"~/img/Excels/TemplateReporteCuentasTorneos.xlsx"));
            IXLWorksheet hoja = libro.Worksheet(1);
            int fila1 = 5;

            foreach (var miEquipo in equipos)
            {
                hoja.Cells("C" + (fila1).ToString()).Value = miEquipo.Torneo;
                hoja.Cells("D" + (fila1).ToString()).Value = miEquipo.Equipo;
                hoja.Cells("E" + (fila1).ToString()).Value = miEquipo.Club;
                hoja.Cells("F" + (fila1).ToString()).Value = miEquipo.NombreJugador;
                hoja.Cells("G" + (fila1).ToString()).Value = miEquipo.ApellidoJugador;
                hoja.Cells("H" + (fila1).ToString()).Value = miEquipo.sn_pago == 1 ? "Pagó" : "Pendiente";
                hoja.Cells("I" + (fila1).ToString()).Value = miEquipo.fec_ult_pago.ToString();
                hoja.Cells("J" + (fila1).ToString()).Value = miEquipo.importe;
                hoja.Cells("K" + (fila1).ToString()).Value = miEquipo.txt_url_comprobante;
                hoja.Cells("L" + (fila1).ToString()).Value = miEquipo.JugadorID;
                fila1++;
            }

            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=\"Reporte de pago de inscripciones.xlsx\"");

            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }

            Response.End();
        }

        public void GetReporteInscripcionesTorneosExcel(int id)
        {
            var equipos = new ReporteSM().GetReporteInscripcionesTorneosExcel(id);


            var libro = new XLWorkbook();
            var nombreTorneo = equipos.First().Torneo.txt_desc;

            var AzulClubes = XLColor.FromArgb(0, 176, 240);
            var AmarilloEquipos = XLColor.FromArgb(255, 255, 0);
            var NaranjaTitulo = XLColor.FromArgb(255, 192, 0);
            var hoja = libro.Worksheets.Add("Equipos");

            hoja.Cells("A1").Value = nombreTorneo;


            int fila1 = 4;

            var miLetra = "A";
            var miLetra2 = "B";
            var esMasculino = false;
            var RangoTemp = "";
            var maxFila = 4;
            var cantLoops = 1;
            IXLRange rngTemp;
            if (equipos.First().Usuarios.Count(x => x.sexo == "Masculino") > 0)
            {
                esMasculino = true;
            }
            foreach (var miEquipo in equipos)
            {
                hoja.Cells(miLetra + (fila1).ToString()).Value = miEquipo.Club.txt_desc;

                RangoTemp = miLetra + (fila1).ToString() + ":" + miLetra2 + (fila1).ToString();
                rngTemp = hoja.Range(RangoTemp);
                rngTemp.Merge();
                rngTemp.FirstCell().Style
                    .Font.SetBold()
                    //                    .Font.SetFontName("Calibri")
                    .Fill.SetBackgroundColor(AzulClubes)
                    .Font.SetFontColor(XLColor.Black)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                    .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                    .Alignment.WrapText = true;
                ;
                fila1++;
                hoja.Cells(miLetra + (fila1).ToString()).Value = miEquipo.Equipo.txt_desc;
                RangoTemp = miLetra + (fila1).ToString() + ":" + miLetra2 + (fila1).ToString();
                rngTemp = hoja.Range(RangoTemp);
                rngTemp.Merge();
                rngTemp.FirstCell().Style
                    .Font.SetBold()
                    //                    .Font.SetFontName("Calibri")
                    .Fill.SetBackgroundColor(AmarilloEquipos)
                    .Font.SetFontColor(XLColor.Black)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                    .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                    .Alignment.WrapText = true;
                ;
                fila1++;
                foreach (var miJugador in miEquipo.Usuarios)
                {
                    hoja.Cells(miLetra + (fila1).ToString()).Value = miJugador.txt_nombre + " " + miJugador.txt_apellido + (miJugador.bool_Capitan == 1 ? "(C)" : miJugador.bool_suplente == 1 ? "(S)" : "");
                    hoja.Cells(miLetra2 + (fila1).ToString()).Value = (miJugador.sexo == "Masculino" ? miJugador.handicap_principal : miJugador.handicap_secundario);
                    fila1++;
                }

                hoja.Cells(miLetra + (fila1).ToString()).Value = "Handicap Total";
                hoja.Cells(miLetra2 + (fila1).ToString()).Value = esMasculino ? miEquipo.Usuarios.Where(x => x.bool_suplente == 0).Sum(x => x.handicap_principal) : miEquipo.Usuarios.Where(x => x.bool_suplente == 0).Sum(x => x.handicap_secundario);
                fila1++;
                var capitan = miEquipo.Usuarios.Where(x => x.bool_Capitan == 1).FirstOrDefault();
                if (capitan != null)
                    hoja.Cells(miLetra + (fila1).ToString()).Value = capitan.telefono + " " + miEquipo.Usuarios.Where(x => x.bool_Capitan == 1).First().email;

                RangoTemp = miLetra + (fila1).ToString() + ":" + miLetra2 + (fila1).ToString();
                rngTemp = hoja.Range(RangoTemp);
                rngTemp.Merge();
                hoja.Column(miLetra).Width = 30;
                hoja.Column(miLetra2).Width = 2.5;


                RangoTemp = miLetra + (4).ToString() + ":" + miLetra2 + (fila1).ToString();
                rngTemp = hoja.Range(RangoTemp);
                rngTemp.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                rngTemp.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                if (miLetra == "J")
                {
                    if (cantLoops == 1)
                    {
                        maxFila = 0;
                    }
                    maxFila = fila1 + 2;
                    miLetra = "A";
                    miLetra2 = "B";
                    fila1 = maxFila;
                    cantLoops = cantLoops + 1;
                }
                else
                {
                    fila1 = maxFila;
                    miLetra = incCol(miLetra);
                    miLetra = incCol(miLetra);
                    miLetra = incCol(miLetra);
                    miLetra2 = incCol(miLetra2);
                    miLetra2 = incCol(miLetra2);
                    miLetra2 = incCol(miLetra2);
                }
            }
            for (int i = 1; i < 25; i++)
            {
                hoja.Row(i).Height = 15;
            }
            RangoTemp = "A1:K1";
            rngTemp = hoja.Range(RangoTemp);
            rngTemp.Merge();
            rngTemp.FirstCell().Style
                .Font.SetBold()
                .Fill.SetBackgroundColor(NaranjaTitulo)
                .Font.SetFontColor(XLColor.Black)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                .Alignment.WrapText = true;
            ;

            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=\"Reporte de pago de inscripciones.xlsx\"");

            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }

            Response.End();
        }

        public void GetReporteInscripcionesTorneoJugadoresExcel(int id)
        {
            //TemplateReporteCuentasJugadorTorneo
            var equipos = new ReporteSM().GetReporteInscripcionesTorneosExcel(id);// (await reporteTorneoService.GetByID(id));
            var libro = new XLWorkbook(System.Web.Hosting.HostingEnvironment.MapPath(@"~/img/Excels/TemplateReporteCuentasCorrientesTorneoJugadores.xlsx"));
            IXLWorksheet hoja = libro.Worksheet(1);
            var nombreTorneo = equipos.First().Torneo.txt_desc;

            hoja.Cells("A1").Value = nombreTorneo;

            int fila1 = 4;

            foreach (var miEquipo in equipos)
            {

                foreach (var miJugador in miEquipo.Usuarios)
                {
                    hoja.Cells("C" + (fila1).ToString()).Value = miJugador.txt_nombre + " " + miJugador.txt_apellido;
                    hoja.Cells("D" + (fila1).ToString()).Value = miJugador.handicap_principal;
                    hoja.Cells("E" + (fila1).ToString()).Value = miJugador.email;
                    hoja.Cells("F" + (fila1).ToString()).Value = miJugador.telefono;
                    if (miEquipo.sn_pago == 1)
                        hoja.Cells("G" + (fila1).ToString()).Value = "Pagó";
                    else
                        hoja.Cells("G" + (fila1).ToString()).Value = "No Pagó";
                    hoja.Cell("H" + (fila1).ToString()).Value = miJugador.fec_nacimiento;
                }

                fila1++;
            }

            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=\"Reporte de pago de inscripciones.xlsx\"");

            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }

            Response.End();
        }


        private static string incCol(string col)
        {
            if (col == "") return "A";
            string fPart = col.Substring(0, col.Length - 1);
            char lChar = col[col.Length - 1];
            if (lChar == 'Z') return incCol(fPart) + "A";
            return fPart + ++lChar;
        }
        #endregion

    }
}