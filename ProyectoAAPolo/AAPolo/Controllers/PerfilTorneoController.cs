﻿using AAPolo.Aplication;
using AAPolo.Controllers.Helper;
using AAPolo.Entities;
using AAPolo.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Controllers
{
    [Authorize]
    public class PerfilTorneoController : ClassHelper
    {
        // GET: PerfilTorneo
        public async Task<ActionResult> Index()
        {
            Persona p = DatosUsuario();
            if (p.Codigo == 0 || p.Rol.Codigo != ConfiguracionAPP.Rol_Jugador)
                return RedirectToAction("Index", "Home");

            string cadena = obtenerTagDelWebConfig("UserJugador");
            List<int> lCodUsuario = cadena.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(Int32.Parse).ToList();
            bool SoloTorneoActivo = true;
            if (lCodUsuario.Any(x => x == p.Codigo))
            {
                SoloTorneoActivo = false;
            }

            var vP = new PersonaSM().BuscaJugadorAsync(p);
            var vPAx = new PersonaSM().BuscaJugadorPagoHandicapAsync(p.Codigo, "", 0, 0, 0);

            var vlTorneo = SoloTorneoActivo ? new TorneoSM().BuscarTorneoInscipcionActivoAsync(0, 0) : new TorneoSM().BuscarTorneoAsync(0, 0, 0, true);
            var vlClub = new ClubSM().BuscaClubAsync(0, 0, "", 0);

            await Task.WhenAll(vP, vPAx, vlTorneo, vlClub);

            p = vP.Result.FirstOrDefault();

            Persona perAux = vPAx.Result.FirstOrDefault();
            p.HandicapPago = perAux.HandicapPago;
            if (p.HandicapPersona != null)
            {
                p.HandicapPersona.strFechaHasta = perAux.HandicapPersona.strFechaHasta;
                p.HandicapPersona.DescripcionHandicapUsuario = perAux.HandicapPersona.DescripcionHandicapUsuario;
            }
            else
            {
                p.HandicapPersona = new HandicapPersona()
                {
                    strFechaHasta = perAux.HandicapPersona.strFechaHasta,
                    DescripcionHandicapUsuario = perAux.HandicapPersona.DescripcionHandicapUsuario
                };
            }

            ViewBag.lTorneo = vlTorneo.Result;
            ViewBag.lClub = vlClub.Result.Where(x => x.EsActivo).ToList();

            return View(p);
        }

        public PartialViewResult BuscarPersona(int Cod_Torneo, string Cadena, int Cod_TipoTorneo, int Cod_CategoriaTorneo, int HandMax, int EHandMin, int EHandMaxJug, int EHandMinJug, string ElementoHtml, int PermiteMenor = 0)
        {
            int Cod_Genero = 0;
            int Cod_TipoJugador = 0;
            List<Persona> lPer;
            try
            {
                ViewBag.ElementoHtml = ElementoHtml;
                if (Cod_CategoriaTorneo == 8)/*Femenino, buscar solo chicas*/
                    Cod_Genero = ConfiguracionAPP.Genero_Femenino;
                else if (Cod_CategoriaTorneo == 9)//Menor
                    Cod_TipoJugador = 4;
                else if (Cod_CategoriaTorneo == 17)//Mayor
                    Cod_TipoJugador = 1;
                else if (Cod_CategoriaTorneo == 18)//Juveniles
                    Cod_TipoJugador = 37;

                lPer = new PersonaSM().BuscaJugadorInscripcionTorneo(0, Cadena, (Cod_TipoJugador==4 ? Cod_TipoJugador : 0), 0, 0, Cod_Torneo, Cod_Genero, false);
                //lPer = new PersonaSM().BuscaJugadorPagoHandicap(0, Cadena, 0, 0, 0, Cod_Genero, false);
                if (Cod_TipoJugador == 1)//Mayor
                    lPer = lPer.Where(x => x.TipoJugador.Codigo == 4 || x.TipoJugador.Codigo == 1 || x.TipoJugador.Codigo == 37 || x.TipoJugador.Codigo == 2).ToList();
                else if(Cod_TipoJugador == 37) {//Juveniles
                    lPer = lPer.Where(x => x.TipoJugador.Codigo == 37 || x.TipoJugador.Codigo == (PermiteMenor == 1? 4 : 37)).ToList();
                }


                lPer = lPer.Where(x => x.HandicapPersona.Categoria.Codigo == 1 /*total*/ || x.HandicapPersona.Categoria.Codigo == 2 /*federado*/).ToList();

                //if (Cod_TipoTorneo == 2) // Categoria handicap federado
                //{
                //    lPer = lPer.Where(x => x.HandicapPersona.Categoria.Codigo == 1 /*total*/ || x.HandicapPersona.Categoria.Codigo == 2 /*federado*/).ToList();
                //}
                //else if(Cod_TipoTorneo == 1)// Categoria handicap total
                //{
                //    lPer = lPer.Where(x => x.HandicapPersona.Categoria.Codigo == 1 /*total*/).ToList();
                //}

            }
            catch (Exception)
            {
                lPer = new List<Persona>();

            }

            ViewBag.Cod_TipoTorneo = Cod_TipoTorneo;
            return PartialView(lPer);
        }

        public PartialViewResult _DatosPersonaInscripcion(int Codigo, string NombreCompleto, string NombreClub, string NombreTipoDocumento, string Documento, int NroHandicap, int NroHandicapSecundario, int Cod_Genero)
        {
            Persona obj = new Persona()
            {
                Codigo = Codigo,
                Nombre = NombreCompleto,
                Genero = new Genero() { Codigo = Cod_Genero},
                clubUsuarioPrincipal = new ClubUsuario()
                {
                    NombreClub = NombreClub,
                    EsPrincipal = true
                },
                DocumentoIdentidad = Documento,
                TipoDocumento = new TipoDocumento()
                {
                    Nombre = NombreTipoDocumento
                },
                HandicapPersona = new HandicapPersona()
                {
                    NroHandicap = NroHandicap,
                    NroHandicapSecundario = NroHandicapSecundario
                }
            };
            return PartialView(obj);
        }

        public PartialViewResult _BuscarEquipo()
        {
            try
            {
                return PartialView(new TorneoSM().BuscarEquipo(0, DatosUsuario().Codigo, 0));
            }
            catch (Exception e)
            {
                return PartialView(new List<Equipo>());
            }
        }

        [HttpPost]
        public PartialViewResult _DeclinarInscripcion(int Codigo, string NombreEquipo, string NombreTorneo)
        {
            ViewBag.Cod_Pago = Codigo;
            ViewBag.NombreEquipo = NombreEquipo;
            ViewBag.NombreTorneo = NombreTorneo;
            return PartialView();
        }

        public JsonResult GuardarInscripcionEquipo(Equipo obj, int EsInscripcionEquipo = 0)
        {

            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, los datos ingresados son inválidos."
            };

            try
            {

                if (obj.lPersona == null)
                {
                    result.Mensaje = "Disculpe, debe seleccionar las personas ha inscribir.";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                if (EsInscripcionEquipo == 1 && String.IsNullOrEmpty(obj.NombreEquipo)) {
                    result.Mensaje = "Por favor, ingrese un nombre para el equipo ha inscribir.";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                if ((EsInscripcionEquipo == 1 && obj.lPersona.Take(4).Where(x => x.Codigo>0).Count() < 4) || (EsInscripcionEquipo == 0 && (obj.lPersona.Count() == 1 && obj.lPersona.FirstOrDefault().Codigo==0)))
                {
                    if (EsInscripcionEquipo == 1)
                    {
                        result.Mensaje = "Por favor, seleccione 4 integrantes para el equipo a inscribir.";
                    }
                    else {
                        result.Mensaje = "Por favor, seleccione la persona ha inscribir.";
                    }
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                string idReference = "AAPolo" + "-user-" + DatosUsuario().Codigo + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss.fff");

                obj.torneo.PermiteInscipcionIndividual = EsInscripcionEquipo == 1 ? false : true;

                if (EsInscripcionEquipo == 1 && (obj.club == null || obj.club.Codigo == 0)) {
                    result.Mensaje = "Por favor, seleccione club.";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

                result = new PagoSM().GuardarInscripcionEquipo(obj, idReference, DatosUsuario().Codigo);
                if (result.EsSatisfactorio)
                {
                    obj.strImporte = result.Cadena;
                    double import = 0;
                    bool PermiteTorneoGratis = false;
                    try
                    {
                        import = (CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator == "," && !obj.strImporte.Contains(",") ? Convert.ToDouble(obj.strImporte.Replace('.', ',')) : Convert.ToDouble(obj.strImporte));
                        PermiteTorneoGratis = Convert.ToBoolean(obtenerTagDelWebConfig("PermiteTorneoGratis"));
                    }
                    catch (Exception)
                    {
                        PermiteTorneoGratis = false;
                        import = 0;
                    }
                    if (import == 0 && PermiteTorneoGratis == true) {
                        result.Cadena = Url.Action("DetalleDePago", "PerfilTorneo", new { external_reference = idReference, collection_id = "" });
                    }
                    else {
                        result.Cadena = obtenerTagDelWebConfig("UrlPasarelaDePago") + HttpUtility.UrlEncode(obtenerDatosPasarelaMercadoPago(obj, idReference));
                    }
                }
            }
            catch (Exception e)
            {
                result.MensajeError = e.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);

        }

        #region Actualizacion Pago torneo
        public ActionResult DetalleDePago(string external_reference, string collection_id = "")
        {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, ocurrio un error al guardar los resultados de su operación, por favor contacte con los administradores."
            };
            try
            {
                result = new PagoSM().ActualizarTransacionPagoInscripcionEquipo(external_reference, 1, collection_id);
                TempData["Mensaje"] = result.Mensaje;
                TempData["EsSatisfactorio"] = result.EsSatisfactorio;
                TempData["MostrarMsj"] = true;
            }
            catch (Exception)
            {

                throw;
            }
            return RedirectToAction("Index", "HistorialPagos");
        }

        public ActionResult DetalleDePagoPending(string external_reference, string collection_id = "")
        {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, ocurrio un error al guardar los resultados de su operación, por favor contacte con los administradores."
            };
            try
            {
                result = new PagoSM().ActualizarTransacionPagoInscripcionEquipo(external_reference, 2, collection_id);
                TempData["Mensaje"] = result.Mensaje;
                TempData["EsSatisfactorio"] = result.EsSatisfactorio;
                TempData["MostrarMsj"] = true;
            }
            catch (Exception)
            {

                throw;
            }
            return RedirectToAction("Index", "HistorialPagos");
        }

        public ActionResult DetalleDePagoError(string external_reference, string collection_id = "")
        {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, ocurrio un error al guardar los resultados de su operación, por favor contacte con los administradores."
            };
            try
            {
                result = new PagoSM().ActualizarTransacionPagoInscripcionEquipo(external_reference, 3, collection_id);
                TempData["Mensaje"] = result.Mensaje;
                TempData["EsSatisfactorio"] = result.EsSatisfactorio;
                TempData["MostrarMsj"] = true;
            }
            catch (Exception)
            {

                throw;
            }
            return RedirectToAction("Index", "HistorialPagos");
        }
        #endregion
        string obtenerDatosPasarelaMercadoPago(Equipo equipo, string idReference)
        {
            DatosMercadoPago mpg;

            mpg = new DatosMercadoPago()
            {
                ID_Credencial = obtenerTagDelWebConfig("MercadoPagoClientID"),// "3906782221688425", 
                Clave_Credencial = obtenerTagDelWebConfig("MercadoPagoClientSecret"),//"8QCz3xC2W2KEzG51Qk37jP10LKMXWC2J" ,
                Referencia_Externa = idReference,
                UrlAPP = Request.Url.Authority,
                UrlSuccess = Request.Url.Authority + "/" + Url.Action("DetalleDePago", "PerfilTorneo", new { external_reference = idReference }).ToString(),
                UrlPending = Request.Url.Authority + "/" + Url.Action("DetalleDePagoPending", "PerfilTorneo", new { external_reference = idReference }).ToString(),
                UrlFailure = Request.Url.Authority + "/" + Url.Action("DetalleDePagoError", "PerfilTorneo", new { external_reference = idReference }).ToString(),
                UrlNotificacion = "",
            };

            List<Item> lItem = new List<Item>();
            
                lItem.Add(new Item()
                {
                    title = DatosUsuario().NombreCompleto + " abona el torneo: " + equipo.torneo.NombreTorneo + (String.IsNullOrEmpty(equipo.NombreEquipo) ? " - Participante: " + equipo.lPersona.FirstOrDefault().NombreCompleto : " - Equipo: " + equipo.NombreEquipo),
                    currency_id = obtenerTagDelWebConfig("MercadoPagoMoneda"),
                    quantity = 1 /*un paquete*/,
                    unit_price = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator == "," && !equipo.strImporte.Contains(",") ? Convert.ToDouble(equipo.strImporte.Replace('.', ',')) : Convert.ToDouble(equipo.strImporte)
                });
            

            mpg.lstItem = lItem;

            mpg.payer = new payer()
            {
                name = DatosUsuario().Nombre,
                surname = DatosUsuario().Apellido,
                email = "",
                date_created = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"),
                identification = new identification()
                {
                    type = "DNI",
                    number = DatosUsuario().DocumentoIdentidad
                }
            };

            return JsonConvert.SerializeObject(mpg);
        }

    }
}