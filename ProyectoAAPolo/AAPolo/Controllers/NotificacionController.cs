﻿using AAPolo.Aplication;
using AAPolo.Controllers.Helper;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Controllers
{
    [Authorize]
    public class NotificacionController : ClassHelper
    {
        // GET: Notificacion
        public ActionResult Index()
        {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            return View();
        }

        public PartialViewResult _Listado(int Codigo = 0)
        {
            try
            {
                return PartialView(new NotificacionSM().BuscarNotificacion(Codigo, ""));
            }
            catch (Exception e)
            {
                return PartialView(new List<Notificacion>());
            }
        }

        public ActionResult CrearNotificacion()
        {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            return View();
        }

        public ActionResult EditarNotificacion(int Codigo = 0)
        {
            if (Codigo == 0)
                return RedirectToAction("Index");

            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            Notificacion notif = new Notificacion();

            try
            {
                notif = new NotificacionSM().BuscarNotificacion(Codigo, "").FirstOrDefault();
                if (notif == null || notif.Codigo == 0)
                    return RedirectToAction("Index");

                if (notif.EsEnviado)
                    return RedirectToAction("VerNotificacion", "Notificacion", new { Codigo = 0 });

                return View(notif);

            }
            catch (Exception e)
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult VerNotificacion(int Codigo = 0)
        {
            if (Codigo == 0)
                return RedirectToAction("Index");

            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            Notificacion notif = new Notificacion();

            try
            {
                notif = new NotificacionSM().BuscarNotificacion(Codigo, "").FirstOrDefault();
                if (notif == null || notif.Codigo == 0)
                    return RedirectToAction("Index");

                return View(notif);

            }
            catch (Exception e)
            {
                return RedirectToAction("Index");
            }
        }

        public PartialViewResult _SelectRol(int Codigo = 0)
        {
            try
            {
                ViewBag.Cod_Rol = Codigo;
                return PartialView(new RolSM().BuscarRolVisible());
            }
            catch (Exception e)
            {
                return PartialView(new List<Rol>());
            }
        }

        public PartialViewResult _SelectTipoJugador(int Codigo = 0)
        {
            try
            {
                ViewBag.Cod_TipoJugador = Codigo;
                return PartialView(new PersonaSM().BuscarTipoJugador());
            }
            catch (Exception e)
            {
                return PartialView(new List<TipoJugador>());
            }
        }

        public PartialViewResult _SelectCategoriaHandicap(List<int> lCodTipoJugador = null)
        {
            List<Handicap> lCategoria = new List<Handicap>();
            try
            {
                ViewBag.lCodTipoJugador = lCodTipoJugador;
                if (lCodTipoJugador != null)
                {
                    lCategoria = new HandicapSM().BuscarHandicap(0, 0, 0, 0, 0).Where(x => lCodTipoJugador.Any(a => a == x.tipoJugador.Codigo)).ToList();
                }
                lCategoria = lCategoria.GroupBy(g => g.Categoria.Codigo).Select(s => s.FirstOrDefault()).ToList();
                return PartialView(lCategoria);
            }
            catch (Exception e)
            {
                return PartialView(new List<Handicap>());
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GuardarNotificacion(Notificacion obj, List<int> lCodRol = null, List<int> lCodTipoJugador = null, List<int> lCodCategoria = null)
        {

            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos."
            };
            
            if (lCodRol != null && lCodRol.Count > 0)
            {
                obj.lRol = lCodRol.Select(s => new Rol() { Codigo = s }).ToList();
            }
            else
            {
                /*obj.lRol = new List<Rol>();
                obj.lRol.Add(new Rol() { Codigo = ConfiguracionAPP.Rol_Jugador});*/
                result.Mensaje = "Disculpe, debe seleccionar roles.";
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            /*if (lCodTipoJugador != null && lCodTipoJugador.Count > 0)
            {
                obj.lTipoJugador = lCodTipoJugador.Select(s => new TipoJugador() { Codigo = s }).ToList();
            }
            else
            {
                result.Mensaje = "Disculpe, debe seleccionar tipo de jugadores.";
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (lCodCategoria != null && lCodCategoria.Count > 0)
            {
                obj.lCategoriaHandicap = lCodCategoria.Select(s => new CategoriaHandicap() { Codigo = s }).ToList();
            }
            else
            {
                result.Mensaje = "Disculpe, debe seleccionar categoría de handicap.";
                return Json(result, JsonRequestBehavior.AllowGet);
            }*/

            obj.PersonaAlta = DatosUsuario();

            int Dia = Convert.ToInt32(obj.strFechaEnvio.Split('/')[0]),
                Mes = Convert.ToInt32(obj.strFechaEnvio.Split('/')[1]),
                Anio = Convert.ToInt32(obj.strFechaEnvio.Split('/')[2]),
                Hora = Convert.ToInt32(obj.strHoraEnvio.Split(':')[0]),
                Min = Convert.ToInt32(obj.strHoraEnvio.Split(':')[1]);

            try
            {
                obj.FechaHoraEnvio = new DateTime(Anio, Mes, Dia, Hora, Min, 0);
            }
            catch (Exception)
            {
                result.Mensaje = "Disculpe, debe fecha y hora de envío válida.";
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (ModelState.IsValid)
            {
                result = new NotificacionSM().GuardarNotificacion(obj);
            }
            else
            {
                result.Mensaje = ObtenerErrorMSJModel(ModelState);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /*public PartialViewResult NotificacionUsuario()
        {
            try
            {
                return PartialView(new NotificacionSM().BuscarNotificacionUsuario(DatosUsuario().Codigo));
            }
            catch (Exception)
            {
                throw;
            }
        }*/

        [HttpPost]
        public PartialViewResult _ListadoNotificacionUsuario(int Indice = 0)
        {

            List<NotificacionUsuario> lNotif = new List<NotificacionUsuario>();

            try
            {
                lNotif = new NotificacionSM().BuscarNotificacionUsuario(DatosUsuario().Codigo, Indice);
            }
            catch (Exception e)
            {
                ViewBag.Error = e.ToString();
            }
            return PartialView(lNotif);
        }

        [HttpPost]
        public JsonResult ActualizarNotificacionLeida(int Codigo)
        {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos."
            };

            try
            {
                if (Codigo > 0)
                    result = new NotificacionSM().ActualizarNotificacionLeida(Codigo);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
    }
}