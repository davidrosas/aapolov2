﻿using AAPolo.Aplication;
using AAPolo.Controllers.Helper;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Controllers
{
    [Authorize]
    public class ClubController : ClassHelper
    {
        // GET: Club
        public ActionResult Index()
        {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            return View();
        }

        public PartialViewResult Listado(int Cod_CategoriaClub = 0, int Cod_Club = 0, string Nombre = "")
        {
            List<Club> lClub = new ClubSM().BuscaClub(Cod_CategoriaClub, Cod_Club, Nombre, 0);
            return PartialView(lClub);
        }

        public PartialViewResult _SelectTipoCategoriaClub()
        {
            try
            {
                return PartialView(new ClubSM().BuscarCategoriaClub());
            }
            catch (Exception)
            {
                return PartialView(new List<CategoriaClub>());
            }
        }

        public ActionResult CrearClub()
        {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            return View();
        }

        public ActionResult EditarClub(int Codigo = 0)
        {
            if (Codigo == 0)
                return RedirectToAction("Index");

            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            try
            {
                return View(new ClubSM().BuscaClub(0, Codigo, "", 0).FirstOrDefault());
            }
            catch (Exception e)
            {
                ViewBag.MensajeError = e.Message;
                return RedirectToAction("Index");
            }

        }

        public JsonResult _CrearClub(Club obj)
        {

            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos."
            };

            try
            {
                if (!String.IsNullOrEmpty(obj.strFechaFundacion))
                {
                    int Dia = Convert.ToInt32(obj.strFechaFundacion.Split('-')[0]),
                        Mes = Convert.ToInt32(obj.strFechaFundacion.Split('-')[1]),
                        Anio = Convert.ToInt32(obj.strFechaFundacion.Split('-')[2]);
                    obj.FechaFundacion = new DateTime(Anio, Mes, Dia);
                }
                else {
                    obj.FechaFundacion = new DateTime(1753, 1, 1);
                }

                if (obj.Imagen != null)
                {
                    obj.UrlImagen = GuardarImagen(obj.Imagen);
                }
                else
                {
                    obj.UrlImagen = "";
                }

                result = new ClubSM().GuardarClub(obj);
            }
            catch (Exception e)
            {
                result.MensajeError = e.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public JsonResult CambiarEstadoClub(int Codigo, int EstadoClub)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };

            try
            {
                resul = new ClubSM().CambiarEstadoClub(Codigo, EstadoClub);
            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CambiarEstadoDestacadoClub(int Codigo, int EstadoDestacadoClub)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };

            try
            {
                resul = new ClubSM().CambiarEstadoDestacadoClub(Codigo, EstadoDestacadoClub);
            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult _Contacto(TipoContacto obj, Contacto contacto = null)
        {
            if (contacto == null)
            {
                contacto = new Contacto() { TipoContacto = obj };
            }
            else
            {
                contacto.TipoContacto = obj;
                if (contacto.Nombre.Equals(contacto.TipoContacto.Nombre) || String.IsNullOrEmpty(contacto.Nombre)) {
                    if (obj.Codigo == ConfiguracionAPP.TipoContacto_Email)
                        contacto.Nombre = "noinformado@noinformado.com";
                    else
                        contacto.Nombre = "No informado";
                }
            }

            return PartialView(contacto);
        }

        public PartialViewResult _Domicilio(TipoDomicilio obj, Domicilio domicilio = null)
        {

            if (domicilio == null)
            {
                domicilio = new Domicilio()
                {
                    tipoDomicilio = obj
                };
            }
            else
            {
                domicilio.tipoDomicilio = obj;
            }

            return PartialView(domicilio);
        }

        public PartialViewResult _SelectPais(int Cod_Pais = 0)
        {
            try
            {
                ViewBag.Cod_Pais = Cod_Pais;
                return PartialView(new PersonaSM().BuscarPais());
            }
            catch (Exception e)
            {
                return PartialView(new List<Pais>());
            }
        }

        public PartialViewResult _SelectProvincia(int Codigo = 0, int Cod_Provincia = 0)
        {
            try
            {
                ViewBag.Cod_Provincia = Cod_Provincia;
                return PartialView(new PersonaSM().BuscarProvincia(Codigo));
            }
            catch (Exception e)
            {
                return PartialView(new List<Provincia>());
            }
        }

        public PartialViewResult _SelectMunicipio(int Codigo = 0, int Cod_Municipio = 0)
        {
            try
            {
                ViewBag.Cod_Municipio = Cod_Municipio;
                return PartialView(new PersonaSM().BuscarMunicipio(Codigo));
            }
            catch (Exception e)
            {
                return PartialView(new List<Municipio>());
            }
        }

        public PartialViewResult _SelectPersona()
        {
            try
            {
                return PartialView(new PersonaSM().BuscaJugador(new Persona() { Codigo = 0 }));
            }
            catch (Exception e)
            {
                ViewBag.ErrorCatch = e.Message;
                return PartialView(new List<Persona>());
            }
        }

        public PartialViewResult _BuscarUsuario(string Cadena)
        {
            try
            {
                return PartialView(new PersonaSM().BuscaJugadorResumen(0, Cadena, 0, 0, -1, 0));
            }
            catch (Exception e)
            {
                ViewBag.MensajeError = e.Message;
                return PartialView(new List<Persona>());
            }
        }



    }
}