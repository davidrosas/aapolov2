﻿using AAPolo.Aplication;
using AAPolo.Controllers.Helper;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace AAPolo.Controllers
{
    [Authorize]
    public class AdministradorController : ClassHelper
    {
        // GET: Administrador
        [AllowAnonymous]
        public ActionResult Index()
        {
            if (DatosUsuario().Codigo == 0)
                return RedirectToAction("LoginUsuario");

            if (DatosUsuario().Rol.Codigo == ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Jugador");

            return RedirectToAction("Index", "Home");
        }


        [AllowAnonymous]
        public ActionResult LoginUsuario(Persona obj)
        {
            ViewBag.MensajeErrorLogin = Convert.ToString(TempData["MensajeErrorLogin"]);
            return View(obj);
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult Login(Persona obj)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Debe ingresar un usuario y contraseña válida."
            };

            if (obj.DocumentoIdentidad != null && !obj.DocumentoIdentidad.Equals("") && obj.Password != null && !obj.Password.Equals(""))
            {
                resul = InicioSesion(obj, ConfiguracionAPP.Rol_Administrador);
                if (resul.EsSatisfactorio)
                {
                    return RedirectToAction("Index", "Jugador");
                }

            }
            TempData["MensajeErrorLogin"] = resul.Mensaje;
            return RedirectToAction("LoginUsuario", "Administrador");

        }

        
        public ActionResult AdminSidepro()
        {
            if (DatosUsuario().Codigo == 0 || DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index","Home");

            Persona p = new PersonaSM().BuscarUsuarioSidepro(DatosUsuario());

            if (p.Rol.Codigo == ConfiguracionAPP.Rol_Administrador)
            {
                string urlRedireccion = obtenerTagDelWebConfig("UrlSideproInisioSesionVeedor") + "/Home/LoginRemotoAdmin?Usuario=" + p.DocumentoIdentidad + "&Password=" + p.Password;
                return Redirect(urlRedireccion);
            }
            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        public ActionResult RemotoUsuario(string Documento = "", string Password = "")
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Debe ingresar un usuario y contraseña válida."
            };
            if (DatosUsuario().DocumentoIdentidad != Documento && Documento != "" && Password != "")
            {
                resul = InicioSesionPasswordEncriptado(new Persona() { DocumentoIdentidad = Documento, Password = Password }, ConfiguracionAPP.Rol_Administrador);
                if (resul.EsSatisfactorio)
                {
                    return RedirectToAction("Index", "Jugador");
                }
            }
            else if (DatosUsuario().DocumentoIdentidad == Documento && Documento != "" && DatosUsuario().Codigo > 0 && DatosUsuario().Rol.Codigo == ConfiguracionAPP.Rol_Administrador) {
                return RedirectToAction("Index", "Jugador");
            }
            TempData["MensajeErrorLogin"] = resul.Mensaje;
            return RedirectToAction("LoginUsuario", "Administrador");
        
        }

        public ActionResult CerrarSession()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}