﻿using AAPolo.Aplication;
using AAPolo.Entities;
using AAPolo.Controllers.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Reflection;
using System.Net.Mail;

namespace AAPolo.Controllers.Helper
{
    public class ClassHelper : Controller
    {

        public Persona DatosUsuario()
        {
            Persona obj = JsonConvert.DeserializeObject<Persona>(User.Identity.Name);

            if (obj == null)
                obj = new Persona();
            return obj;
        }

        public string obtenerTagDelWebConfig(string pNombreTag)
        {
            var AppSettings = ConfigurationManager.AppSettings;
            string key = AppSettings[pNombreTag];
            return key;
        }

        public string ObtenerErrorMSJModel(ModelStateDictionary obj)
        {

            return string.Join(", ", obj.Values.SelectMany(v => v.Errors)
                                                      .Select(e => e.ErrorMessage)) + ".";
        }

        public String RenderRazorViewToString(ControllerContext controllerContext, String viewName, Object model)
        {
            controllerContext.Controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var ViewResult = ViewEngines.Engines.FindPartialView(controllerContext, viewName);
                var ViewContext = new ViewContext(controllerContext, ViewResult.View, controllerContext.Controller.ViewData, controllerContext.Controller.TempData, sw);
                ViewResult.View.Render(ViewContext, sw);
                ViewResult.ViewEngine.ReleaseView(controllerContext, ViewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        public void SetupFormsAuthTicket(UsuarioLogin obj, bool persistanceFlag)
        {
            string cadena = JsonConvert.SerializeObject(obj).ToString();
            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1,
                                    obj.Codigo.ToString(),
                                    DateTime.Now,
                                    DateTime.Now.AddMinutes(30),
                                    persistanceFlag,
                                    cadena,
                                    FormsAuthentication.FormsCookiePath
                                    );

            string encTicket = FormsAuthentication.Encrypt(authTicket);
            this.Response.Cookies.Add(
                 new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));

        }

        public Persona InicioSesion(Persona model, int CodRol)
        {
            FormsAuthentication.SignOut();
            Persona Per = new PersonaSM().BuscarUsuarioLogin(model);

            UsuarioLogin usuario = new UsuarioLogin(Per);

            if (usuario != null && usuario.Codigo > 0)
            {
                model.EsSatisfactorio = true;
                if (usuario.EsActivo && usuario.Rol.Codigo == CodRol)
                {
                    var Memb = new ProveedorSeguridad().GetUser(usuario, false);
                    string key = Memb.ProviderUserKey.ToString();
                    int log = key.Length;
                    SetupFormsAuthTicket(usuario, true);
                    FormsAuthentication.SetAuthCookie(key, false);
                    model.Mensaje = "Bienvenido.";
                }
                else
                {
                    model.Mensaje = "Disculpe, el usuario ingresado se encuentra inactivo.";
                    model.EsSatisfactorio = false;
                }
            }
            else
            {
                model.EsSatisfactorio = false;
                model.Mensaje = "Usuario y/ó contraseña inválida.";
            }
            return model;
        }

        public Persona InicioSesionPasswordEncriptado(Persona model, int CodRol)
        {
            FormsAuthentication.SignOut();
            Persona Per = new PersonaSM().BuscarUsuarioLoginPasswordEncriptado(model);

            UsuarioLogin usuario = new UsuarioLogin(Per);

            if (usuario != null && usuario.Codigo > 0)
            {
                model.EsSatisfactorio = true;
                if (usuario.EsActivo && usuario.Rol.Codigo == CodRol)
                {
                    var Memb = new ProveedorSeguridad().GetUser(usuario, false);
                    string key = Memb.ProviderUserKey.ToString();
                    int log = key.Length;
                    SetupFormsAuthTicket(usuario, true);
                    FormsAuthentication.SetAuthCookie(key, false);
                    model.Mensaje = "Bienvenido.";
                }
                else
                {
                    model.Mensaje = "Disculpe, el usuario ingresado se encuentra inactivo.";
                }
            }
            else
            {
                model.EsSatisfactorio = false;
                model.Mensaje = "Usuario y/ó contraseña inválida.";
            }
            return model;
        }

        public Persona ActualizarSession(Persona model)
        {

            UsuarioLogin usuario = new UsuarioLogin(model);
            if (usuario != null && usuario.Codigo > 0)
            {
                model.EsSatisfactorio = true;
                if (usuario.EsActivo)
                {
                    var Memb = new ProveedorSeguridad().GetUser(usuario, false);
                    string key = Memb.ProviderUserKey.ToString();
                    int log = key.Length;
                    SetupFormsAuthTicket(usuario, true);
                    FormsAuthentication.SetAuthCookie(key, false);
                    model.Mensaje = "Bienvenido.";
                }
                else
                {
                    model.Mensaje = "Disculpe, el usuario ingresado se encuentra inactivo.";
                }
            }
            else
            {
                model.EsSatisfactorio = false;
                model.Mensaje = "Usuario y/ó contraseña inválida.";
            }
            return model;
        }

        public string GuardarImagen(HttpPostedFileBase archivo)
        {
            string urlFolderSave = obtenerTagDelWebConfig("UrlImg");
            string UrlTemp = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            string Url_Archivo = "";
            try
            {
                if (archivo != null && archivo.ContentLength > 0 && !string.IsNullOrEmpty(archivo.FileName))
                {
                    UrlTemp = UrlTemp + "_" + archivo.FileName.Replace(" ", "_");
                    if (!Directory.Exists(Server.MapPath(urlFolderSave)))
                    {
                        Directory.CreateDirectory(Server.MapPath(urlFolderSave));
                    }
                    archivo.SaveAs(Server.MapPath(urlFolderSave + UrlTemp));
                    if (Convert.ToBoolean(obtenerTagDelWebConfig("IndIP")))
                    {
                        Url_Archivo = obtenerTagDelWebConfig("ProtocoloNetWork") + GetIpV4() + obtenerTagDelWebConfig("PuertoServer") + urlFolderSave + UrlTemp;
                    }
                    else
                    {
                        Url_Archivo = urlFolderSave + UrlTemp;
                    }
                }
                else
                {
                    Url_Archivo = "";
                }
                return Url_Archivo;
            }
            catch (Exception e)
            {
                return "not/" + urlFolderSave + UrlTemp;
            }
        }

        public IPAddress GetIpV4()
        {
            try
            {
                string hostName = Dns.GetHostName(); // Retrive the Name of HOST   
                IPAddress myIP = Dns.GetHostEntry(hostName).AddressList.First(a => a.AddressFamily == AddressFamily.InterNetwork);

                return myIP;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public T Cast<T>(Object myobj)
        {
            Type objectType = myobj.GetType();
            Type target = typeof(T);
            var x = Activator.CreateInstance(target, false);
            var z = from source in objectType.GetMembers().ToList()
                    where source.MemberType == MemberTypes.Property
                    select source;
            var d = from source in target.GetMembers().ToList()
                    where source.MemberType == MemberTypes.Property
                    select source;

            List<MemberInfo> members = d.Where(memberInfo => d.Select(c => c.Name)
               .ToList().Contains(memberInfo.Name)).ToList();

            List<MemberInfo> newMembers = z.Where(memberInfo => z.Select(c => c.Name)
               .ToList().Contains(memberInfo.Name)).ToList();

            PropertyInfo propertyInfo;
            object value;
            foreach (MemberInfo memberInfo in members)
            {
                if (newMembers.Any(a => a.Name == memberInfo.Name))
                {
                    propertyInfo = typeof(T).GetProperty(memberInfo.Name);
                    if (propertyInfo.CanWrite == true) {
                        value = myobj.GetType().GetProperty(memberInfo.Name).GetValue(myobj, null);

                        propertyInfo.SetValue(x, value, null);
                    }
                }
            }
            return (T)x;
        }

        public bool EsEmailValido(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
        
    }
}