﻿using AAPolo.Aplication;
using AAPolo.Controllers.Helper;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Controllers
{
    [Authorize]
    public class CommunityController : ClassHelper
    {
        // GET: Community
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Beneficio()
        {
            return View();
        }

        public ActionResult NuevoMiembro()
        {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Jugador && !(DatosUsuario().Codigo > 0))
                return RedirectToAction("Index", "Home");

            Miembro miemb = new Miembro();
            try
            {
                var obj = (new PersonaSM().BuscaJugador(new Persona() { Codigo = DatosUsuario().Codigo }).FirstOrDefault());
                miemb = Cast<Miembro>(obj);
            }
            catch (Exception e)
            {
                ViewBag.MensajeError = e.Message;
                return RedirectToAction("Index");
            }

            miemb.EsMiembroJugador = true;
            return View(miemb);
        }

        [AllowAnonymous]
        public ActionResult NuevoMiembroAficionado(int ex=1)
        {
            bool ExigeTarjeta = ex== 1 ? true : false;
            
            ViewBag.ExigeTarjeta = ExigeTarjeta;

            return View(new Miembro() { EsMiembroJugador = false });
        }

        [AllowAnonymous]
        public ActionResult MiembroAficionado()
        {
            return View(new Miembro() { EsMiembroJugador = false });
        }

        [HttpPost]
        [AllowAnonymous]
        public PartialViewResult _CuerpoForm(int Cod_TipoDocumento, string Documento, bool ExigeTarjeta) {
            Miembro miemb = new Miembro();
            try
            {
                ViewBag.ExigeTarjeta = ExigeTarjeta;
                miemb = new BeneficioSM().BuscarMiembro(Cod_TipoDocumento, Documento).FirstOrDefault();
                if (miemb!= null && !String.IsNullOrEmpty(miemb.DocumentoIdentidad))
                {
                    ViewBag.ExigeTarjeta = true;
                }
            }
            catch (Exception e)
            {

            }
            return PartialView(miemb);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public JsonResult GuardarMiembroJugador(Miembro p)
        {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos valídos."
            };

            if (!String.IsNullOrEmpty(p.strFechaNacimiento) && p.Codigo == 0 && p.EsMiembroJugador == false)
            {
                try
                {
                    int Dia = Convert.ToInt32(p.strFechaNacimiento.Split('/')[0]),
                        Mes = Convert.ToInt32(p.strFechaNacimiento.Split('/')[1]),
                        Anio = Convert.ToInt32(p.strFechaNacimiento.Split('/')[2]);
                    p.FechaNacimiento = new DateTime(Anio, Mes, Dia);
                }
                catch (Exception)
                {
                    result.Mensaje = "Disculpe, debe ingresar una dirección de email válida.";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }

            if (String.IsNullOrEmpty(p.Correo) || p.lContacto.Find(f => f.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Email).Nombre != p.Correo)
            {
                result.Mensaje = "Disculpe, debe ingresar una dirección de email válida.";
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (!EsEmailValido(p.Correo))
            {
                result.Mensaje = "Disculpe, debe ingresar una dirección de email válida.";
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            try
            {
                result = new PersonaSM().GuardarMiembro(p);
            }
            catch (Exception e)
            {
                result.MensajeError = e.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public PartialViewResult _Contacto(TipoContacto obj, Contacto contacto = null)
        {
            if (contacto == null)
            {
                contacto = new Contacto() { TipoContacto = obj };
            }
            else
            {
                contacto.TipoContacto = contacto.TipoContacto != null && !String.IsNullOrEmpty(contacto.TipoContacto.Nombre)? contacto.TipoContacto : obj;
                if (contacto.Nombre == null || contacto.Nombre.Equals(contacto.TipoContacto.Nombre) || String.IsNullOrEmpty(contacto.Nombre))
                {
                    //contacto.Nombre = "";
                    if (obj.Codigo == ConfiguracionAPP.TipoContacto_Email || contacto.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Email)
                        contacto.Nombre = "noinformado@noinformado.com";
                    else
                        contacto.Nombre = "No informado";
                }
            }

            return PartialView(contacto);
        }

        [AllowAnonymous]
        public PartialViewResult _Domicilio(TipoDomicilio obj, Domicilio domicilio = null)
        {

            if (domicilio == null)
            {
                domicilio = new Domicilio()
                {
                    tipoDomicilio = obj
                };
            }
            else
            {
                domicilio.tipoDomicilio = obj;
            }

            return PartialView(domicilio);
        }

        [AllowAnonymous]
        public PartialViewResult _SelectGenero()
        {
            try
            {
                return PartialView(new PersonaSM().BuscarGenero());
            }
            catch (Exception e)
            {
                return PartialView(new List<Genero>());
            }
        }

        [AllowAnonymous]
        public PartialViewResult _SelectTipoDocumento()
        {
            try
            {
                return PartialView(new PersonaSM().BuscarTipoDocumento());
            }
            catch (Exception e)
            {
                return PartialView(new List<TipoDocumento>());
            }
        }

        [AllowAnonymous]
        public PartialViewResult _SelectPais(int Cod_Pais = 0)
        {
            try
            {
                ViewBag.Cod_Pais = Cod_Pais;
                return PartialView(new PersonaSM().BuscarPais());
            }
            catch (Exception e)
            {
                return PartialView(new List<Pais>());
            }
        }

        [AllowAnonymous]
        public PartialViewResult _SelectProvincia(int Codigo = 0, int Cod_Provincia = 0)
        {
            try
            {
                ViewBag.Cod_Provincia = Cod_Provincia;
                return PartialView(new PersonaSM().BuscarProvincia(Codigo));
            }
            catch (Exception e)
            {
                return PartialView(new List<Provincia>());
            }
        }

        [AllowAnonymous]
        public PartialViewResult _SelectMunicipio(int Codigo = 0, int Cod_Municipio = 0)
        {
            try
            {
                ViewBag.Cod_Municipio = Cod_Municipio;
                return PartialView(new PersonaSM().BuscarMunicipio(Codigo));
            }
            catch (Exception e)
            {
                return PartialView(new List<Municipio>());
            }
        }

        [AllowAnonymous]
        public PartialViewResult _ListadoBeneficioDestacado() {
            try
            {
                return PartialView(new BeneficioSM().BuscarBeneficioDestacadoActivo(0, "", 0, 0, true));
            }
            catch (Exception e)
            {
                return PartialView(new List<Beneficio>());
            }    
        }

        [AllowAnonymous]
        public PartialViewResult _ListadoBeneficioDestacadoLanding()
        {
            try
            {
                return PartialView(new BeneficioSM().BuscarBeneficioDestacadoActivo(0, "", 0, 0, true));
            }
            catch (Exception e)
            {
                return PartialView(new List<Beneficio>());
            }
        }

        [AllowAnonymous]
        public PartialViewResult _ListadoBeneficioActivo(string Contenido = "", int CodTipoBeneficiario = 1, string lstCodCategoriaBeneficio = "")
        {
            try
            {
                List<Beneficio> lstBeneficio = new BeneficioSM().BuscarBeneficioActivo(0, Contenido, 0, CodTipoBeneficiario, true);
                if (!String.IsNullOrEmpty(lstCodCategoriaBeneficio)) {
                    List<int> lstCod = lstCodCategoriaBeneficio.Split(',').Select(Int32.Parse).ToList();

                    lstBeneficio = lstBeneficio.Where(x => lstCod.Any(a => a == x.CategoriaBeneficio.Codigo)).ToList();

                }

                return PartialView(lstBeneficio);
            }
            catch (Exception e)
            {
                return PartialView(new List<Beneficio>());
            }
        }

        [AllowAnonymous]
        public PartialViewResult _ListaCategoriaBeneficio() {
            try
            {
                return PartialView(new BeneficioSM().BuscarCategoriaBeneficio());
            }
            catch (Exception e)
            {
                return PartialView(new List<CategoriaBeneficio>());
            }
        }

        [AllowAnonymous]
        public ActionResult _ListaTipoBeneficiario()
        {
            try
            {
                return PartialView(new BeneficioSM().BuscarTipoBeneficiario());
            }
            catch (Exception e)
            {
                return PartialView(new List<TipoBeneficiario>());
            }
        }


    }
}