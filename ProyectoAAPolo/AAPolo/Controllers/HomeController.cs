﻿using AAPolo.Aplication;
using AAPolo.Controllers.Helper;
using AAPolo.Entities;
using AAPolo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace AAPolo.Controllers
{
    [AllowAnonymous]
    public class HomeController : ClassHelper
    {
        public ActionResult Index(Persona obj = null)
        {
            try
            {
                ViewBag.MensajeErrorLogin = TempData["MensajeErrorLogin"].ToString();
            }
            catch (Exception) { }

            try
            {

                if (!Convert.ToString(TempData["ClaveVerificacion"]).Equals(""))
                {
                    obj = new PersonaSM().BuscarPersonaPorClaveVerificacion(TempData["ClaveVerificacion"].ToString());
                    ViewBag.CambioPassword = true;
                }
                else
                {
                    ViewBag.CambioPassword = false;
                }
            }
            catch (Exception) { ViewBag.CambioPassword = false; }

            return View(obj);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(Persona obj)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Debe ingresar un usuario y contraseña válida."
            };

            if (obj.DocumentoIdentidad != null && !obj.DocumentoIdentidad.Equals("") && obj.Password != null && !obj.Password.Equals(""))
            {
                resul = InicioSesion(obj, ConfiguracionAPP.Rol_Jugador);
                if (resul.EsSatisfactorio)
                {
                    return RedirectToAction("Index", "PagoHandicap");
                }
            }
            TempData["MensajeErrorLogin"] = resul.Mensaje;
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult LoginConCambioPassword(Persona obj)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Debe ingresar datos válida."
            };

            if (obj.Password != null && !obj.Password.Equals("") && obj.PasswordConfirmar != null && !obj.PasswordConfirmar.Equals(""))
            {
                if (obj.Password == obj.PasswordConfirmar)
                {
                    if (obj.Password.Length > 5)
                    {
                        resul = new PersonaSM().CambiarPasswordJugador(obj);
                        if (resul.EsSatisfactorio)
                        {
                            resul = InicioSesion(obj, ConfiguracionAPP.Rol_Jugador);
                            //if (resul.EsSatisfactorio)
                            //{
                            //    return RedirectToAction("Index", "PagoHandicap");
                            //}
                        }
                    }
                    else
                    {
                        resul.Mensaje = "Disculpe, la contraseña debe tener al menos 6 caracteres.";
                    }
                }
                else
                {
                    resul.Mensaje = "Disculpe, debe las nuevas contraseñas no coinciden.";
                }
            }
            else
            {
                resul.Mensaje = "Disculpe, debe ingresar las nuevas contraseñas.";
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult _AceptaTerminosCondiciones()
        {
            Persona p = DatosUsuario();
            if (p.Codigo > 0 && !p.AceptaTerminosCondiciones)
            {
                p = new PersonaSM().BuscaJugador(p).FirstOrDefault();
            }

            return PartialView(p);
        }

        public JsonResult UsuarioAceptaTerminosCondiciones(string telefono, string email)
        {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos."
            };
            try
            {
                result = new PersonaSM().AceptaTerminosCondiciones(DatosUsuario().Codigo, telefono, email);
                if (result.EsSatisfactorio)
                {
                    ActualizarSession(new PersonaSM().BuscaJugador(new Persona() { Codigo = DatosUsuario().Codigo }).FirstOrDefault());
                }

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.MensajeError = e.Message;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult CambioDePassword(string DocumentoIdentidad)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe debe ingresar un documento válido."
            };
            try
            {
                resul = new PersonaSM().CambioDePassword(DocumentoIdentidad);
            }
            catch (Exception)
            {

                throw;
            }
            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CambioDePasswordIndex(string id = "")
        {

            TempData["ClaveVerificacion"] = id;

            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult Jugadores()
        {
            return View();
        }

        public PartialViewResult ListadoJugador(int Cod_Persona = 0, string Cadena = "", int Cod_TipoJugador = 0, int Cod_Club = 0, int Nro_Handicap = -1, int Nro_HandicapSecundario = -1)
        {
            try
            {
                List<Persona> lPersona = new PersonaSM().BuscaJugadorResumenPerfilPublico(Cod_Persona, Cadena, Cod_TipoJugador, Cod_Club, Nro_Handicap, Nro_HandicapSecundario > -1 ? ConfiguracionAPP.Genero_Femenino : 0).Where(x => x.EsActivo).ToList();
                if (Nro_HandicapSecundario > -1)
                    lPersona = lPersona.Where(x => x.HandicapPersonaSecundario.NroHandicap == Nro_HandicapSecundario).ToList();
                else if (Cod_Persona == 0 && Cadena == "" && Cod_TipoJugador == 0 && Cod_Club == 0 && Nro_Handicap == -1 && Nro_HandicapSecundario == -1)
                    lPersona = lPersona.OrderByDescending(x => x.HandicapPersona.NroHandicap).ToList();

                return PartialView(lPersona);
            }
            catch (Exception e)
            {
                ViewBag.ErrorCatch = e.Message;
                return PartialView(new List<Persona>());
            }
        }

        public PartialViewResult _SelectTipoJugador(string FechaNacimiento = "")
        {
            try
            {
                return PartialView(new PersonaSM().BuscarTipoJugador());
            }
            catch (Exception e)
            {
                return PartialView(new List<TipoJugador>());
            }
        }

        public PartialViewResult _SelectClub(int Cod_Club = 0, string NombreClub = "", int Cod_ClubOmitir = 0)
        {
            try
            {
                List<Club> lClub = new ClubSM().BuscaClub(0, Cod_Club, NombreClub, 0);
                if (Cod_ClubOmitir > 0)
                    lClub = lClub.Where(x => x.Codigo != Cod_ClubOmitir).ToList();
                return PartialView(lClub);
            }
            catch (Exception e)
            {
                return PartialView(new List<JugadorHandicap>());
            }
        }

        public ActionResult Referees()
        {
            return View();
        }

        public PartialViewResult _ListadoReferees(string Nombre = "", int Cod_TipoReferee = 0)
        {
            ViewBag.DatosUsuario = DatosUsuario();
            try
            {
                return PartialView(new RefereeSM().RankingReferee(Nombre, Cod_TipoReferee, 0));
            }
            catch (Exception)
            {
                return PartialView(new List<Referee>());
            }
        }

        public PartialViewResult _BuscarTipoReferee()
        {
            try
            {
                return PartialView(new RefereeSM().BuscaTipoReferee());
            }
            catch (Exception)
            {
                return PartialView(new List<TipoReferee>());
            }
        }

        [AllowAnonymous]
        public ActionResult Noticia()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Asociacion()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult AsociacionHistoria()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult AsociacionSede()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult AsociacionConsejo()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult AsociacionSubComision()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult AsociacionStaff()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult AsociacionStatutos()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult AsociacionRse()
        {
            return View();
        }

        public PartialViewResult _ListadoNoticia(int Cod_Noticia = 0, int Cod_TipoNoticia = 0, int Cod_Etiqueta = 0, string Cadena = "")
        {
            try
            {
                List<Noticia> lNoticia = new NoticiaSM().BuscarNoticia(Cod_Noticia, Cod_TipoNoticia, Cadena, true, true).Where(x => x.EsActivo).ToList();

                if (Cod_Etiqueta > 0)
                    lNoticia = lNoticia.Where(x => (x.lTags.Any(a => a.Codigo == Cod_Etiqueta))).ToList();

                return PartialView(lNoticia);
            }
            catch (Exception e)
            {
                ViewBag.MensajeError = e.Message;
                return PartialView(new List<Noticia>());
            }
        }

        public PartialViewResult _ListadoNoticiaRecientes(int Cod_Noticia = 0, int Cod_TipoNoticia = 0, int Cod_Etiqueta = 0, string Cadena = "")
        {
            try
            {
                List<Noticia> lNoticia = new NoticiaSM().BuscarNoticiaDestacadaHome();
                /*List<Noticia> lNoticia = new NoticiaSM().BuscarNoticia(Cod_Noticia, Cod_TipoNoticia, Cadena, true, true)
                    .Where(x => x.EsActivo && x.EsDestacado).Take(3).ToList();*/

                return PartialView(lNoticia);
            }
            catch (Exception e)
            {
                ViewBag.MensajeError = e.Message;
                return PartialView(new List<Noticia>());
            }
        }

        [AllowAnonymous]
        public ActionResult DetalleNoticia(int Cod_Noticia = 0, int Cod_TipoNoticia = 0)
        {
            try
            {
                List<Noticia> lNoticia = new NoticiaSM().BuscarNoticia(0, Cod_TipoNoticia, "", true, true).Where(x => x.EsActivo).ToList();
                ViewBag.lNoticia = lNoticia;
                Noticia noticia = lNoticia.Find(f => f.Codigo == Cod_Noticia);

                if (noticia == null || noticia.Codigo == 0)
                    return RedirectToAction("Noticia");

                return View(noticia);
            }
            catch (Exception e)
            {
                ViewBag.MensajeError = e.Message;
                return RedirectToAction("Noticia");

            }
        }

        [AllowAnonymous]
        public PartialViewResult _SelectTipoNoticia()
        {
            try
            {
                return PartialView(new NoticiaSM().BuscarTipoNoticia());
            }
            catch (Exception)
            {

                return PartialView(new List<TipoNoticia>());
            }
        }

        [AllowAnonymous]
        public PartialViewResult _SelectEtiqueta()
        {
            try
            {
                return PartialView(new NoticiaSM().BuscarEtiqueta());
            }
            catch (Exception)
            {

                return PartialView(new List<Etiqueta>());
            }
        }

        public PartialViewResult _SelectMunicipio(int Codigo = 0)
        {
            try
            {
                return PartialView(new PersonaSM().BuscarMunicipio(Codigo));
            }
            catch (Exception e)
            {
                return PartialView(new List<Municipio>());
            }
        }

        [AllowAnonymous]
        public ActionResult Torneo()
        {
            return View();
        }

        public PartialViewResult _ListadoTorneo(string Cadena = "", int Cod_TipoTorneo = 0, int Cod_CategoriaHandicap = 0)
        {
            try
            {
                return PartialView(new TorneoSM().BuscarTorneo(0, Cod_TipoTorneo, Cod_CategoriaHandicap, true).Where(x => x.NombreTorneo.ToLower().Contains(Cadena.ToLower())).OrderBy(o => o.FechaInicio).ToList());
            }
            catch (Exception)
            {
                return PartialView(new List<Torneo>());
            }
        }

        [OutputCache(Duration = 10)]
        public PartialViewResult _ListadoTorneoHistorico(string Cadena = "", int Cod_TipoTorneo = 0, int Cod_CategoriaHandicap = 0)
        {
            try
            {
                return PartialView(new TorneoSM().BuscarTorneoHistorico(0, Cadena, Cod_TipoTorneo, Cod_CategoriaHandicap, false));
            }
            catch (Exception)
            {
                return PartialView(new List<Torneo>());
            }
        }

        public PartialViewResult _SelectTipoTorneo()
        {
            try
            {
                return PartialView(new TorneoSM().BuscarTipoTorneo());
            }
            catch (Exception)
            {
                return PartialView(new List<TipoTorneo>());
            }
        }

        public PartialViewResult _SelectCategoriaHandicapTorneo()
        {
            try
            {
                return PartialView(new TorneoSM().BuscarCategoriaHandicapTorneo());
            }
            catch (Exception)
            {
                return PartialView(new List<CategoriaHandicap>());
            }
        }

        public PartialViewResult _ListadoTorneoInscripcionAbierta(string Cadena = "", int Cod_TipoTorneo = 0, int Cod_CategoriaHandicap = 0)
        {
            try
            {
                return PartialView(new TorneoSM().BuscarTorneo(0, Cod_TipoTorneo, Cod_CategoriaHandicap, true)
                    .Where(x => x.InscripcionActiva).OrderByDescending(o => o.FechaInicio).ToList());
            }
            catch (Exception)
            {
                return PartialView(new List<Torneo>());
            }
        }

        [AllowAnonymous]
        public ActionResult DetalleTorneo(int Codigo = 0)
        {
            try
            {
                if (Codigo < 1)
                    return RedirectToAction("Torneo", "Home");

                Torneo t = new TorneoSM().BuscarTorneo(Codigo, 0, 0, false).FirstOrDefault();

                if (t == null || t.Codigo == 0)
                    return RedirectToAction("Torneo", "Home");

                return View(t);

            }
            catch (Exception)
            {
                return RedirectToAction("Torneo", "Home");
            }
        }

        [OutputCache(Duration = 30, VaryByParam = "Codigo")]
        public PartialViewResult _DetalleFixture(int Codigo)
        {
            ViewModelFixture vmFixture = Task.Run(async () =>
            {
                var lZona = new TorneoSM().BuscarZonaTorneoDisponibleAsync(Codigo, true, 0);
                var lCopa = new TorneoSM().BuscarCopaTorneoDisponibleAsync(Codigo, 0, true);

                await Task.WhenAll(lZona, lCopa);

                return new ViewModelFixture(lZona.Result, lCopa.Result);
            }).Result;

            return PartialView(vmFixture);

        }

        public PartialViewResult _ListadoTorneoProximo(string Cadena = "", int Cod_TipoTorneo = 0, int Cod_CategoriaHandicap = 0)
        {
            try
            {
                return PartialView(new TorneoSM().BuscarTorneo(0, Cod_TipoTorneo, Cod_CategoriaHandicap, true).Where(x => x.NombreTorneo.ToLower().Contains(Cadena.ToLower())).OrderBy(o => o.FechaInicio).Take(15));
            }
            catch (Exception)
            {
                return PartialView(new List<Torneo>());
            }
        }

        public ActionResult Club()
        {
            return View();
        }

        public PartialViewResult _ListadoClub(int Cod_CategoriaClub = 0, int Cod_Club = 0, string Nombre = "", int Cod_Provincia = 0)
        {
            //ViewBag.lPais = new PersonaSM().BuscarPais();
            List<Club> lClub = new ClubSM().BuscarClubActivo(Cod_CategoriaClub, Cod_Club, Nombre, 0, Cod_Provincia);
            return PartialView(lClub);
        }

        public PartialViewResult _SelectTipoCategoriaClub()
        {
            try
            {
                return PartialView(new ClubSM().BuscarCategoriaClub());
            }
            catch (Exception)
            {
                return PartialView(new List<CategoriaClub>());
            }
        }

        public PartialViewResult _SelectProvincia()
        {
            try
            {
                return PartialView(new PersonaSM().BuscarProvincia(0));
            }
            catch (Exception)
            {
                return PartialView(new List<Provincia>());
            }
        }

        public PartialViewResult _ListadoTribunalDisiplina()
        {
            try
            {
                return PartialView(new SancionSM().BuscarSancionActiva());
            }
            catch (Exception)
            {
                return PartialView(new List<Sancion>());
            }
        }

        [AllowAnonymous]
        public PartialViewResult _ModalPDF(string Titulo, string Link)
        {
            ViewBag.Titulo = Titulo;
            ViewBag.Link = Link;
            return PartialView();
        }

        [AllowAnonymous]
        public ActionResult RemotoUsuario(string Documento = "", string Password = "")
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Debe ingresar un usuario y contraseña válida."
            };
            if (DatosUsuario().DocumentoIdentidad != Documento && Documento != "" && Password != "")
            {
                resul = InicioSesionPasswordEncriptado(new Persona() { DocumentoIdentidad = Documento, Password = Password }, ConfiguracionAPP.Rol_Jugador);
                if (resul.EsSatisfactorio)
                {
                    return RedirectToAction("Index", "PagoHandicap");
                }
            }
            else if (DatosUsuario().DocumentoIdentidad == Documento && Documento != "" && DatosUsuario().Codigo > 0 && DatosUsuario().Rol.Codigo == ConfiguracionAPP.Rol_Administrador)
            {
                return RedirectToAction("Index", "PagoHandicap");
            }
            TempData["MensajeErrorLogin"] = resul.Mensaje;
            return RedirectToAction("Index", "Home");

        }

        public PartialViewResult _ListadoTemporadaClub()
        {
            try
            {
                return PartialView(new TemporadaSM().BuscarTemporadaPuntajeXClubPublico(0, 0, 0, 0));
            }
            catch (Exception)
            {
                return PartialView(new List<PuntoXClub>());
            }
        }

        public ActionResult CerrarSession()
        {
            Response.RemoveOutputCacheItem(Url.Action("_RankingReferee", "PerfilReferee"));
            Response.RemoveOutputCacheItem(Url.Action("_DatosUsuarioReferee", "PerfilJugador"));
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult UrlAbiertoDePalermo(string Cadena = "")
        {
            try
            {
                return Redirect(obtenerTagDelWebConfig("UrlAbiertoDePalermo") + "/" + Cadena);
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        #region Registro de usuario
        public PartialViewResult _SelectGenero()
        {
            try
            {
                return PartialView(new PersonaSM().BuscarGenero());
            }
            catch (Exception e)
            {
                return PartialView(new List<Genero>());
            }
        }

        public PartialViewResult _SelectTipoDocumento()
        {
            try
            {
                return PartialView(new PersonaSM().BuscarTipoDocumento());
            }
            catch (Exception e)
            {
                return PartialView(new List<TipoDocumento>());
            }
        }

        public PartialViewResult _SelectPais(int Cod_Pais = 0)
        {
            try
            {
                ViewBag.Cod_Pais = Cod_Pais;
                return PartialView(new PersonaSM().BuscarPais());
            }
            catch (Exception e)
            {
                return PartialView(new List<Pais>());
            }
        }

        public PartialViewResult _Contacto(TipoContacto obj, Contacto contacto = null, int CodContactoUsuario = 0)
        {
            if (contacto == null)
            {
                contacto = new Contacto() { TipoContacto = obj };
            }
            else
            {
                contacto.TipoContacto = obj;
                if ((contacto.Nombre.Equals(contacto.TipoContacto.Nombre) || String.IsNullOrEmpty(contacto.Nombre)) && CodContactoUsuario > 0)
                {
                    //contacto.Nombre = "";
                    if (obj.Codigo == ConfiguracionAPP.TipoContacto_Email)
                        contacto.Nombre = "noinformado@noinformado.com";
                    else
                        contacto.Nombre = "No informado";
                }
                else if (contacto.Nombre.Equals(contacto.TipoContacto.Nombre) && CodContactoUsuario == 0)
                {
                    contacto.Nombre = "";

                }
            }

            return PartialView(contacto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegistroPersona(Persona obj)
        {

            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos."
            };

            int Dia = 0,
                Mes = 0,
                Anio = 0;

            if (String.IsNullOrEmpty(obj.Nombre))
            {
                result.Mensaje = "Disculpe, debe ingresar un nombre.";
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            if (String.IsNullOrEmpty(obj.Apellido))
            {
                result.Mensaje = "Disculpe, debe ingresar un apellido.";
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            if (obj.TipoDocumento.Codigo == 0)
            {
                result.Mensaje = "Disculpe, debe seleccionar un tipo de documento.";
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            if (String.IsNullOrEmpty(obj.DocumentoIdentidad))
            {
                result.Mensaje = "Disculpe, debe ingresar su documento de identidad.";
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            if (String.IsNullOrEmpty(obj.strFechaNacimiento))
            {
                result.Mensaje = "Disculpe, debe ingresar fecha de nacimiento.";
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                try
                {
                    Dia = Convert.ToInt32(obj.strFechaNacimiento.Split('-')[0]);
                    Mes = Convert.ToInt32(obj.strFechaNacimiento.Split('-')[1]);
                    Anio = Convert.ToInt32(obj.strFechaNacimiento.Split('-')[2]);
                    obj.FechaNacimiento = new DateTime(Anio, Mes, Dia);
                }
                catch (Exception)
                {
                    result.Mensaje = "Disculpe, debe ingresar fecha de nacimiento válida.";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

            }

            if (obj.Genero.Codigo == 0)
            {
                result.Mensaje = "Disculpe, debe selecionar un genero.";
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            if (String.IsNullOrEmpty(obj.lContacto.Find(x => x.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Email).Nombre))
            {
                result.Mensaje = "Disculpe, debe ingresar una dirección de email válida.";
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else if (!EsEmailValido(obj.lContacto.Find(f => f.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Email).Nombre))
            {
                result.Mensaje = "Disculpe, debe ingresar una dirección de email válida.";
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (String.IsNullOrEmpty(obj.lContacto.Find(x => x.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Celular).Nombre))
            {
                result.Mensaje = "Disculpe, debe ingresar un celular/teléfono válido.";
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            try
            {
                result = new PersonaSM().GuardarMember(obj);
                if (result.EsSatisfactorio)
                {
                    obj.Password = obj.DocumentoIdentidad;
                    result = InicioSesion(obj, ConfiguracionAPP.Rol_Jugador);
                    if (result.EsSatisfactorio)
                    {
                        return RedirectToAction("Index", "PerfilMember");
                    }
                }
                else
                {
                    TempData["MensajeErrorLogin"] = result.Mensaje;
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception e)
            {
                result.Mensaje = "Disculpe, no es posible realizar la operación.";
                result.MensajeError = e.Message;
            }

            return RedirectToAction("Index", "Home");
        }

        #endregion
    }
}