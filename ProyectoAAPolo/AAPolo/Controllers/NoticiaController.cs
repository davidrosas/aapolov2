﻿using AAPolo.Aplication;
using AAPolo.Controllers.Helper;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Controllers
{
    [Authorize]
    public class NoticiaController : ClassHelper
    {
        // GET: Noticia
        public ActionResult Index()
        {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            return View();
        }

        public PartialViewResult _Listado(int Cod_TipoNoticia = 0, string Titulo = "")
        {
            try
            {
                return PartialView(new NoticiaSM().BuscarNoticia(0, Cod_TipoNoticia, Titulo, false, false));
            }
            catch (Exception e)
            {
                ViewBag.MensajeError = e.Message;
                return PartialView(new List<Noticia>());
            }
        }

        public ActionResult CrearNoticia()
        {

            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            return View();
        }

        public ActionResult EditarNoticia(int Codigo = 0)
        {

            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            if (Codigo == 0)
                return RedirectToAction("Index", "Home");

            Noticia notic = new Noticia();
            try
            {
                notic = new NoticiaSM().BuscarNoticia(Codigo, 0, "", true, true).FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }

            return View(notic);
        }
        public JsonResult GuardarNoticia(Noticia obj, List<int> lTags)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };

            if (lTags == null)
                return Json(resul, JsonRequestBehavior.AllowGet);

            if (obj.lAdjunto == null)
                return Json(resul, JsonRequestBehavior.AllowGet);

            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
            {
                resul.Mensaje = "Disculpe, no está autorizado para crear noticias";
                return Json(resul, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (lTags != null && lTags.Count() > 0)
                {
                    obj.lTags = (from value in lTags
                                 where value > 0
                                 select new Tag() { Codigo = value }).ToList();
                }

                for (int i = 0; i < obj.lAdjunto.Count(); i++)
                {
                    if (obj.lAdjunto[i].Archivo != null)
                    {
                        obj.lAdjunto[i].UrlAdjunto = GuardarImagen(obj.lAdjunto[i].Archivo);
                        obj.lAdjunto[i].Descripcion = obj.lAdjunto[i].Archivo.FileName;
                    }
                }

                int Dia = Convert.ToInt32(obj.strFechaPublicacion.Split('/')[0]),
                    Mes = Convert.ToInt32(obj.strFechaPublicacion.Split('/')[1]),
                    Anio = Convert.ToInt32(obj.strFechaPublicacion.Split('/')[2]);
                obj.FechaPublicacion = new DateTime(Anio, Mes, Dia);

                obj.PersonaAdmin = DatosUsuario();
                resul = new NoticiaSM().GuardarNoticia(obj);

            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult _Adjunto(Adjunto obj, int Cod_TipoAdjunto = 0)
        {
            if (Cod_TipoAdjunto > 0)
            {
                obj.tipoAdjunto = new TipoAdjunto()
                {
                    Codigo = Cod_TipoAdjunto
                };
            }
            return PartialView(obj);
        }

        public PartialViewResult _SelectTipoNoticia()
        {
            try
            {
                return PartialView(new NoticiaSM().BuscarTipoNoticia());
            }
            catch (Exception)
            {

                return PartialView(new List<TipoNoticia>());
            }
        }

        public PartialViewResult _SelectEtiqueta()
        {
            try
            {
                return PartialView(new NoticiaSM().BuscarEtiqueta());
            }
            catch (Exception)
            {

                return PartialView(new List<Etiqueta>());
            }
        }

        public JsonResult CambiarEstadoNoticia(int Codigo, int EstadoNoticia)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };

            try
            {
                resul = new NoticiaSM().CambiarEstadoNoticia(Codigo, EstadoNoticia);
            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CambiarEstadoNoticiaDestacado(int Codigo, int EstadoNoticia)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };

            try
            {
                resul = new NoticiaSM().CambiarEstadoNoticiaDestacado(Codigo, EstadoNoticia);
            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

    }
}