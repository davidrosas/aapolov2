﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Controllers
{
    public class LenguajeController : Controller
    {
        // GET: Lenguaje
        public ActionResult Index(string leng = "")
        {
            if (leng != "")
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(leng);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(leng);
            }

            HttpCookie cookie = new HttpCookie("Lenguage");
            cookie.Value = leng;
            Response.Cookies.Add(cookie);
            string urlRecarga = Request.UrlReferrer.ToString();
            return Redirect(urlRecarga);// RedirectToAction("Index", "Home");
        }
    }
}