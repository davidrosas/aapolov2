﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Controllers
{
    public class AbiertoController : Controller
    {
        // GET: Abierto
        public ActionResult Index()
        {
            return RedirectToAction("Palermo");
        }

        public ActionResult Palermo(int id = 0)
        {
            if (id == 0)
                return View();

            return View("Palermo" + id);
        }

        public ActionResult Hurlingham(int id = 0)
        {
            if (id == 0)
                return View();

            return View("Hurlingham" + id);
        }

        public ActionResult Tortugas(int id = 0)
        {
            if (id == 0)
                return View();

            return View("Tortugas" + id);
        }

    }
}