﻿using AAPolo.Aplication;
using AAPolo.Controllers.Helper;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Controllers
{
    [Authorize]
    public class TemporadaController : ClassHelper
    {
        // GET: PuntajeCLub
        public ActionResult Index()
        {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            return View();
        }

        public PartialViewResult _Listado() {
            try
            {
                return PartialView(new TemporadaSM().BuscarTemporadaPuntajeXClub(0,0,0,0));
            }
            catch (Exception)
            {
                return PartialView(new List<PuntoXClub>());
            }
        }

        public ActionResult CrearRegistroPuntaje() {

            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            return View();
        }

        public ActionResult EditarRegistroPuntaje(int Codigo = 0)
        {
            if (Codigo == 0)
                return RedirectToAction("Index");

            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            try
            {
                return View(new TemporadaSM().BuscarTemporadaPuntajeXClub(Codigo, 0, 0, 0).FirstOrDefault());
            }
            catch (Exception e)
            {
                ViewBag.MensajeError = e.Message;
                return RedirectToAction("Index");
            }
        }

        public PartialViewResult _SelectCategiriaAccion(int Codigo = 0) {
            try
            {
                ViewBag.Codigo = Codigo;
                return PartialView(new TemporadaSM().BuscarCategoriaAccion());
            }
            catch (Exception)
            {
                return PartialView(new List<CategoriaAccion>());
            }
        }

        public PartialViewResult _SelectTemporada(int Codigo = 0)
        {
            try
            {
                ViewBag.Codigo = Codigo;
                return PartialView(new TemporadaSM().BuscarTemporada());
            }
            catch (Exception)
            {
                return PartialView(new List<Temporada>());
            }
        }

        public PartialViewResult _BuscarClub(string Nombre) {
            try
            {
                return PartialView(new ClubSM().BuscaClub(0, 0, Nombre, 0));
            }
            catch (Exception)
            {
                return PartialView(new List<Club>());
            }
        }

        public JsonResult GuardarTemporada(PuntoXClub obj) {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos."
            };

            try
            {
                if (obj.Codigo < 1)
                {
                    result.Mensaje = "Disculpe, debe seleccionar un club.";
                }
                else if (obj.Puntos < 1)
                {
                    result.Mensaje = "Disculpe, debe ingresar la cantidad de puntos.";
                }
                else if (obj.CategoriaAccion.Codigo < 1)
                {
                    result.Mensaje = "Disculpe, debe seleccionar una categoría de acción.";
                }
                else if (obj.Temporada.Codigo < 1)
                {
                    result.Mensaje = "Disculpe, debe seleccionar temporada.";
                }
                else if (String.IsNullOrEmpty(obj.Accion))
                {
                    result.Mensaje = "Disculpe, debe ingresar la acción realizada.";
                }
                else if (String.IsNullOrEmpty(obj.Premio))
                {
                    result.Mensaje = "Disculpe, debe ingresar premio camnjeado.";
                }
                else if (obj.ImagenEvidencia == null && obj.Cod_Canje == 0)
                {
                    result.Mensaje = "Disculpe, debe ingresar una imagen de portada.";
                }
                else {

                    if (obj.ImagenEvidencia != null)
                        obj.UrlImagenEvidencia = GuardarImagen(obj.ImagenEvidencia);
                    else
                        obj.UrlImagenEvidencia = "";

                    result = new TemporadaSM().GuardarTemporada(obj, DatosUsuario().Codigo);
                }
            }
            catch (Exception e)
            {
                result.MensajeError = e.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CambiarEstadoTemporada(int Codigo, int EstadoTemporada)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };

            try
            {
                resul = new TemporadaSM().CambiarEstadoTemporada(Codigo, EstadoTemporada);
            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

    }
}