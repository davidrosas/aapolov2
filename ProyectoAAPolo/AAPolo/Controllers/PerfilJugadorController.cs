﻿using AAPolo.Aplication;
using AAPolo.Controllers.Helper;
using AAPolo.Entities;
using AAPolo.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Controllers
{

    [Authorize]
    public class PerfilJugadorController : ClassHelper
    {
        // GET: PerfilJugador
        public ActionResult Index()
        {
            Persona p = DatosUsuario();
            if (p.Codigo == 0 || p.Rol.Codigo != ConfiguracionAPP.Rol_Jugador)
                return RedirectToAction("Index", "Home");

            p = new PersonaSM().BuscaJugador(p).FirstOrDefault();
            Persona perAux = new PersonaSM().BuscaJugadorPagoHandicap(p.Codigo, "", 0, 0, 0).FirstOrDefault();
            p.HandicapPago = perAux.HandicapPago;
            
            if (p.HandicapPersona != null)
            {
                p.HandicapPersona.strFechaHasta = perAux.HandicapPersona.strFechaHasta;
                p.HandicapPersona.DescripcionHandicapUsuario = perAux.HandicapPersona.DescripcionHandicapUsuario;
            }
            else
            {
                p.HandicapPersona = new HandicapPersona()
                {
                    strFechaHasta = perAux.HandicapPersona.strFechaHasta,
                    DescripcionHandicapUsuario = perAux.HandicapPersona.DescripcionHandicapUsuario
                };
            }

            if (p.HandicapPago && p.lClubUsuarioSecundario.Where(x => x.EsPrincipal && x.Codigo == ConfiguracionAPP.Clud_NoInformado).Count() > 0) {
                ViewBag.lstClub = new ClubSM().BuscaClub(0,0,"",0).Where(x => x.EsActivo).ToList();
            }

            return View(p);
        }

        public JsonResult GuardarDatosUsuario(string Correo, string Celular, string DireccionDomicilio, int NroDireccion, int NroPiso, string Departamento, string CodigoPostal, string Facebook, string Twitter, string Instagram)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, los datos ingresados son inválidos."
            };

            try
            {
                Persona p = new Persona()
                {
                    Codigo = DatosUsuario().Codigo,
                    Facebook = Facebook,
                    Twitter = Twitter,
                    Instagram = Instagram,
                    lDomicilio = new List<Domicilio>(){
                            new Domicilio() {
                            DireccionDomicilio = DireccionDomicilio,
                            NroDomicilio = NroDireccion,
                            NroPiso = NroPiso,
                            Departamento = Departamento,
                            CodigoPostal = CodigoPostal
                        }
                    },
                };
                p.lContacto = new List<Contacto>();
                p.lContacto.Add(new Contacto()
                {
                    Nombre = Correo,
                    TipoContacto = new TipoContacto()
                    {
                        Codigo = ConfiguracionAPP.TipoContacto_Email
                    }
                });
                p.lContacto.Add(new Contacto()
                {
                    Nombre = Celular,
                    TipoContacto = new TipoContacto()
                    {
                        Codigo = ConfiguracionAPP.TipoContacto_Celular
                    }
                });

                resul = new PersonaSM().GuardarDatosUsuario(p);
            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GuardarDatosClub(int Codigo, int Cod_Domicilio, int Cod_CategoriaClub, string strFundacion, int NumeroCancha, string Correo, string Celular, string Telefono, string DireccionDomicilio, int NroDireccion, int NroPiso, string Departamento, string CodigoPostal, int Cod_Pais, int Cod_Provincia = 0, int Cod_Municipio = 0, string Facebook = "", string Twitter = "", string Instagram = "", string Historia = "")
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, los datos ingresados son inválidos."
            };

            try
            {
                int Dia = Convert.ToInt32(strFundacion.Split('/')[0]),
                    Mes = Convert.ToInt32(strFundacion.Split('/')[1]),
                    Anio = Convert.ToInt32(strFundacion.Split('/')[2]);
                DateTime fecha = DateTime.Now;
                try
                {
                    fecha = new DateTime(Anio, Mes, Dia);
                }
                catch (Exception)
                {
                    resul.Mensaje = "Disculpe, debe ingresar una fecha válida.";
                    return Json(resul, JsonRequestBehavior.AllowGet);
                }

                Club c = new Club()
                {
                    Codigo = Codigo,
                    NumeroCancha = NumeroCancha,
                    FechaFundacion = fecha,
                    Categoria = new CategoriaClub() { Codigo = Cod_CategoriaClub },
                    Facebook = Facebook,
                    Twitter = Twitter,
                    Instagram = Instagram,
                    Historia = Historia,
                    lDomicilio = new List<Domicilio>(){
                            new Domicilio() {
                            DireccionDomicilio = DireccionDomicilio,
                            NroDomicilio = NroDireccion,
                            NroPiso = NroPiso,
                            Departamento = Departamento,
                            CodigoPostal = CodigoPostal,
                            pais = new Pais(){ Codigo = Cod_Pais},
                            provincia = new Provincia(){ Codigo = Cod_Provincia},
                            municipio = new Municipio(){ Codigo = Cod_Municipio},
                        }
                    },
                };
                c.lContacto = new List<Contacto>();
                c.lContacto.Add(new Contacto()
                {
                    Nombre = Correo,
                    TipoContacto = new TipoContacto()
                    {
                        Codigo = ConfiguracionAPP.TipoContacto_Email
                    }
                });
                c.lContacto.Add(new Contacto()
                {
                    Nombre = Celular,
                    TipoContacto = new TipoContacto()
                    {
                        Codigo = ConfiguracionAPP.TipoContacto_Celular
                    }
                });
                c.lContacto.Add(new Contacto()
                {
                    Nombre = Telefono,
                    TipoContacto = new TipoContacto()
                    {
                        Codigo = ConfiguracionAPP.TipoContacto_Telefono
                    }
                });

                resul = new ClubSM().GuardarDatosClub(c);
            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public PartialViewResult _DatosUsuario()
        {
            try
            {
                return PartialView(DatosUsuario());

            }
            catch (Exception)
            {
                return PartialView(new Persona());
            }
        }


        //[OutputCache(Duration = 240)] setear en cerrar sesion
        public PartialViewResult _DatosUsuarioVeedor()
        {
            try
            {
                return PartialView(DatosUsuario());
            }
            catch (Exception)
            {
                return PartialView(new Persona());
            }
        }

        //[OutputCache(Duration = 600/*, Location = System.Web.UI.OutputCacheLocation.Any*/)] //setear en cerrar sesion
        public PartialViewResult _DatosUsuarioReferee()
        {
            try
            {
                if (DatosUsuario().EsReferee)
                {
                    return PartialView(new RefereeSM().BuscaReferee("", 0, DatosUsuario().Codigo, 0).FirstOrDefault());
                }
                else {
                    return PartialView(new Referee() { EsActivo = false, Persona = DatosUsuario() });
                }
            }
            catch (Exception)
            {
                return PartialView(new Referee() { EsActivo = false, Persona = DatosUsuario()});
            }
        }

        public PartialViewResult _ListadoClub()
        {
            try
            {
                List<Club> lClub = new ClubSM().BuscaClub(0, 0, "", DatosUsuario().Codigo).Where(x => x.EsActivo).ToList();
                return PartialView(lClub);
            }
            catch (Exception)
            {
                return PartialView(new List<Club>());
            }
        }

        public ActionResult Club(int Codigo = 0)
        {

            if (Codigo == 0)
                return RedirectToAction("Index");

            Persona p = DatosUsuario();
            if (p.Rol.Codigo != ConfiguracionAPP.Rol_Jugador)
                return RedirectToAction("Index", "Home");

            try
            {
                Club club = new ClubSM().BuscaClub(0, Codigo, "", DatosUsuario().Codigo).Where(x => x.EsActivo).FirstOrDefault();

                if (club == null || club.Codigo == 0 || club.PersonaAdmin.Codigo != DatosUsuario().Codigo)
                    return RedirectToAction("Index");

                if (club.lDomicilio.FirstOrDefault().pais.Codigo == ConfiguracionAPP.Pais_Argentina)
                {
                    ViewBag.lProvincia = new PersonaSM().BuscarProvincia(club.lDomicilio.FirstOrDefault().pais.Codigo);
                    ViewBag.lMunicipio = new PersonaSM().BuscarMunicipio(club.lDomicilio.FirstOrDefault().provincia.Codigo);
                }

                return View(club);
            }
            catch (Exception)
            {
                return RedirectToAction("Index");
            }
        }

        public JsonResult PagoAfiliacionClub(int Cod_Club)
        {

            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, los datos ingresados son inválidos."
            };
            Club obj;
            string idReference = "AAPolo" + "-user-" + DatosUsuario().Codigo + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss.fff");
            try
            {
                if (Cod_Club > 0)
                {
                    result = new PagoSM().PagoAfiliacionClub(Cod_Club, DatosUsuario().Codigo, idReference);
                    if (result.EsSatisfactorio)
                    {
                        obj = new ClubSM().BuscaClub(0, Cod_Club, "", 0).FirstOrDefault();
                        obj.Categoria.strImporte = result.Cadena;
                        result.Cadena = obtenerTagDelWebConfig("UrlPasarelaDePago") + HttpUtility.UrlEncode(obtenerDatosPasarelaMercadoPago(obj, idReference));
                    }
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.MensajeError = e.Message;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult _UsuarioXClub(int Codigo)
        {
            try
            {
                return PartialView(new PersonaSM().BuscaJugadorResumen(0, "", 0, Codigo, -1, 0));
            }
            catch (Exception)
            {
                return PartialView(new List<Persona>());
            }
        }

        public PartialViewResult _ImgPerfilUsuario() {
            return PartialView(DatosUsuario());
        }

        public ActionResult GuardarImagenPerfil(HttpPostedFileBase imagen)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion() {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar una imagen válida."
            };
            string urlImg = "";
            Persona p = DatosUsuario();
            if (imagen != null && p.Codigo>0)
            {
                urlImg = GuardarImagen(imagen);
                resul = new PersonaSM().GurdarImgPerfil(urlImg, p.Codigo);
                if (resul.EsSatisfactorio) {
                    p.UrlFoto = urlImg;
                    ActualizarSession(p);
                }

            }
            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GuardarImagenCertificado(HttpPostedFileBase imagen)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar una imagen válida."
            };
            string urlImg = "";
            Persona p = DatosUsuario();
            if (imagen != null && p.Codigo > 0)
            {
                urlImg = GuardarImagen(imagen);
                resul = new PersonaSM().GurdarImgCertificado(urlImg, p.Codigo);
               
            }
            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GuardarImagenClub(HttpPostedFileBase imagen, int Cod_Club)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar una imagen válida."
            };
            string urlImg = "";
            if (imagen != null && Cod_Club > 0)
            {
                urlImg = GuardarImagen(imagen);
                resul = new ClubSM().GuardarImagenClub(urlImg, Cod_Club);

            }
            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult _ModalRechazarJugador(Persona obj, string NombreClub, int Cod_Club) {
            obj.clubUsuarioPrincipal = new ClubUsuario()
            {
                Codigo = Cod_Club,
                NombreClub = NombreClub
            };
            return PartialView(obj);
        }

        public JsonResult RechazarJugadorClubPrincipal(int Codigo, int CodigoClub) {
            ResultadoTransaccion result = new ResultadoTransaccion() {
                EsSatisfactorio = false,
                Mensaje = "Por favor, ingrese datos válidos."
            };

            try
            {
                result = new ClubSM().RechazarJugadorClubPrincipal(Codigo);
                if (result.EsSatisfactorio) {
                    result.Vista = RenderRazorViewToString(this.ControllerContext, "_UsuarioXClub", new PersonaSM().BuscaJugadorResumen(0, "", 0, CodigoClub, -1, 0));
                }
            }
            catch (Exception e){
                result.MensajeError = e.Message;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActualizarClubPrincipalUsuario(int Cod_Club = 0) {
            ResultadoTransaccion result = new ResultadoTransaccion() {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe seleccionar un club."
            };

            Persona p = DatosUsuario();

            if (Cod_Club > 0 && p.Codigo > 0) {
                result = new PersonaSM().ActualizarClubPrincipalUsuario(p.Codigo, Cod_Club);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult VeedorSidepro() {
            if (DatosUsuario().Codigo == 0 || !DatosUsuario().EsVeedor) 
                return RedirectToAction("Index");

            Persona p = new PersonaSM().BuscarUsuarioSidepro(DatosUsuario());

            if (p.EsActivo && p.AceptaTerminosCondiciones && p.EsVeedor) {
                string urlRedireccion = obtenerTagDelWebConfig("UrlSideproInisioSesionVeedor") + "/Home/LoginRemotoVeedor?Usuario="+ p.DocumentoIdentidad + "&Password=" + p.Password ;
                return Redirect(urlRedireccion);
            }
            return RedirectToAction("Index");
        }

        public PartialViewResult _MenuUsuario() {
            
            return PartialView(DatosUsuario());
        }

        /**************************************************************************************************************************************/
        #region metodos mercado pago
        public ActionResult DetalleDePago(string external_reference, string collection_id = "")
        {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, ocurrio un error al guardar los resultados de su operación, por favor contacte con los administradores."
            };
            try
            {
                result = new PagoSM().ActualizarTransacionPagoHandicap(external_reference, 1, collection_id);
                TempData["Mensaje"] = result.Mensaje;
                TempData["EsSatisfactorio"] = result.EsSatisfactorio;
                TempData["MostrarMsj"] = true;
            }
            catch (Exception)
            {

            }
            return RedirectToAction("Index", "HistorialPagos");
        }

        public ActionResult DetalleDePagoPending(string external_reference, string collection_id = "")
        {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, ocurrio un error al guardar los resultados de su operación, por favor contacte con los administradores."
            };
            try
            {
                result = new PagoSM().ActualizarTransacionPagoHandicap(external_reference, 2, collection_id);
                TempData["Mensaje"] = result.Mensaje;
                TempData["EsSatisfactorio"] = result.EsSatisfactorio;
                TempData["MostrarMsj"] = true;
            }
            catch (Exception)
            {

                throw;
            }
            return RedirectToAction("Index", "HistorialPagos");
        }

        public ActionResult DetalleDePagoError(string external_reference, string collection_id = "")
        {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, ocurrio un error al guardar los resultados de su operación, por favor contacte con los administradores."
            };
            try
            {
                result = new PagoSM().ActualizarTransacionPagoHandicap(external_reference, 3, collection_id);
                TempData["Mensaje"] = result.Mensaje;
                TempData["EsSatisfactorio"] = result.EsSatisfactorio;
                TempData["MostrarMsj"] = true;
            }
            catch (Exception)
            {

                throw;
            }
            return RedirectToAction("Index", "HistorialPagos");
        }

        string obtenerDatosPasarelaMercadoPago(Club c, string idReference)
        {
            DatosMercadoPago mpg;

            mpg = new DatosMercadoPago()
            {
                ID_Credencial = obtenerTagDelWebConfig("MercadoPagoClientID"),// "3906782221688425", 
                Clave_Credencial = obtenerTagDelWebConfig("MercadoPagoClientSecret"),//"8QCz3xC2W2KEzG51Qk37jP10LKMXWC2J" ,
                Referencia_Externa = idReference,
                UrlAPP = Request.Url.Authority,
                UrlSuccess = Request.Url.Authority + "/" + Url.Action("DetalleDePago", "PerfilJugador", new { external_reference = idReference }).ToString(),
                UrlPending = Request.Url.Authority + "/" + Url.Action("DetalleDePagoPending", "PerfilJugador", new { external_reference = idReference }).ToString(),
                UrlFailure = Request.Url.Authority + "/" + Url.Action("DetalleDePagoError", "PerfilJugador", new { external_reference = idReference }).ToString(),
                UrlNotificacion = "",
            };

            List<Item> lItem = new List<Item>();
            c.NombreClub = DatosUsuario().NombreCompleto + " abona cuota del club: " + c.NombreClub;
            lItem.Add(new Item()
            {
                title = c.NombreClub,
                currency_id = obtenerTagDelWebConfig("MercadoPagoMoneda"),
                quantity = 1 /*un paquete*/,
                unit_price = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator == "," && !c.Categoria.strImporte.Contains(",") ? Convert.ToDouble(c.Categoria.strImporte.Replace('.', ',')) : Convert.ToDouble(c.Categoria.strImporte)
            });


            mpg.lstItem = lItem;

            mpg.payer = new payer()
            {
                name = DatosUsuario().Nombre,
                surname = DatosUsuario().Apellido,
                email = "",
                date_created = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"),
                identification = new identification()
                {
                    type = "DNI",
                    number = DatosUsuario().DocumentoIdentidad
                }
            };

            return JsonConvert.SerializeObject(mpg);
            //return JsonConvert.SerializeObject(mp.DatosMercadoPago);
        }
        #endregion
    }
}