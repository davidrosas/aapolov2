﻿using AAPolo.Aplication;
using AAPolo.Controllers.Helper;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Controllers
{
    [Authorize]
    public class HistorialPagosController : ClassHelper
    {
        // GET: HistorialPagos
        public ActionResult Index()
        {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Jugador)
                return RedirectToAction("Index", "Home");

            Persona p = new PersonaSM().BuscaJugador(DatosUsuario()).FirstOrDefault();
            Persona perAux = new PersonaSM().BuscaJugadorPagoHandicap(p.Codigo, "", 0, 0, 0).FirstOrDefault();
            p.HandicapPago = perAux.HandicapPago;
            if (p.HandicapPersona != null)
            {
                p.HandicapPersona.strFechaHasta = perAux.HandicapPersona.strFechaHasta;
                p.HandicapPersona.DescripcionHandicapUsuario = perAux.HandicapPersona.DescripcionHandicapUsuario;
            }
            else
            {
                p.HandicapPersona = new HandicapPersona()
                {
                    strFechaHasta = perAux.HandicapPersona.strFechaHasta,
                    DescripcionHandicapUsuario = perAux.HandicapPersona.DescripcionHandicapUsuario
                };
            }

            ViewBag.lPagos = new PagoSM().BuscarPago(DatosUsuario().Codigo);
            return View(p);
        }

        public PartialViewResult _ListadoPago() {
            try
            {
                return PartialView(new PagoSM().BuscarPago(DatosUsuario().Codigo));
            }
            catch (Exception)
            {
                return PartialView(new List<Pago>());
            }
        }

        public PartialViewResult _ListadoPagoClub()
        {
            try
            {
                return PartialView(new List<Pago>());
            }
            catch (Exception)
            {
                return PartialView(new List<Pago>());
            }
        }

    }
}