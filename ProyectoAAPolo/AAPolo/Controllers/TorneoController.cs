﻿using AAPolo.Aplication;
using AAPolo.Controllers.Helper;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Controllers
{
    [Authorize]
    public class TorneoController : ClassHelper
    {
        #region Torneo
        // GET: Torneo
        public ActionResult Index()
        {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            return View();
        }

        public PartialViewResult _Listado()
        {
            try
            {
                return PartialView(new TorneoSM().BuscarTorneo(0, 0, 0, false));
            }
            catch (Exception)
            {
                return PartialView(new List<Torneo>());
            }
        }

        public ActionResult CrearTorneo()
        {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            return View();
        }

        public ActionResult EditarTorneo(int Codigo = 0)
        {
            if (Codigo == 0)
                return RedirectToAction("Index");

            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            try
            {
                Torneo t = new TorneoSM().BuscarTorneo(Codigo, 0, 0, false).FirstOrDefault();
                /*if (!t.Url_Imagen.Equals("")) {
                    byte[] bytes = System.IO.File.ReadAllBytes(Server.MapPath(t.Url_Imagen));
                    t.Imagen = (HttpPostedFileBase)new MemoryPostedFile(bytes, t.Url_Imagen.Split('/').LastOrDefault());
                }*/
                return View(t);
            }
            catch (Exception e)
            {
                ViewBag.MensajeError = e.Message;
                return RedirectToAction("Index");
            }



        }

        public JsonResult GuardarTorneo(Torneo obj)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };

            try
            {
                if (obj.Codigo == 0)
                {
                    obj.Url_Imagen = GuardarImagen(obj.Imagen);
                }
                else if (obj.Imagen != null)
                {
                    obj.Url_Imagen = GuardarImagen(obj.Imagen);
                }
                else if (obj.ArchivoDocumento != null)
                {
                    obj.Url_ArchivoDocumento = GuardarImagen(obj.ArchivoDocumento);
                }

                obj.UsuarioAlta = DatosUsuario();

                int Dia = Convert.ToInt32(obj.strFechaInicio.Split('/')[0]),
                    Mes = Convert.ToInt32(obj.strFechaInicio.Split('/')[1]),
                    Anio = Convert.ToInt32(obj.strFechaInicio.Split('/')[2]);
                obj.FechaInicio = new DateTime(Anio, Mes, Dia);

                Dia = Convert.ToInt32(obj.strFechaInicioInscripcion.Split('/')[0]);
                Mes = Convert.ToInt32(obj.strFechaInicioInscripcion.Split('/')[1]);
                Anio = Convert.ToInt32(obj.strFechaInicioInscripcion.Split('/')[2]);
                obj.FechaInicioInscripcion = new DateTime(Anio, Mes, Dia);

                Dia = Convert.ToInt32(obj.strFechaFinInscripcion.Split('/')[0]);
                Mes = Convert.ToInt32(obj.strFechaFinInscripcion.Split('/')[1]);
                Anio = Convert.ToInt32(obj.strFechaFinInscripcion.Split('/')[2]);
                obj.FechaFinInscripcion = new DateTime(Anio, Mes, Dia);

                resul = new TorneoSM().GuardarTorneo(obj);
            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult _SelectTipoTorneo()
        {
            try
            {
                return PartialView(new TorneoSM().BuscarTipoTorneo());
            }
            catch (Exception)
            {
                return PartialView(new List<TipoTorneo>());
            }
        }

        [ChildActionOnly]
        public PartialViewResult _SelectTorneoActivo()
        {
            try
            {
                return PartialView(new TorneoSM().BuscarTorneo(0, 0, 0, true));
            }
            catch (Exception)
            {
                return PartialView(new List<Torneo>());
            }
        }

        public PartialViewResult _SelectCategoriaHandicapTorneo()
        {
            try
            {
                return PartialView(new TorneoSM().BuscarCategoriaHandicapTorneo());
            }
            catch (Exception)
            {
                return PartialView(new List<CategoriaHandicap>());
            }
        }

        public PartialViewResult _SelectCodigoProducto(int CodigoProducto = 0)
        {
            try
            {
                ViewBag.CodigoProducto = CodigoProducto;
                return PartialView(new TorneoSM().BuscarProductoTorneo(true));
            }
            catch (Exception)
            {
                return PartialView(new List<ProductoTorneo>());
            }
        }

        public JsonResult CambiarEstadoTorneo(int Codigo, int EstadoTorneo)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };

            try
            {
                resul = new TorneoSM().CambiarEstadoTorneo(Codigo, EstadoTorneo);
            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult _HtmlModalCrearCodigoProducto()
        {
            return PartialView();
        }

        public JsonResult GuardarCodigoProducto(ProductoTorneo obj)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };

            if (!String.IsNullOrEmpty(obj.Nombre))
                resul = new TorneoSM().GuardarCodigoProducto(obj);

            if (resul.EsSatisfactorio)
            {
                this.ControllerContext.Controller.ViewBag.CodigoProducto = resul.CodigoResultado;
                resul.Vista = RenderRazorViewToString(this.ControllerContext, "_SelectCodigoProducto", new TorneoSM().BuscarProductoTorneo(true));
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Fixture

        public ActionResult FixtureTorneo(int Codigo = 0, bool Actualizar = false)
        {

            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            if (Codigo == 0)
                return RedirectToAction("Index");

            List<Zona> lZona = new TorneoSM().BuscarZonaTorneo(Codigo, false, 0);

            if (lZona != null && lZona.Count() > 0 && Actualizar == false)
                return RedirectToAction("AsignarEquipoZona", new { Codigo = Codigo });

            List<Equipo> lEquipo = new ReporteSM().BuscarReporteEquipo(0, Codigo, "", 0);
            int CantEquipo = lEquipo.Where(x => x.detallePago.estado.Codigo == ConfiguracionAPP.Pago_Satisfactorio).Count(),
                SugeridoZona3 = 0,
                SugeridoZona4 = 0,
                resto = 0,
                TotalZona = 0;
            bool Continuar;

            if (CantEquipo >= 33 && CantEquipo <= 64)
            {
                if (CantEquipo / 3 <= 16 || Convert.ToDecimal(CantEquipo / 4) <= 16)
                {
                    if (CantEquipo % 3 == 0 && !(Enumerable.Contains(new int[] { 5, 6, 9, 10 }, (CantEquipo / 3))) && (CantEquipo / 3) <= 16 && TotalZona <= 16)
                    {
                        SugeridoZona3 = CantEquipo / 3;
                        SugeridoZona4 = 0;
                    }
                    else if (CantEquipo % 4 == 0 && !(Enumerable.Contains(new int[] { 5, 6, 9, 10 }, (CantEquipo / 4))) && (CantEquipo / 4) <= 16 && TotalZona <= 16)
                    {
                        SugeridoZona4 = CantEquipo / 4;
                        SugeridoZona3 = 0;
                    }
                    else
                    {
                        resto = CantEquipo % 4;
                        CantEquipo -= resto;
                        Continuar = true;
                        while (Continuar && CantEquipo > 0)
                        {
                            TotalZona = (CantEquipo / 4) + (resto / 3);
                            if ((CantEquipo) % 4 == 0 && (resto % 3 == 0) && !(Enumerable.Contains(new int[] { 5, 6, 9, 10 }, TotalZona)) && TotalZona <= 16)
                            {
                                SugeridoZona4 = (CantEquipo / 4);
                                SugeridoZona3 = (resto / 3);
                                Continuar = false;
                            }
                            else
                            {
                                if ((CantEquipo) % 4 > 0)
                                {
                                    resto += (CantEquipo) % 4;
                                    CantEquipo -= (CantEquipo) % 4;
                                }
                                else
                                {
                                    resto += 1;
                                    CantEquipo -= 1;
                                }

                            }
                        }
                    }
                }
            }
            else if (CantEquipo > 0 && CantEquipo <= 32)//Menos de 33 equipos - evitar recomendar zonas de 5, 6, 9 y 10
            {
                if (CantEquipo % 4 == 0 && !(Enumerable.Contains(new int[] { 5, 6, 9, 10 }, (CantEquipo / 4))))
                {
                    SugeridoZona4 = CantEquipo / 4;
                    SugeridoZona3 = 0;
                }
                else if (CantEquipo % 3 == 0 && !(Enumerable.Contains(new int[] { 5, 6, 9, 10 }, (CantEquipo / 3))))
                {
                    SugeridoZona4 = 0;
                    SugeridoZona3 = CantEquipo / 3;
                }
                else
                {
                    resto = CantEquipo % 4;
                    CantEquipo -= resto;
                    Continuar = true;
                    while (Continuar && CantEquipo > 0)
                    {
                        TotalZona = (CantEquipo / 4) + (resto / 3);
                        if ((CantEquipo) % 4 == 0 && (resto % 3 == 0) && !(Enumerable.Contains(new int[] { 5, 6, 9, 10 }, TotalZona)))
                        {
                            SugeridoZona4 = (CantEquipo / 4);
                            SugeridoZona3 = (resto / 3);
                            Continuar = false;
                        }
                        else
                        {
                            if ((CantEquipo) % 4 > 0)
                            {
                                resto += (CantEquipo) % 4;
                                CantEquipo -= (CantEquipo) % 4;
                            }
                            else
                            {
                                resto += 1;
                                CantEquipo -= 1;
                            }

                        }
                    }
                }

            }
            else
            {
                SugeridoZona4 = 0;
                SugeridoZona3 = 0;
                // El sistema NO soporta mas de 64 equipos debido a la configuracion de zonas 
            }

            Torneo t = new TorneoSM().BuscarTorneo(Codigo, 0, 0, false).FirstOrDefault();
            if (t == null)
                return RedirectToAction("Index");

            //TotalZona = SugeridoZona3 + SugeridoZona4; // variable informativa! "POLICIA".

            ViewBag.lEquipo = lEquipo;
            ViewBag.SugeridoZona3 = SugeridoZona3;
            ViewBag.SugeridoZona4 = SugeridoZona4;
            return View(t);
        }

        [ValidateAntiForgeryToken]
        public JsonResult GuardarZonaTorneo(Torneo obj)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };

            try
            {
                if (obj.Codigo > 0 && obj.CantZona > 0)
                    resul = new TorneoSM().GuardarZonaTorneo(obj);
            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AsignarEquipoZona(int Codigo = 0)
        {

            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            if (Codigo == 0)
                return RedirectToAction("Index");

            List<Zona> lZona = new TorneoSM().BuscarZonaTorneo(Codigo, true, 0);

            if (lZona == null || lZona.Count() == 0)
                return RedirectToAction("FixtureTorneo", new { Codigo = Codigo });

            return View(lZona);
        }

        public PartialViewResult _SelectTipoZona()
        {
            try
            {
                return PartialView(new TorneoSM().BuscarTipoZonaTorneo());
            }
            catch (Exception)
            {
                return PartialView(new List<TipoZona>());
            }

        }

        [ValidateAntiForgeryToken]
        public PartialViewResult _BuscarEquipoZona(int Cod_Zona, int Cod_Torneo, string Cadena = "")
        {
            try
            {
                return PartialView(new TorneoSM().BuscarEquipoZonaTorneo(0, Cadena, Cod_Torneo, Cod_Zona).Where(x => x.detallePago.estado.Codigo == 1));
            }
            catch (Exception)
            {
                return PartialView(new List<Equipo>());
            }

        }

        public PartialViewResult AgregarEquipo(Equipo obj)
        {
            return PartialView(obj);
        }

        [ValidateAntiForgeryToken]
        public JsonResult GuardarEquipoZona(Zona obj)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };

            try
            {
                if (obj.Codigo > 0 && obj.TipoZona.Codigo > 0 && obj.lEquipo.Count() > 0)
                    resul = new TorneoSM().GuardarEquipoZona(obj);

                if (resul.EsSatisfactorio)
                    resul.Vista = RenderRazorViewToString(this.ControllerContext, "_LitadoEquipoZona", new TorneoSM().BuscarEquipoZona(obj.Codigo, 0, 0));

            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult _LitadoEquipoZona(int Cod_Zona = 0, int Cod_Torneo = 0, int Cod_Equipo = 0)
        {
            try
            {
                return PartialView(new TorneoSM().BuscarEquipoZona(Cod_Zona, Cod_Torneo, Cod_Equipo));
            }
            catch (Exception)
            {
                return PartialView(new List<Equipo>());
            }
        }

        public PartialViewResult _ZonaPartido(int Cod_Torneo, int Cod_Zona)
        {
            try
            {
                return PartialView(new TorneoSM().BuscarZonaTorneo(Cod_Torneo, true, Cod_Zona).FirstOrDefault());
            }
            catch (Exception)
            {
                return PartialView(new List<Equipo>());
            }
        }

        public PartialViewResult _ModalPartidoZona(int NroPartido, int Cod_Zona, int Cod_Torneo, int Cod_Instancia = 1, int Cod_Partido = 0, int CantidadEquipo = 0)
        {
            try
            {
                ViewBag.Cod_Zona = Cod_Zona;
                ViewBag.Cod_Instancia = Cod_Instancia;
                ViewBag.NroPartido = NroPartido;
                ViewBag.Cod_Torneo = Cod_Torneo;
                Partido obj = new Partido();
                if (Cod_Partido > 0)
                    obj = new TorneoSM().BuscarPartidoZona(Cod_Zona, Cod_Torneo, Cod_Instancia, Cod_Partido).FirstOrDefault();

                if (obj == null)
                    obj = new Partido();
                return PartialView(obj);
            }
            catch (Exception)
            {
                return PartialView(new Partido());
            }
        }

        //[OutputCache(Duration = 60, VaryByParam = "*")]//VaryByParam = "Cod_Zona, Cod_Torneo, Cod_Partido, Cod_Equipo"
        public PartialViewResult _SelectEquipoZona(int Cod_Zona, int Cod_Torneo, int Cod_Partido, int Cod_Equipo = 0, int NroPartido = 0)
        {
            try
            {
                ViewBag.Cod_Equipo = Cod_Equipo;
                List<Equipo> lEquipo = new List<Equipo>();
                if (NroPartido > 1)
                    lEquipo = new TorneoSM().BuscarEquipoPartidoSegundaFase(Cod_Zona, Cod_Torneo).Where(x => x.EsGanador == (NroPartido == 2 ? true : false)).ToList();
                else if (NroPartido >= 0)
                    lEquipo = new TorneoSM().BuscarEquipoPartido(Cod_Zona, Cod_Torneo, Cod_Partido);
                else
                    lEquipo = new TorneoSM().BuscarEquipoZona(Cod_Zona, Cod_Torneo, Cod_Partido);

                if (lEquipo.Count == 0)
                {
                    Equipo ELocal = new TorneoSM().BuscarPartidoZona(Cod_Zona, Cod_Torneo, 0, Cod_Partido).Select(s => s.EquipoLocal).FirstOrDefault();
                    Equipo EVisitante = new TorneoSM().BuscarPartidoZona(Cod_Zona, Cod_Torneo, 0, Cod_Partido).Select(s => s.EquipoVisitante).FirstOrDefault();
                    lEquipo.Add(ELocal);
                    lEquipo.Add(EVisitante);
                }

                return PartialView(lEquipo);
            }
            catch (Exception)
            {
                return PartialView(new List<Equipo>());
            }
        }

        [OutputCache(Duration = 300, VaryByParam = "Cod_Referee")]
        public PartialViewResult _SelectReferee(int Cod_Referee = 0)
        {
            try
            {
                ViewBag.Cod_Referee = Cod_Referee;
                return PartialView(new RefereeSM().BuscaReferee("", 0, 0, 0));
            }
            catch (Exception)
            {
                return PartialView(new List<Referee>());
            }
        }

        [OutputCache(Duration = 300, VaryByParam = "Cod_Persona")]
        public PartialViewResult _SelectVeedor(int Cod_Persona = 0)
        {
            try
            {
                ViewBag.Cod_Persona = Cod_Persona;
                return PartialView(new PersonaSM().BuscaVeedor("", 0));
            }
            catch (Exception)
            {
                return PartialView(new List<Persona>());
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult GuardarPartido(Partido obj, int NroPartido, string HoraPartido, bool ValidarFecha = false)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };

            try
            {
                if (obj.EquipoLocal.Codigo == 0 || obj.EquipoVisitante.Codigo == 0)
                {
                    resul.Mensaje = "Disculpe, debe seleccionar los equipos del partido.";
                    return Json(resul, JsonRequestBehavior.AllowGet);
                }

                try
                {
                    int Dia = Convert.ToInt32(obj.strFechaPartido.Split('/')[0]),
                        Mes = Convert.ToInt32(obj.strFechaPartido.Split('/')[1]),
                        Anio = Convert.ToInt32(obj.strFechaPartido.Split('/')[2]),
                        Hora = Convert.ToInt32(HoraPartido.Split(':')[0]),
                        Minuto = Convert.ToInt32(HoraPartido.Split(':')[1]);

                    obj.FechaPartido = new DateTime(Anio, Mes, Dia, Hora, Minuto, 0);

                    DateTime FecActual = DateTime.UtcNow.AddHours(-3);
                    if (DateTime.Compare(obj.FechaPartido, FecActual) < 0 && ValidarFecha)
                    {
                        resul.Mensaje = "Disculpe, la fecha y hora del partido debe ser mayor a la fecha y hora actual.";
                        return Json(resul, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception)
                {
                    resul.Mensaje = "disculpe, debe ingresar fecha y hora válida.";
                    return Json(resul, JsonRequestBehavior.AllowGet);
                }

                if (obj.Cancha < 0)
                {
                    obj.Cancha = 0;
                }
                resul = new TorneoSM().GuardarPartido(obj, DatosUsuario().Codigo);

                if (resul.EsSatisfactorio)
                {
                    Response.RemoveOutputCacheItem(Url.Action("_SelectEquipoZona", "Torneo"));

                    resul.Vista = RenderRazorViewToString(this.ControllerContext, "_ZonaPartido", new TorneoSM().BuscarZonaTorneo(obj.Torneo.Codigo, true, obj.Zona.Codigo).FirstOrDefault());
                    resul.CodigoResultado = obj.Zona.Codigo;
                }

            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(Duration = 600, VaryByParam = "Cod_Incidencia")]
        public PartialViewResult _SelectIncidencia(int Cod_Incidencia = 0)
        {
            try
            {
                ViewBag.Cod_Incidencia = Cod_Incidencia;
                return PartialView(new TorneoSM().BuscarIncidencia());
            }
            catch (Exception)
            {
                throw;
            }
        }

        public PartialViewResult _CopaTorneo(int Cod_Torneo, int Cod_Copa = 0)
        {
            List<Copa> lCopa = new TorneoSM().BuscarCopaTorneo(Cod_Torneo, Cod_Copa, true);

            if (lCopa == null || lCopa.Count == 0)
            {
                List<Zona> lZona = new TorneoSM().BuscarZonaTorneo(Cod_Torneo, false, 0);

                lCopa.Add(new Copa()
                {
                    Codigo = 0,
                    Nombre = "",
                    EsPrincipal = true,
                    lZona = lZona,
                    Torneo = new Torneo()
                    {
                        Codigo = Cod_Torneo
                    }
                });

                lCopa.Add(new Copa()
                {
                    Codigo = 0,
                    Nombre = "",
                    EsPrincipal = false,
                    lZona = lZona,
                    Torneo = new Torneo()
                    {
                        Codigo = Cod_Torneo
                    }
                });

            }

            return PartialView(lCopa);

        }

        public PartialViewResult _Copa(int Cod_Torneo, int Cod_Copa)
        {
            /**/
            List<Copa> lCopa = new TorneoSM().BuscarCopaTorneo(Cod_Torneo, Cod_Copa, true);

            if (lCopa != null && lCopa.Count > 0)
            {
                return PartialView(lCopa);
            }

            return PartialView();

        }

        [ValidateAntiForgeryToken]
        public JsonResult GuardarCopa(List<Copa> lCopa, int CopasActiva = 0, List<Partido> lPartidoLLave1 = null, List<Partido> lPartidoLLave2 = null)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };

            if (lCopa == null || lCopa.Count < 2)
            {
                return Json(resul, JsonRequestBehavior.AllowGet);
            }
            else if (String.IsNullOrEmpty(lCopa[0].Nombre) || String.IsNullOrEmpty(lCopa[1].Nombre))
            {
                resul.Mensaje = "Disculpe, debe ingresar nombre de la copa para ambas llaves.";
                return Json(resul, JsonRequestBehavior.AllowGet);
            }
            else if (lPartidoLLave1 == null || lPartidoLLave2 == null || lPartidoLLave1.Any(a => a.ZonaLocal.Codigo == 0 || a.ZonaVisitante.Codigo == 0) || lPartidoLLave2.Any(a => a.ZonaLocal.Codigo == 0 || a.ZonaVisitante.Codigo == 0))
            {
                resul.Mensaje = "Recuerda asignar las zonas en la llave para continuar";
                return Json(resul, JsonRequestBehavior.AllowGet);
            }

            lCopa[0].EsActivo = CopasActiva == 1 ? true : false;
            lCopa[1].EsActivo = CopasActiva == 1 ? true : false;

            lCopa[0].lPartido = lPartidoLLave1;
            lCopa[1].lPartido = lPartidoLLave2;

            try
            {
                resul = new TorneoSM().GuardarCopa(lCopa, DatosUsuario().Codigo);
                if (resul.EsSatisfactorio)
                {
                    resul.Vista = this.RenderRazorViewToString(this.ControllerContext, "_CopaTorneo", new TorneoSM().BuscarCopaTorneo(lCopa.FirstOrDefault().Torneo.Codigo, 0, true));
                }
            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);

        }

        public PartialViewResult _ListadoPartidoInstancia(int Cod_Torneo, int Cod_Instancia = 1, int Cod_Partido = 0, int SoloCopaPrincipal = 2)
        {
            try
            {
                return PartialView(new TorneoSM().BuscarPartidoInstancia(Cod_Torneo, Cod_Instancia, Cod_Partido, SoloCopaPrincipal));
            }
            catch (Exception e)
            {
                return PartialView(new List<Partido>());
            }
        }

        public PartialViewResult _ModalPartidoInstancia(int Cod_Instancia, int Cod_Torneo, int Cod_Partido, int Cod_EquipoGanadorSigPartido = 0, int Orden = 0)
        {
            ViewBag.Cod_Partido = Cod_Partido;
            ViewBag.Cod_Instancia = Cod_Instancia;
            ViewBag.Cod_Torneo = Cod_Torneo;
            Partido objPartido = new TorneoSM().BuscarPartidoInstancia(Cod_Torneo, Cod_Instancia, Cod_Partido, 2).FirstOrDefault();
            objPartido.EquipoDeSigPartido = new Equipo()
            {
                Codigo = Cod_EquipoGanadorSigPartido
            };
            objPartido.NroOrdenSiguientePartido = Orden;
            return PartialView(objPartido);
        }

        public PartialViewResult _SelectEquipoPartido(int Cod_Partido, int Cod_Equipo = 0)
        {

            ViewBag.Cod_Equipo = Cod_Equipo;
            List<Equipo> lEquipo = new TorneoSM().BuscarEquipoPartidoInstancia(Cod_Partido);
            return PartialView(lEquipo);
        }

        [ValidateAntiForgeryToken]
        public JsonResult GuardarPartidoInstancia(Partido obj, string HoraPartido, bool ValidarFecha = false)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };
            DateTime FecActual = DateTime.UtcNow.AddHours(-3);
            try
            {
                if (obj.EquipoLocal.Codigo == 0 || obj.EquipoVisitante.Codigo == 0)
                {
                    resul.Mensaje = "Disculpe, debe seleccionar los equipos del partido.";
                    return Json(resul, JsonRequestBehavior.AllowGet);
                }

                try
                {
                    int Dia = Convert.ToInt32(obj.strFechaPartido.Split('/')[0]),
                        Mes = Convert.ToInt32(obj.strFechaPartido.Split('/')[1]),
                        Anio = Convert.ToInt32(obj.strFechaPartido.Split('/')[2]),
                        Hora = Convert.ToInt32(HoraPartido.Split(':')[0]),
                        Minuto = Convert.ToInt32(HoraPartido.Split(':')[1]);

                    obj.FechaPartido = new DateTime(Anio, Mes, Dia, Hora, Minuto, 0);

                    if (DateTime.Compare(obj.FechaPartido, FecActual) < 0 && ValidarFecha)
                    {
                        resul.Mensaje = "Disculpe, la fecha y hora del partido debe ser mayor a la fecha y hora actual.";
                        return Json(resul, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception)
                {
                    resul.Mensaje = "disculpe, debe ingresar fecha y hora válida.";
                    return Json(resul, JsonRequestBehavior.AllowGet);
                }

                if (obj.Cancha < 1)
                {
                    obj.Cancha = 0;
                }

                resul = new TorneoSM().GuardarPartido(obj, DatosUsuario().Codigo);

                if (resul.EsSatisfactorio)
                {
                    List<Partido> lPart = new TorneoSM().BuscarPartidoInstancia(obj.Torneo.Codigo, obj.Intancia.Codigo, 0, obj.Copa.EsPrincipal ? 1 : 0);
                    resul.Vista = RenderRazorViewToString(this.ControllerContext, "_ListadoPartidoInstancia", lPart);
                    resul.Cadena = "divContenedor_" + obj.Intancia.Codigo + "_" + (obj.Copa.EsPrincipal ? 1 : 0);
                }

            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult _ListaJugadorEquipo(int Codigo = 0)
        {
            try
            {
                Equipo Eq = new Equipo();
                if (Codigo > 0)
                {
                    Eq = new TorneoSM().BuscarEquipoZona(0, 0, Codigo).FirstOrDefault();
                    if (Eq != null)
                        Eq.lPersona = new TorneoSM().ListadoPersonaEquipoPartido(Codigo, 0);
                }
                return PartialView(Eq);
            }
            catch (Exception)
            {
                return PartialView(new List<Persona>());
            }
        }

        public PartialViewResult _ListaJugadorEquipoIncidencia(int Codigo = 0)
        {
            try
            {
                Equipo Eq = new Equipo();
                if (Codigo > 0)
                {
                    Eq = new TorneoSM().BuscarEquipoZona(0, 0, Codigo).FirstOrDefault();
                    if (Eq != null)
                        Eq.lPersona = new TorneoSM().ListadoPersonaEquipoPartido(Codigo, 0);
                }
                return PartialView(Eq);
            }
            catch (Exception)
            {
                return PartialView(new List<Persona>());
            }
        }

        public PartialViewResult _PartidoDeZona(int Cod_Torneo)
        {

            List<Zona> lZona = new TorneoSM().BuscarZonaTorneo(Cod_Torneo, true, 0);
            return PartialView(lZona);
        }

        public PartialViewResult _GraficaCopa(int Cod_Torneo, int Cod_Copa = 0)
        {
            List<Copa> lCopa = new TorneoSM().BuscarCopaTorneo(Cod_Torneo, Cod_Copa, false);

            if (lCopa == null || lCopa.Count == 0)
            {
                lCopa.Add(new Copa()
                {
                    Codigo = 0,
                    Nombre = "Llave 1",
                    EsPrincipal = true,
                    Torneo = new Torneo()
                    {
                        Codigo = Cod_Torneo
                    }
                });

                lCopa.Add(new Copa()
                {
                    Codigo = 0,
                    Nombre = "Llave 1",
                    EsPrincipal = false,
                    Torneo = new Torneo()
                    {
                        Codigo = Cod_Torneo
                    }
                });

            }

            return PartialView(lCopa);
        }

        public JsonResult DisponibleZona(int Cod_Torneo, int Disponible)
        {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos."
            };

            if (Cod_Torneo > 0 && (Disponible == 1 || Disponible == 0))
            {
                result = new TorneoSM().DisponibleZona(Cod_Torneo, (Disponible == 1 ? true : false));
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DisponibleLlaves(int Cod_Torneo, int Disponible)
        {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos."
            };

            if (Cod_Torneo > 0 && (Disponible == 1 || Disponible == 0))
            {
                result = new TorneoSM().DisponibleLlaves(Cod_Torneo, (Disponible == 1 ? true : false));
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DisponibleParidoLlaves(int Cod_Torneo, int Disponible)
        {
            ResultadoTransaccion result = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos."
            };

            if (Cod_Torneo > 0 && (Disponible == 1 || Disponible == 0))
            {
                result = new TorneoSM().DisponibleParidoLlaves(Cod_Torneo, (Disponible == 1 ? true : false));
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}