﻿using AAPolo.Aplication;
using AAPolo.Controllers.Helper;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Controllers
{
    [Authorize]
    public class JugadorController : ClassHelper
    {
        // GET: Jugador
        public ActionResult Index()
        {
            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");
            return View();
        }

        public PartialViewResult Listado(int Cod_Persona = 0, string Cadena = "", int Cod_TipoJugador = 0, int Cod_Club = 0, int Nro_Handicap = -1)
        {
            //List<Persona> lPersona = new PersonaSM().BuscaJugadorResumen(Cod_Persona, Cadena, Cod_TipoJugador, Cod_Club, Nro_Handicap, 0);
            List<Persona> lPersona = new PersonaSM().BuscaJugadorDetalleResumen(Cod_Persona, Cadena, Cod_TipoJugador, Cod_Club, Nro_Handicap, 0);
            return PartialView(lPersona);
        }

        public ActionResult CrearJugador()
        {

            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            return View();
        }

        public ActionResult EditarJugador(int Codigo = 0)
        {
            if (Codigo == 0)
                return RedirectToAction("Index");

            if (DatosUsuario().Rol.Codigo != ConfiguracionAPP.Rol_Administrador)
                return RedirectToAction("Index", "Home");

            Persona p = new Persona() { Codigo = Codigo };
            try
            {
                p = new PersonaSM().BuscaJugador(p).FirstOrDefault();

            }
            catch (Exception e)
            {
                ViewBag.MensajeError = e.Message;
                return RedirectToAction("Index");
            }
            if (p == null || p.Codigo == 0)
                return RedirectToAction("Index");

            if (String.IsNullOrEmpty(p.DocumentoIdentidad) || p.DocumentoIdentidad.Equals("1"))
                p.DocumentoIdentidad = "No Informado";

            return View(p);
        }
        
        public JsonResult _CrearJugador(Persona obj, List<int> _lClubUsuarioSecundario = null, int Cod_Handicap = 0, int NroHandicap = 0, int NroHandicapSecundario = 0)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };

            try
            {
                if (!String.IsNullOrEmpty(obj.strFechaNacimiento))
                {
                    int Dia = Convert.ToInt32(obj.strFechaNacimiento.Split('-')[0]),
                        Mes = Convert.ToInt32(obj.strFechaNacimiento.Split('-')[1]),
                        Anio = Convert.ToInt32(obj.strFechaNacimiento.Split('-')[2]);
                    obj.FechaNacimiento = new DateTime(Anio, Mes, Dia);
                }

                if (!obj.EsMember)
                {
                    obj.HandicapPersona = new HandicapPersona()
                    {
                        Codigo = Cod_Handicap,
                        NroHandicap = NroHandicap
                    };

                    obj.HandicapPersonaSecundario = new HandicapPersona()
                    {
                        NroHandicap = NroHandicapSecundario
                    };

                    if (_lClubUsuarioSecundario != null && _lClubUsuarioSecundario.Count() > 0)
                    {
                        obj.lClubUsuarioSecundario = (from value in _lClubUsuarioSecundario
                                                      where value > 0
                                                      select new ClubUsuario() { Codigo = value }).ToList();
                    }
                }

                if (obj.Foto != null)
                {
                    obj.UrlFoto = GuardarImagen(obj.Foto);
                }
                else
                {
                    obj.UrlFoto = "";
                }

                if (obj.EsReferee && obj.TipoReferee != null && obj.TipoReferee.Codigo < 1)
                {
                    resul.Mensaje = "Por favor, seleccione el tipo de referees.";
                }
                else if ((obj.Codigo == 0 && !obj.DocumentoIdentidad.ToLower().Equals("no informado".ToLower())) || obj.Codigo > 0)
                {
                    resul = new PersonaSM().GuardarJugador(obj, DatosUsuario().Codigo);
                }
                else
                {
                    resul.Mensaje = "Por favor, ingrese un documento válido.";
                }
            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CambiarEstadoJugador(int Codigo, int EstadoJugador)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };

            try
            {
                resul = new PersonaSM().CambiarEstadoJugador(Codigo, EstadoJugador);
            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CambiarEstadoDestacadoJugador(int Codigo, int EstadoDestacadoJugador)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, debe ingresar datos válidos"
            };

            try
            {
                resul = new PersonaSM().CambiarEstadoDestacadoJugador(Codigo, EstadoDestacadoJugador);
            }
            catch (Exception e)
            {
                resul.MensajeError = e.Message;
            }

            return Json(resul, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult _SelectGenero()
        {
            try
            {
                return PartialView(new PersonaSM().BuscarGenero());
            }
            catch (Exception e)
            {
                return PartialView(new List<Genero>());
            }
        }

        public PartialViewResult _SelectTipoDocumento()
        {
            try
            {
                return PartialView(new PersonaSM().BuscarTipoDocumento());
            }
            catch (Exception e)
            {
                return PartialView(new List<TipoDocumento>());
            }
        }

        public PartialViewResult _SelectPais(int Cod_Pais = 0)
        {
            try
            {
                ViewBag.Cod_Pais = Cod_Pais;
                return PartialView(new PersonaSM().BuscarPais());
            }
            catch (Exception e)
            {
                return PartialView(new List<Pais>());
            }
        }

        public PartialViewResult _SelectProvincia(int Codigo = 0, int Cod_Provincia = 0)
        {
            try
            {
                ViewBag.Cod_Provincia = Cod_Provincia;
                return PartialView(new PersonaSM().BuscarProvincia(Codigo));
            }
            catch (Exception e)
            {
                return PartialView(new List<Provincia>());
            }
        }

        public PartialViewResult _SelectMunicipio(int Codigo = 0, int Cod_Municipio = 0)
        {
            try
            {
                ViewBag.Cod_Municipio = Cod_Municipio;
                return PartialView(new PersonaSM().BuscarMunicipio(Codigo));
            }
            catch (Exception e)
            {
                return PartialView(new List<Municipio>());
            }
        }

        [AllowAnonymous]
        public PartialViewResult _SelectTipoJugador(string FechaNacimiento = "")
        {
            int Edad = 0;
            if (!FechaNacimiento.Equals(""))
            {
                int Dia = Convert.ToInt32(FechaNacimiento.Split('-')[0]),
                    Mes = Convert.ToInt32(FechaNacimiento.Split('-')[1]),
                    Anio = Convert.ToInt32(FechaNacimiento.Split('-')[2]);

                DateTime dtFechaNacimiento = new DateTime(Anio, Mes, Dia);

                Edad = DateTime.Today.Year - dtFechaNacimiento.Year;
            }

            try
            {
                List<TipoJugador> lJugador = new PersonaSM().BuscarTipoJugador();
                if (Edad > 0)
                    lJugador = lJugador.Where(x => Edad >= x.EdadDesde && Edad <= x.EdadHasta).ToList();

                return PartialView(lJugador);
            }
            catch (Exception e)
            {
                return PartialView(new List<TipoJugador>());
            }
        }

        public PartialViewResult _Contacto(TipoContacto obj, Contacto contacto = null, int CodContactoUsuario = 0)
        {
            if (contacto == null)
            {
                contacto = new Contacto() { TipoContacto = obj };
            }
            else
            {
                contacto.TipoContacto = obj;
                if ((contacto.Nombre.Equals(contacto.TipoContacto.Nombre) || String.IsNullOrEmpty(contacto.Nombre)) && CodContactoUsuario > 0)
                {
                    //contacto.Nombre = "";
                    if (obj.Codigo == ConfiguracionAPP.TipoContacto_Email)
                        contacto.Nombre = "noinformado@noinformado.com";
                    else
                        contacto.Nombre = "No informado";
                }
                else if (contacto.Nombre.Equals(contacto.TipoContacto.Nombre) && CodContactoUsuario == 0)
                {
                    contacto.Nombre = "";

                }
            }

            return PartialView(contacto);
        }

        public PartialViewResult _Domicilio(TipoDomicilio obj, Domicilio domicilio = null)
        {

            if (domicilio == null)
            {
                domicilio = new Domicilio()
                {
                    tipoDomicilio = obj
                };
            }
            else
            {
                domicilio.tipoDomicilio = obj;
            }

            return PartialView(domicilio);
        }

        public PartialViewResult _SelectJugadorHandicap(int Cod_TipoJugador = 0, int Cod_Handicap = 0, int Cod_CategoriaHandicap = 0)
        {
            try
            {
                return PartialView(new JugadorSM().BuscarJugadorHandicap(Cod_TipoJugador, Cod_Handicap, Cod_CategoriaHandicap));
            }
            catch (Exception e)
            {
                return PartialView(new List<JugadorHandicap>());
            }
        }

        [AllowAnonymous]
        public PartialViewResult _SelectClub(int Cod_ClubSelect = 0, int Cod_Club = 0, string NombreClub = "", int Cod_ClubOmitir = 0)
        {
            try
            {
                ViewBag.Cod_ClubSelect = Cod_ClubSelect;
                List<Club> lClub = new ClubSM().BuscaClub(0, Cod_Club, NombreClub, 0);
                if (Cod_ClubOmitir > 0)
                    lClub = lClub.Where(x => x.Codigo != Cod_ClubOmitir).ToList();
                return PartialView(lClub);
            }
            catch (Exception e)
            {
                return PartialView(new List<JugadorHandicap>());
            }
        }

        public PartialViewResult _SelectPersona()
        {

            try
            {
                return PartialView(new PersonaSM().BuscaJugador(new Persona() { Codigo = 0 }));
            }
            catch (Exception e)
            {
                ViewBag.ErrorCatch = e.Message;
                return PartialView(new List<Persona>());
            }
        }

        public PartialViewResult _BuscarTipoReferee(int Codigo = 0)
        {
            try
            {
                ViewBag.Cod_TipoReferee = Codigo;
                return PartialView(new RefereeSM().BuscaTipoReferee());
            }
            catch (Exception)
            {
                return PartialView(new List<TipoReferee>());
            }
        }

    }
}