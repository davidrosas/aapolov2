﻿using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAPolo.Models
{
    public class ViewModelFixture
    {
        public ViewModelFixture() {

        }

        public ViewModelFixture(List<Zona> _lZona, List<Copa> _lCopa)
        {
            this.lZona = _lZona;
            this.lCopa = _lCopa;
            this.lEquipo = new List<Equipo>();
            foreach (var item in _lZona)
            {
                this.lEquipo.AddRange(item.lEquipo);
            }
        }

        public List<Zona> lZona { get; set; }

        public List<Copa> lCopa { get; set; }

        public List<Equipo> lEquipo { get; set; }

    }
}