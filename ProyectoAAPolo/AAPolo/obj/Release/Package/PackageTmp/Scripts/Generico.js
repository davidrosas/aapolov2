﻿
var ExprecionRegular_validarMail = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66}){1}\.([a-z]{2,6}(?:\.[a-z]{2})?)$/igm;;

function PaginacionDeTablas(ElementTabla, numItems) {
    var ElementItem = ElementTabla.replace('.', '').replace('#', '');
    $('#nav ' + ElementItem).remove();
    $(ElementTabla).after('<ul id="nav' + ElementItem + '" class="pagination"></ul>');
    var rowsShown = numItems;
    var rowsTotal = $(ElementTabla + ' tbody tr').length;
    var numPages = rowsTotal / rowsShown;
    for (i = 0; i < numPages; i++) {
        var pageNum = i + 1;
        $('#nav' + ElementItem).append('<li><a style="cursor:pointer" rel="' + i + '">' + pageNum + '</a> </li>');
    }
    $(ElementTabla + ' tbody tr').hide();
    $(ElementTabla + ' tbody tr').slice(0, rowsShown).show();

    var classActivePag = 'current';

    $('#nav' + ElementItem + ' li:first').addClass(classActivePag);
    $('#nav' + ElementItem + ' a').bind('click', function () {

        $('#nav' + ElementItem + ' li').removeClass(classActivePag);
        $(this).closest('li').addClass(classActivePag);
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $(ElementTabla + ' tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
            css('display', 'table-row').animate({ opacity: 1 }, 300);
    });

}
try {
    $('.summernoteFormat').summernote({
        lang: 'es-ES',
        airMode: true,
        height: 500,
        minHeight: null,
        maxHeight: null,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ]
    });

} catch (e) {
    console.log(e);
}

function swicheAcepta() {

    if ($('#aceptarTerminos').is(':disabled') == true) {

        $('#aceptarTerminos').attr('disabled', false);
    } else {
        $('#aceptarTerminos').attr('disabled', true);
    }

}

function closeModal() {
    $('.modal').fadeOut(300).removeClass('modal-abierto');
    $('body, html').removeClass('overflow-hidden');
    setTimeout(function () {
        $('header').css('z-index', 12);
    }, 100);
    console.log('...');
}

function clickModal(modal) {
    $('.modal#' + modal).fadeIn(300).addClass('modal-abierto');
    inicializarClases();
    if ($('.modal#' + modal).attr('data-duracion')) {
        var duracion = $('.modal#' + modal).attr('data-duracion');
        setTimeout(function () {
            $('.modal#' + modal).fadeOut(300).removeClass('modal-abierto')
        }, duracion);
    }
}

function ModalMensajeTransaccion(_Msj, _Titulo = '') {
    if (_Titulo != '') {
        $('#TituloTransaccion').text(_Titulo);
    }
    $('#MsjOperacion').html(_Msj);
    clickModal('ModalMensajeTransacciones');

}

$(document).ready(function () {
    if ($('.modal').is(':visible')) {
        $('body, html').addClass('overflow-hidden');
    }
});

function inicializarClases() {
    $('body, html').addClass('overflow-hidden');
}

$(document).ready(function (e) {
    if ($(window).width() < 768) {
        var headHeight = $('#condicionesUsuario .modal-header').height();
        var formHeight = $('#condicionesUsuario .condiciones-form').height();
        var windowHeight = $(window).height();
        var termHeight = windowHeight - headHeight - formHeight;
        $('.condiciones-text').height(termHeight);
    }
});

$(document).ready(function () {

    $(document).on('click', 'button[type=submit]', function() {
        var contenidoHtml = '';
        var elementoHtml = $(this);
        if ($(this).is(':visible') && $(this).html() != 'Cargando ...') {
            contenidoHtml = $(this).html();
            $(this).html('Cargando ...');
            setTimeout(function (e) {
                $(elementoHtml).html(contenidoHtml).attr('disabled', false);
            }, 2000);
        }
    });

    $(document).on('click', 'button[type=button].btnCargarForm', function () {
        var contenidoHtml = '';
        var elementoHtml = $(this);
        if ($(this).html() != 'Cargando ...') {
            contenidoHtml = $(this).html();
            //$(this).data('cargando', true);
            $(this).html('Cargando ...');
            $(this).attr('disabled', true);
            setTimeout(function (e) {
                $(elementoHtml).html(contenidoHtml).attr('disabled', false);
            }, 2000);
        }
    });

    $(document).on('click', 'body', function () {
        if ($('div.modal').css('display') == 'block') {
            $('header').css('z-index', 0);
        } else {
            $('header').css('z-index', 12);
        }
    });

    $('.DisableEnter').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    PaginacionDeTablas('.paginarTabla', 20);
    
    $('.FormFile').attr('enctype', 'multipart/form-data');
    window.addEventListener("submit", function (e) {
        console.log('submit addEventListener');
        var form = e.target;
        if (form.getAttribute("enctype") === "multipart/form-data") {
            console.log('multipart/form-data id: ' + form.id + ' - action: ' + form.action);
            if (form.dataset.ajax) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var xhr = new XMLHttpRequest();
                xhr.open(form.method, form.action);
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        var objResp = JSON.parse(xhr.responseText);
                        fnSuccessProcesar(objResp);
                        if (form.dataset.ajaxUpdate) {
                            var updateTarget = document.querySelector(form.dataset.ajaxUpdate);
                            if (updateTarget) {
                                updateTarget.innerHTML = xhr.responseText;
                                console.log('updateTarget');
                            }
                        }
                    } else if (xhr.status != 200) {
                        try {
                            console.log('xhr');
                            console.log(xhr);
                            fnFailProcesar(xhr);
                            console.log('fnFailProcesar');
                        } catch (e) {
                            console.log('catch');
                            fnFailProcesar(e);
                        }
                    }
                };
                xhr.send(new FormData(form));
            }
        }
    }, true);
    
    $(document).on('keyup', '.ValidarMail', function (event) {
        
        if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test($(this).val())) {
            console.log("La dirección de email " + $(this).val() + " es correcta!.");
        } else {
            console.log("La dirección de email es incorrecta!.");
        }
    });


});

//--------------------- ARIEL

$(document).on('click','[data-dismiss]',function () {
    var modal = $(this).attr('data-dismiss');
    $('body, html').removeClass('overflow-hidden');
    $('.modal#' + modal).fadeOut(300).removeClass('modal-abierto').find('.videoModal').get(0).pause();
});

$(document).on('click','[data-tab]', function () {
    var showTab = $(this).attr('data-tab');
    $('#' + showTab).siblings('.tab-content').slideUp(500);
    $('#' + showTab).slideDown(500);
    $(this).parent().siblings().removeClass('current');
    $(this).parent().addClass('current');
});

$(document).on('click', '[data-slidebasic]', function () {
    var slide = $(this).attr('data-slidebasic');
    $('#' + slide).slideToggle(500);
});

$(document).on('click','.toggle',function () {
    var on = $(this).attr('data-on');
    var off = $(this).attr('data-off');
    if ($(this).hasClass('toggle-active')) {
        $(this).prop('title', off).tooltip('fixTitle').tooltip('show');;
    } else {
        $(this).prop('title', on).tooltip('fixTitle').tooltip('show');;
    }
    $(this).toggleClass('toggle-active');
});

$(document).on('click', '[data-tab]', function () {
    var showTab = $(this).attr('data-tab');
    var showTabSplit = showTab.split(',');

    for (i = 0; i <= showTabSplit.length; i++) {
        $('#' + showTabSplit[i]).siblings('.tab-content').slideUp(500);
        $('#' + showTabSplit[i]).slideDown(500);
        $('[data-tab="' + showTabSplit[i] + '"]').parent().siblings().removeClass('current');
        $('[data-tab="' + showTabSplit[i] + '"]').parent().addClass('current');
        if ($(window).width() > 768) {
            fixHeight('#inscripciones .panel');
            fixHeight('#posiciones .fixture-partido');
        };
    }
});

$(document).on('click','.toggle',function () {
    var on = $(this).attr('data-on');
    var off = $(this).attr('data-off');
    if ($(this).hasClass('toggle-active')) {
        $(this).prop('title', off).tooltip('fixTitle').tooltip('show');;
    } else {
        $(this).prop('title', on).tooltip('fixTitle').tooltip('show');;
    }
    $(this).toggleClass('toggle-active');
});

