﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class Domicilio
    {
        [Key]
        public int Codigo { get; set; }

        public string DireccionDomicilio { get; set; }

        public Pais pais { get; set; }

        public Provincia provincia { get; set; }

        public Municipio municipio { get; set; }

        public int NroDomicilio { get; set; }

        public int NroPiso { get; set; }

        public string DireccionCompleta { get { return this.DireccionDomicilio + " " + this.NroDomicilio; } }

        public string Departamento { get; set; }

        public string CodigoPostal { get; set; }

        public TipoDomicilio tipoDomicilio { get; set; }
    }

    public class TipoDomicilio {

        [Key]
        public int Codigo { get; set; }

        public string Nombre { get; set; }
    }
}
