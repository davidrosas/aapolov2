﻿using AAPolo.Entities.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class OrderItem
    {
        [Required]
        [DisplayName("ProductCode*")]
        public string ProductCode { get; set; }

        public string SKUCode { get; set; }

        public string VariantCode { get; set; }

        [Required]
        [DisplayName("Description*")]
        public string Description { get; set; }

        public string VariantDescription { get; set; }

        [MinValueStrict(0, "{0} must be greater than zero")]
        [Required]
        [DisplayName("Quantity*")]
        public decimal Quantity { get; set; }

        [MinValueStrict(0, "{0} must be greater than zero")]
        [Required]
        [DisplayName("UnitPrice*")]
        public decimal UnitPrice { get; set; }

        [MinValue(0, "{0} must be greater than or equal to zero")]
        [MaxValueStrict(100, "{0} must be less than 100")]
        public decimal DiscountPercentage { get; set; }
    }
}
