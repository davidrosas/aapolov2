﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class EstadisticaJugador
    {
        public int Cant_AbiertoPalermo { get; set; }

        public int Cant_Gol { get; set; }

        public int Cant_Final { get; set; }

        public int Cant_Copa { get; set; }
        
    }
}
