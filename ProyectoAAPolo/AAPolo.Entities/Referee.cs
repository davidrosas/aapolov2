﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class Referee
    {
        [Key]
        public int Codigo { get; set; }

        public Persona Persona { get; set; }

        public bool EsActivo { get; set; }

        public int CantidadEvaluados { get; set; }

        public int PromedioPuntaje { get; set; }

        public int SumaPuntaje { get; set; }

        public int Ranking { get; set; }

        public Evaluacion Evaluacion { get; set; }

    }

}
