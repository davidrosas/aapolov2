﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AAPolo.Entities
{
    public class Adjunto
    {
        [Key]
        public int Codigo { get; set; }

        public string Descripcion { get; set; }

        public string UrlAdjunto { get; set; }

        public HttpPostedFileBase Archivo { get; set; }

        public TipoAdjunto tipoAdjunto { get; set; }

        public int Orden { get; set; }

        public bool EsActivo { get; set; }

        public Persona PersonaAlta { get; set; }

        public bool PermiteEliminar { get; set; }
    }
}
