﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AAPolo.Entities
{
    public class PuntoXClub: Club
    {
        public int Cod_Canje { get; set; }

        public CategoriaAccion CategoriaAccion { get; set; }

        public Temporada Temporada { get; set; }

        public int Puntos { get; set; }

        public string Accion { get; set; }

        public string Premio { get; set; }

        public HttpPostedFileBase ImagenEvidencia { get; set; }

        public string UrlImagenEvidencia { get; set; }

        public int AnioTemporada { get; set; }

        public bool EsActivoCanje { get; set; }

    }

    public class CategoriaAccion
    {
        [Key]
        public int Codigo { get; set; }

        public string Nombre { get; set; }
    }

    public class Temporada
    {
        [Key]
        public int Codigo { get; set; }

        public string Nombre { get; set; }
    }

}
