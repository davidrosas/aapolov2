﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class reporteJugadorDTO
    {
        public string JugadorID { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string TipoDoc { get; set; }
        public string Documento { get; set; }
        public string TipoJugador { get; set; }
        public string FecNac { get; set; }
        public string Nacionalidad { get; set; }
        public string Genero { get; set; }
        public string Activo { get; set; }
        public string Mail { get; set; }
        public string Telefono { get; set; }
        public string Club { get; set; }
        public string ClubRepublica { get; set; }
        public string ClubSec1 { get; set; }
        public string ClubSec2 { get; set; }
        public string ClubSec3 { get; set; }
        public string ClubSec4 { get; set; }
        public string TipoHandicapPrincipal { get; set; }
        public string CategoriaHandicapPrincipal { get; set; }
        public string HandicapPrincipal { get; set; }
        public string TipoHandicapSecundario { get; set; }
        public string CategoriaHandicapSecundario { get; set; }
        public string HandicapSecundario { get; set; }
        public string Domicilio { get; set; }
        public string TipoDomicilio { get; set; }
        public string PaisDomicilio { get; set; }
        public string ProvinciaDomicilio { get; set; }
        public string MunicipioDomicilio { get; set; }
        public string Piso { get; set; }
        public string NroDomicilio { get; set; }        
        public string Dpto { get; set; }
        public string CodPostal { get; set; }
        public string Veedor { get; set; }
        public string AdminClub { get; set; }
        public string Instagram { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string cod_historial_pago { get; set; }
        public string fec_alta { get; set; }
        public string TipoPago { get; set; }
        public string txt_url_comprobante { get; set; }
        public int sn_pago { get; set; }
        public decimal importe { get; set; }
        public string fec_ult_pago { get; set; }
        public string FechaAlta { get; set; }

    }
}
