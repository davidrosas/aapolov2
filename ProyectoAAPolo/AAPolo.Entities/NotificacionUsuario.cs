﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class NotificacionUsuario
    {
        public int Codigo { get; set; }

        public bool EsLeido { get; set; }

        public DateTime FechaEntrega { get; set; }

        public Notificacion Notificacion { get; set; }

        public Persona PersonaDestinatario { get; set; }

        public int TotalNoLeido { get; set; }

        public int Indice { get; set; }

    }
}
