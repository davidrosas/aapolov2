﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AAPolo.Entities.Validators;

namespace AAPolo.Entities
{
    public class Order
    {
        #region Constructor
        public Order()
        {
            OrderID = "";
            OrderNumber = "";
        }
        #endregion

        [Required]
        public DateTime Date { get; set; }

        [MinValueStrict(0, "{0} must be greater than zero")]
        [Required]
        public decimal Total { get { return this.OrderItems.Select(s => s.UnitPrice).Sum(); } }

        [MinValue(0, "{0} must be greater than or equal to zero")]
        public decimal TotalDiscount { get; set; }

        [MinValue(0, "{0} must be greater than or equal to zero")]
        public decimal PaidTotal { get { return this.Total; } }

        [MinValue(0, "{0} must be greater than or equal to zero")]
        public decimal FinancialSurcharge { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string OrderID { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string OrderNumber { get; set; }

        [Required]
        public Customer Customer { get; set; }

        public bool CancelOrder { get; set; }

        [Required]
        public IList<OrderItem> OrderItems { get; set; }

        //public ShippingDto Shipping { get; set; }

        public CashPayment CashPayment { get; set; }

        public IList<Payment> Payments { get; set; }
    }
}
