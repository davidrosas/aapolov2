﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class Partido
    {
        [Key]
        public int Codigo { get; set; }

        public Torneo Torneo { get; set; }

        public Equipo EquipoLocal { get; set; }

        public Equipo EquipoVisitante { get; set; }

        public DateTime FechaPartido { get; set; }

        public string strFechaPartido { get; set; }

        public Referee Referee { get; set; }

        public Persona Veedor { get; set; }

        public Instancia Intancia { get; set; }

        public bool EsActivo { get; set; }

        public Zona Zona { get; set; }

        public Zona ZonaLocal { get; set; }

        public Zona ZonaVisitante { get; set; }

        public Copa Copa { get; set; }

        public int Cancha { get; set; }

        public string Observacion { get; set; }

        public List<Persona> lPersonasGol { get; set; }

        public List<Persona> lPersonasIncidencia { get; set; }

        public Equipo EquipoGanador { get; set; }

        public Equipo EquipoPerdedor { get; set; }

        public Equipo EquipoDeSigPartido { get; set; }

        public int NroOrdenSiguientePartido { get; set; }

    }
}
