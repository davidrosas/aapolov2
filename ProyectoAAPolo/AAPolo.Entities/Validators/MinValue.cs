﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AAPolo.Entities.Validators.ValidatorsHelper;

namespace AAPolo.Entities.Validators
{
    public class MinValueAttribute : ValidationAttribute
    {
        private readonly double MinValue;

        public MinValueAttribute(double minValue, string errorMessage)
        {
            MinValue = minValue;
            ErrorMessage = errorMessage;
        }

        public override bool IsValid(object value)
        {
            if (value != null)
            {
                return GetDecimal(value) >= (decimal)MinValue;
            }
            else
            {
                return true;
            }
        }
    }
}
