﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AAPolo.Entities.Validators.ValidatorsHelper;

namespace AAPolo.Entities.Validators
{
    public class MaxValueStrictAttribute : ValidationAttribute
    {
        private readonly double MaxValue;

        public MaxValueStrictAttribute(double maxValue, string errorMessage)
        {
            MaxValue = maxValue;
            ErrorMessage = errorMessage;
        }

        public override bool IsValid(object value)
        {
            if (value != null)
            {
                return GetDecimal(value) < (decimal)MaxValue;
            }
            else
            {
                return true;
            }
        }
    }
}
