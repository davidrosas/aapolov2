﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class TipoEstado
    {
        [Key]
        public int Codigo { get; set; }

        public string NombreTipoEstado { get; set; }
    }
}
