﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class ConfiguracionAPP
    {

        /*Estado de persona*/
        public static int Estado_Persona_Activo = 1;
        public static int Estado_Persona_Inactivo = 2;

        /*Rol*/
        public static int Rol_Administrador = 1;
        public static int Rol_Jugador = 2;

        /*Tipo de contacto*/
        public static int TipoContacto_Telefono = 1;
        public static int TipoContacto_Celular = 2;
        public static int TipoContacto_Email = 3;
        public static int TipoContacto_WEB = 4;
        public static int TipoContacto_FAX = 5;
        public static int TipoContacto_Telefono_Titular = 6;
        public static int TipoContacto_Telefono_Suplente = 7;
        
        /*Tipo domicilio*/
        public static int TipoDomicilio_Domicilio = 1;
        public static int TipoDomicilio_Sede = 2;
        public static int TipoDomicilio_Club = 3;

        /*Tipo domicilio / País*/
        public static int Pais_Argentina = 33;

        public static int Adjunto_Imagen = 1;
        public static int Adjunto_PDF = 2;
        public static int Adjunto_Video = 3;

        /*Tipo genero*/
        public static int Genero_Masculino = 1;
        public static int Genero_Femenino = 2;

        /*Tipo Resultado Mercado pago*/
        public static int Pago_Satisfactorio = 1;
        public static int Pago_EnEspera = 2;
        public static int Pago_Cancelado = 3;
        public static int Pago_PorOtroMedio = 4;

        /*Medios de pago*/
        public static int MedioDePago_MercadoPago = 1;
        public static int MedioDePago_Transferencia = 2;
        public static int MedioDePago_Efectivo = 3;

        /*Categoria Handicap*/
        public static int Categoria_Handicap_Total = 1;
        public static int Categoria_Handicap_Federado = 2;
        public static int Categoria_Handicap_Membresia = 3;
        public static int Categoria_Handicap_Basico = 52;

        /*Vigencia Handicap*/
        public static int Vigencia_Anual = 1;
        public static int Vigencia_Temporal = 2;

        /*Club*/
        public static int Clud_NoInformado = 1539;

        /*Beneficiario de polo - members*/
        public static int Cod_TipoBeneficiario_PoloTodos = 1;
        public static int Cod_TipoBeneficiario_PoloMembers = 2;
        public static int Cod_TipoBeneficiario_PoloPlayers = 3;

        /*Tipos de pago*/
        public static int TipoPago_Handicap = 1;
        public static int TipoPago_Club = 2;
        public static int TipoPago_Torneo = 3;

        /*Instancia*/
        public static int Instancia_Zona = 1;
        public static int Instancia_Cuartos = 3;
        public static int Instancia_Semifinal = 4;
        public static int Instancia_Final = 5;

        /*Tipo de zona*/
        public static int TipoZona_3Equipo = 1;
        public static int TipoZona_4Equipo = 2;

    }
}
