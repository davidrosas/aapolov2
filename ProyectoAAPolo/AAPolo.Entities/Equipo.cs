﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class Equipo
    {
        [Key]
        public int Codigo { get; set; }

        public string NombreEquipo { get; set; }

        public Torneo torneo { get; set; }

        public Club club { get; set; }

        public string strImporte { get; set; }

        public List<Persona> lPersona { get; set; }

        public DetallePago detallePago { get; set; }

        public Zona Zona { get; set; }

        public int ResultadoEquipo { get { return (this.lPersona != null && this.lPersona.Count > 0 ? (this.lPersona.Sum(s => s.Gol) > this.ResultadoEquipoGol ? this.lPersona.Sum(s => s.Gol) : this.ResultadoEquipoGol) : 0); } }

        public int SumaHandicap
        {
            get
            {
                if (this.lPersona != null && this.lPersona.Count > 0) {
                    try
                    {
                        return (this.lPersona.Sum(s => s.HandicapPersona.NroHandicap));
                    }
                    catch (Exception)
                    {
                        return 0;
                    }
                }
                return 0;
            }
        }

        public int ResultadoEquipoGol { get; set; }

        public int TotalGolZona { get; set; }

        public bool EsGanador { get; set; }

        public int PartidosGanados { get; set; }
    }
}
