﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class reporteCuentaTorneo
    {
        public int cod_torneo { get; set; }
        public int cod_equipo { get; set; }
        public int cod_club { get; set; }
        public int cod_usuario { get; set; }
        public string Torneo { get; set; }
        public string Equipo { get; set; }
        public string Club { get; set; }
        public string NombreJugador { get; set; }
        public string ApellidoJugador { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string JugadorID { get; set; }
        public int? cod_historial_pago { get; set; }
        public string txt_url_comprobante { get; set; }
        public int? sn_pago { get; set; }
        public decimal? importe { get; set; }
        public int CantTorneos { get; set; }
        public DateTime? fec_ult_pago { get; set; }
    }
}
