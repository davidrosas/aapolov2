﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AAPolo.Entities
{
    public class Torneo
    {
        [Key]
        public int Codigo { get; set; }

        public string NombreTorneo { get; set; }

        public DateTime FechaInicio { get; set; }

        public string strFechaInicio { get; set; }

        public bool InscripcionActiva { get; set; }

        public string strCostoInscripcion { get; set; }

        public CategoriaHandicap categoriaHandicap { get; set; }

        public string DescripcionTorneo { get; set; }

        public bool EsActivo { get; set; }

        public bool EsDisponibleZona { get; set; }

        public bool EsDisponibleLlave { get; set; }

        public bool EsDisponiblePartidoLlave { get; set; }

        public bool PermiteSuplente { get; set; }

        public bool PermiteInscipcionIndividual { get; set; }

        public bool PermiteMenores { get; set; }

        public CategoriaTorneo categoriaTorneo { get; set; }

        public string Url_Imagen { get; set; }

        public HttpPostedFileBase Imagen { get; set; }

        public string Url_ArchivoDocumento { get; set; }

        public HttpPostedFileBase ArchivoDocumento { get; set; }

        public TipoTorneo tipoTorneo { get; set; }

        public string strFechaInicioInscripcion { get; set; }

        public DateTime FechaInicioInscripcion { get; set; }

        public string strFechaFinInscripcion { get; set; }

        public DateTime FechaFinInscripcion { get; set; }

        public int Handicap_Max_XEquipo { get; set; }

        public int Handicap_Min_XEquipo { get; set; }

        public int Handicap_Max_XJugador { get; set; }

        public int Handicap_Min_XJugador { get; set; }

        public int Handicap_JugadorMin { get; set; }

        public Persona UsuarioAlta { get; set; }

        public bool PermiteMenor { get; set; }

        public ProductoTorneo ProductoTorneo { get; set; }

        public int CantZona { get; set; }

        public Copa Copa { get; set; }

    }
}
