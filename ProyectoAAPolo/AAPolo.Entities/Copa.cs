﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class Copa
    {
        [Key]
        public int Codigo { get; set; }

        public string Nombre { get; set; }

        public Torneo Torneo { get; set; }

        public bool EsPrincipal { get; set; }

        public List<Equipo> lEquipo { get; set; }

        public List<Partido> lPartido { get; set; }

        public List<Zona> lZona { get; set; }

        public bool EsActivo { get; set; }

    }
}
