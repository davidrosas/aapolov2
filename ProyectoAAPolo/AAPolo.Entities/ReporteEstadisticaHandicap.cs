﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class ReporteEstadisticaHandicap
    {
        public int TotalJugadores { get; set; }

        public int TotalPagaronEsteAnio { get; set; }

        public int TotalNoPagaronEsteAnio { get { return this.TotalJugadores - this.TotalPagaronEsteAnio; } }

    }
}
