﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class Pais
    {

        [Key]
        [Required(ErrorMessage = "Disculpe, debe seleccionar país.")]
        [Range(0, int.MaxValue, ErrorMessage = "Disculpe, debe seleccionar un país válido.")]
        public int Codigo { get; set; }

        public string Nombre { get; set; }

        public string NombreAbrev { get; set; }

        public string UrlImagen { get; set; }

    }
}
