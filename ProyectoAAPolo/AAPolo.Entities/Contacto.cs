﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class Contacto
    {
        [Key]
        public int Codigo { get; set; }

        public int Cod_ContactoUsuario { get; set; }
        
        [Required(ErrorMessage = "Disculpe, debe ingresar un dato válido.")]
        public string Nombre { get; set; }

        public TipoContacto TipoContacto { get; set; }

    }
}
