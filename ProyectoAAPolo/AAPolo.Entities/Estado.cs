﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class Estado
    {
        [Key]
        public int Codigo { get; set; }

        public string NombreEstado { get; set; }

        public TipoEstado TipoEstado { get; set; }
    }
}
