﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AAPolo.Entities
{
    public class Club
    {
        [Key]
        public int Codigo { get; set; }

        public string NombreClub { get; set; }

        public int NumeroCancha { get; set; }

        public int NroSocios { get; set; }

        public string CludID { get; set; }

        public DateTime FechaAlta { get; set; }

        public DateTime Fecha_Ultima_Modificacion { get; set; }

        public bool EsActivo { get; set; }

        public CategoriaClub Categoria { get; set; }

        public Persona PersonaAdmin { get; set; }

        public string strPresidente { get; set; }

        public string strVicepresidente { get; set; }

        public string strTesorero { get; set; }

        public string strVocal1 { get; set; }

        public string strVocal2 { get; set; }

        public string strVocal3 { get; set; }

        public List<Domicilio> lDomicilio{ get; set; }

        public string strFechaFundacion { get; set; }

        public DateTime FechaFundacion { get; set; }

        public string Historia { get; set; }

        public string Facebook { get; set; }

        public string Twitter { get; set; }

        public string Instagram { get; set; }

        public HttpPostedFileBase Imagen { get; set; }

        public string UrlImagen { get; set; }

        public List<Contacto> lContacto { get; set; }

        public int Anios {
            get {
                int edad = DateTime.Today.Year - this.FechaFundacion.Year;

                if (DateTime.Today < this.FechaFundacion.AddYears(edad))
                    return --edad;
                else
                    return edad;
               
            }
        }

        public bool AfiliacionVenciada { get; set; }

        public DateTime FechaPagoAfilicion { get; set; }

        public string strFechaPagoAfilicion { get; set; }

        public DetallePago detallePago { get; set; }

        public bool EsDestacado { get; set; }

    }

    public class CategoriaClub {

        [Key]
        public int Codigo { get; set; }

        public string NombreCategoria { get; set; }

        public string NombreCategoriaAbreviado { get; set; }

        public decimal Importe { get; set; }

        public string strImporte { get; set; }

        public string strFechaPago { get; set; }

        public DateTime FechaPago { get; set; }

    }

    public class ClubUsuario : Club {

        [Key]
        public int CodigoClubUsuario { get; set; }

        public bool EsPrincipal { get; set; }

        public bool EsRechazado { get; set; }

        public string strFechaVigencia { get; set; }

        public DateTime FechaVigencia { get; set; }

        public string strFechaAltaUsuarioClub { get; set; }

        public DateTime FechaAltaUsuarioClub { get; set; }
        
        public bool EsRepublica { get; set; }
    }

}
