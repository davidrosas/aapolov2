﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class reporteTorneo
    {
        public ttorneo Torneo { get; set; }
        public tequipo Equipo { get; set; }
        public tclub Club { get; set; }
        public List<EquipoUserReporte> Usuarios { get; set; }
        public int? sn_pago { get; set; }
        public decimal? importe { get; set; }
        public DateTime? fec_ult_pago { get; set; }
        public int? cod_historial_pago { get; set; }
        public string txt_url_comprobante { get; set; }
        public bool InscripcionInvididual { get; set; }

    }

    public class EquipoUserReporte
    {
        public int cod_usuario { get; set; }
        public string txt_nombre { get; set; }
        public string txt_apellido { get; set; }
        public int? bool_Capitan { get; set; }
        public int? bool_suplente { get; set; }
        public string sexo { get; set; }
        public string email { get; set; }
        public string telefono { get; set; }
        public int? handicap_principal { get; set; }
        public int? handicap_secundario { get; set; }
        public string fec_nacimiento { get; set; }
    }

    public class ttorneo
    {
        public int cod_torneo { get; set; }


        public string txt_desc { get; set; }


        public DateTime? fec_comienzo { get; set; }

        public int cod_categoria_torneo { get; set; }


        public string txt_sexo { get; set; }


        public string txt_edad_desc { get; set; }

        public int? nro_handicap_equipo_minimo { get; set; }

        public int? nro_handicap_equipo_maximo { get; set; }

        public int? nro_handicap_jugador_minimo { get; set; }

        public int? nro_handicap_jugador_maximo { get; set; }


        public DateTime? fec_alta { get; set; }


        public DateTime? fec_ult_modificacion { get; set; }


        public string usuario_alta { get; set; }

        public bool? bool_inscripcion { get; set; }

        public int? nro_handicap_especifico { get; set; }

        public decimal? nro_precio_inscripcion { get; set; }

        public int? cod_categoria_handicap { get; set; }

        public string txt_desc_larga { get; set; }

        public string txt_imagen { get; set; }

        public bool? bool_activo { get; set; }

        public bool? bool_suplente { get; set; }

        public bool? bool_menores { get; set; }

        public bool? bool_inscripcion_individual { get; set; }

    }

    public class tequipo
    {
        public int cod_equipo { get; set; }

        public string txt_desc { get; set; }

        public DateTime fec_alta { get; set; }

        public string txt_alta { get; set; }

        public DateTime? fec_ultima_modificacion { get; set; }

        public string txt_ultima_modificación { get; set; }

        public int cod_torneo { get; set; }

        public int? cod_club { get; set; }

    }


    public partial class tclub
    {
        public int cod_club { get; set; }

        public int? cod_categoria_club { get; set; }

        public string txt_desc { get; set; }

        public string cod_club_desc { get; set; }

        public int? nro_canchas { get; set; }

        public string txt_presidente { get; set; }

        public string txt_vicepresidente { get; set; }

        public DateTime? fec_alta { get; set; }

        public DateTime? fec_ult_modificacion { get; set; }

        public string txt_vocal1 { get; set; }

        public string txt_vocal2 { get; set; }

        public string txt_vocal3 { get; set; }

        public string txt_tesorero { get; set; }

        public int? cod_domicilio { get; set; }

        public string usuario_alta { get; set; }

        public DateTime? fec_fundacion { get; set; }

        public string txt_historia { get; set; }

        public int? cod_admin_club { get; set; }

        public int? nro_cant_socios { get; set; }

        public bool? bool_activo { get; set; }
        
        public string ClubID { get; set; }
        
        public string txt_facebook { get; set; }
        
        public string txt_twitter { get; set; }
        
        public string txt_instagram { get; set; }

        public string txt_imagen { get; set; }
        
    }
}
