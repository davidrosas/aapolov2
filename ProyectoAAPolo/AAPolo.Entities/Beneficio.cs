﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AAPolo.Entities
{
    public class Beneficio
    {
        [Key]
        public int Codigo { get; set; }

        public string Nombre { get; set; }

        public DateTime FechaInicio { get; set; }

        public DateTime FechaFin { get; set; }

        public string strFechaInicio { get; set; }

        public string strFechaFin { get; set; }

        public string Descripcion { get; set; }

        public HttpPostedFileBase Imagen { get; set; }

        public string UrlImagen { get; set; }

        public CategoriaBeneficio CategoriaBeneficio { get; set; }

        public TipoBeneficiario TipoBeneficiario { get; set; }

        public List<PrecioXBeneficio> lstPrecioXBeneficios { get; set; }

        public bool EsActivo { get; set; }

        public bool EsDestacado { get; set; }

        public string UrlMarca { get; set; }

        public string Condicion { get; set; }

    }

    public class PrecioXBeneficio {

        public TipoBeneficiario TipoBeneficiario { get; set; }

        public string Precio { get; set; }

    }

}
