﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class Pago
    {
        [Key]
        public int Codigo { get; set; }

        public string Detalle { get; set; }

        public string strImporte { get; set; }

        public DateTime Fecha { get; set; }

        public string strFecha { get; set; }

        public DateTime FechaVencimiento { get; set; }

        public string strFechaVencimiento { get; set; }

        public Persona persona { get; set; }

        public Persona personaAlta { get; set; }

        public Club club { get; set; }

    }
}
