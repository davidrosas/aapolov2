﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class Notificacion
    {
        [Key]
        public int Codigo { get; set; }

        [Required(ErrorMessage = "Disculpe, debe ingresar un título para la notificación")]
        public string Titulo { get; set; }

        [Required(ErrorMessage = "Disculpe, debe ingresar un mensaje para la notificación")]
        public string Mensaje { get; set; }

        [Required(ErrorMessage = "Disculpe, debe ingresar la hora de envío de la notificación")]
        public string strHoraEnvio { get; set; }

        [Required(ErrorMessage = "Disculpe, debe ingresar la fecha de envío de la notificación")]
        public string strFechaEnvio { get; set; }

        public DateTime FechaHoraEnvio { get; set; }

        public DateTime FechaEnviado { get; set; }

        public Persona PersonaAlta { get; set; }

        public List<Rol> lRol { get; set; }

        public List<TipoJugador> lTipoJugador { get; set; }

        public List<CategoriaHandicap> lCategoriaHandicap { get; set; }

        [Required(ErrorMessage = "Disculpe, debe ingresar rango de handicap para la notificación")]
        [Range(minimum:0, maximum:10, ErrorMessage = "Disculpe, el valor debe ser entre 0 y 10")]
        public int HandicapDesde { get; set; }

        [Required(ErrorMessage = "Disculpe, debe ingresar rango de handicap para la notificación")]
        [Range(minimum: 0, maximum: 10, ErrorMessage = "Disculpe, el valor debe ser entre 0 y 10")]
        public int HandicapHasta { get; set; }

        public bool EsActivo { get; set; }

        public bool EsEnviado { get; set; }

        public TipoNotificacion TipoNotificacion { get; set; }

        public Persona PersonaDestinatario { get; set; }
        
    }
}
