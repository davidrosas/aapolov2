﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class ResultadoTransaccion
    {
        public int CodigoResultado { get; set; }

        public string Mensaje { get; set; }

        public string MensajeRequerimiento { get; set; }

        public string MensajeError { get; set; }

        public bool EsSatisfactorio { get; set; }

        public bool Bandera { get; set; }

        public DateTime Fecha { get; set; }

        public string strFecha { get; set; }

        public object obj { get; set; }

        public string Vista { get; set; }

        public string Cadena { get; set; }
    }
}
