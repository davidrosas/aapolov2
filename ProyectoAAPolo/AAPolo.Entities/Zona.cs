﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class Zona
    {
        [Key]
        public int Codigo { get; set; }

        public Torneo Torneo { get; set; }

        public TipoZona TipoZona { get; set; }

        public List<Equipo> lEquipo { get; set; }

        public List<Partido> lPartido { get; set; }

        public int NroPartido { get; set; }

        public string LetraZona { get; set; }
    }
}
