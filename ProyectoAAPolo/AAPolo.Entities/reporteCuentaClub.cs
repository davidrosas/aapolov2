﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class reporteCuentaClub
    {
        public int cod_club { get; set; }
        public string Club { get; set; }
        public int cod_categoria_club { get; set; }
        public string CategoriaClub { get; set; }
        public int? UsuarioAdmin { get; set; }
        public string NombreAdmin { get; set; }
        public string ApellidoAdmin { get; set; }
        public int? sn_pago { get; set; }
        public decimal? importe { get; set; }
        public DateTime? fec_ult_pago { get; set; }
        public decimal? ImporteCat { get; set; }
        public string TipoPago { get; set; }
        public string txt_url_comprobante { get; set; }
        public string ClubID { get; set; }

        public int NroCanchas { get; set; }
        public string Presidente { get; set; }
        public string VicePresidente { get; set; }
        public string FecAlta { get; set; }
        public string Vocal1 { get; set; }
        public string Vocal2 { get; set; }
        public string Vocal3 { get; set; }
        public string Tesorero { get; set; }
        public string FecFundacion { get; set; }
        public string Historia { get; set; }
        public int CantSocios { get; set; }
        public string BoolActivo { get; set; }
        public string Mail { get; set; }
        public string Teléfono { get; set; }
        public string TeléfonoClub { get; set; }
        public string Calle { get; set; }
        public string Numero { get; set; }
        public string Pais { get; set; }
        public string Provincia { get; set; }
        public string Ciudad { get; set; }
        public string Departamento { get; set; }
        public string CodPostal { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
    }
}
