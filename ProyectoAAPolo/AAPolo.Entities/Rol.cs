﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class Rol
    {
        [Key]
        public int Codigo { get; set; }

        [Display(Name = "Rol")]
        public string NombreRol { get; set; }

        public Estado Estado { get; set; }
    }
}
