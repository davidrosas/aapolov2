﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class Sancion
    {
        [Key]
        public int Codigo { get; set; }

        public Persona Persona { get; set; }

        public string DescripcionSancion { get; set; }

        public string DescripcionSancionResumen
        {
            get
            {
                if (String.IsNullOrEmpty(this.DescripcionSancion))
                    this.DescripcionSancion = "";
                return (this.DescripcionSancion.Length>100? this.DescripcionSancion.Substring(0,100): this.DescripcionSancion);
            }
        }

        public string CodigoAritculo { get; set; }

        public DateTime Fecha { get; set; }

        public string strFecha { get; set; }

        public bool EsActivo { get; set; }

    }
}
