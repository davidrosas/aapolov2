﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class TipoJugador
    {
        [Key]
        public int Codigo { get; set; }

        public string Nombre { get; set; }

        public int EdadDesde { get; set; }

        public int EdadHasta { get; set; }
    }
}
