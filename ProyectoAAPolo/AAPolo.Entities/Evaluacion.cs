﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class Evaluacion
    {
        [Key]
        public int Codigo { get; set; }

        public Persona Veedor { get; set; }

        public Torneo Torneo { get; set; }

        public Equipo EquipoLocal { get; set; }

        public Equipo EquipoVisitante { get; set; }

        public Partido PartidoEvaluado { get; set; }

        public Instancia Instancia { get; set; }

        public NivelPolo Nivel { get; set; }

        public ZonaPartido ZonaPartido { get; set; }

        public DateTime FechaPartido { get; set; }

        public DateTime FechaEvaluacion { get; set; }

        public decimal Puntaje { get; set; }

        public List<PreguntasEvaluacion> lstPregunta { get; set; }

        public string Comentario { get; set; }

    }
}
