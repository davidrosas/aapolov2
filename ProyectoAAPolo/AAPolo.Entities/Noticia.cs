﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AAPolo.Entities
{
    public class Noticia
    {
        [Key]
        public int Codigo { get; set; }

        public string Titulo { get; set; }

        public string strFechaPublicacion { get; set; }

        public DateTime FechaPublicacion { get; set; }

        public string Copete { get; set; }

        [AllowHtml]
        public string CuerpoNoticia { get; set; }

        public bool EsActivo { get; set; }

        public bool EsDestacado { get; set; }

        public TipoNoticia TipoNoticia { get; set; }

        public List<Adjunto> lAdjunto { get; set; }

        public List<Tag> lTags { get; set; }

        public Persona PersonaAdmin { get; set; }
    }
}
