﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AAPolo.Entities
{
    public class DetallePago
    {
        [Key]
        public int Codigo { get; set; }

        public string Detalle { get; set; }
        
        public string strFechaPago { get; set; }

        public DateTime FechaTransaccion { get; set; }

        public string strFechaTransaccion { get; set; }

        public DateTime FechaVencimiento { get; set; }

        public string strFechaVencimiento { get; set; }

        public string strImporte { get; set; }

        public bool PagadoEsteAnio { get; set; }

        public Estado estado { get; set; }

        public Persona personaAlta { get; set; }

        public string collection_id { get; set; }

        public string ReferenciaExterna { get; set; }

        public MedioDePago medioDePago { get; set; }

        public HttpPostedFileBase Imagen { get; set; }

        public bool EsPagoEnProceso { get; set; }

        public string Url_Imagen { get; set; }

        public Persona persona { get; set; }

        public Club club { get; set; }

        public TipoPago TipoPago { get; set; }
    }

    public class TipoPago {

        [Key]
        public int Codigo { get; set; }

        public string Nombre { get; set; }

    }

}
