﻿using AAPolo.Entities.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class CashPayment
    {
        #region Constructor
        public CashPayment()
        {
            PaymentID = 0;
            PaymentMethod = "";
            PaymentTotal = 0;
        }
        #endregion

        [Required]
        [MinValueStrict(0, "{0} must be greater than zero")]
        public long PaymentID { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string PaymentMethod { get; set; }

        [Required]
        [MinValueStrict(0, "{0} must be greater than zero")]
        public decimal PaymentTotal { get; set; }
    }
}
