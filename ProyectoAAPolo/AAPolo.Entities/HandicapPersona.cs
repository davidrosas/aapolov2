﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class HandicapPersona: Handicap
    {
        [Key]
        public int CodigoHandicapPersona { get; set; }

        public bool EsPrincipal { get; set; }

        public int NroHandicap { get; set; }

        public int NroHandicapSecundario { get; set; }

        public string strFechaDesde { get; set; }

        public string strFechaHasta { get; set; }

        public string strFechaPago { get; set; }

        public bool EsVencido { get; set; }

        public DetallePago detallePago { get; set; }

        public bool EsPase { get; set; }

        public string DescripcionHandicapUsuario { get; set; }

    }
}
