﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AAPolo.Entities
{

    public class UsuarioLogin {

        public UsuarioLogin()
        {

        }

        public UsuarioLogin(Persona p){
            this.Codigo = p.Codigo;
            this.Nombre = p.Nombre;
            this.Apellido = p.Apellido;
            this.DocumentoIdentidad = p.DocumentoIdentidad;
            this.TipoDocumento = p.TipoDocumento;
            this.Rol = p.Rol;
            this.EsActivo = p.EsActivo;
            this.AceptaTerminosCondiciones = p.AceptaTerminosCondiciones;
            this.UrlFoto = p.UrlFoto;
            this.EsVeedor = p.EsVeedor;
            this.EsReferee = p.EsReferee;
            this.EsMember = p.EsMember;
        }

        [Key]
        public int Codigo { get; set; }

        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public string DocumentoIdentidad { get; set; }

        public string Password { get; set; }

        public TipoDocumento TipoDocumento { get; set; }

        public Rol Rol { get; set; }

        public bool EsActivo { get; set; }

        public bool AceptaTerminosCondiciones { get; set; }

        public string UrlFoto { get; set; }

        public bool EsVeedor { get; set; }

        public bool EsReferee { get; set; }

        public bool EsMember { get; set; }


    }

    public class Persona: ResultadoTransaccion
    {
        [Key]
        public int Codigo { get; set; }

        [Required(ErrorMessage = "Disculpe, debe ingresar nombre")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Disculpe, debe ingresar apellido")]
        public string Apellido { get; set; }

        public string NombreCompleto { get { return this.Nombre + " " + this.Apellido; } }

        public string ApellidoNombreCompleto { get { return this.Apellido + " " + this.Nombre; } }

        [Required(ErrorMessage = "Disculpe, debe ingresar documento")]
        public string DocumentoIdentidad { get; set; }

        public string Password { get; set; }

        public string PasswordConfirmar { get; set; }
        
        public TipoDocumento TipoDocumento { get; set; }

        public Estado Estado { get; set; }

        public Genero Genero { get; set; }

        public Pais Pais { get; set; }
        
        public Rol Rol { get; set; }

        public DateTime FechaNacimiento { get; set; }

        [Required(ErrorMessage = "Disculpe, debe ingresar fecha de nacimiento")]
        public string strFechaNacimiento { get; set; }

        public DateTime FechaIngreso { get; set; }

        public HttpPostedFileBase Foto { get; set; }

        public string UrlFoto { get; set; }

        public bool EsVeedor { get; set; }

        public bool EsMember { get; set; }

        public bool AceptaTermino { get; set; }

        public string IdentificadorUsuario { get; set; }

        public bool EsActivo { get; set; }

        public bool EsCapitan { get; set; }

        public bool EsSuplente { get; set; }

        public bool EsDestacado { get; set; }

        public TipoJugador TipoJugador { get; set; }

        public HandicapPersona HandicapPersona { get; set; }

        public int Cod_Hand { get; set; }

        public int Cod_HandTransicion { get; set; }

        public HandicapPersona HandicapPersonaSecundario { get; set; }

        public ClubUsuario clubUsuarioPrincipal { get; set; }

        public ClubUsuario clubUsuarioRepublica { get; set; }

        public List<ClubUsuario> lClubUsuarioSecundario { get; set; }

        public List<Contacto> lContacto { get; set; }

        public List<Domicilio> lDomicilio { get; set; }

        public string Twitter { get; set; }

        public string Instagram { get; set; }

        public string Facebook { get; set; }

        public string JugadorID { get; set; }

        public int Edad {
            get
            {
                int edad = DateTime.Today.Year - this.FechaNacimiento.Year;

                if (this.FechaNacimiento != null) {
                    if (DateTime.Today < this.FechaNacimiento.AddYears(edad))
                        return --edad;
                    else
                        return edad;
                }
                return 0;
            }
        }

        public int EdadTipoJugador {
            get
            {
                if (this.FechaNacimiento != null) {
                    return DateTime.Today.Year - this.FechaNacimiento.Year;
                }
                return 0;
            }
        }

        public Vigencia vigencia { get; set; }

        public CategoriaHandicap categoriaHandicap { get; set; }

        public Persona PersonaTramite { get; set; }

        public string ReferenciaExterna { get; set; }

        public bool PermitePase { get; set; }

        public bool HandicapPago { get; set; }

        public bool InscriptoEnTorneo { get; set; }

        public string MsjInscripcionTorneo { get; set; }

        public string strFechaAlta { get; set; }

        public DateTime FechaAlta { get; set; }

        public bool AceptaTerminosCondiciones { get; set; }
        
        public EstadisticaJugador EstadisticaJugador { get; set; }

        public bool EsReferee { get; set; }

        public TipoReferee TipoReferee { get; set; }

        public int Gol { get; set; }

        public Incidencia Incidencia { get; set; }

        public string IdRegistroMobile { get; set; }

        public string PlataformaMobile { get; set; }

    }

    public class Miembro : Persona {

        public string NumeroTarjeta { get; set; }

        public bool EsMiembroJugador { get; set; }

        public string Correo { get; set; }

        public string TelefonoMiembro { get; set; }

        public string TipoMiembro { get; set; }
    }

    public class Genero
    {
        [Key]
        [Required(ErrorMessage = "Disculpe, debe seleccionar género.")]
        [Range(0, int.MaxValue, ErrorMessage = "Disculpe, debe seleccionar un género válido.")]
        public int Codigo { get; set; }

        public string Nombre { get; set; }

    }

    public class TipoDocumento {

        [Key]
        [Required(ErrorMessage = "Disculpe, debe seleccionar tipo de documento.")]
        [Range(0, int.MaxValue, ErrorMessage = "Disculpe, debe seleccionar un tipo de documento válido.")]
        public int Codigo { get; set; }

        public string Nombre { get; set; }
    }

}
