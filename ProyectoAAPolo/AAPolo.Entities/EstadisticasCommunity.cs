﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class EstadisticasCommunity
    {
        public int TotalPlayers { get; set; }

        public int TotalMembers { get; set; }

        public int Total { get { return (this.TotalPlayers + this.TotalMembers); } }
    }
}
