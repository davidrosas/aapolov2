﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class TangoResultadoTransaccion
    {
        public int Status { get; set; }

        public string Message { get; set; }

        public object Data { get; set; }

        public bool isOk { get; set; }

    }
}
