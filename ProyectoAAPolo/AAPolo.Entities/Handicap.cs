﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class Handicap
    {
        [Key]
        public int Codigo { get; set; }

        public int CodigoHandicapDesde { get; set; }

        public int CodigoHandicapTransicion { get; set; }

        public string Nombre { get; set; }

        public string Vigencia { get; set; }

        public Pais Nacionalidad { get; set; }

        public string Edad { get; set; }

        public decimal Importe { get; set; }

        public string strImporte { get; set; }

        public CategoriaHandicap Categoria { get; set; }

        public bool PagoEsteAnio { get; set; }

        public TipoJugador tipoJugador { get; set; }

        public Vigencia vigencia { get; set; }

        public Handicap HandicapTransicion { get; set; }

    }
}
