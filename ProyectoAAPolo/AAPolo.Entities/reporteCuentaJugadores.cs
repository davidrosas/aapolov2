﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class reporteCuentaJugadores
    {
        public int cod_usuario { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int? cod_tipo_jugador { get; set; }
        public string TipoJugador { get; set; }
        public int? cod_handicap { get; set; }
        public string Handicap { get; set; }
        public string CategoriaHandicap { get; set; }
        public int? cod_club { get; set; }
        public string Club { get; set; }
        public string Vigencia { get; set; }
        public int? sn_pago { get; set; }
        public decimal? importe { get; set; }
        public DateTime? fec_ult_pago { get; set; }
        public string Pais { get; set; }
        public decimal? PrecioHandicap { get; set; }
        public int? NroHandicap { get; set; }
        public string Documento { get; set; }
        public int? cod_historial_pago { get; set; }
        public string txt_url_comprobante { get; set; }
        public string JugadorID { get; set; }
        public DateTime? FechaAlta { get; set; }
        public string TipoPago { get; set; }
        public string cod_mercado_pago { get; set; }
    }
}
