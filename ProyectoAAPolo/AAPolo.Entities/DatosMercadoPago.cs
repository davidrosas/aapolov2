﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Entities
{
    public class DatosMercadoPago
    {
        public string ID_Credencial { get; set; }

        public string Clave_Credencial { get; set; }

        public string UrlAPP { get; set; }

        public string UrlNotificacion { get; set; }

        public string UrlSuccess { get; set; }

        public string UrlPending { get; set; }

        public string UrlFailure { get; set; }

        public string Moneda { get; set; }

        public string Referencia_Externa { get; set; }

        public payer payer { get; set; }

        public List<Item> lstItem { get; set; }
    }

    public class Item
    {
        public string title { get; set; }
        public int quantity { get; set; }
        public string currency_id { get; set; }
        public double unit_price { get; set; }
    }

    public class payer
    {

        public string name { get; set; }

        public string surname { get; set; }

        public string email { get; set; }

        public string date_created { get; set; }

        public phone phone { get; set; }

        public identification identification { get; set; }

        public address address { get; set; }

    }

    public class phone
    {
        public string area_code { get; set; }

        public string number { get; set; }
    }

    public class identification
    {
        public string type { get; set; } // Available ID types at https://api.mercadopago.com/v1/identification_types

        public string number { get; set; }
    }

    public class address
    {
        public string street_name { get; set; }

        public string street_number { get; set; }

        public string zip_code { get; set; }
    }
}
