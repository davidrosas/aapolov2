﻿using System.Web;
using System.Web.Optimization;

namespace AAPoloAbiertoDePalermo
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/styles").Include(
                      "~/Content/site.css",
                      "~/Content/css/normalize.css",
                      "~/Content/css/bootstrap.min.css",
                      "~/Content/css/animate.css",
                      "~/Content/css/icons.css",
                      "~/Content/css/select2.css",
                      "~/Content/css/slick.css",
                      "~/Content/css/slick-theme.css",
                      "~/Content/css/jquery.fancybox.min.css",
                      "~/Content/css/main.css",
                      "~/Content/css/main-abierto-palermo.css",
                      "~/Content/css/calendar.css"));

            bundles.Add(new StyleBundle("~/Content/stylesLayout").Include(
                      "~/Content/css/normalize.css",
                      "~/Content/css/bootstrap.min.css",
                      "~/Content/css/animate.css",
                      "~/Content/css/icons.css",
                      "~/Content/css/select2.min.css",
                      "~/Content/css/slick.css",
                      "~/Content/css/slick-theme.css",
                      "~/Content/css/calendar.css",
                      "~/Content/css/jquery.fancybox.min.css",
                      "~/Content/css/main.css",
                      "~/Content/css/abierto-palermo.css"));
        }
    }
}
