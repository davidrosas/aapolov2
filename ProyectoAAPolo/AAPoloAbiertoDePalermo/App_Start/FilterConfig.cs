﻿using System.Web;
using System.Web.Mvc;

namespace AAPoloAbiertoDePalermo
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
