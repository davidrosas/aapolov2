﻿using AAPoloAbiertoDePalermo.Controllers.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAPoloAbiertoDePalermo.Controllers
{
    public class HomeController : ClassHelper
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Resultados()
        {
            return View();
        }

        public ActionResult Posiciones()
        {
            return View();
        }

        public ActionResult Goleadores()
        {
            return View();
        }

        public ActionResult Planteles()
        {
            return View();
        }

        public ActionResult Equipos()
        {
            return View();
        }

        public ActionResult Jugadores()
        {
            return View();
        }

        public ActionResult AAPolo() {
            return Redirect(obtenerTagDelWebConfig("LinkAAPolo"));
        }

        public ActionResult Cashless() {
            return View();
        }

        public ActionResult InfoEvento() {
            return View();
        }

        public ActionResult AfterPolo() {
            return View();
        }

        public ActionResult Faq() {
            return View();
        }

        public ActionResult Resultados2018()
        {
            return View();
        }

        public ActionResult UrlAAPoloRegistroMember() {
            try
            {
                return Redirect(obtenerTagDelWebConfig("LinkAAPoloRegistroMember"));
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Home");
            }
            
        }

    }
}