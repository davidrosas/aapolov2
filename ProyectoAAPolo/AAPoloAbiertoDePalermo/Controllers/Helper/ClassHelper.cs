﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAPoloAbiertoDePalermo.Controllers.Helper
{
    public class ClassHelper : Controller
    {
        public string obtenerTagDelWebConfig(string pNombreTag)
        {
            var AppSettings = ConfigurationManager.AppSettings;
            string key = AppSettings[pNombreTag];
            return key;
        }
    }
}