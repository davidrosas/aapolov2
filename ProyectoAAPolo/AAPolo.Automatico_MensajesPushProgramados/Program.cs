﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using AAPolo.Entities;
using AAPolo.Aplication;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;

namespace AAPolo.Automatico_MensajesPushProgramados
{
    class Program
    {
        public static string obtenerTagDelWebConfig(string pNombreTag)
        {
            var AppSettings = ConfigurationManager.AppSettings;
            string key = AppSettings[pNombreTag];
            return key;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Inciando procesos de envío de notificaciones. AAPOLO {0}", DateTime.Now);

            List<NotificacionUsuario> lNotif = new NotificacionSM().BuscarNotificacionAEnviar(0).Where(x => !String.IsNullOrEmpty(x.PersonaDestinatario.IdRegistroMobile) && !String.IsNullOrEmpty(x.PersonaDestinatario.PlataformaMobile)).ToList();
            string IdAplicacion = "";
            int ContNotificacionFallida = 0;
            Console.WriteLine("Total de notificaciones a enviar: {0}", lNotif.Count());

            try
            {
                IdAplicacion = obtenerTagDelWebConfig("IdAplicacion");
                object data;

                foreach (var item in lNotif)
                {
                    try
                    {
                        //----------------------------------------------------------------------------------------------------
                        WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                        tRequest.Method = "post";
                        tRequest.ContentType = "application/json";

                        if (item.PersonaDestinatario.PlataformaMobile.ToUpper() == "ANDROID")
                        {
                            data = new
                            {
                                to = item.PersonaDestinatario.IdRegistroMobile,
                                data = new
                                {
                                    body = item.Notificacion.Mensaje,
                                    title = item.Notificacion.Titulo,
                                    style = "inbox",
                                    message = item.Notificacion.Mensaje,
                                    sound = "default",
                                    badge = 1,
                                }
                            };
                        }
                        else
                        {
                            data = new
                            {
                                to = item.PersonaDestinatario.IdRegistroMobile,
                                notification = new
                                {
                                    body = item.Notificacion.Mensaje,
                                    title = item.Notificacion.Titulo,
                                    style = "inbox",
                                    message = item.Notificacion.Mensaje,
                                    sound = "default",
                                    badge = 1,
                                }
                            };
                        }
                        //----------------------------------------------------------------------------------------------------

                        var serializer = new JavaScriptSerializer();
                        var json = serializer.Serialize(data);
                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        tRequest.Headers.Add(string.Format("Authorization: key={0}", IdAplicacion));
                        tRequest.ContentLength = byteArray.Length;

                        using (Stream dataStream = tRequest.GetRequestStream())
                        {
                            dataStream.Write(byteArray, 0, byteArray.Length);
                            using (WebResponse tResponse = tRequest.GetResponse())
                            {
                                using (Stream dataStreamResponse = tResponse.GetResponseStream())
                                {
                                    using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                    {
                                        string str = tReader.ReadToEnd();
                                    }
                                }
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Execion con notificación " + JsonConvert.SerializeObject(e, Formatting.Indented));
                        ContNotificacionFallida++;
                        //Console.ReadLine();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Execion con carga de listado de notificaciones " + JsonConvert.SerializeObject(e, Formatting.Indented));
                //Console.ReadLine();
            }
            Console.WriteLine("Total de notificaciones enviadas {0} - Total de notificaciones fallidas: {1} \n", (lNotif.Count-ContNotificacionFallida), ContNotificacionFallida);
            Console.WriteLine("Finalización de servicio de envío de notificaciones. {0} \n", DateTime.Now);
            //Console.ReadLine();
        }
    }
}
