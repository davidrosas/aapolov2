﻿using AAPolo.Data.Utilidades;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Data
{
    public class JugadorAD
    {

        public List<JugadorHandicap> BuscarJugadorHandicap(int Cod_TipoJugador, int Cod_Handicap, int Cod_CategoriaHandicap)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<JugadorHandicap> lJugadorHandicap = new List<JugadorHandicap>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoJugador",
                    Value = Cod_TipoJugador
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Handicap",
                    Value = Cod_Handicap
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_CategoriaHandicap",
                    Value = Cod_CategoriaHandicap
                });
                
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_tipo_jugador_handicap", lstParametro);

                while (miDataReader.Read())
                {
                    lJugadorHandicap.Add(new JugadorHandicap()
                    {
                        handicap = new Handicap() {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Handicap"]),
                            Nombre = miDataReader["NombreHandicap"].ToString(),
                            strImporte = miDataReader["Costo"].ToString(),
                            Categoria = new CategoriaHandicap() {
                                Codigo = Convert.ToInt32(miDataReader["Cod_Categoria_Handicap"]),
                                Nombre = miDataReader["NombreCategoriaHandicap"].ToString(),
                            },
                            tipoJugador = new TipoJugador()
                            {
                                Codigo = Convert.ToInt32(miDataReader["Cod_Tipo_Jugador"]),
                                Nombre = miDataReader["NombreTipoJugador"].ToString()
                            },
                            vigencia = new Vigencia() {
                                Codigo = Convert.ToInt32(miDataReader["Cod_Vigencia"]),
                                Nombre = miDataReader["Vigencia"].ToString()
                            }
                        },
                        tipoJugador = new TipoJugador() {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Tipo_Jugador"]),
                            Nombre = miDataReader["NombreTipoJugador"].ToString()
                        }
                    });
                }

                Conn.Close();
                return lJugadorHandicap;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
