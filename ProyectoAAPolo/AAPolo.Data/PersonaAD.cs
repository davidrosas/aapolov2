﻿using AAPolo.Data.Utilidades;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace AAPolo.Data
{
    public class PersonaAD
    {
        public string ToSHA256(string value)
        {
            SHA256 sha256 = SHA256.Create();

            byte[] hashData = sha256.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder returnValue = new StringBuilder();

            for (int i = 0; i < hashData.Length; i++)
            {
                returnValue.Append(hashData[i].ToString());
            }

            return returnValue.ToString();
        }

        public Persona BuscarUsuarioLogin(Persona p)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            Persona per = new Persona();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Documento",
                    Value = p.DocumentoIdentidad.ToLower()
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Password",
                    Value = this.ToSHA256(p.Password)
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@IdRegistroMobile",
                    Value = String.IsNullOrEmpty(p.IdRegistroMobile) ? "" : p.IdRegistroMobile
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@PlataformaMobile",
                    Value = String.IsNullOrEmpty(p.PlataformaMobile)? "" : p.PlataformaMobile
                });

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_usuario_login", lstParametro);

                while (miDataReader.Read())
                {
                    per.Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]);
                    per.DocumentoIdentidad = miDataReader["DocumentoIdentidad"].ToString();
                    per.Nombre = miDataReader["Nombre"].ToString();
                    per.Apellido = miDataReader["Apellido"].ToString();
                    per.AceptaTerminosCondiciones = Convert.ToInt32(miDataReader["sn_AceptaTerminos"]) == 1 ? true : false;
                    per.EsVeedor = Convert.ToInt32(miDataReader["sn_Veedor"]) == 1 ? true : false;
                    per.EsReferee = Convert.ToInt32(miDataReader["sn_Referee"]) == 1 ? true : false;
                    per.EsMember = Convert.ToInt32(miDataReader["sn_Member"]) == 1 ? true : false;
                    per.Rol = new Rol()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Rol"]),
                        NombreRol = miDataReader["NombreRol"].ToString()
                    };
                    per.EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false;
                    /*per.Estado = new Estado()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Estado"]),
                        NombreEstado = miDataReader["NombreEstado"].ToString()
                    };*/
                    per.UrlFoto = miDataReader["Url_Foto"].ToString();
                }

                Conn.Close(); 
                return per;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Persona BuscarUsuarioLoginPasswordEncriptado(Persona p)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            Persona per = new Persona();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Documento",
                    Value = p.DocumentoIdentidad.ToLower()
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Password",
                    Value = p.Password
                });

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_usuario_login", lstParametro);

                while (miDataReader.Read())
                {
                    per.Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]);
                    per.DocumentoIdentidad = miDataReader["DocumentoIdentidad"].ToString();
                    per.Nombre = miDataReader["Nombre"].ToString();
                    per.Apellido = miDataReader["Apellido"].ToString();
                    per.AceptaTerminosCondiciones = Convert.ToInt32(miDataReader["sn_AceptaTerminos"]) == 1 ? true : false;
                    per.EsVeedor = Convert.ToInt32(miDataReader["sn_Veedor"]) == 1 ? true : false;
                    per.EsReferee = Convert.ToInt32(miDataReader["sn_Referee"]) == 1 ? true : false;
                    per.Rol = new Rol()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Rol"]),
                        NombreRol = miDataReader["NombreRol"].ToString()
                    };
                    per.EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false;
                    per.UrlFoto = miDataReader["Url_Foto"].ToString();
                }

                Conn.Close();
                return per;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Persona BuscarUsuarioSidepro(Persona p)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            Persona per = new Persona();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = p.Codigo
                });

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_usuario_login_sidepro", lstParametro);

                while (miDataReader.Read())
                {
                    per.Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]);
                    per.DocumentoIdentidad = miDataReader["DocumentoIdentidad"].ToString();
                    per.Password = miDataReader["Password"].ToString();
                    per.EsVeedor = Convert.ToBoolean(miDataReader["sn_Veedor"]);
                    per.AceptaTerminosCondiciones = Convert.ToBoolean(miDataReader["sn_AceptaTerminos"]);
                    per.EsActivo = Convert.ToBoolean(miDataReader["EsActivo"]);
                    per.Rol = new Rol() { Codigo = Convert.ToInt32(miDataReader["Cod_Rol"]) };
                }

                Conn.Close();
                return per;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion AceptaTerminosCondiciones(int Cod_Persona, string telefono, string email)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Persona
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@telefono",
                    Value = telefono
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@email",
                    Value = email
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_AceptaTerminosCondiciones", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GurdarImgPerfil(string urlImg, int Cod_Persona)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Persona
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@UrlImg",
                    Value = urlImg
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_ImagenPerfilUsuario", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GurdarImgCertificado(string urlImg, int Cod_Persona)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Persona
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@UrlImg",
                    Value = urlImg
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_ImgagenCertificadoUsuario", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Persona BuscarPersonaPorClaveVerificacion(string ClaveVerificacion)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            Persona per = new Persona();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@ClaveVerificacion",
                    Value = ClaveVerificacion
                });

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_DatosJugador", lstParametro);

                while (miDataReader.Read())
                {
                    per.Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]);
                    per.DocumentoIdentidad = miDataReader["DocumentoIdentidad"].ToString();
                    per.Nombre = miDataReader["Nombre"].ToString();
                    per.Apellido = miDataReader["Apellido"].ToString();
                    /*
                    per.Rol = new Rol()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Rol"]),
                        NombreRol = miDataReader["NombreRol"].ToString()
                    };*/
                    per.EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false;
                    /*per.Estado = new Estado()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Estado"]),
                        NombreEstado = miDataReader["NombreEstado"].ToString()
                    };*/
                    //per.UrlFoto = miDataReader["Url_Foto"].ToString();
                }

                Conn.Close();
                return per;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambioDePassword(string DocumentoIdentidad)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Documento",
                    Value = DocumentoIdentidad
                });
                #endregion

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_ins_ClaveVerificacionUsuario", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                    if (result.EsSatisfactorio)
                    {
                        new ExtensionUtils().enviarMailGmail(miDataReader["Email"].ToString(), "AAPolo - Cambio de contraseña", new ExtensionUtils().CambioDeContraseña(miDataReader["ClaveVerificacion"].ToString()));
                    }
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarPasswordJugador(Persona obj)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = obj.Codigo
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Password",
                    Value = ExtensionMethods.ToSHA256(obj.Password)
                });
                #endregion

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_PasswordJugador", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscaJugador(Persona obj)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Persona> lper = new List<Persona>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = obj.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Documento",
                    Value = String.IsNullOrEmpty(obj.DocumentoIdentidad) ? "" : obj.DocumentoIdentidad
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Nombre",
                    Value = String.IsNullOrEmpty(obj.Nombre) ? "" : obj.Nombre
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Apellido",
                    Value = String.IsNullOrEmpty(obj.Apellido) ? "" : obj.Apellido
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_usuario", lstParametro);

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["FechaNacimiento"].ToString()) ? miDataReader["FechaNacimiento"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]);

                    lper.Add(new Persona()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]),
                        JugadorID = miDataReader["JugadorID"].ToString(),
                        DocumentoIdentidad = miDataReader["DocumentoIdentidad"].ToString(),
                        TipoDocumento = new TipoDocumento()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_TipoDocumento"]),
                            Nombre = miDataReader["NombreTipoDocumento"].ToString(),
                        },
                        Nombre = miDataReader["Nombre"].ToString(),
                        Apellido = miDataReader["Apellido"].ToString(),
                        UrlFoto = miDataReader["Url_Foto"].ToString(),
                        Genero = new Genero()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Genero"]),
                            Nombre = miDataReader["NombreGenero"].ToString()
                        },
                        Rol = new Rol() { Codigo = Convert.ToInt32(miDataReader["Cod_Rol"]) },
                        FechaNacimiento = new DateTime(Anio, Mes, Dia),
                        Facebook = miDataReader["Facebook"].ToString(),
                        Twitter = miDataReader["Twitter"].ToString(),
                        Instagram = miDataReader["Instagram"].ToString(),
                        EsVeedor = Convert.ToInt32(miDataReader["sn_Veedor"]) == 1 ? true : false,
                        EsReferee = Convert.ToInt32(miDataReader["EsReferee"]) == 1 ? true : false,
                        TipoReferee = new TipoReferee() { Codigo = Convert.ToInt32(miDataReader["Cod_Tipo_Referee"]) },
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        AceptaTerminosCondiciones = Convert.ToInt32(miDataReader["sn_AceptaTerminos"]) == 1 ? true : false,
                        EsMember = Convert.ToInt32(miDataReader["EsMember"]) == 1 ? true : false,
                        Pais = new Pais()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Pais"]),
                            Nombre = miDataReader["NombrePais"].ToString()
                        },
                        TipoJugador = new TipoJugador
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_TipoJugador"])
                        },
                        lDomicilio = new List<Domicilio>(){
                            new Domicilio() {
                                Codigo = Convert.ToInt32(miDataReader["Cod_Domicilio"]),
                                tipoDomicilio = new TipoDomicilio(){ Codigo = ConfiguracionAPP.TipoDomicilio_Domicilio },
                                DireccionDomicilio = miDataReader["DireccionDomicilio"].ToString(),
                                Departamento = miDataReader["DepartamentoDomicilio"].ToString(),
                                NroDomicilio = Convert.ToInt32(miDataReader["NroDomicilio"]),
                                NroPiso = Convert.ToInt32(miDataReader["NroPisoDomicilio"]),
                                CodigoPostal = miDataReader["NroCodigoPostalDomicilio"].ToString(),
                                pais = new Pais() {
                                    Codigo = Convert.ToInt32(miDataReader["Cod_PaisDomicilio"])
                                },
                                provincia= new Provincia() {
                                    Codigo = Convert.ToInt32(miDataReader["Cod_ProvinciaDomicilio"])
                                },
                                municipio = new Municipio() {
                                    Codigo = Convert.ToInt32(miDataReader["Cod_MunicipioDomicilio"])
                                }
                            }
                        },
                        HandicapPersona = new HandicapPersona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_HandicapPrincipal"]),
                            NroHandicap = Convert.ToInt32(miDataReader["NroHandicap"]),
                            Nombre = miDataReader["CategoriaHandicap"].ToString(),
                            Categoria = new CategoriaHandicap(),
                            EsPrincipal = true,
                        },
                        HandicapPersonaSecundario = new HandicapPersona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_HandicapPrincipal"]),
                            NroHandicap = Convert.ToInt32(miDataReader["NroHandicapSecundario"]),
                            Nombre = miDataReader["CategoriaHandicap"].ToString(),
                            Categoria = new CategoriaHandicap(),
                            EsPrincipal = false,
                        },
                        lClubUsuarioSecundario = BuscarClubPersona(Convert.ToInt32(miDataReader["Cod_Usuario"]))
                    });
                    lper.LastOrDefault().lContacto = new List<Contacto>();
                    lper.LastOrDefault().lContacto.Add(new Contacto()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Contacto_Mail"]),
                        Nombre = miDataReader["Mail"].ToString(),
                        TipoContacto = new TipoContacto()
                        {
                            Codigo = ConfiguracionAPP.TipoContacto_Email
                        }
                    });
                    lper.LastOrDefault().lContacto.Add(new Contacto()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Contacto_Celular"]),
                        Nombre = miDataReader["Celular"].ToString(),
                        TipoContacto = new TipoContacto()
                        {
                            Codigo = ConfiguracionAPP.TipoContacto_Celular
                        }
                    });
                }

                Conn.Close();
                return lper;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<ClubUsuario> BuscarClubPersona(int Cod_Persona)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<ClubUsuario> lClub = new List<ClubUsuario>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Persona
                });

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_UsuarioClub", lstParametro);

                while (miDataReader.Read())
                {
                    lClub.Add(new ClubUsuario()
                    {
                        CodigoClubUsuario = Convert.ToInt32(miDataReader["Cod_UsuarioClub"]),
                        Codigo = Convert.ToInt32(miDataReader["Cod_Club"]),
                        EsPrincipal = Convert.ToInt32(miDataReader["bool_principal"]) == 1 ? true : false,
                        EsRepublica = Convert.ToInt32(miDataReader["bool_republica"]) == 1 ? true : false,
                        EsRechazado = Convert.ToInt32(miDataReader["bool_rechazado"]) == 1 ? true : false,
                        NombreClub = miDataReader["NombreClub"].ToString()
                    });
                }

                Conn.Close();
                return lClub;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscaJugadorResumen(int Cod_Persona, string Cadena, int Cod_TipoJugador, int Cod_Club, int Nro_Handicap, int Cod_Genero)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Persona> lper = new List<Persona>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Persona
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NombreUsuarioDocumento",
                    Value = Cadena
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoJugador",
                    Value = Cod_TipoJugador
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Club",
                    Value = Cod_Club
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Nro_Handicap",
                    Value = Nro_Handicap
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Genero",
                    Value = Cod_Genero
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_jugador", lstParametro);

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["FechaNacimiento"].ToString()) ? miDataReader["FechaNacimiento"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]);

                    lper.Add(new Persona()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]),
                        DocumentoIdentidad = miDataReader["DocumentoIdentidad"].ToString(),
                        Nombre = miDataReader["Nombre"].ToString(),
                        Apellido = miDataReader["Apellido"].ToString(),
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        EsDestacado = Convert.ToInt32(miDataReader["EsDestacado"]) == 1 ? true : false,
                        FechaNacimiento = new DateTime(Anio, Mes, Dia),
                        strFechaAlta = miDataReader["FechaAlta"].ToString(),
                        UrlFoto = miDataReader["url_Foto"].ToString(),
                        TipoDocumento = new TipoDocumento()
                        {
                            Nombre = miDataReader["NombreTipoDocumento"].ToString(),
                        },
                        Genero = new Genero()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Genero"]),
                            Nombre = miDataReader["NombreGenero"].ToString()
                        },
                        Pais = new Pais()
                        {
                            Nombre = miDataReader["NombrePais"].ToString()
                        },
                        TipoJugador = new TipoJugador
                        {
                            Nombre = miDataReader["TipoJugador"].ToString()
                        },
                        HandicapPersona = new HandicapPersona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Handicap"]),
                            Nombre = miDataReader["TipoHandicap"].ToString(),
                            NroHandicap = Convert.ToInt32(miDataReader["NroHandicap"]),
                            Categoria = new CategoriaHandicap()
                            {
                                Nombre = miDataReader["CategoriaHandicap"].ToString()
                            },
                            EsPrincipal = Convert.ToInt32(miDataReader["EsHandicapPrincipal"]) == 1 ? true : false,
                            EsVencido = Convert.ToInt32(miDataReader["EsVencidoPagoHandicap"]) == 1 ? true : false,
                        },
                        HandicapPersonaSecundario = new HandicapPersona()
                        {
                            NroHandicap = Convert.ToInt32(miDataReader["NroHandicapSecundario"])
                        },
                        clubUsuarioPrincipal = new ClubUsuario()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Club"]),
                            NombreClub = miDataReader["Club"].ToString()
                        }
                    });
                }

                Conn.Close();
                return lper.GroupBy(x => x.Codigo).Select(s => s.FirstOrDefault()).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscaJugadorDetalleResumen(int Cod_Persona, string Cadena, int Cod_TipoJugador, int Cod_Club, int Nro_Handicap, int Cod_Genero)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Persona> lper = new List<Persona>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Persona
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NombreUsuarioDocumento",
                    Value = Cadena
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoJugador",
                    Value = Cod_TipoJugador
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Club",
                    Value = Cod_Club
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Nro_Handicap",
                    Value = Nro_Handicap
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Genero",
                    Value = Cod_Genero
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_jugador_detalle_resumen", lstParametro);

                while (miDataReader.Read())
                {
                    lper.Add(new Persona()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]),
                        DocumentoIdentidad = miDataReader["DocumentoIdentidad"].ToString(),
                        Nombre = miDataReader["Nombre"].ToString(),
                        Apellido = miDataReader["Apellido"].ToString(),
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        EsDestacado = Convert.ToInt32(miDataReader["EsDestacado"]) == 1 ? true : false,
                        EsMember = Convert.ToInt32(miDataReader["EsMember"]) == 1 ? true : false,
                        Pais = new Pais()
                        {
                            Nombre = miDataReader["NombrePais"].ToString()
                        },
                        TipoJugador = new TipoJugador
                        {
                            Nombre = miDataReader["TipoJugador"].ToString()
                        },
                        HandicapPersona = new HandicapPersona()
                        {
                            Nombre = miDataReader["TipoHandicap"].ToString(),
                            NroHandicap = Convert.ToInt32(miDataReader["NroHandicap"]),
                            Categoria = new CategoriaHandicap()
                            {
                                Nombre = miDataReader["CategoriaHandicap"].ToString()
                            },
                            
                        },
                        clubUsuarioPrincipal = new ClubUsuario()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Club"]),
                            NombreClub = miDataReader["Club"].ToString()
                        }
                    });
                }

                Conn.Close();
                return lper;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscaJugadorResumenPerfilPublico(int Cod_Persona, string Cadena, int Cod_TipoJugador, int Cod_Club, int Nro_Handicap, int Cod_Genero)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Persona> lper = new List<Persona>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Persona
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NombreUsuarioDocumento",
                    Value = Cadena
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoJugador",
                    Value = Cod_TipoJugador
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Club",
                    Value = Cod_Club
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Nro_Handicap",
                    Value = Nro_Handicap
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Genero",
                    Value = Cod_Genero
                });
                #endregion

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_jugador_perfil_publico", lstParametro);

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["FechaNacimiento"].ToString()) ? miDataReader["FechaNacimiento"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]);

                    lper.Add(new Persona()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]),
                        DocumentoIdentidad = miDataReader["DocumentoIdentidad"].ToString(),
                        Nombre = miDataReader["Nombre"].ToString(),
                        Apellido = miDataReader["Apellido"].ToString(),
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        EsDestacado = Convert.ToInt32(miDataReader["bool_Destacado"]) == 1 ? true : false,
                        FechaNacimiento = new DateTime(Anio, Mes, Dia),
                        strFechaAlta = miDataReader["FechaAlta"].ToString(),
                        UrlFoto = miDataReader["url_Foto"].ToString(),
                        TipoDocumento = new TipoDocumento()
                        {
                            Nombre = miDataReader["NombreTipoDocumento"].ToString(),
                        },
                        Genero = new Genero()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Genero"]),
                            Nombre = miDataReader["NombreGenero"].ToString()
                        },
                        Pais = new Pais()
                        {
                            Nombre = miDataReader["NombrePais"].ToString(),
                            UrlImagen = miDataReader["UrlImagenPais"].ToString()
                        },
                        TipoJugador = new TipoJugador
                        {
                            Nombre = miDataReader["TipoJugador"].ToString()
                        },
                        HandicapPersona = new HandicapPersona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Handicap"]),
                            Nombre = miDataReader["TipoHandicap"].ToString(),
                            NroHandicap = Convert.ToInt32(miDataReader["NroHandicap"]),
                            Categoria = new CategoriaHandicap()
                            {
                                Nombre = miDataReader["CategoriaHandicap"].ToString()
                            },
                            EsPrincipal = Convert.ToInt32(miDataReader["EsHandicapPrincipal"]) == 1 ? true : false,
                            EsVencido = Convert.ToInt32(miDataReader["EsVencidoPagoHandicap"]) == 1 ? true : false,
                        },
                        HandicapPersonaSecundario = new HandicapPersona()
                        {
                            NroHandicap = Convert.ToInt32(miDataReader["NroHandicapSecundario"])
                        },
                        clubUsuarioPrincipal = new ClubUsuario()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Club"]),
                            NombreClub = miDataReader["Club"].ToString(),
                            UrlImagen = miDataReader["UrlImagenClub"].ToString()
                        },
                        EstadisticaJugador = new EstadisticaJugador()
                        {
                            Cant_AbiertoPalermo = Convert.ToInt32(miDataReader["Cant_AbiertoPalermo"]),
                            Cant_Copa = Convert.ToInt32(miDataReader["Cant_Copa"]),
                            Cant_Final = Convert.ToInt32(miDataReader["Cant_Final"]),
                            Cant_Gol = Convert.ToInt32(miDataReader["Cant_Gol"]),
                        }
                    });
                }

                Conn.Close();
                return lper.GroupBy(x => x.Codigo).Select(s => s.FirstOrDefault()).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscaJugadorPagoHandicap(int Cod_Persona, string Cadena, int Cod_TipoJugador, int Cod_Club, int Nro_Handicap, bool EsPaseHandicap, int Cod_Genero)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Persona> lper = new List<Persona>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Persona
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NombreUsuarioDocumento",
                    Value = Cadena
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@EsPaseHandicap",
                    Value = EsPaseHandicap
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Genero",
                    Value = Cod_Genero
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoJugador",
                    Value = Cod_TipoJugador
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_Get_JugadorPagarHandicap", lstParametro);

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["FechaNacimiento"].ToString()) ? miDataReader["FechaNacimiento"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]);

                    lper.Add(new Persona()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]),
                        DocumentoIdentidad = miDataReader["DocumentoIdentidad"].ToString(),
                        Nombre = miDataReader["Nombre"].ToString(),
                        Apellido = miDataReader["Apellido"].ToString(),
                        strFechaNacimiento = fec,
                        FechaNacimiento = new DateTime(Anio, Mes, Dia),
                        UrlFoto = miDataReader["url_Foto"].ToString(),
                        TipoDocumento = new TipoDocumento()
                        {
                            Nombre = miDataReader["NombreTipoDocumento"].ToString(),
                        },
                        Genero = new Genero()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Genero"]),
                            Nombre = miDataReader["NombreGenero"].ToString()
                        },

                        TipoJugador = new TipoJugador
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_TipoJugador"]),
                            Nombre = miDataReader["TipoJugador"].ToString()
                        },
                        HandicapPersona = new HandicapPersona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Handicap"]),
                            DescripcionHandicapUsuario = miDataReader["DescripcionHandicap"].ToString(),
                            Nombre = miDataReader["TipoHandicap"].ToString(),
                            NroHandicap = Convert.ToInt32(miDataReader["NroHandicap"]),
                            strFechaHasta = Convert.ToString(miDataReader["FechaVencimiento"]),
                            Categoria = new CategoriaHandicap()
                            {
                                Codigo = Convert.ToInt32(miDataReader["Cod_CategoriaHandicap"]),
                                Nombre = miDataReader["CategoriaHandicap"].ToString(),
                            },
                            EsPrincipal = Convert.ToInt32(miDataReader["EsHandicapPrincipal"]) == 1 ? true : false,
                            detallePago = new DetallePago()
                            {
                                estado = new Estado() { Codigo = Convert.ToInt32(miDataReader["nro_resultado_pago"]) }
                            }

                        },
                        clubUsuarioPrincipal = new ClubUsuario()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Club"]),
                            NombreClub = miDataReader["Club"].ToString()
                        },
                        lClubUsuarioSecundario = BuscarClubPersona(Convert.ToInt32(miDataReader["Cod_Usuario"])),
                        HandicapPago = Convert.ToInt32(miDataReader["PermitePase"]) == 1 ? true : false,
                        PermitePase = Convert.ToInt32(miDataReader["CumpleRestriccionPase"]) == 1 ? true : false,
                    });
                }

                Conn.Close();
                return lper.GroupBy(x => x.Codigo).Select(s => s.FirstOrDefault()).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscaJugadorInscripcionTorneo(int Cod_Persona, string Cadena, int Cod_TipoJugador, int Cod_Club, int Nro_Handicap, int Cod_Torneo, int Cod_Genero = 0, bool EsPaseHandicap = false)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Persona> lper = new List<Persona>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Persona
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NombreUsuarioDocumento",
                    Value = Cadena
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@EsPaseHandicap",
                    Value = EsPaseHandicap
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Genero",
                    Value = Cod_Genero
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoJugador",
                    Value = Cod_TipoJugador
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = Cod_Torneo
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_Get_JugadorInscripcionTorneo", lstParametro);

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["FechaNacimiento"].ToString()) ? miDataReader["FechaNacimiento"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]);

                    lper.Add(new Persona()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]),
                        DocumentoIdentidad = miDataReader["DocumentoIdentidad"].ToString(),
                        Nombre = miDataReader["Nombre"].ToString(),
                        Apellido = miDataReader["Apellido"].ToString(),
                        strFechaNacimiento = fec,
                        FechaNacimiento = new DateTime(Anio, Mes, Dia),
                        UrlFoto = miDataReader["url_Foto"].ToString(),
                        TipoDocumento = new TipoDocumento()
                        {
                            Nombre = miDataReader["NombreTipoDocumento"].ToString(),
                        },
                        Genero = new Genero()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Genero"]),
                            Nombre = miDataReader["NombreGenero"].ToString()
                        },

                        TipoJugador = new TipoJugador
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_TipoJugador"]),
                            Nombre = miDataReader["TipoJugador"].ToString()
                        },
                        HandicapPersona = new HandicapPersona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Handicap"]),
                            Nombre = miDataReader["TipoHandicap"].ToString(),
                            NroHandicap = Convert.ToInt32(miDataReader["NroHandicap"]),
                            NroHandicapSecundario = Convert.ToInt32(miDataReader["NroHandicapSecundario"]),
                            strFechaHasta = Convert.ToString(miDataReader["FechaVencimiento"]),
                            Categoria = new CategoriaHandicap()
                            {
                                Codigo = Convert.ToInt32(miDataReader["Cod_CategoriaHandicap"]),
                                Nombre = miDataReader["CategoriaHandicap"].ToString(),
                            },
                            EsPrincipal = Convert.ToInt32(miDataReader["EsHandicapPrincipal"]) == 1 ? true : false,
                            detallePago = new DetallePago()
                            {
                                estado = new Estado() { Codigo = Convert.ToInt32(miDataReader["nro_resultado_pago"]) }
                            }

                        },
                        clubUsuarioPrincipal = new ClubUsuario()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Club"]),
                            NombreClub = miDataReader["Club"].ToString()
                        },
                        lClubUsuarioSecundario = BuscarClubPersona(Convert.ToInt32(miDataReader["Cod_Usuario"])),
                        HandicapPago = Convert.ToInt32(miDataReader["PermitePase"]) == 1 ? true : false,
                        InscriptoEnTorneo = Convert.ToInt32(miDataReader["InscriptoEnTorneo"]) == 1 ? true : false,
                        MsjInscripcionTorneo = miDataReader["MsjInscriptoEnTorneo"].ToString()
                    });
                }

                Conn.Close();
                return lper.GroupBy(x => x.Codigo).Select(s => s.FirstOrDefault()).ToList();
                //return lper.Where(w => !w.InscriptoEnTorneo).GroupBy(x => x.Codigo).Select(s => s.FirstOrDefault()).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Genero> BuscarGenero()
        {
            SqlDataReader miDataReader;
            List<Genero> lGenero = new List<Genero>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();


                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_genero");

                while (miDataReader.Read())
                {
                    lGenero.Add(new Genero()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Genero"]),
                        Nombre = miDataReader["NombreGenero"].ToString()
                    });
                }

                Conn.Close();
                return lGenero;
            }
            catch (Exception e)
            {
                throw e;

            }

        }

        public List<TipoDocumento> BuscarTipoDocumento()
        {
            SqlDataReader miDataReader;
            List<TipoDocumento> lTipoDocumento = new List<TipoDocumento>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();


                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_Tipo_Documento");

                while (miDataReader.Read())
                {
                    lTipoDocumento.Add(new TipoDocumento()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Tipo_Doc"]),
                        Nombre = miDataReader["Nombre"].ToString()
                    });
                }

                Conn.Close();
                return lTipoDocumento;
            }
            catch (Exception e)
            {
                throw e;

            }

        }

        public List<Pais> BuscarPais()
        {
            SqlDataReader miDataReader;
            List<Pais> lPais = new List<Pais>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_pais");

                while (miDataReader.Read())
                {
                    lPais.Add(new Pais()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Pais"]),
                        Nombre = miDataReader["NombrePais"].ToString(),
                        NombreAbrev = miDataReader["abrev"].ToString()
                    });
                }

                Conn.Close();
                return lPais;
            }
            catch (Exception e)
            {
                throw e;

            }

        }

        public List<Provincia> BuscarProvincia(int Cod_Pais)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Provincia> lProvincia = new List<Provincia>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Pais",
                    Value = Cod_Pais
                });

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_provincia", lstParametro);

                while (miDataReader.Read())
                {
                    lProvincia.Add(new Provincia()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Provincia"]),
                        Nombre = miDataReader["NombreProvincia"].ToString(),
                    });
                }

                Conn.Close();
                return lProvincia;
            }
            catch (Exception e)
            {
                throw e;

            }
        }

        public List<Municipio> BuscarMunicipio(int Cod_Provincia)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Municipio> lMunicipio = new List<Municipio>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Provincia",
                    Value = Cod_Provincia
                });

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_Municipio", lstParametro);

                while (miDataReader.Read())
                {
                    lMunicipio.Add(new Municipio()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Municipio"]),
                        Nombre = miDataReader["NombreMunicipio"].ToString(),
                    });
                }

                Conn.Close();
                return lMunicipio;
            }
            catch (Exception e)
            {
                throw e;

            }

        }

        public List<TipoJugador> BuscarTipoJugador()
        {
            SqlDataReader miDataReader;
            List<TipoJugador> lTipoJugador = new List<TipoJugador>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();


                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_tipoJugador");

                while (miDataReader.Read())
                {
                    lTipoJugador.Add(new TipoJugador()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Tipo_Jugador"]),
                        Nombre = miDataReader["Nombre"].ToString(),
                        EdadDesde = Convert.ToInt32(miDataReader["EdadDesde"]),
                        EdadHasta = Convert.ToInt32(miDataReader["EdadHasta"]),
                    });
                }

                Conn.Close();
                return lTipoJugador;
            }
            catch (Exception e)
            {
                throw e;

            }

        }

        public ResultadoTransaccion GuardarJugador(Persona obj, int Cod_Usuario_Alta)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();
            DataTable tablaContacto = new DataTable();
            DataTable tablaClub = new DataTable();
            string NombreAdmin = "", CorreoAdmin = "";
            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Nombre",
                    Value = obj.Nombre
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Apellido",
                    Value = obj.Apellido
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Genero",
                    Value = obj.Genero.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@FechaNacimiento",
                    Value = obj.FechaNacimiento.ToString("yyyyMMdd")
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoDocumento",
                    Value = obj.TipoDocumento.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Documento",
                    Value = obj.DocumentoIdentidad.ToLower()
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Password",
                    Value = ExtensionMethods.ToSHA256(obj.DocumentoIdentidad)
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_PaisOrigen",
                    Value = obj.Pais.Codigo
                });

                /*-------------------------------------------------------------------------------------------------------*/
                tablaContacto.Columns.Add("Codigo", typeof(int));
                tablaContacto.Columns.Add("Descripcion", typeof(string));
                tablaContacto.Columns.Add("CodigoContacto", typeof(int));
                foreach (var item in obj.lContacto)
                {
                    tablaContacto.Rows.Add(item.TipoContacto.Codigo, item.Nombre, item.Codigo);
                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Structured,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@lstContacto",
                    Value = tablaContacto
                });
                /*-------------------------------------------------------------------------------------------------------*/

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@DireccionDomicilio",
                    Value = String.IsNullOrEmpty(obj.lDomicilio.FirstOrDefault().DireccionDomicilio) ? "" : obj.lDomicilio.FirstOrDefault().DireccionDomicilio
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NroDomicilio",
                    Value = obj.lDomicilio.FirstOrDefault().NroDomicilio
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NroPiso",
                    Value = obj.lDomicilio.FirstOrDefault().NroPiso
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Departamento",
                    Value = String.IsNullOrEmpty(obj.lDomicilio.FirstOrDefault().Departamento) ? "" : obj.lDomicilio.FirstOrDefault().Departamento
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@CodigoPostal",
                    Value = String.IsNullOrEmpty(obj.lDomicilio.FirstOrDefault().CodigoPostal) ? "" : obj.lDomicilio.FirstOrDefault().CodigoPostal
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_PaisDomicilio",
                    Value = obj.lDomicilio.FirstOrDefault().pais != null ? obj.lDomicilio.FirstOrDefault().pais.Codigo : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Provincia",
                    Value = obj.lDomicilio.FirstOrDefault().provincia != null ? obj.lDomicilio.FirstOrDefault().provincia.Codigo : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Municipio",
                    Value = obj.lDomicilio.FirstOrDefault().municipio != null ? obj.lDomicilio.FirstOrDefault().municipio.Codigo : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoJugador",
                    Value = obj.TipoJugador== null && obj.EsMember? 0 : obj.TipoJugador.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NroHandicap",
                    Value = obj.EsMember? 0 : obj.HandicapPersona.NroHandicap
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NroHandicapSecundario",
                    Value = obj.EsMember ? 0 : obj.HandicapPersonaSecundario.NroHandicap
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Handicap",
                    Value = obj.EsMember ? 0 : obj.HandicapPersona.Codigo
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_ClubPrincipal",
                    Value = obj.clubUsuarioPrincipal != null ? obj.clubUsuarioPrincipal.Codigo : 0
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_ClubRepublica",
                    Value = obj.clubUsuarioRepublica != null ? obj.clubUsuarioRepublica.Codigo : 0
                });


                /*-------------------------------------------------------------------------------------------------------*/
                tablaClub.Columns.Add("Codigo", typeof(int));
                tablaClub.Columns.Add("CodigoSubTipo", typeof(int));
                if (obj.lClubUsuarioSecundario != null && obj.lClubUsuarioSecundario.Count() > 0)
                {
                    foreach (var item in obj.lClubUsuarioSecundario)
                    {
                        tablaClub.Rows.Add(item.Codigo, 0);
                    }

                }
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Structured,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@lstTipo",
                    Value = tablaClub
                });
                /*-------------------------------------------------------------------------------------------------------*/

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@EsVeedor",
                    Value = obj.EsVeedor ? 1 : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Facebook",
                    Value = String.IsNullOrEmpty(obj.Facebook) ? "" : obj.Facebook
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Twitter",
                    Value = String.IsNullOrEmpty(obj.Twitter) ? "" : obj.Twitter
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Instagram",
                    Value = String.IsNullOrEmpty(obj.Instagram) ? "" : obj.Instagram
                });

                string query = "sp_ins_usuario";
                if (obj.Codigo > 0)
                {
                    query = "sp_upd_usuario";

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_Usuario",
                        Value = obj.Codigo
                    });

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_Domicilio",
                        Value = obj.lDomicilio.FirstOrDefault().Codigo
                    });


                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario_Alta",
                    Value = Cod_Usuario_Alta
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@UrlFoto",
                    Value = obj.UrlFoto
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@EsReferee",
                    Value = obj.EsReferee
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoReferee",
                    Value = obj.EsReferee ? obj.TipoReferee.Codigo : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@EsMember",
                    Value = obj.EsMember ? 1 : 0
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, query, lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                    if (result.EsSatisfactorio && obj.Codigo == 0) {
                        obj.JugadorID = miDataReader["JugadorID"].ToString();
                        obj.TipoDocumento.Nombre = miDataReader["TipoDocumento"].ToString();
                        NombreAdmin = miDataReader["NombreAdmin"].ToString();
                        CorreoAdmin = miDataReader["CorreoAdmin"].ToString();
                    }
                }

                Conn.Close();

                if (result.EsSatisfactorio && obj.Codigo == 0) {
                    new ExtensionUtils().NotificarNuevoUsuario(obj, NombreAdmin, CorreoAdmin);
                }

                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarMember(Persona obj)

        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();
            DataTable tablaContacto = new DataTable();
            DataTable tablaClub = new DataTable();
            string NombreAdmin = "", CorreoAdmin = "";
            string query = "sp_ins_usuarioMember";
            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                if (obj.Codigo > 0)
                {
                    query = "sp_upd_usuario";

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_Usuario",
                        Value = obj.Codigo
                    });
                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Nombre",
                    Value = obj.Nombre
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Apellido",
                    Value = obj.Apellido
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Genero",
                    Value = obj.Genero.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@FechaNacimiento",
                    Value = obj.FechaNacimiento.ToString("yyyyMMdd")
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoDocumento",
                    Value = obj.TipoDocumento.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Pais",
                    Value = obj.Pais.Codigo
                }); 

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@DocumentoIdentidad",
                    Value = obj.DocumentoIdentidad.ToLower()
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Password",
                    Value = ExtensionMethods.ToSHA256(obj.DocumentoIdentidad)
                });

                /*-------------------------------------------------------------------------------------------------------*/
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Email",
                    Value = obj.lContacto.Find(f => f.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Email).Nombre
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Telefono ",
                    Value = obj.lContacto.Find(x => x.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Celular).Nombre
                });


                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, query, lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                    if (result.EsSatisfactorio && obj.Codigo == 0)
                    {
                        /*obj.JugadorID = miDataReader["JugadorID"].ToString();
                        obj.TipoDocumento.Nombre = miDataReader["TipoDocumento"].ToString();
                        NombreAdmin = miDataReader["NombreAdmin"].ToString();
                        CorreoAdmin = miDataReader["CorreoAdmin"].ToString();*/
                    }
                }

                Conn.Close();

                if (result.EsSatisfactorio && obj.Codigo == 0)
                {
                    new ExtensionUtils().NotificarNuevoUsuario(obj, NombreAdmin, CorreoAdmin);
                }

                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarDatosUsuario(Persona obj)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = obj.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@DireccionDomicilio",
                    Value = String.IsNullOrEmpty(obj.lDomicilio.FirstOrDefault().DireccionDomicilio) ? "" : obj.lDomicilio.FirstOrDefault().DireccionDomicilio
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NroDomicilio",
                    Value = obj.lDomicilio.FirstOrDefault().NroDomicilio
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NroPiso",
                    Value = obj.lDomicilio.FirstOrDefault().NroPiso
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Departamento",
                    Value = String.IsNullOrEmpty(obj.lDomicilio.FirstOrDefault().Departamento) ? "" : obj.lDomicilio.FirstOrDefault().Departamento
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@CodigoPostal",
                    Value = String.IsNullOrEmpty(obj.lDomicilio.FirstOrDefault().CodigoPostal) ? "" : obj.lDomicilio.FirstOrDefault().CodigoPostal
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Facebook",
                    Value = String.IsNullOrEmpty(obj.Facebook) ? "" : obj.Facebook
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Twitter",
                    Value = String.IsNullOrEmpty(obj.Twitter) ? "" : obj.Twitter
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Instagram",
                    Value = String.IsNullOrEmpty(obj.Instagram) ? "" : obj.Instagram
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Email",
                    Value = obj.lContacto != null && obj.lContacto.Count() > 0 ? obj.lContacto.Where(x => x.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Email).FirstOrDefault().Nombre : ""
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Celular",
                    Value = obj.lContacto != null && obj.lContacto.Count() > 0 ? obj.lContacto.Where(x => x.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Celular).FirstOrDefault().Nombre : ""
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_DatosUsuario", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarMiembro(Miembro obj)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();
            string query = "sp_ins_DatosUsuarioMiembro";
            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                if (obj.Codigo > 0)
                {
                    query = "sp_upd_DatosUsuarioMiembro";
                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_Usuario",
                        Value = obj.Codigo
                    });

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@DireccionDomicilio",
                        Value = String.IsNullOrEmpty(obj.lDomicilio.FirstOrDefault().DireccionDomicilio) ? "" : obj.lDomicilio.FirstOrDefault().DireccionDomicilio
                    });

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@NroDomicilio",
                        Value = obj.lDomicilio.FirstOrDefault().NroDomicilio
                    });

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@NroPiso",
                        Value = obj.lDomicilio.FirstOrDefault().NroPiso
                    });

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Departamento",
                        Value = String.IsNullOrEmpty(obj.lDomicilio.FirstOrDefault().Departamento) ? "" : obj.lDomicilio.FirstOrDefault().Departamento
                    });

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@CodigoPostal",
                        Value = String.IsNullOrEmpty(obj.lDomicilio.FirstOrDefault().CodigoPostal) ? "" : obj.lDomicilio.FirstOrDefault().CodigoPostal
                    });

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_PaisResidencia",
                        Value = obj.lDomicilio.FirstOrDefault().pais != null ? obj.lDomicilio.FirstOrDefault().pais.Codigo : 0
                    });

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_ProvinciaResidencia",
                        Value = obj.lDomicilio.FirstOrDefault().provincia != null ? obj.lDomicilio.FirstOrDefault().provincia.Codigo : 0
                    });

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_MunicipioResidencia",
                        Value = obj.lDomicilio.FirstOrDefault().municipio != null ? obj.lDomicilio.FirstOrDefault().municipio.Codigo : 0
                    });

                }
                else
                {

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Nombre",
                        Value = obj.Nombre
                    });

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Apellido",
                        Value = obj.Apellido
                    });

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_Genero",
                        Value = obj.Genero.Codigo
                    });

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.DateTime,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@FechaNacimiento",
                        Value = obj.FechaNacimiento
                    });

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_TipoDocumento",
                        Value = obj.TipoDocumento.Codigo
                    });

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@DocumentoIdentidad",
                        Value = obj.DocumentoIdentidad
                    });

                    /*lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_Pais",
                        Value = obj.Pais.Codigo
                    });*/

                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NumeroTarjeta",
                    Value = String.IsNullOrEmpty(obj.NumeroTarjeta)? "": obj.NumeroTarjeta
                });
                
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Email",
                    Value = obj.lContacto != null && obj.lContacto.Count() > 0 ? obj.lContacto.Where(x => x.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Email).FirstOrDefault().Nombre : ""
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Celular",
                    Value = obj.lContacto != null && obj.lContacto.Count() > 0 ? obj.lContacto.Where(x => x.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Celular).FirstOrDefault().Nombre : ""
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, query, lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoJugador(int Codigo, int EstadoJugador)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@EstadoJugador",
                    Value = EstadoJugador
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_EstadoUsuario", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoDestacadoJugador(int Codigo, int EstadoDestacadoJugador)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@EstadoJugador",
                    Value = EstadoDestacadoJugador
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_EstadoDestacadoUsuario", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ActualizarClubPrincipalUsuario(int Cod_Persona, int Cod_Club)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Persona
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Club",
                    Value = Cod_Club
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_ActualizarClubPrincipalUsuario", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
                
        public List<Persona> BuscaVeedor(string DocumentoNombre, int Cod_Usuario)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            //XmlReader miDataReader;
            SqlDataReader miDataReader;
            List<Persona> lper = new List<Persona>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Usuario
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@DocumentoNombre",
                    Value = DocumentoNombre
                });

                #endregion
                //miDataReader = ExtensionMethods.ObtDataReaderXML(Conn, "sp_get_Veedor", lstParametro);
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_Veedor", lstParametro);

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["FechaNacimiento"].ToString()) ? miDataReader["FechaNacimiento"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]);

                    lper.Add(new Persona()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]),
                        DocumentoIdentidad = miDataReader["DocumentoIdentidad"].ToString(),
                        Nombre = miDataReader["Nombre"].ToString(),
                        Apellido = miDataReader["Apellido"].ToString(),
                        //EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        FechaNacimiento = new DateTime(Anio, Mes, Dia)
                    });
                }

                Conn.Close();
                return lper;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        

    }
}
