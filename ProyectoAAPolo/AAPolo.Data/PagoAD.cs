﻿using AAPolo.Data.Utilidades;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Data
{
    public class PagoAD
    {
        public List<Vigencia> BuscarVigencia()
        {
            SqlDataReader miDataReader;
            List<Vigencia> lVigencia = new List<Vigencia>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_Vigencia");

                while (miDataReader.Read())
                {
                    lVigencia.Add(new Vigencia()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Vigencia"]),
                        Nombre = miDataReader["txt_desc"].ToString(),
                    });
                }
                Conn.Close();
                return lVigencia;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<CategoriaHandicap> BuscarCategoriaHandicap(int Cod_TipoJugador)
        {

            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<CategoriaHandicap> lCategoriaHandicap = new List<CategoriaHandicap>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoJugador",
                    Value = Cod_TipoJugador
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_CategoriaHandicap", lstParametro);

                while (miDataReader.Read())
                {
                    lCategoriaHandicap.Add(new CategoriaHandicap()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Codigo"]),
                        Nombre = miDataReader["Descripcion"].ToString(),
                    });
                }
                Conn.Close();
                return lCategoriaHandicap;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Handicap> BuscarCostoHandicap(int Cod_TipoJugador, int Cod_Vigencia, int Cod_CategoriaHandicap)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Handicap> lHandicap = new List<Handicap>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Tipo_Jugador",
                    Value = Cod_TipoJugador
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Vigencia",
                    Value = Cod_Vigencia
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Categoria_Handicap",
                    Value = Cod_CategoriaHandicap
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_CostoHandicap", lstParametro);

                while (miDataReader.Read())
                {
                    lHandicap.Add(new Handicap()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Handicap"]),
                        strImporte = miDataReader["Costo"].ToString()
                    });
                }
                Conn.Close();
                return lHandicap;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ProcesarPagoHandicap(List<Persona> lPersona, string idReference, int Cod_UsuarioTramite)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            DataTable tablaContacto = new DataTable();
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros

                /*-------------------------------------------------------------------------------------------------------*/
                tablaContacto.Columns.Add("Cod_Persona", typeof(int));
                tablaContacto.Columns.Add("Cod_TipoJugador", typeof(int));
                tablaContacto.Columns.Add("Cod_CategoriaHandicap", typeof(int));
                tablaContacto.Columns.Add("Cod_Vigencia", typeof(int));
                tablaContacto.Columns.Add("Cod_Handicap", typeof(string));
                tablaContacto.Columns.Add("Cod_ClubPrincipal", typeof(int));
                tablaContacto.Columns.Add("Cod_UsuarioTransaccion", typeof(int));
                tablaContacto.Columns.Add("ReferenciaExterna", typeof(string));

                foreach (var item in lPersona)
                {
                    tablaContacto.Rows.Add(item.Codigo, item.TipoJugador.Codigo, item.categoriaHandicap.Codigo, item.vigencia.Codigo, item.HandicapPersona.Codigo, item.clubUsuarioPrincipal.Codigo, Cod_UsuarioTramite, idReference);
                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Structured,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@lstPagoHandicap",
                    Value = tablaContacto
                });
                /*-------------------------------------------------------------------------------------------------------*/

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_ins_PagoHandicap", lstParametro);

                while (miDataReader.Read())
                {
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                    result.Mensaje = miDataReader["msj"].ToString();
                    if (result.EsSatisfactorio)
                        result.Cadena = miDataReader["SaldoTotalContado"].ToString();
                }
                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ProcesarPagoHandicapPase(List<Persona> lPersona, string idReference, int Cod_UsuarioTramite)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            DataTable tablaContacto = new DataTable();
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros

                /*-------------------------------------------------------------------------------------------------------*/
                tablaContacto.Columns.Add("Cod_Persona", typeof(int));
                tablaContacto.Columns.Add("Cod_Handicap", typeof(string));
                tablaContacto.Columns.Add("Cod_UsuarioTransaccion", typeof(int));
                tablaContacto.Columns.Add("Cod_HandicapTransicion", typeof(int));
                tablaContacto.Columns.Add("ReferenciaExterna", typeof(string));

                foreach (var item in lPersona)
                {
                    tablaContacto.Rows.Add(item.Codigo, item.HandicapPersona.Codigo, Cod_UsuarioTramite, item.HandicapPersona.CodigoHandicapTransicion, idReference);
                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Structured,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@lstPagoPaseHandicap",
                    Value = tablaContacto
                });
                /*-------------------------------------------------------------------------------------------------------*/

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_ins_PagoPaseHandicap", lstParametro);

                while (miDataReader.Read())
                {
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                    result.Mensaje = miDataReader["msj"].ToString();
                    if (result.EsSatisfactorio)
                        result.Cadena = miDataReader["SaldoTotalContado"].ToString();
                }
                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion PagoAfiliacionClub(int Cod_Club, int Cod_Persona, string ReferenciaExterna)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Club",
                    Value = Cod_Club
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Persona",
                    Value = Cod_Persona
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@ReferenciaExterna",
                    Value = ReferenciaExterna
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_ins_PagoAfiliacionClub", lstParametro);

                while (miDataReader.Read())
                {
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                    result.Mensaje = miDataReader["msj"].ToString();
                    if (result.EsSatisfactorio)
                        result.Cadena = miDataReader["SaldoTotalContado"].ToString();
                }
                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarInscripcionEquipo(Equipo obj, string idReference, int Cod_Persona)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();
            if (obj.lPersona.Count() == 0)
            {
                return result = new ResultadoTransaccion()
                {
                    EsSatisfactorio = false,
                    Mensaje = "Disculpe, debe agregar personas para el equipo a inscribir."
                };
            }
            else {
                foreach (var item in obj.lPersona)
                {
                    if (item.Codigo > 0 && obj.lPersona.Where(x => x.Codigo == item.Codigo).Count() > 1) {
                        return result = new ResultadoTransaccion()
                        {
                            EsSatisfactorio = false,
                            Mensaje = "Disculpe, no debe repetir jugadores en el equipo."
                        };
                    }
                }
            }

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = obj.torneo.Codigo
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Club",
                    Value = obj.torneo.PermiteInscipcionIndividual ? 0 : obj.club.Codigo
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NombreEquipo",
                    Value = (obj.NombreEquipo != null && !String.IsNullOrEmpty(obj.NombreEquipo)) ? obj.NombreEquipo : obj.lPersona.FirstOrDefault().NombreCompleto
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Persona1",
                    Value = obj.lPersona.Count() >= 1 ? obj.lPersona[0].Codigo : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Persona2",
                    Value = obj.lPersona.Count() >= 2 ? obj.lPersona[1].Codigo : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Persona3",
                    Value = obj.lPersona.Count() >= 3 ? obj.lPersona[2].Codigo : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Persona4",
                    Value = obj.lPersona.Count() >= 4 ? obj.lPersona[3].Codigo : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Persona5",
                    Value = obj.lPersona.Count() >= 5 ? obj.lPersona[4].Codigo : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Persona6",
                    Value = obj.lPersona.Count() >= 6 ? obj.lPersona[5].Codigo : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@ReferenciaExterna",
                    Value = idReference
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Persona_Alta",
                    Value = Cod_Persona
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_ins_PagoInscripcionTorneo", lstParametro);

                while (miDataReader.Read())
                {
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                    result.Mensaje = miDataReader["msj"].ToString();
                    if (result.EsSatisfactorio)
                    {
                        result.Cadena = miDataReader["SaldoTotalContado"].ToString();
                    }
                    else
                    {
                        result.CodigoResultado = Convert.ToInt32(miDataReader["CodigoError"]);
                        if (result.CodigoResultado == 3)
                        {
                            result.MensajeRequerimiento = miDataReader["MsjReq"].ToString();
                        }
                    }
                    //result.MensajeRequerimiento = miDataReader["NroHand_Persona4"].ToString(); 
                }
                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;

            }
        }

        public ResultadoTransaccion ActualizarTransacionPagoHandicap(string external_reference, int Cod_Estado, string collection_id)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Handicap> lHandicap = new List<Handicap>();
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@ReferenciaExterna",
                    Value = external_reference
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Estado",
                    Value = Cod_Estado
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@collection_id",
                    Value = collection_id
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_TransacionPagoHandicap", lstParametro);

                while (miDataReader.Read())
                {
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                    result.Mensaje = miDataReader["msj"].ToString();
                }
                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ActualizarTransacionPagoInscripcionEquipo(string external_reference, int Cod_Estado, string collection_id)

        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Handicap> lHandicap = new List<Handicap>();
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@ReferenciaExterna",
                    Value = external_reference
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Estado",
                    Value = Cod_Estado
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@collection_id",
                    Value = collection_id
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_TransacionPagoInscripcionEquipo", lstParametro);

                while (miDataReader.Read())
                {
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                    result.Mensaje = miDataReader["msj"].ToString();
                }
                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ActualizarTransaccionHandicap(string external_reference, int Cod_Estado, string collection_id, int Cod_Persona)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Handicap> lHandicap = new List<Handicap>();
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@ReferenciaExterna",
                    Value = external_reference
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Estado",
                    Value = Cod_Estado
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@collection_id",
                    Value = collection_id
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_TransacionHandicap", lstParametro);

                while (miDataReader.Read())
                {
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                    result.Mensaje = miDataReader["msj"].ToString();
                    if (Cod_Estado == ConfiguracionAPP.Pago_Satisfactorio && Cod_Persona > 0 && result.EsSatisfactorio)
                    {
                        result.obj = Convert.ToBoolean(miDataReader["EnviarMail"]);
                    }
                }
                Conn.Close();

                try
                {
                    if (Cod_Estado == ConfiguracionAPP.Pago_Satisfactorio && Cod_Persona > 0 && result.EsSatisfactorio && Convert.ToBoolean(result.obj) == true)
                    {
                        result.MensajeError = NotificarNuevosJugadoresParaAdministradoresDeClub(external_reference);
                    }
                }
                catch (Exception e)
                {
                    result.MensajeError = e.Message;
                }

                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public string NotificarNuevosJugadoresParaAdministradoresDeClub(string external_reference)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            string result = "", NombreAdminClub = "", MailAdminClub = "", NombreNuevoJugador = "", NombreClub = "";

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@ReferenciaExterna",
                    Value = external_reference
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_TransacionHandicapXReferenciaExterna", lstParametro);

                while (miDataReader.Read())
                {
                    try
                    {
                        NombreAdminClub = miDataReader["NombreAdminClub"].ToString();
                        MailAdminClub = miDataReader["MailAdminClub"].ToString();
                        NombreNuevoJugador = miDataReader["NombreNuevoJugador"].ToString();
                        NombreClub = miDataReader["NombreClub"].ToString();
                        if (!String.IsNullOrEmpty(NombreAdminClub) && !String.IsNullOrEmpty(MailAdminClub) && !MailAdminClub.Equals("noinformado@noinformado.com") && !String.IsNullOrEmpty(NombreNuevoJugador))
                        {
                            new ExtensionUtils().enviarMailGmail(MailAdminClub, "AAPolo - Asociación de nuevo jugador al club: " + NombreClub, new ExtensionUtils().AsociacionDeNuevoJugadorAClub(NombreAdminClub, NombreNuevoJugador, NombreClub));
                        }
                    }
                    catch (Exception e)
                    {
                        result += e.Message;
                    }
                }
                Conn.Close();

                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Pago> BuscarPago(int Cod_Persona)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Pago> lPago = new List<Pago>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Persona",
                    Value = Cod_Persona
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_PagosHandicapClub", lstParametro);

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["Fecha"].ToString()) ? miDataReader["Fecha"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy"),
                        FechaVencimiento = !String.IsNullOrEmpty(miDataReader["FechaVencimiento"].ToString()) ? miDataReader["FechaVencimiento"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]),

                        DiaVencimiento = Convert.ToInt32(FechaVencimiento.Split('/')[0]),
                        MesVencimiento = Convert.ToInt32(FechaVencimiento.Split('/')[1]),
                        AnioVencimiento = Convert.ToInt32(FechaVencimiento.Split('/')[2]);

                    lPago.Add(new Pago()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_HistorialPago"]),
                        strImporte = miDataReader["Importe"].ToString().Replace(".", ","),
                        Detalle = miDataReader["Detalle"].ToString(),
                        Fecha = new DateTime(Anio, Mes, Dia),
                        strFecha = fec,
                        FechaVencimiento = new DateTime(AnioVencimiento, MesVencimiento, DiaVencimiento),
                        strFechaVencimiento = FechaVencimiento,
                        persona = new Persona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]),
                            Nombre = miDataReader["Nombre"].ToString(),
                            Apellido = miDataReader["Apellido"].ToString(),
                        },
                        personaAlta = new Persona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_UsuarioAlta"]),
                            Nombre = miDataReader["NombreAlta"].ToString(),
                            Apellido = miDataReader["ApellidoAlta"].ToString(),
                        },
                        club = new Club()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Club"]),
                            NombreClub = miDataReader["NombreClub"].ToString(),
                            Categoria = new CategoriaClub()
                            {
                                Codigo = Convert.ToInt32(miDataReader["Cod_CategoriaClub"]),
                                NombreCategoria = miDataReader["NombreCategoriaClub"].ToString(),
                            }
                        }
                    });
                }
                Conn.Close();
                return lPago;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Handicap> BuscarHandicapTransicion(int Cod_Handicap)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Handicap> lHandicap = new List<Handicap>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Handicap",
                    Value = Cod_Handicap
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_HandicapTransicion", lstParametro);

                while (miDataReader.Read())
                {
                    lHandicap.Add(new Handicap()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Handicap"]),
                        CodigoHandicapDesde = Convert.ToInt32(miDataReader["Cod_Handicap_desde"]),
                        CodigoHandicapTransicion = Convert.ToInt32(miDataReader["Cod_transicion"]),
                        strImporte = miDataReader["Costo"].ToString(),
                        Nombre = miDataReader["Handicap"].ToString(),
                        Categoria = new CategoriaHandicap()
                        {
                            Codigo = Convert.ToInt32(miDataReader["cod_categoria_handicap"]),
                        },
                        vigencia = new Vigencia()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Vigencia"]),
                            Nombre = miDataReader["Vigencia"].ToString(),
                        }
                    });
                }
                Conn.Close();
                return lHandicap;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<DetallePago> BuscarPagosEnEspera()
        {
            SqlDataReader miDataReader;
            List<DetallePago> lDetallePago = new List<DetallePago>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                //miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_PagosEnEspera");
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_PagosEnEsperaConfirmacion"); /*Uso exclusivo para pruebas y/o confirmaciones por parte del programador*/

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["Fecha"].ToString()) ? miDataReader["Fecha"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy"),
                        FechaVencimiento = !String.IsNullOrEmpty(miDataReader["FechaVencimiento"].ToString()) ? miDataReader["FechaVencimiento"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]),

                        DiaVencimiento = Convert.ToInt32(FechaVencimiento.Split('/')[0]),
                        MesVencimiento = Convert.ToInt32(FechaVencimiento.Split('/')[1]),
                        AnioVencimiento = Convert.ToInt32(FechaVencimiento.Split('/')[2]);

                    lDetallePago.Add(new DetallePago()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Historial_Pagos"]),
                        strImporte = miDataReader["Importe"].ToString().Replace(".", ","),
                        Detalle = miDataReader["Detalle"].ToString(),
                        ReferenciaExterna = miDataReader["ReferenciaExterna"].ToString(),
                        collection_id = miDataReader["collection_id"].ToString(),
                        FechaTransaccion = new DateTime(Anio, Mes, Dia),
                        strFechaTransaccion = fec,
                        FechaVencimiento = new DateTime(AnioVencimiento, MesVencimiento, DiaVencimiento),
                        strFechaVencimiento = FechaVencimiento,
                        TipoPago = new TipoPago { Codigo = Convert.ToInt32(miDataReader["Cod_Tipo_Pago"]) },
                        persona = new Persona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]),
                        },
                        personaAlta = new Persona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_UsuarioAlta"]),
                        },

                    });
                }
                Conn.Close();
                return lDetallePago;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Order> BuscarPagosNoFacturados(int CodPago)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Order> lDetallePago = new List<Order>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Pago",
                    Value = CodPago
                });
                #endregion
                //miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_PagoNoFacturado", lstParametro);
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_PagoNoFacturadoPrueba", lstParametro);

                while (miDataReader.Read())
                {
                    lDetallePago.Add(new Order()
                    {
                        Date = Convert.ToDateTime(miDataReader["fec_alta"]),
                        TotalDiscount = 0,
                        FinancialSurcharge = 0,
                        OrderID = miDataReader["Cod_Pago"].ToString(),
                        OrderNumber = miDataReader["Cod_Pago"].ToString(),
                        Customer = new Customer()
                        {
                            CustomerID = !String.IsNullOrEmpty(miDataReader["JugadorID"].ToString()) ? Convert.ToInt64(miDataReader["JugadorID"]): 0,
                            DocumentType = miDataReader["TipoDocumento"].ToString(),
                            DocumentNumber = miDataReader["Documento"].ToString(),
                            IVACategoryCode = "CF",
                            User = miDataReader["Documento"].ToString(),
                            Email = miDataReader["Correo"].ToString(),
                            FirstName = miDataReader["Nombre"].ToString(),
                            LastName = miDataReader["Apellido"].ToString(),
                            BusinessName = "",
                            Street = "",
                            HouseNumber = "",
                            Floor = "",
                            Apartment = "",
                            City = "",
                            ProvinceCode = "1",//---------------------
                            PostalCode = "",
                            PhoneNumber1 = "",
                            PhoneNumber2 = "",
                            MobilePhoneNumber = "",
                            BusinessAddress = "",
                            Comments = ""
                        },
                        CancelOrder = false,
                        OrderItems = new List<OrderItem>(),// this.BuscarProductoDeFactura(miDataReader["ReferenciaExterna"].ToString()),
                        CashPayment = new CashPayment()
                        {
                            PaymentID = String.IsNullOrEmpty(miDataReader["Cod_Mercado_Pago"].ToString()) ? Convert.ToInt64(miDataReader["Cod_Pago"]) : Convert.ToInt64(miDataReader["Cod_Mercado_Pago"]),
                            PaymentMethod = "MPA",
                            PaymentTotal = Convert.ToDecimal(miDataReader["Importe"])
                        }
                        //Shipping= null,
                        //Payments = []
                    });

                    /*long Cod = String.IsNullOrEmpty(miDataReader["Cod_Mercado_Pago"].ToString()) ? Convert.ToInt64(miDataReader["Cod_Pago"]) : Convert.ToInt64(miDataReader["Cod_Mercado_Pago"]);
                    lDetallePago.LastOrDefault().CashPayment = new CashPayment()
                    {
                        PaymentID = Cod,
                        PaymentMethod = "MPA",
                        PaymentTotal = lDetallePago.LastOrDefault().Total
                    };*/
                    lDetallePago.LastOrDefault().OrderItems.Add(new OrderItem() {
                        ProductCode = miDataReader["ArticuloID"].ToString(),
                        Description = miDataReader["Descripcion"].ToString(),
                        Quantity = 1,
                        UnitPrice = Convert.ToDecimal(miDataReader["Importe"]),
                        DiscountPercentage = 0,
                        SKUCode = "",
                        VariantCode = "",
                        VariantDescription = ""
                    });

                }
                Conn.Close();
                return lDetallePago;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /*private List<OrderItem> BuscarProductoDeFactura(string ReferenciaExterna) {

            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<OrderItem> lOrderItem = new List<OrderItem>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@ReferenciaExterna",
                    Value = ReferenciaExterna
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_ProductoDeFactura", lstParametro);

                while (miDataReader.Read())
                {
                    lOrderItem.Add(new OrderItem()
                    {
                        ProductCode = miDataReader["ArticuloID"].ToString(),
                        Description = miDataReader["Descripcion"].ToString(),
                        Quantity = 1,
                        UnitPrice = Convert.ToInt32(miDataReader["nro_importe"]),
                        DiscountPercentage = 0,
                        SKUCode = "",
                        VariantCode = "",
                        VariantDescription = ""
                    });
                }
                Conn.Close();
                return lOrderItem;
            }
            catch (Exception e)
            {
                throw e;
            }
        }*/

        public ResultadoTransaccion ActualizarRegistroTransaccionFacturacion(int Cod_Pago) {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Pago",
                    Value = Cod_Pago
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_RegistroFacturacion", lstParametro);

                while (miDataReader.Read())
                {
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                    result.Mensaje = miDataReader["msj"].ToString();
                }
                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
