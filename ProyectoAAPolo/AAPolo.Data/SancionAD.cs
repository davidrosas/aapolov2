﻿using AAPolo.Data.Utilidades;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Data
{
    public class SancionAD
    {
        public List<Sancion> BuscarSancion(int Cod_Sancion, int Cod_Usuario, string CodigoArticulo, string Documento_NombreUsuario)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Sancion> lSancion = new List<Sancion>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Sancion",
                    Value = Cod_Sancion
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Usuario
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@CodigoArticulo",
                    Value = CodigoArticulo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Documento_NombreUsuario",
                    Value = Documento_NombreUsuario
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_sancion", lstParametro);

                while (miDataReader.Read())
                {
                    string fec = miDataReader["fechaSancion"].ToString();
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]);

                    lSancion.Add(new Sancion()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Sancion"]),
                        DescripcionSancion = Convert.ToString(miDataReader["Sancion"]),
                        CodigoAritculo = Convert.ToString(miDataReader["CodigoArticulo"]),
                        Fecha = new DateTime(Anio, Mes, Dia),
                        strFecha = fec,
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"])== 1 ? true: false,
                        Persona = new Persona() {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]),
                            Nombre = Convert.ToString(miDataReader["Nombre"]),
                            Apellido = Convert.ToString(miDataReader["Apellido"]),
                            DocumentoIdentidad = Convert.ToString(miDataReader["Documento"]),
                            TipoDocumento = new TipoDocumento() {
                                Codigo = Convert.ToInt32(miDataReader["Cod_Tipo_Documento"]),
                                Nombre = Convert.ToString(miDataReader["NombreTipoDocumento"]),
                                
                            }
                        },
                    });
                }

                Conn.Close();
                return lSancion;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Sancion> BuscarSancionActiva()
        {
            SqlDataReader miDataReader;
            List<Sancion> lSancion = new List<Sancion>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_sancionActiva");

                while (miDataReader.Read())
                {
                    string fec = miDataReader["fechaSancion"].ToString();
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]);

                    lSancion.Add(new Sancion()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Sancion"]),
                        DescripcionSancion = Convert.ToString(miDataReader["Sancion"]),
                        CodigoAritculo = Convert.ToString(miDataReader["CodigoArticulo"]),
                        Fecha = new DateTime(Anio, Mes, Dia),
                        strFecha = fec,
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        Persona = new Persona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]),
                            Nombre = Convert.ToString(miDataReader["Nombre"]),
                            Apellido = Convert.ToString(miDataReader["Apellido"]),
                            DocumentoIdentidad = Convert.ToString(miDataReader["Documento"]),
                            TipoDocumento = new TipoDocumento()
                            {
                                Codigo = Convert.ToInt32(miDataReader["Cod_Tipo_Documento"]),
                                Nombre = Convert.ToString(miDataReader["NombreTipoDocumento"]),

                            }
                        },
                    });
                }

                Conn.Close();
                return lSancion;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarSancion(Sancion obj)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Persona",
                    Value = obj.Persona.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@DescripcionSancion",
                    Value = obj.DescripcionSancion
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@CodigoAritculo",
                    Value = obj.CodigoAritculo
                });

                string query = "sp_ins_sancion";
                if (obj.Codigo > 0)
                {
                    query = "sp_upd_sancion";

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_Sancion",
                        Value = obj.Codigo
                    });

                }

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, query, lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoSancion(int Codigo, int EstadoSancion)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Sancion",
                    Value = Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@EstadoSancion",
                    Value = EstadoSancion
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_EstadoSancion", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
