﻿using AAPolo.Data.Utilidades;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Data
{
    public class BeneficioAD
    {
        public List<CategoriaBeneficio> BuscarCategoriaBeneficio()
        {
            SqlDataReader miDataReader;
            List<CategoriaBeneficio> lCategoriaBeneficio = new List<CategoriaBeneficio>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_CategoriaBeneficio");

                while (miDataReader.Read())
                {
                    lCategoriaBeneficio.Add(new CategoriaBeneficio()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_CategoriaBeneficio"]),
                        Nombre = miDataReader["Nombre"].ToString()
                    });
                }

                Conn.Close();
                return lCategoriaBeneficio;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public List<TipoBeneficiario> BuscarTipoBeneficiario()
        {
            SqlDataReader miDataReader;
            List<TipoBeneficiario> lTipoBeneficiario = new List<TipoBeneficiario>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();


                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_TipoBeneficiario");

                while (miDataReader.Read())
                {
                    lTipoBeneficiario.Add(new TipoBeneficiario()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_TipoBeneficiario"]),
                        Nombre = miDataReader["Nombre"].ToString()
                    });
                }

                Conn.Close();
                return lTipoBeneficiario;
            }
            catch (Exception e)
            {
                throw e;

            }

        }

        public List<Beneficio> BuscarBeneficio(int Cod_Beneficio, string Nombre, int Cod_CategoriaBeneficio, int Cod_TipoBeneficiario, bool CargarPrecios)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Beneficio> lBeneficio = new List<Beneficio>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Beneficio",
                    Value = Cod_Beneficio
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Nombre",
                    Value = Nombre
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_CategoriaBeneficio",
                    Value = Cod_CategoriaBeneficio
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoBeneficiario",
                    Value = Cod_TipoBeneficiario
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_beneficio", lstParametro);

                while (miDataReader.Read())
                {
                    string fecInicio = miDataReader["FechaInicio"].ToString(), fecFin = miDataReader["FechaFin"].ToString();
                    int DiaI = Convert.ToInt32(fecInicio.Split('/')[0]),
                        MesI = Convert.ToInt32(fecInicio.Split('/')[1]),
                        AnioI = Convert.ToInt32(fecInicio.Split('/')[2]),
                        DiaF = Convert.ToInt32(fecFin.Split('/')[0]),
                        MesF = Convert.ToInt32(fecFin.Split('/')[1]),
                        AnioF = Convert.ToInt32(fecFin.Split('/')[2]);

                    lBeneficio.Add(new Beneficio()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Beneficio"]),
                        Nombre = Convert.ToString(miDataReader["Nombre"]),
                        Descripcion = Convert.ToString(miDataReader["Descripcion"]),
                        FechaInicio = new DateTime(AnioI, MesI, DiaI),
                        FechaFin = new DateTime(AnioF, MesF, DiaF),
                        strFechaInicio = fecInicio,
                        strFechaFin = fecFin,
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        EsDestacado = Convert.ToInt32(miDataReader["EsDestacado"]) == 1 ? true : false,
                        UrlImagen = Convert.ToString(miDataReader["UrlImagen"]),
                        UrlMarca = Convert.ToString(miDataReader["UrlMarca"]),
                        Condicion = Convert.ToString(miDataReader["Condicion"]),
                        CategoriaBeneficio = new CategoriaBeneficio()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_CategoriaBeneficio"]),
                            Nombre = Convert.ToString(miDataReader["CategoriaBeneficio"]),
                        },
                        TipoBeneficiario = new TipoBeneficiario()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_TipoBeneficiario"]),
                            Nombre = Convert.ToString(miDataReader["TipoBeneficiario"]),
                        },
                        lstPrecioXBeneficios = CargarPrecios ? this.BuscarPrecioXBeneficio(Convert.ToInt32(miDataReader["Cod_Beneficio"])) : new List<PrecioXBeneficio>()
                    });
                }

                Conn.Close();
                return lBeneficio;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Beneficio> BuscarBeneficioDestacadoActivo(int Cod_Beneficio, string Nombre, int Cod_CategoriaBeneficio, int Cod_TipoBeneficiario, bool CargarPrecios)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Beneficio> lBeneficio = new List<Beneficio>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Beneficio",
                    Value = Cod_Beneficio
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Nombre",
                    Value = Nombre
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_CategoriaBeneficio",
                    Value = Cod_CategoriaBeneficio
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoBeneficiario",
                    Value = Cod_TipoBeneficiario
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_BeneficioDestacadoActivo", lstParametro);

                while (miDataReader.Read())
                {
                    string fecInicio = miDataReader["FechaInicio"].ToString(), fecFin = miDataReader["FechaFin"].ToString();
                    int DiaI = Convert.ToInt32(fecInicio.Split('/')[0]),
                        MesI = Convert.ToInt32(fecInicio.Split('/')[1]),
                        AnioI = Convert.ToInt32(fecInicio.Split('/')[2]),
                        DiaF = Convert.ToInt32(fecFin.Split('/')[0]),
                        MesF = Convert.ToInt32(fecFin.Split('/')[1]),
                        AnioF = Convert.ToInt32(fecFin.Split('/')[2]);

                    lBeneficio.Add(new Beneficio()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Beneficio"]),
                        Nombre = Convert.ToString(miDataReader["Nombre"]),
                        Descripcion = Convert.ToString(miDataReader["Descripcion"]),
                        FechaInicio = new DateTime(AnioI, MesI, DiaI),
                        FechaFin = new DateTime(AnioF, MesF, DiaF),
                        strFechaInicio = fecInicio,
                        strFechaFin = fecFin,
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        EsDestacado = Convert.ToInt32(miDataReader["EsDestacado"]) == 1 ? true : false,
                        UrlImagen = Convert.ToString(miDataReader["UrlImagen"]),
                        UrlMarca = Convert.ToString(miDataReader["UrlMarca"]),
                        Condicion = Convert.ToString(miDataReader["Condicion"]),
                        CategoriaBeneficio = new CategoriaBeneficio()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_CategoriaBeneficio"]),
                            Nombre = Convert.ToString(miDataReader["CategoriaBeneficio"]),
                        },
                        TipoBeneficiario = new TipoBeneficiario()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_TipoBeneficiario"]),
                            Nombre = Convert.ToString(miDataReader["TipoBeneficiario"]),
                        },
                        lstPrecioXBeneficios = CargarPrecios ? this.BuscarPrecioXBeneficio(Convert.ToInt32(miDataReader["Cod_Beneficio"])) : new List<PrecioXBeneficio>()
                    });
                }

                Conn.Close();
                return lBeneficio;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Beneficio> BuscarBeneficioActivo(int Cod_Beneficio, string NombreContenido, int Cod_CategoriaBeneficio, int Cod_TipoBeneficiario, bool CargarPrecios)

        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Beneficio> lBeneficio = new List<Beneficio>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Beneficio",
                    Value = Cod_Beneficio
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NombreContenido",
                    Value = NombreContenido
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_CategoriaBeneficio",
                    Value = Cod_CategoriaBeneficio
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoBeneficiario",
                    Value = Cod_TipoBeneficiario
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_BeneficioActivo", lstParametro);

                while (miDataReader.Read())
                {
                    string fecInicio = miDataReader["FechaInicio"].ToString(), fecFin = miDataReader["FechaFin"].ToString();
                    int DiaI = Convert.ToInt32(fecInicio.Split('/')[0]),
                        MesI = Convert.ToInt32(fecInicio.Split('/')[1]),
                        AnioI = Convert.ToInt32(fecInicio.Split('/')[2]),
                        DiaF = Convert.ToInt32(fecFin.Split('/')[0]),
                        MesF = Convert.ToInt32(fecFin.Split('/')[1]),
                        AnioF = Convert.ToInt32(fecFin.Split('/')[2]);

                    lBeneficio.Add(new Beneficio()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Beneficio"]),
                        Nombre = Convert.ToString(miDataReader["Nombre"]),
                        Descripcion = Convert.ToString(miDataReader["Descripcion"]),
                        FechaInicio = new DateTime(AnioI, MesI, DiaI),
                        FechaFin = new DateTime(AnioF, MesF, DiaF),
                        strFechaInicio = fecInicio,
                        strFechaFin = fecFin,
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        EsDestacado = Convert.ToInt32(miDataReader["EsDestacado"]) == 1 ? true : false,
                        UrlImagen = Convert.ToString(miDataReader["UrlImagen"]),
                        UrlMarca = Convert.ToString(miDataReader["UrlMarca"]),
                        Condicion = Convert.ToString(miDataReader["Condicion"]),
                        CategoriaBeneficio = new CategoriaBeneficio()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_CategoriaBeneficio"]),
                            Nombre = Convert.ToString(miDataReader["CategoriaBeneficio"]),
                        },
                        TipoBeneficiario = new TipoBeneficiario()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_TipoBeneficiario"]),
                            Nombre = Convert.ToString(miDataReader["TipoBeneficiario"]),
                        },
                        lstPrecioXBeneficios = CargarPrecios ? this.BuscarPrecioXBeneficio(Convert.ToInt32(miDataReader["Cod_Beneficio"])) : new List<PrecioXBeneficio>()
                    });
                }

                Conn.Close();
                return lBeneficio;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<PrecioXBeneficio> BuscarPrecioXBeneficio(int Codigo)
        {

            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<PrecioXBeneficio> lPrecioXBeneficio = new List<PrecioXBeneficio>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Beneficio",
                    Value = Codigo
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_PrecioXBeneficio", lstParametro);

                while (miDataReader.Read())
                {
                    lPrecioXBeneficio.Add(new PrecioXBeneficio()
                    {
                        Precio = Convert.ToString(miDataReader["Precio"]),
                        TipoBeneficiario = new TipoBeneficiario() { Codigo = Convert.ToInt32(miDataReader["Cod_TipoBeneficiario"]) }
                    });
                }

                Conn.Close();
                return lPrecioXBeneficio;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarBeneficio(Beneficio obj)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros

                string query = "sp_ins_beneficio";
                if (obj.Codigo > 0)
                {
                    query = "sp_upd_beneficio";

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_Beneficio",
                        Value = obj.Codigo
                    });
                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Nombre",
                    Value = obj.Nombre
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.DateTime,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@FechaInicio",
                    Value = obj.FechaInicio
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.DateTime,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@FechaFin",
                    Value = obj.FechaFin
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_CategoriaBeneficio",
                    Value = obj.CategoriaBeneficio.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoBeneficiario",
                    Value = obj.TipoBeneficiario.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@PrecioPoloPlayers",
                    Value = (obj.TipoBeneficiario.Codigo == ConfiguracionAPP.Cod_TipoBeneficiario_PoloTodos || obj.TipoBeneficiario.Codigo == ConfiguracionAPP.Cod_TipoBeneficiario_PoloPlayers) ? obj.lstPrecioXBeneficios.Find(f => f.TipoBeneficiario.Codigo == ConfiguracionAPP.Cod_TipoBeneficiario_PoloPlayers).Precio : ""
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@PrecioPoloMembers",
                    Value = (obj.TipoBeneficiario.Codigo == ConfiguracionAPP.Cod_TipoBeneficiario_PoloTodos || obj.TipoBeneficiario.Codigo == ConfiguracionAPP.Cod_TipoBeneficiario_PoloMembers) ? obj.lstPrecioXBeneficios.Find(f => f.TipoBeneficiario.Codigo == ConfiguracionAPP.Cod_TipoBeneficiario_PoloMembers).Precio : ""
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Descripcion",
                    Value = obj.Descripcion
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@UrlImagen",
                    Value = String.IsNullOrEmpty(obj.UrlImagen) ? "" : obj.UrlImagen
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@UrlMarca",
                    Value = obj.UrlMarca
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Condicion",
                    Value = obj.Condicion
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, query, lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoBeneficio(int Codigo, int EstadoBeneficio)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Beneficio",
                    Value = Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@EstadoBeneficio",
                    Value = EstadoBeneficio
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_EstadoBeneficio", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoDestacadoBeneficio(int Codigo, int EstadoDestacadoBeneficio)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Beneficio",
                    Value = Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@EstadoBeneficio",
                    Value = EstadoDestacadoBeneficio
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_EstadoDestacadoBeneficio", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public EstadisticasCommunity BuscarEstadisticasCommunity()
        {

            SqlDataReader miDataReader;
            EstadisticasCommunity Estadistica = new EstadisticasCommunity();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_CantidadMiembroCommunity");

                while (miDataReader.Read())
                {
                    Estadistica.TotalMembers += Convert.ToInt32(miDataReader["Players"]);
                    Estadistica.TotalPlayers += Convert.ToInt32(miDataReader["Players"]);
                }

                Conn.Close();
                return Estadistica;
            }
            catch (Exception e)
            {
                throw e;
            }


        }

        public List<Miembro> BuscarMiembro(int Cod_TipoDocumento, string Documento)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Miembro> lMiembro = new List<Miembro>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoDocumento",
                    Value = Cod_TipoDocumento
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Documento",
                    Value = Documento
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_UsuarioMiembro", lstParametro);

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["FechaNacimiento"].ToString()) ? miDataReader["FechaNacimiento"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy"),
                           fecAlta = !String.IsNullOrEmpty(miDataReader["FechaAlta"].ToString()) ? miDataReader["FechaAlta"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]),
                        DiaAlta = Convert.ToInt32(fecAlta.Split('/')[0]),
                        MesAlta = Convert.ToInt32(fecAlta.Split('/')[1]),
                        AnioAlta = Convert.ToInt32(fecAlta.Split('/')[2]);

                    lMiembro.Add(new Miembro()
                    {
                        Nombre = miDataReader["Nombre"].ToString(),
                        Apellido = miDataReader["Apellido"].ToString(),
                        TipoDocumento = new TipoDocumento()
                        {
                            Nombre = miDataReader["TipoDocumento"].ToString(),
                        },
                        DocumentoIdentidad = miDataReader["Documento"].ToString(),
                        strFechaAlta = miDataReader["FechaAlta"].ToString(),
                        FechaAlta = new DateTime(AnioAlta, MesAlta, DiaAlta),
                        lDomicilio = new List<Domicilio>(),
                        strFechaNacimiento = fec,
                        FechaNacimiento = new DateTime(Anio, Mes, Dia),
                        Genero = new Genero() { Nombre = miDataReader["Genero"].ToString() },
                        EsActivo = Convert.ToInt32(miDataReader["Activo"]) == 1 ? true : false,
                        Pais = new Pais() { Nombre = miDataReader["Nacionalidad"].ToString() },
                        NumeroTarjeta = miDataReader["tarjerta"].ToString(),
                        clubUsuarioPrincipal = new ClubUsuario() { NombreClub = miDataReader["Club"].ToString() },
                        Correo = miDataReader["Mail"].ToString(),
                        TelefonoMiembro = miDataReader["Telefono"].ToString(),
                        TipoMiembro = miDataReader["TipoMiembro"].ToString(),
                    });

                    lMiembro.LastOrDefault().lDomicilio.Add(new Domicilio()
                    {
                        pais = new Pais() { Nombre = miDataReader["PaisDomicilio"].ToString(), },
                        provincia = new Provincia() { Nombre = miDataReader["ProvinciaDomicilio"].ToString(), },
                        municipio = new Municipio() { Nombre = miDataReader["MunicipioDomicilio"].ToString(), },
                        NroDomicilio = Convert.ToInt32(miDataReader["NroDomicilio"]),
                        NroPiso = Convert.ToInt32(miDataReader["NroPiso"]),
                        Departamento = miDataReader["Departamento"].ToString(),
                        CodigoPostal = miDataReader["CodigoPostal"].ToString()
                    });

                }

                Conn.Close();
                return lMiembro;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
