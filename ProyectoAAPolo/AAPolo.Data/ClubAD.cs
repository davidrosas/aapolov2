﻿using AAPolo.Data.Utilidades;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Data
{
    public class ClubAD
    {

        public List<Club> BuscaClub(int Cod_CategoriaClub, int Cod_Club, string NombreClub, int Cod_UsuarioAdmin)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Club> lClub = new List<Club>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region PArametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Club",
                    Value = Cod_Club
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NombreClub",
                    Value = NombreClub
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_CategoriaClub",
                    Value = Cod_CategoriaClub
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_UsuarioAdmin",
                    Value = Cod_UsuarioAdmin
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_Club", lstParametro);

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["FechaFundacion"].ToString()) ? miDataReader["FechaFundacion"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]);

                    string FechaPago = !String.IsNullOrEmpty(miDataReader["FechaPago"].ToString()) ? miDataReader["FechaPago"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int DiaPago = Convert.ToInt32(FechaPago.Split('/')[0]),
                        MesPago = Convert.ToInt32(FechaPago.Split('/')[1]),
                        AnioPago = Convert.ToInt32(FechaPago.Split('/')[2]);

                    lClub.Add(new Club()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Club"]),
                        NombreClub = miDataReader["NombreClub"].ToString(),
                        UrlImagen = miDataReader["UrlImagen"].ToString(),
                        NroSocios = Convert.ToInt32(miDataReader["NroSocios"]),
                        NumeroCancha = Convert.ToInt32(miDataReader["NroCanchas"]),
                        strPresidente = miDataReader["Presidente"].ToString(),
                        strVicepresidente = miDataReader["Vicepresidente"].ToString(),
                        strTesorero = miDataReader["Tesorero"].ToString(),
                        strVocal1 = miDataReader["Vocal1"].ToString(),
                        strVocal2 = miDataReader["Vocal2"].ToString(),
                        strVocal3 = miDataReader["Vocal3"].ToString(),
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        EsDestacado = Convert.ToInt32(miDataReader["EsDestacado"]) == 1 ? true : false,
                        FechaFundacion = new DateTime(Anio, Mes, Dia),
                        Facebook = miDataReader["Facebook"].ToString(),
                        Twitter = miDataReader["Twitter"].ToString(),
                        Instagram = miDataReader["Instagram"].ToString(),
                        Historia = miDataReader["Historia"].ToString(),
                        AfiliacionVenciada = Convert.ToInt32(miDataReader["AfiliacionVenciada"]) == 1 ? true : false,
                        FechaPagoAfilicion = new DateTime(AnioPago, MesPago, DiaPago),
                        strFechaPagoAfilicion = miDataReader["FechaPago"].ToString(),
                        lDomicilio = new List<Domicilio>(){
                            new Domicilio() {
                                Codigo = Convert.ToInt32(miDataReader["Cod_Domicilio"]),
                                DireccionDomicilio = miDataReader["DireccionDomicilio"].ToString(),
                                pais = new Pais() {
                                    Codigo = Convert.ToInt32(miDataReader["Cod_Pais"]),
                                    Nombre = miDataReader["NombrePais"].ToString()
                                },
                                provincia = new Provincia() {
                                    Codigo = Convert.ToInt32(miDataReader["Cod_Provincia"]),
                                    Nombre = miDataReader["NombreProvincia"].ToString()
                                },
                                municipio = new Municipio() {
                                    Codigo = Convert.ToInt32(miDataReader["Cod_Municipio"]),
                                    Nombre = miDataReader["NombreMunicipio"].ToString()
                                },
                                NroDomicilio = Convert.ToInt32(miDataReader["NroDomicilio"]),
                                NroPiso = Convert.ToInt32(miDataReader["NroPiso"]),
                                Departamento = miDataReader["Departamento"].ToString(),
                                CodigoPostal = miDataReader["CodigoPostal"].ToString(),
                            }
                        },
                        Categoria = new CategoriaClub()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Categoria_Club"]),
                            NombreCategoria = miDataReader["NombreCategoriaClub"].ToString(),
                            strImporte = miDataReader["nro_importe"].ToString()
                        },
                        PersonaAdmin = new Persona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Admin_Club"]),
                            Nombre = miDataReader["NombreAdmin"].ToString(),
                            Apellido = miDataReader["ApellidoAdmin"].ToString(),
                        },
                    });
                    lClub.LastOrDefault().lContacto = new List<Contacto>();
                    lClub.LastOrDefault().lContacto.Add(new Contacto()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Contacto_Mail"]),
                        Nombre = miDataReader["Mail"].ToString(),
                        TipoContacto = new TipoContacto()
                        {
                            Codigo = ConfiguracionAPP.TipoContacto_Email
                        }
                    });
                    lClub.LastOrDefault().lContacto.Add(new Contacto()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Contacto_Celular"]),
                        Nombre = miDataReader["Celular"].ToString(),
                        TipoContacto = new TipoContacto()
                        {
                            Codigo = ConfiguracionAPP.TipoContacto_Celular
                        }
                    });
                    lClub.LastOrDefault().lContacto.Add(new Contacto()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Contacto_Telefono"]),
                        Nombre = miDataReader["Telefono"].ToString(),
                        TipoContacto = new TipoContacto()
                        {
                            Codigo = ConfiguracionAPP.TipoContacto_Telefono
                        }
                    });
                }

                Conn.Close();
                return lClub.GroupBy(x => x.Codigo).Select(s => s.FirstOrDefault()).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Club> BuscarClubActivo(int Cod_CategoriaClub, int Cod_Club, string NombreClub, int Cod_UsuarioAdmin, int Cod_Provincia)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Club> lClub = new List<Club>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region PArametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Club",
                    Value = Cod_Club
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NombreClub",
                    Value = NombreClub
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_CategoriaClub",
                    Value = Cod_CategoriaClub
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_UsuarioAdmin",
                    Value = Cod_UsuarioAdmin
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Provincia",
                    Value = Cod_Provincia
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_ClubActivo", lstParametro);

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["FechaFundacion"].ToString()) ? miDataReader["FechaFundacion"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]);

                    /*string FechaPago = !String.IsNullOrEmpty(miDataReader["FechaPago"].ToString()) ? miDataReader["FechaPago"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int DiaPago = Convert.ToInt32(FechaPago.Split('/')[0]),
                        MesPago = Convert.ToInt32(FechaPago.Split('/')[1]),
                        AnioPago = Convert.ToInt32(FechaPago.Split('/')[2]);*/

                    lClub.Add(new Club()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Club"]),
                        NombreClub = miDataReader["NombreClub"].ToString(),
                        UrlImagen = miDataReader["UrlImagen"].ToString(),
                        NroSocios = Convert.ToInt32(miDataReader["NroSocios"]),
                        NumeroCancha = Convert.ToInt32(miDataReader["NroCanchas"]),
                        strPresidente = miDataReader["Presidente"].ToString(),
                        strVicepresidente = miDataReader["Vicepresidente"].ToString(),
                        strTesorero = miDataReader["Tesorero"].ToString(),
                        strVocal1 = miDataReader["Vocal1"].ToString(),
                        strVocal2 = miDataReader["Vocal2"].ToString(),
                        strVocal3 = miDataReader["Vocal3"].ToString(),
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        EsDestacado = Convert.ToInt32(miDataReader["EsDestacado"]) == 1 ? true : false,
                        FechaFundacion = new DateTime(Anio, Mes, Dia),
                        /*Facebook = miDataReader["Facebook"].ToString(),
                        Twitter = miDataReader["Twitter"].ToString(),
                        Instagram = miDataReader["Instagram"].ToString(),
                        Historia = miDataReader["Historia"].ToString(),
                        AfiliacionVenciada = Convert.ToInt32(miDataReader["AfiliacionVenciada"]) == 1 ? true : false,
                        FechaPagoAfilicion = new DateTime(AnioPago, MesPago, DiaPago),
                        strFechaPagoAfilicion = miDataReader["FechaPago"].ToString(),*/
                        lDomicilio = new List<Domicilio>(){
                            new Domicilio() {
                                Codigo = Convert.ToInt32(miDataReader["Cod_Domicilio"]),
                                DireccionDomicilio = miDataReader["DireccionDomicilio"].ToString(),
                                pais = new Pais() {
                                    Codigo = Convert.ToInt32(miDataReader["Cod_Pais"]),
                                    Nombre = miDataReader["NombrePais"].ToString()
                                },
                                provincia = new Provincia() {
                                    Codigo = Convert.ToInt32(miDataReader["Cod_Provincia"]),
                                    Nombre = miDataReader["NombreProvincia"].ToString()
                                },
                                municipio = new Municipio() {
                                    Codigo = Convert.ToInt32(miDataReader["Cod_Municipio"]),
                                    Nombre = miDataReader["NombreMunicipio"].ToString()
                                },
                                /*NroDomicilio = Convert.ToInt32(miDataReader["NroDomicilio"]),
                                NroPiso = Convert.ToInt32(miDataReader["NroPiso"]),
                                Departamento = miDataReader["Departamento"].ToString(),
                                CodigoPostal = miDataReader["CodigoPostal"].ToString(),*/
                            }
                        },
                        Categoria = new CategoriaClub()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Categoria_Club"]),
                            NombreCategoria = miDataReader["NombreCategoriaClub"].ToString(),
                            strImporte = miDataReader["nro_importe"].ToString()
                        },
                        /*PersonaAdmin = new Persona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Admin_Club"]),
                            Nombre = miDataReader["NombreAdmin"].ToString(),
                            Apellido = miDataReader["ApellidoAdmin"].ToString(),
                        },*/
                    });
                    /*lClub.LastOrDefault().lContacto = new List<Contacto>();
                    lClub.LastOrDefault().lContacto.Add(new Contacto()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Contacto_Mail"]),
                        Nombre = miDataReader["Mail"].ToString(),
                        TipoContacto = new TipoContacto()
                        {
                            Codigo = ConfiguracionAPP.TipoContacto_Email
                        }
                    });
                    lClub.LastOrDefault().lContacto.Add(new Contacto()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Contacto_Celular"]),
                        Nombre = miDataReader["Celular"].ToString(),
                        TipoContacto = new TipoContacto()
                        {
                            Codigo = ConfiguracionAPP.TipoContacto_Celular
                        }
                    });
                    lClub.LastOrDefault().lContacto.Add(new Contacto()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Contacto_Telefono"]),
                        Nombre = miDataReader["Telefono"].ToString(),
                        TipoContacto = new TipoContacto()
                        {
                            Codigo = ConfiguracionAPP.TipoContacto_Telefono
                        }
                    });*/
                }

                Conn.Close();
                return lClub.GroupBy(x => x.Codigo).Select(s => s.FirstOrDefault()).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarImagenClub(string urlImg, int Cod_Club)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Club",
                    Value = Cod_Club
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@UrlImg",
                    Value = urlImg
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_ImagenClub", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion RechazarJugadorClubPrincipal(int Cod_Persona)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Persona
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_RechazarJugadorClubPrincipal", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                    if (result.EsSatisfactorio) {
                        string CorreoUsuario = miDataReader["CorreoUsuario"].ToString(),
                               NombreUsuario = miDataReader["NombreUsuario"].ToString(),
                               NombreClub = miDataReader["NombreClub"].ToString();
                        if (!String.IsNullOrEmpty(CorreoUsuario)) {
                            new ExtensionUtils().enviarMailGmail(CorreoUsuario, "AAPolo - Desasociación de club", new ExtensionUtils().DesasociacionDeClub(NombreUsuario, NombreClub));
                        }
                    }
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /*public List<Club> BuscaClubUsuario(int Cod_Club, int CodUsuario)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Club> lClub = new List<Club>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Club",
                    Value = Cod_Club
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = CodUsuario
                });

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_Usuario_Club", lstParametro);

                while (miDataReader.Read())
                {
                    lClub.Add(new Club()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]),
                    });
                }

                Conn.Close();
                return lClub;
            }
            catch (Exception e)
            {
                throw e;
            }
        }*/

        public List<CategoriaClub> BuscarCategoriaClub()
        {
            SqlDataReader miDataReader;
            List<CategoriaClub> lCategoriaClub = new List<CategoriaClub>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_CategoriaClub");

                while (miDataReader.Read())
                {
                    lCategoriaClub.Add(new CategoriaClub()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Categoria_Club"]),
                        NombreCategoria = miDataReader["NombreCategoriaClub"].ToString(),
                        strImporte = miDataReader["nro_importe"].ToString()
                    });
                }

                Conn.Close();
                return lCategoriaClub;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarClub(Club obj)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NombreClub",
                    Value = obj.NombreClub
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_CategoriaClub",
                    Value = obj.Categoria.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NroSocios",
                    Value = obj.NroSocios
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NroCancha",
                    Value = obj.NumeroCancha
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Presidente",
                    Value = !String.IsNullOrEmpty(obj.strPresidente) ? obj.strPresidente : ""
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Vicepresidente",
                    Value = !String.IsNullOrEmpty(obj.strVicepresidente) ? obj.strVicepresidente : ""
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Tesorero",
                    Value = !String.IsNullOrEmpty(obj.strTesorero) ? obj.strTesorero : ""
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Vocal1",
                    Value = !String.IsNullOrEmpty(obj.strVocal1) ? obj.strVocal1 : ""
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Vocal2",
                    Value = !String.IsNullOrEmpty(obj.strVocal2) ? obj.strVocal2 : ""
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Vocal3",
                    Value = !String.IsNullOrEmpty(obj.strVocal3) ? obj.strVocal3 : ""
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_UsuarioAdmin",
                    Value = obj.PersonaAdmin != null ? obj.PersonaAdmin.Codigo : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@DireccionDomicilio",
                    Value = String.IsNullOrEmpty(obj.lDomicilio.FirstOrDefault().DireccionDomicilio) ? "" : obj.lDomicilio.FirstOrDefault().DireccionDomicilio
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NroDomicilio",
                    Value = obj.lDomicilio.FirstOrDefault().NroDomicilio
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NroPiso",
                    Value = obj.lDomicilio.FirstOrDefault().NroPiso
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Departamento",
                    Value = String.IsNullOrEmpty(obj.lDomicilio.FirstOrDefault().Departamento) ? "" : obj.lDomicilio.FirstOrDefault().Departamento
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@CodigoPostal",
                    Value = String.IsNullOrEmpty(obj.lDomicilio.FirstOrDefault().CodigoPostal) ? "" : obj.lDomicilio.FirstOrDefault().CodigoPostal
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Pais",
                    Value = (obj.lDomicilio.FirstOrDefault().pais != null) ? obj.lDomicilio.FirstOrDefault().pais.Codigo : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Provincia",
                    Value = obj.lDomicilio.FirstOrDefault().provincia != null ? obj.lDomicilio.FirstOrDefault().provincia.Codigo : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Municipio",
                    Value = obj.lDomicilio.FirstOrDefault().municipio != null ? obj.lDomicilio.FirstOrDefault().municipio.Codigo : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Facebook",
                    Value = String.IsNullOrEmpty(obj.Facebook) ? "" : obj.Facebook
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Twitter",
                    Value = String.IsNullOrEmpty(obj.Twitter) ? "" : obj.Twitter
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Instagram",
                    Value = String.IsNullOrEmpty(obj.Instagram) ? "" : obj.Instagram
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Email",
                    Value = obj.lContacto != null && obj.lContacto.Count() > 0 ? obj.lContacto.Where(x => x.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Email).FirstOrDefault().Nombre : ""
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Celular",
                    Value = obj.lContacto != null && obj.lContacto.Count() > 0 && !String.IsNullOrEmpty(obj.lContacto.Where(x => x.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Celular).FirstOrDefault().Nombre) ? obj.lContacto.Where(x => x.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Celular).FirstOrDefault().Nombre : ""
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@TelefonoContacto",
                    Value = obj.lContacto != null && obj.lContacto.Count() > 0 && !String.IsNullOrEmpty(obj.lContacto.Where(x => x.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Telefono).FirstOrDefault().Nombre) ? obj.lContacto.Where(x => x.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Telefono).FirstOrDefault().Nombre : ""
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.DateTime,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@FechaFundacion",
                    Value = obj.FechaFundacion
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Historia",
                    Value = !String.IsNullOrEmpty(obj.Historia) ? obj.Historia : ""
                });

                string query = "sp_ins_club";
                if (obj.Codigo > 0)
                {
                    query = "sp_upd_club";

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_Club",
                        Value = obj.Codigo
                    });

                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@UrlImagen",
                    Value = obj.UrlImagen
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, query, lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarDatosClub(Club obj)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_CategoriaClub",
                    Value = obj.Categoria.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NroCancha",
                    Value = obj.NumeroCancha
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@DireccionDomicilio",
                    Value = String.IsNullOrEmpty(obj.lDomicilio.FirstOrDefault().DireccionDomicilio) ? "" : obj.lDomicilio.FirstOrDefault().DireccionDomicilio
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NroDomicilio",
                    Value = obj.lDomicilio.FirstOrDefault().NroDomicilio
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NroPiso",
                    Value = obj.lDomicilio.FirstOrDefault().NroPiso
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Departamento",
                    Value = String.IsNullOrEmpty(obj.lDomicilio.FirstOrDefault().Departamento) ? "" : obj.lDomicilio.FirstOrDefault().Departamento
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@CodigoPostal",
                    Value = String.IsNullOrEmpty(obj.lDomicilio.FirstOrDefault().CodigoPostal) ? "" : obj.lDomicilio.FirstOrDefault().CodigoPostal
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Pais",
                    Value = obj.lDomicilio.FirstOrDefault().pais.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Provincia",
                    Value = obj.lDomicilio.FirstOrDefault().provincia != null ? obj.lDomicilio.FirstOrDefault().provincia.Codigo : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Municipio",
                    Value = obj.lDomicilio.FirstOrDefault().municipio != null ? obj.lDomicilio.FirstOrDefault().municipio.Codigo : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Facebook",
                    Value = String.IsNullOrEmpty(obj.Facebook) ? "" : obj.Facebook
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Twitter",
                    Value = String.IsNullOrEmpty(obj.Twitter) ? "" : obj.Twitter
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Instagram",
                    Value = String.IsNullOrEmpty(obj.Instagram) ? "" : obj.Instagram
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Email",
                    Value = obj.lContacto != null && obj.lContacto.Count() > 0 ? obj.lContacto.Where(x => x.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Email).FirstOrDefault().Nombre : ""
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Celular",
                    Value = obj.lContacto != null && obj.lContacto.Count() > 0 && !String.IsNullOrEmpty(obj.lContacto.Where(x => x.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Celular).FirstOrDefault().Nombre) ? obj.lContacto.Where(x => x.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Celular).FirstOrDefault().Nombre : ""
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@TelefonoContacto",
                    Value = obj.lContacto != null && obj.lContacto.Count() > 0 && !String.IsNullOrEmpty(obj.lContacto.Where(x => x.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Telefono).FirstOrDefault().Nombre) ? obj.lContacto.Where(x => x.TipoContacto.Codigo == ConfiguracionAPP.TipoContacto_Telefono).FirstOrDefault().Nombre : ""
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@FechaFundacion",
                    Value = obj.FechaFundacion.ToString("yyyyMMdd")
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Historia",
                    Value = !String.IsNullOrEmpty(obj.Historia) ? obj.Historia : ""
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Club",
                    Value = obj.Codigo
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_clubUsuario", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoClub(int Codigo, int EstadoClub)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Club",
                    Value = Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@EstadoClub",
                    Value = EstadoClub
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_EstadoClub", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoDestacadoClub(int Codigo, int EstadoDestacadoClub)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Club",
                    Value = Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@EstadoClub",
                    Value = EstadoDestacadoClub
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_EstadoDestacadoClub", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
