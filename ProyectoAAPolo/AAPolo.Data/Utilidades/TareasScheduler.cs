﻿using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskScheduler;

namespace AAPolo.Data.Utilidades
{
    public class TareasScheduler
    {
        TaskScheduler.TaskScheduler objScheduler;
        //To hold Task Definition
        ITaskDefinition objTaskDef;
        //To hold Trigger Information
        ITimeTrigger objTrigger;
        //To hold Action Information
        IExecAction objAction;

        private int Nivel_Prioridad_Critico = 0; // REALTIME_PRIORITY_CLASS THREAD_PRIORITY_TIME_CRITICAL
        private int Nivel_Prioridad_Alta = 1; //  HIGH_PRIORITY_CLASS THREAD_PRIORITY_HIGHEST
        private int Nivel_Prioridad_Avobe_Normal = 2; //  ABOVE_NORMAL_PRIORITY_CLASS THREAD_PRIORITY_ABOVE_NORMAL
        private int Nivel_Prioridad_Avobe_Normal2 = 3; //  ABOVE_NORMAL_PRIORITY_CLASS THREAD_PRIORITY_ABOVE_NORMAL
        private int Nivel_Prioridad_Normal = 4; //  NORMAL_PRIORITY_CLASS THREAD_PRIORITY_NORMAL
        private int Nivel_Prioridad_Normal2 = 5; //  NORMAL_PRIORITY_CLASS THREAD_PRIORITY_NORMAL
        private int Nivel_Prioridad_Normal3 = 6; //  NORMAL_PRIORITY_CLASS THREAD_PRIORITY_NORMAL
        private int Nivel_Prioridad_Baja = 7; //  BELOW_NORMAL_PRIORITY_CLASS THREAD_PRIORITY_BELOW_NORMAL
        private int Nivel_Prioridad_Baja2 = 8; //  BELOW_NORMAL_PRIORITY_CLASS THREAD_PRIORITY_BELOW_NORMAL
        private int Nivel_Prioridad_Baja3 = 9; //  IDLE_PRIORITY_CLASS THREAD_PRIORITY_LOWEST
        private int Nivel_Prioridad_Baja4 = 10; //    IDLE_PRIORITY_CLASS THREAD_PRIORITY_IDLE

        /// <summary>
        /// Permite crear tareas en el Task Scheduler del sistema operativo
        /// </summary>
        /// <param name="FechaEjecucion">Fecha de la ejecución de la tarea, la misma se ejecutara una única vez</param>
        /// <param name="UtcPais">UTC de la zona horaria en la que se quiere ejecurar la tarea</param>
        /// <param name="Path">Ruta donde se encuentra alojado el .exe a ejecutar</param>
        /// <param name="IdAction"></param>
        /// <param name="NombreTarea">Nombre de la tarea</param>
        /// <param name="DescripcionTarea">Descripción de la tarea</param>
        /// <param name="Usuario">Usuario ADM para crear la tarea</param>
        /// <param name="Password">Password del usuario ADM para crear la tarea</param>
        /// <param name="ExecutionTimeLimit">Tiempo maximo para la ejecucion de la tarea, por defecto 10min. Formato: PT10M</param>
        /// <returns></returns>
        public bool CrearTarea(DateTime FechaEjecucion, int UtcPais, string Path, string IdAction, string NombreTarea, string DescripcionTarea, object Usuario, object Password, string ExecutionTimeLimit = "PT10M")
        {
            //FechaEjecucion = FechaEjecucion.ToUniversalTime().AddHours(UtcPais);

            double diferencia = (DateTime.Now - DateTime.UtcNow).TotalMilliseconds;
            FechaEjecucion = FechaEjecucion.AddMilliseconds(diferencia).AddHours((-1) * UtcPais);
            DateTime HoraActual = DateTime.UtcNow.AddHours(UtcPais).AddMinutes(2);

            if (DateTime.Compare(FechaEjecucion, HoraActual) == 0 || DateTime.Compare(FechaEjecucion, HoraActual) < 0)
            {
                FechaEjecucion = DateTime.Now.AddMinutes(4);
            }

            try
            {
                objScheduler = new TaskScheduler.TaskScheduler();
                objScheduler.Connect();

                //Setting Task Definition
                SetTaskDefinition(NombreTarea, DescripcionTarea, ExecutionTimeLimit);
                //Setting Task Trigger Information

                SetTriggerInfo(FechaEjecucion);
                //Setting Task Action Information

                SetActionInfo(Path, IdAction);

                ITaskFolder root = objScheduler.GetFolder("\\");
                //Registering the task, if the task is already exist then it will be updated
                IRegisteredTask regTask = root.RegisterTaskDefinition(IdAction, objTaskDef, (int)_TASK_CREATION.TASK_CREATE_OR_UPDATE, Usuario, Password, _TASK_LOGON_TYPE.TASK_LOGON_INTERACTIVE_TOKEN, "");

                //To execute the task immediately calling Run()
                //IRunningTask runtask = regTask.Run(null);

                return true;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        private void SetTaskDefinition(string NombreTarea, string DescripcionTarea, string ExecutionTimeLimit)
        {
            try
            {
                objTaskDef = objScheduler.NewTask(0);
                //Registration Info for task
                //Name of the task Author
                objTaskDef.RegistrationInfo.Author = NombreTarea;// "AApoloSite";
                //Description of the task 
                objTaskDef.RegistrationInfo.Description = DescripcionTarea; // "SampleTask";
                //Registration date of the task 
                objTaskDef.RegistrationInfo.Date = DateTime.Today.ToString("yyyy-MM-ddTHH:mm:ss"); //Date format 

                //Settings for task
                //Thread Priority
                objTaskDef.Settings.Priority = Nivel_Prioridad_Avobe_Normal;
                //Enabling the task
                objTaskDef.Settings.Enabled = true;
                //To hide/show the task
                objTaskDef.Settings.Hidden = false;
                //Execution Time Lmit for task
                objTaskDef.Settings.ExecutionTimeLimit = ExecutionTimeLimit; /* "PT10M" 10 minutes*/
                //Specifying no need of network connection
                objTaskDef.Settings.RunOnlyIfNetworkAvailable = false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SetTriggerInfo(DateTime FechaEjecucion)
        {
            try
            {
                //Trigger information based on time - TASK_TRIGGER_TIME
                objTrigger = (ITimeTrigger)objTaskDef.Triggers.Create(_TASK_TRIGGER_TYPE2.TASK_TRIGGER_TIME);
                //Trigger ID
                objTrigger.Id = "SampleTaskTrigger";
                //Start Time
                objTrigger.StartBoundary = FechaEjecucion.ToString("yyyy-MM-ddTHH:mm:ss");// "2014-01-09T10:10:00"; //yyyy-MM-ddTHH:mm:ss
                //End Time
                //objTrigger.EndBoundary = "2016-01-01T07:30:00"; //yyyy-MM-ddTHH:mm:ss
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Setting Task Action Information
        private void SetActionInfo(string Path, string IdAction)
        {
            try
            {
                //Action information based on exe- TASK_ACTION_EXEC
                objAction = (IExecAction)objTaskDef.Actions.Create(_TASK_ACTION_TYPE.TASK_ACTION_EXEC);
                //Action ID
                objAction.Id = String.IsNullOrEmpty(IdAction) ? "testAction1" : IdAction;
                //Set the path of the exe file to execute, Here mspaint will be opened
                objAction.Path = Path;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
