﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Xml;
using TaskScheduler;

namespace AAPolo.Data.Utilidades
{
    public static class ExtensionMethods
    {
        public static SqlConnection ObtConnection()
        {
            SqlConnection miConexion = new SqlConnection();
            miConexion.ConnectionString = ConfigurationManager.ConnectionStrings["sql-conexion"].ConnectionString;
            miConexion.Open();
            return miConexion;

        }

        public static SqlDataReader ObtDataReader(SqlConnection miConn, string query, List<SqlParameter> misParametros)
        {
            SqlCommand miCommand = new SqlCommand();
            SqlDataReader miDataReader;
            try
            {
                miCommand.Connection = miConn;
                miCommand.CommandType = CommandType.StoredProcedure;
                miCommand.CommandText = query;
                miCommand.CommandTimeout = 500;

                foreach (SqlParameter miParam in misParametros)
                    miCommand.Parameters.Add(miParam);

                miDataReader = miCommand.ExecuteReader();
                return miDataReader;
            }
            catch (DbException ex)
            {
                throw new Exception("Disculpe, no es posible establecer conexión con el servidor de BD. " + ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static XmlReader ObtDataReaderXML(SqlConnection miConn, string query, List<SqlParameter> misParametros)
        {
            SqlCommand miCommand = new SqlCommand();
            XmlReader xml;
            //SqlDataReader miDataReader;
            try
            {
                miCommand.Connection = miConn;
                miCommand.CommandType = CommandType.StoredProcedure;
                miCommand.CommandText = query;
                miCommand.CommandTimeout = 500;

                foreach (SqlParameter miParam in misParametros)
                    miCommand.Parameters.Add(miParam);

                //miDataReader = miCommand.ExecuteReader();
                xml = miCommand.ExecuteXmlReader();
                return xml;
            }
            catch (DbException ex)
            {
                throw new Exception("Disculpe, no es posible establecer conexión con el servidor de BD. " + ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static SqlDataReader SqlCommandTextExecute(SqlConnection miConn, string query, List<SqlParameter> misParametros)
        {
            SqlCommand miCommand = new SqlCommand();
            SqlDataReader miDataReader;
            try
            {
                miCommand.Connection = miConn;
                miCommand.CommandType = CommandType.Text;
                miCommand.CommandText = query;
                miCommand.CommandTimeout = 500;

                foreach (SqlParameter miParam in misParametros)
                    miCommand.Parameters.Add(miParam);

                miDataReader = miCommand.ExecuteReader();
                return miDataReader;
            }
            catch (DbException ex)
            {
                throw new Exception("Disculpe, no es posible establecer conexión con el servidor de BD. " + ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static SqlDataReader ObtDataReader(SqlConnection miConn, string query)
        {
            SqlCommand miCommand = new SqlCommand();
            SqlDataReader miDataReader;
            try
            {
                miCommand.Connection = miConn;
                miCommand.CommandType = CommandType.StoredProcedure;
                miCommand.CommandText = query;
                miCommand.CommandTimeout = 500;
                miDataReader = miCommand.ExecuteReader();
                return miDataReader;
            }
            catch (DbException ex)
            {
                throw new Exception("Disculpe, no es posible establecer conexión con el servidor de BD." + ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string ToSHA256(string value)
        {
            SHA256 sha256 = SHA256.Create();

            byte[] hashData = sha256.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder returnValue = new StringBuilder();

            for (int i = 0; i < hashData.Length; i++)
            {
                returnValue.Append(hashData[i].ToString());
            }

            return returnValue.ToString();
        }

    }
}
