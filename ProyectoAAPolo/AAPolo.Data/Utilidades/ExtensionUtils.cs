﻿using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Data.Utilidades
{
    public class ExtensionUtils
    {
        public string obtenerTagDelWebConfig(string pNombreTag)
        {
            try
            {
                var AppSettings = ConfigurationManager.AppSettings;
                string key = AppSettings[pNombreTag];
                return key;
            }
            catch (Exception)
            {
                return "";
            }
        }

        public bool enviarMailGmail(string to, string title, string body)
        {
            using (MailMessage mailMessage = new MailMessage(new MailAddress(obtenerTagDelWebConfig("SmtpCorreo")), new MailAddress(obtenerTagDelWebConfig("SmtpCorreo"), "Mudaton")))
            {
                mailMessage.Body = "body";
                mailMessage.Subject = "subject";
                try
                {
                    SmtpClient SmtpServer = new SmtpClient();
                    SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    SmtpServer.Credentials = new System.Net.NetworkCredential(obtenerTagDelWebConfig("SmtpCorreo"), obtenerTagDelWebConfig("SmtpCorreoPassword"));
                    SmtpServer.Port = 587;
                    SmtpServer.Host = "smtp.gmail.com";
                    SmtpServer.EnableSsl = true;
                    var mail = new MailMessage();
                    mail.From = new MailAddress(obtenerTagDelWebConfig("SmtpCorreo"), "AAPolo");
                    foreach (var item in to.Split(';'))
                    {
                        mail.To.Add(item);
                    }
                    mail.Subject = title;
                    mail.Body = body;
                    mail.IsBodyHtml = true;
                    mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                    mail.ReplyToList.Add(obtenerTagDelWebConfig("SmtpCorreo"));
                    SmtpServer.Send(mail);
                    return true;
                }
                catch (Exception ex)
                {
                    string exp = ex.ToString();
                    return false;
                }
            }

        }

        public string CambioDeContraseña(string idEnconde)
        {
            string HtmlCuerpoMail = "<table role='presentation' cellspacing='0' cellpadding='0' border='0' width='100%'>" +
                                                "<tbody>" +
                                                    "<tr>" +
                                                        "<td style='padding: 40px; font-family: 'Raleway', sans-serif; font-size: 15px; line-height: 140%; color: #555555;'>" +
                                                            "<p style='margin: 0;'>" + DateTime.Now.ToString("dd \"de\" MMMM \"de\" yyyy") + "</p>" +
                                                            "<h1 style='margin: 0 0 30px 0; font-family: 'Raleway', sans-serif; font-size: 24px; line-height: 125%; color: #2198ff; font-weight: bold;'>Solicitud de cambio de contraseña </h1> " +

                                                            "<p style='margin: 0 0 30px 0;'>Para continuar con el cambio de contraseña por favor hacer <a href='" + obtenerTagDelWebConfig("UrlAPP") + "/Home/CambioDePasswordIndex?id=" + idEnconde + "'>click aqui</a> .</p> " +
                                                        "</td> " +
                                                    "</tr> " +
                                                "</tbody>" +
                                            "</table>";

            return LayoutMail(HtmlCuerpoMail);
        }

        public string DesasociacionDeClub(string NombreUsuario, string NombreClub)
        {
            string HtmlCuerpoMail = "<table role='presentation' cellspacing='0' cellpadding='0' border='0' width='100%'>" +
                                        "<tbody>" +
                                            "<tr>" +
                                                "<td style='padding: 40px; font-family: 'Raleway', sans-serif; font-size: 15px; line-height: 140%; color: #555555;'>" +
                                                    "<p style='margin: 0;'>" + DateTime.Now.ToString("dd \"de\" MMMM \"de\" yyyy") + "</p>" +
                                                    "<h1 style='margin: 0 0 30px 0; font-family: 'Raleway', sans-serif; font-size: 24px; line-height: 125%; color: #2198ff; font-weight: bold;'>Hola " + NombreUsuario + ".</h1> " +

                                                    "<p style='margin: 0 0 30px 0;'>El administrador del club: " + NombreClub + " te ha desvinculado. Podrás asociarte a un nuevo club principal desde tu perfil!</p> " +
                                                    "<p style='margin: 0 0 30px 0;'>Para seguir participando en torneos y asociarte a un nuevo club escribinos a info@aaplo.com.</p> " +
                                                "</td> " +
                                            "</tr> " +
                                        "</tbody>" +
                                    "</table>";

            return LayoutMail(HtmlCuerpoMail);
        }

        public string AsociacionDeNuevoJugadorAClub(string NombreAdminClub, string NombreNuevoJugador, string NombreClub)
        {
            string HtmlCuerpoMail = "<table role='presentation' cellspacing='0' cellpadding='0' border='0' width='100%'>" +
                                        "<tbody>" +
                                            "<tr>" +
                                                "<td style='padding: 40px; font-family: 'Raleway', sans-serif; font-size: 15px; line-height: 140%; color: #555555;'>" +
                                                    "<p style='margin: 0;'>" + DateTime.Now.ToString("dd \"de\" MMMM \"de\" yyyy") + "</p>" +
                                                    "<h1 style='margin: 0 0 30px 0; font-family: 'Raleway', sans-serif; font-size: 24px; line-height: 125%; color: #2198ff; font-weight: bold;'>Hola " + NombreAdminClub + ".</h1> " +

                                                    "<p style='margin: 0 0 30px 0;'>El jugador: " + NombreNuevoJugador + " se ha vinculado al club: " + NombreClub + ".</p> " +
                                                    "<p style='margin: 0 0 30px 0;'>Para más información escribinos a info@aaplo.com</p> " +
                                                "</td> " +
                                            "</tr> " +
                                        "</tbody>" +
                                    "</table>";

            return LayoutMail(HtmlCuerpoMail);
        }

        public string RegistroNuevoJugador(Persona obj, string NombreAdmin)
        {
            string HtmlCuerpoMail = "<table role='presentation' cellspacing='0' cellpadding='0' border='0' width='100%'>" +
                                        "<tbody>" +
                                            "<tr>" +
                                                "<td style='padding: 40px; font-family: 'Raleway', sans-serif; font-size: 15px; line-height: 140%; color: #555555;'>" +
                                                    "<p style='margin: 0;'>" + DateTime.Now.ToString("dd \"de\" MMMMM \"de\" yyyy") + "</p>" +
                                                    "<h1 style='margin: 0 0 30px 0; font-family: 'Raleway', sans-serif; font-size: 24px; line-height: 125%; color: #2198ff; font-weight: bold;'>Hola " + NombreAdmin + ".</h1> " +

                                                    "<p style='margin: 0 0 30px 0;'>" + 
                                                        "El jugador: " + obj.NombreCompleto + " fue dado de alta. <br> " +
                                                        "Datos del jugador: <br> " +
                                                        "IdJ: " + obj.JugadorID + " <br> " +
                                                        "Tipo de documento: " + obj.TipoDocumento.Nombre + " .<br> " +
                                                        "Número de documento: " + obj.DocumentoIdentidad + " <br> " +
                                                        "Desde el administrador podrás conocer sus datos completos" + 
                                                    " </p> " +
                                                "</td> " +
                                            "</tr> " +
                                        "</tbody>" +
                                    "</table>";
            return LayoutMail(HtmlCuerpoMail);
        }

        public void NotificarNuevoUsuario(Persona obj, string NombreAdmin, string CorreoAdmin)
        {
            this.enviarMailGmail(CorreoAdmin, "AAPolo - Registro de nuevo jugador: ", this.RegistroNuevoJugador(obj, NombreAdmin));
        }

        public string LayoutMail(string CuerpoMail)
        {
            return "<!DOCTYPE html>" +
                   "<html lang='en' xmlns='http://www.w3.org/1999/xhtml' xmlns:v='urn:schemas-microsoft-com:vml' xmlns:o='urn:schemas-microsoft-com:office:office'><head>" +
                        "<meta charset='utf-8'> " +
                        "<meta name='viewport' content='width=device-width'>" +
                        "<meta http-equiv='X-UA-Compatible' content='IE=edge'> " +
                        "<meta name='x-apple-disable-message-reformatting'>" +
                        "<title>Nueva comunicación desde Asociación Argentina de Polo</title>" +

                        "<style>" +
                            "html,body {" +
                                "margin: 0 auto !important;" +
                                "padding: 0 !important;" +
                                "height: 100% !important;" +
                                "width: 100% !important;" +
                            "}" +

                            "* {" +
                            "	-ms-text-size-adjust: 100%;" +
                            "	-webkit-text-size-adjust: 100%;" +
                            "	font-family: 'Raleway', sans-serif !important;" +
                            "}" +

                            "div[style*='margin: 16px 0'] {" +
                                "margin: 0 !important;" +
                            "}" +

                            "table," +
                            "td {" +
                                "mso-table-lspace: 0pt !important;" +
                                "mso-table-rspace: 0pt !important;" +
                            "}" +

                            "table {" +
                                "border-spacing: 0 !important;" +
                                "border-collapse: collapse !important;" +
                                "table-layout: fixed !important;" +
                                "margin: 0 auto !important;" +
                            "}" +
                            "table table table {" +
                                "table-layout: auto;" +
                            "}" +

                            "img {" +
                                "-ms-interpolation-mode:bicubic;" +
                            "}" +


                            "*[x-apple-data-detectors],  " +
                            ".x-gmail-data-detectors,   " +
                            ".x-gmail-data-detectors*," +
                            ".aBn {" +
                                "border-bottom: 0 !important;" +
                                "cursor: default !important;" +
                                "color: inherit !important;" +
                                "text-decoration: none !important;" +
                                "font-size: inherit !important;" +
                                "font-family: inherit !important;" +
                                "font-weight: inherit !important;" +
                                "line-height: inherit !important;" +
                            "}" +

                            ".a6S {" +
                                "display: none !important;" +
                                "opacity: 0.01 !important;" +
                            "}" +
                            "img.g-img + div {" +
                                "display: none !important;" +
                            "}" +

                            ".button-link {" +
                                "text-decoration: none !important;" +
                            "}" +

                            "@media only screen and (min-device-width: 375px) and (max-device-width: 413px) {" +
                                ".email-container {" +
                                    "min-width: 375px !important; " +
                                "}" +
                            "}" +

                            "@media screen and (max-width: 480px) {" +
                                "div > u ~ div .gmail {" +
                                    "min-width: 100vw; " +
                                "}" +
                            "}" +

                        "</style> " +
                        "<style>" +
                            ".button-td," +
                            ".button-a { " +
                                "transition: all 100ms ease-in; " +
                            "}" +
                            ".button-td:hover," +
                            ".button-a:hover {" +
                                "background: #3f34bb !important;" +
                                "border -color: #3f34bb !important;" +
                            "}" +

                            "@media screen and (max-width: 600px) {" +

                                ".email-container p {" +
                                    "font -size: 17px !important;" +
                                "}" +

                            "}" +
                        "</style>" +
                        "<xml>" +
                            "<o:OfficeDocumentSettings>" +
                                "<o:AllowPNG/>" +
                                "<o:PixelsPerInch></o:PixelsPerInch>" +
                            "</o:OfficeDocumentSettings>" +
                        "</xml>" +

                    "</head>" +
                    "<body width='100%' bgcolor='#eee' style='margin: 0; mso-line-height-rule: exactly;'>" +
                        "<center style='width: 100%; background: #eee; text-align: left;'> " +
                            "<div style='max-width: 600px; margin: 30px auto;' class='email-container'>" +
                                "<table role='presentation' cellspacing='0' cellpadding='0' border='0' width='600' align='center'>" +
                                "<tr>" +
                                "<td>" +
                                "<table role='presentation' cellspacing='0' cellpadding='0' border='0' align='center' width='100%' style='max-width: 600px; box-shadow: rgba(0,0,0,.1) 0 20px 10px -10px;'>" +

                                    "<tbody><tr>" +
                                        "<td bgcolor='#ffffff' align='center'>" +
                                            "<img src='http://grupoamedia.com.ar/aapolo/maquetacion/img/email-bg.jpg' width='600' height='' alt='' border='0' align='center' style='width: 100%; max-width: 600px; height: auto; margin: auto;' class='g-img'>" +
                                        "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td bgcolor='#ffffff'>" +
                                            CuerpoMail +
                                        "</td>" +
                                    "</tr>" +

                                    "<tr>" +
                                        "<td bgcolor='#ffffff'> " +
                                            "<table role='presentation' cellspacing='0' cellpadding='0' border='0' width='100%'>" +
                                                "<tbody><tr>" +
                                                    "<td><br>" +
                                                    "<br>" +
                                                    "<br></td>" +
                                                "</tr>" +
                                            "</tbody></table>" +
                                        "</td>" +
                                    "</tr>" +

                                "</tbody></table>" +
                                "<table role='presentation' cellspacing='0' cellpadding='0' border='0' align='center' width='100%' style='max-width: 680px; font-family: 'Raleway', sans-serif; color: #888888; font-size: 12px; line-height: 140%;'>" +
                                    "<tbody><tr>" +
                                        "<td style='padding: 40px 10px; width: 100%; font-family: 'Raleway', sans-serif; font-size: 12px; line-height: 140%; text-align: center; color: #888888;' class='x-gmail-data-detectors'>" +
                                            "<webversion style='color: #cccccc; text-decoration: underline; font-weight: bold;'>Este mail ha sido enviada desde <a href='http://www.aapolo.com/' target='_blank'>Asociación Argentina de Polo</a>.</webversion>" +
                                            "<br><br>" +
                                        "</td>" +
                                    "</tr>" +
                                "</tbody></table>" +

                                "</td>" +
                                "</tr>" +
                                "</table>" +
                            "</div>" +
                        "</center>" +

                "</body></html>";
        }

    }
}
