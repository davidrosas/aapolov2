﻿using AAPolo.Data.Utilidades;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Data
{
    public class HandicapAD
    {
        public List<Handicap> BuscarHandicap(int Cod_Handicap, int Cod_HandicapTransicion, int Cod_TipoJugador, int Cod_CategoriaHandicap, int Cod_Vigencia)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Handicap> lHandicap = new List<Handicap>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Handicap",
                    Value = Cod_Handicap
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_HandicapTransicion",
                    Value = Cod_HandicapTransicion
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoJugador",
                    Value = Cod_TipoJugador
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_CategoriaHandicap",
                    Value = Cod_CategoriaHandicap
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Vigencia",
                    Value = Cod_Vigencia
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_HandicapDetalle", lstParametro);

                while (miDataReader.Read())
                {
                    lHandicap.Add(new Handicap()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Handicap"]),
                        HandicapTransicion = new Handicap() {
                            Codigo = Convert.ToInt32(miDataReader["Cod_HandicapTransicion"]),
                            Importe = Convert.ToDecimal(miDataReader["ImporteTransicion"])
                        },
                        tipoJugador = new TipoJugador() {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Tipo_Jugador"]),
                            Nombre = miDataReader["TipoJugador"].ToString()
                        },
                        Categoria = new CategoriaHandicap() {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Categoria_Handicap"]),
                            Nombre = miDataReader["CategoriaHandicap"].ToString()
                        },
                        vigencia = new Vigencia() {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Vigencia"]),
                            Nombre = miDataReader["Vigencia"].ToString()
                        },
                        Importe = Convert.ToDecimal(miDataReader["Importe"])
                    });

                }

                Conn.Close();
                return lHandicap;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
