﻿using AAPolo.Data.Utilidades;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Data
{
    public class TorneoAD
    {
        public List<ProductoTorneo> BuscarProductoTorneo(bool SoloActivo)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<ProductoTorneo> lProductoTorneo = new List<ProductoTorneo>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@SoloActivo",
                    Value = SoloActivo
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_ProductoTorneo", lstParametro);

                while (miDataReader.Read())
                {
                    lProductoTorneo.Add(new ProductoTorneo()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Producto"]),
                        Nombre = miDataReader["Nombre"].ToString(),
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false
                    });
                }
                Conn.Close();
                return lProductoTorneo;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Torneo> BuscarTorneo(int Cod_Torneo, int Cod_CategoriaTorneo, int Cod_CategoriaHandicap, bool SoloActivo)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Torneo> lTorneo = new List<Torneo>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = Cod_Torneo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_CategoriaTorneo",
                    Value = Cod_CategoriaTorneo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_CategoriaHandicap",
                    Value = Cod_CategoriaHandicap
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@SoloActivo",
                    Value = SoloActivo
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_torneo", lstParametro);

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["FechaInicio"].ToString()) ? miDataReader["FechaInicio"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]);

                    lTorneo.Add(new Torneo()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Torneo"]),
                        NombreTorneo = miDataReader["NombreTorneo"].ToString(),
                        FechaInicio = new DateTime(Anio, Mes, Dia),
                        strFechaInicio = miDataReader["FechaInicio"].ToString(),
                        strFechaInicioInscripcion = miDataReader["FechaInicioInscripcion"].ToString(),
                        strFechaFinInscripcion = miDataReader["FechaFinInscripcion"].ToString(),
                        Url_Imagen = miDataReader["UrlImagen"].ToString(),
                        Url_ArchivoDocumento = miDataReader["UrlDocumento"].ToString(),
                        InscripcionActiva = Convert.ToInt32(miDataReader["InscripcionActiva"]) == 1 ? true : false,
                        strCostoInscripcion = miDataReader["CostoInscripcion"].ToString(),
                        Handicap_Max_XEquipo = Convert.ToInt32(miDataReader["Handicap_Max_XEquipo"]),
                        Handicap_Min_XEquipo = Convert.ToInt32(miDataReader["Handicap_Min_XEquipo"]),
                        Handicap_Max_XJugador = Convert.ToInt32(miDataReader["Handicap_Max_XJugador"]),
                        Handicap_Min_XJugador = Convert.ToInt32(miDataReader["Handicap_Min_XJugador"]),
                        Handicap_JugadorMin = Convert.ToInt32(miDataReader["Handicap_JugadorMin"]),
                        PermiteMenor = Convert.ToInt32(miDataReader["PermiteMenores"]) == 1 ? true : false,
                        EsDisponibleZona = Convert.ToInt32(miDataReader["EsDisponibleZona"]) == 1 ? true : false,
                        EsDisponibleLlave = Convert.ToInt32(miDataReader["EsDisponibleLlave"]) == 1 ? true : false,
                        EsDisponiblePartidoLlave = Convert.ToInt32(miDataReader["EsDisponiblePartidoLlave"]) == 1 ? true : false,
                        categoriaHandicap = new CategoriaHandicap()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Categoria_Handicap"]),
                            Nombre = miDataReader["NombreCategoriaHandicap"].ToString(),
                        },
                        DescripcionTorneo = miDataReader["DescripcionTorneo"].ToString(),
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        PermiteSuplente = Convert.ToInt32(miDataReader["PermiteSuplente"]) == 1 ? true : false,
                        PermiteInscipcionIndividual = Convert.ToInt32(miDataReader["PermiteInscipcionIndividual"]) == 1 ? true : false,
                        PermiteMenores = Convert.ToInt32(miDataReader["PermiteMenores"]) == 1 ? true : false,
                        tipoTorneo = new TipoTorneo()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Categoria_Handicap"]),
                            Nombre = miDataReader["NombreCategoriaHandicap"].ToString(),
                        },
                        categoriaTorneo = new CategoriaTorneo()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Categoria_Torneo"]),
                            NombreCategoriaTorneo = miDataReader["NombreCategoriaTorneo"].ToString(),
                        },
                        ProductoTorneo = new ProductoTorneo { Codigo = Convert.ToInt32(miDataReader["Cod_Producto"]) }
                    });
                }
                Conn.Close();
                return lTorneo;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Torneo> BuscarTorneoHistorico(int Cod_Torneo, string NombreTorneo, int Cod_CategoriaTorneo, int Cod_CategoriaHandicap, bool SoloActivo)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Torneo> lTorneo = new List<Torneo>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = Cod_Torneo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NombreTorneo",
                    Value = NombreTorneo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_CategoriaTorneo",
                    Value = Cod_CategoriaTorneo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_CategoriaHandicap",
                    Value = Cod_CategoriaHandicap
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@SoloActivo",
                    Value = SoloActivo
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_torneoHistorial", lstParametro);

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["FechaInicio"].ToString()) ? miDataReader["FechaInicio"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]);

                    lTorneo.Add(new Torneo()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Torneo"]),
                        NombreTorneo = miDataReader["NombreTorneo"].ToString(),
                        FechaInicio = new DateTime(Anio, Mes, Dia),
                        strFechaInicio = miDataReader["FechaInicio"].ToString(),
                        strFechaInicioInscripcion = miDataReader["FechaInicioInscripcion"].ToString(),
                        strFechaFinInscripcion = miDataReader["FechaFinInscripcion"].ToString(),
                        Url_Imagen = miDataReader["UrlImagen"].ToString(),
                        Url_ArchivoDocumento = miDataReader["UrlDocumento"].ToString(),
                        InscripcionActiva = Convert.ToInt32(miDataReader["InscripcionActiva"]) == 1 ? true : false,
                        strCostoInscripcion = miDataReader["CostoInscripcion"].ToString(),
                        Handicap_Max_XEquipo = Convert.ToInt32(miDataReader["Handicap_Max_XEquipo"]),
                        Handicap_Min_XEquipo = Convert.ToInt32(miDataReader["Handicap_Min_XEquipo"]),
                        Handicap_Max_XJugador = Convert.ToInt32(miDataReader["Handicap_Max_XJugador"]),
                        Handicap_Min_XJugador = Convert.ToInt32(miDataReader["Handicap_Min_XJugador"]),
                        Handicap_JugadorMin = Convert.ToInt32(miDataReader["Handicap_JugadorMin"]),
                        PermiteMenor = Convert.ToInt32(miDataReader["PermiteMenores"]) == 1 ? true : false,
                        EsDisponibleZona = Convert.ToInt32(miDataReader["EsDisponibleZona"]) == 1 ? true : false,
                        EsDisponibleLlave = Convert.ToInt32(miDataReader["EsDisponibleLlave"]) == 1 ? true : false,
                        EsDisponiblePartidoLlave = Convert.ToInt32(miDataReader["EsDisponiblePartidoLlave"]) == 1 ? true : false,
                        categoriaHandicap = new CategoriaHandicap()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Categoria_Handicap"]),
                            Nombre = miDataReader["NombreCategoriaHandicap"].ToString(),
                        },
                        DescripcionTorneo = miDataReader["DescripcionTorneo"].ToString(),
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        PermiteSuplente = Convert.ToInt32(miDataReader["PermiteSuplente"]) == 1 ? true : false,
                        PermiteInscipcionIndividual = Convert.ToInt32(miDataReader["PermiteInscipcionIndividual"]) == 1 ? true : false,
                        PermiteMenores = Convert.ToInt32(miDataReader["PermiteMenores"]) == 1 ? true : false,
                        tipoTorneo = new TipoTorneo()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Categoria_Handicap"]),
                            Nombre = miDataReader["NombreCategoriaHandicap"].ToString(),
                        },
                        categoriaTorneo = new CategoriaTorneo()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Categoria_Torneo"]),
                            NombreCategoriaTorneo = miDataReader["NombreCategoriaTorneo"].ToString(),
                        },
                        ProductoTorneo = new ProductoTorneo { Codigo = Convert.ToInt32(miDataReader["Cod_Producto"]) }
                    });
                }
                Conn.Close();
                return lTorneo;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Torneo> BuscarTorneoInscipcionActivo(int Cod_Torneo, int Cod_CategoriaTorneo)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Torneo> lTorneo = new List<Torneo>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = Cod_Torneo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_CategoriaTorneo",
                    Value = Cod_CategoriaTorneo
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_torneoInscipcionActivo", lstParametro);

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["FechaInicio"].ToString()) ? miDataReader["FechaInicio"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]);

                    lTorneo.Add(new Torneo()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Torneo"]),
                        NombreTorneo = miDataReader["NombreTorneo"].ToString(),
                        FechaInicio = new DateTime(Anio, Mes, Dia),
                        strFechaInicio = miDataReader["FechaInicio"].ToString(),
                        strFechaInicioInscripcion = miDataReader["FechaInicioInscripcion"].ToString(),
                        strFechaFinInscripcion = miDataReader["FechaFinInscripcion"].ToString(),
                        Url_Imagen = miDataReader["UrlImagen"].ToString(),
                        Url_ArchivoDocumento = miDataReader["UrlDocumento"].ToString(),
                        InscripcionActiva = Convert.ToInt32(miDataReader["InscripcionActiva"]) == 1 ? true : false,
                        strCostoInscripcion = miDataReader["CostoInscripcion"].ToString(),
                        Handicap_Max_XEquipo = Convert.ToInt32(miDataReader["Handicap_Max_XEquipo"]),
                        Handicap_Min_XEquipo = Convert.ToInt32(miDataReader["Handicap_Min_XEquipo"]),
                        Handicap_Max_XJugador = Convert.ToInt32(miDataReader["Handicap_Max_XJugador"]),
                        Handicap_Min_XJugador = Convert.ToInt32(miDataReader["Handicap_Min_XJugador"]),
                        Handicap_JugadorMin = Convert.ToInt32(miDataReader["Handicap_JugadorMin"]),
                        PermiteMenor = Convert.ToInt32(miDataReader["PermiteMenores"]) == 1 ? true : false,
                        categoriaHandicap = new CategoriaHandicap()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Categoria_Handicap"]),
                            Nombre = miDataReader["NombreCategoriaHandicap"].ToString(),
                        },
                        DescripcionTorneo = miDataReader["DescripcionTorneo"].ToString(),
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        PermiteSuplente = Convert.ToInt32(miDataReader["PermiteSuplente"]) == 1 ? true : false,
                        PermiteInscipcionIndividual = Convert.ToInt32(miDataReader["PermiteInscipcionIndividual"]) == 1 ? true : false,
                        PermiteMenores = Convert.ToInt32(miDataReader["PermiteMenores"]) == 1 ? true : false,
                        tipoTorneo = new TipoTorneo()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Categoria_Handicap"]),
                            Nombre = miDataReader["NombreCategoriaHandicap"].ToString(),
                        },
                        categoriaTorneo = new CategoriaTorneo()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Categoria_Torneo"]),
                            NombreCategoriaTorneo = miDataReader["NombreCategoriaTorneo"].ToString(),
                        }
                    });
                }
                Conn.Close();
                return lTorneo;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<TipoTorneo> BuscarTipoTorneo()
        {
            SqlDataReader miDataReader;
            List<TipoTorneo> lTipoTorneo = new List<TipoTorneo>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_TipoTorneo");

                while (miDataReader.Read())
                {
                    lTipoTorneo.Add(new TipoTorneo()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Categoria_Torneo"]),
                        Nombre = miDataReader["NombreTipo"].ToString(),

                    });
                }
                Conn.Close();
                return lTipoTorneo;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<CategoriaHandicap> BuscarCategoriaHandicapTorneo()
        {
            SqlDataReader miDataReader;
            List<CategoriaHandicap> lCategoriaHandicap = new List<CategoriaHandicap>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_categoriaHAndicapTorneo");

                while (miDataReader.Read())
                {
                    lCategoriaHandicap.Add(new CategoriaHandicap()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Categoria_Handicap"]),
                        Nombre = miDataReader["NombreCategoriaHandicap"].ToString(),

                    });
                }
                Conn.Close();
                return lCategoriaHandicap;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarTorneo(Torneo obj)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Nombre",
                    Value = obj.NombreTorneo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoTorneo",
                    Value = obj.tipoTorneo.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.DateTime,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@FechaInicioTorneo",
                    Value = obj.FechaInicio
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.DateTime,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@FechaInicioInscripcion",
                    Value = obj.FechaInicioInscripcion
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.DateTime,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@FechaFinInscripcion",
                    Value = obj.FechaFinInscripcion
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_CategoriaHandicap",
                    Value = obj.categoriaHandicap.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@UrlImagen",
                    Value = String.IsNullOrEmpty(obj.Url_Imagen) ? "" : obj.Url_Imagen
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@UrlArchivo",
                    Value = String.IsNullOrEmpty(obj.Url_ArchivoDocumento) ? "" : obj.Url_ArchivoDocumento
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@DescripcionTorneo",
                    Value = String.IsNullOrEmpty(obj.DescripcionTorneo) ? "" : obj.DescripcionTorneo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Handicap_Max_XEquipo",
                    Value = obj.Handicap_Max_XEquipo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Handicap_Min_XEquipo",
                    Value = obj.Handicap_Min_XEquipo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Handicap_Max_XJugador",
                    Value = obj.Handicap_Max_XJugador
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Handicap_Min_XJugador",
                    Value = obj.Handicap_Min_XJugador
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Handicap_JugadorMin",
                    Value = obj.Handicap_JugadorMin
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@PermiteInscipcionIndividual",
                    Value = obj.PermiteInscipcionIndividual
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@PermiteInscipcionMenor",
                    Value = obj.PermiteMenor
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_UsuarioAlta",
                    Value = obj.UsuarioAlta.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Decimal,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@CostoInscripcion",
                    Value = !String.IsNullOrEmpty(obj.strCostoInscripcion) ? Convert.ToDecimal(obj.strCostoInscripcion.Replace('.', ',')) : 0
                });

                string query = "sp_ins_torneo";
                if (obj.Codigo > 0)
                {
                    query = "sp_upd_torneo";

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_Torneo",
                        Value = obj.Codigo
                    });

                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@CodigoProducto",
                    Value = obj.ProductoTorneo.Codigo
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, query, lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoTorneo(int Codigo, int EstadoTorneo)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@EstadoTorneo",
                    Value = EstadoTorneo
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_EstadoTorneo", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Equipo> BuscarEquipo(int Cod_Equipo, int Cod_Persona, int Cod_Torneo)
        {

            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Equipo> lEquipo = new List<Equipo>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Equipo",
                    Value = Cod_Equipo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Persona",
                    Value = Cod_Persona
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = Cod_Torneo
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_equipo", lstParametro);

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["FechaInicio"].ToString()) ? miDataReader["FechaInicio"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]);
                    if (!lEquipo.Any(a => a.Codigo == Convert.ToInt32(miDataReader["Cod_Equipo"])))
                    {
                        lEquipo.Add(new Equipo()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Equipo"]),
                            NombreEquipo = miDataReader["NombreEquipo"].ToString(),
                            torneo = new Torneo()
                            {
                                Codigo = Convert.ToInt32(miDataReader["Cod_Torneo"]),
                                NombreTorneo = miDataReader["NombreTorneo"].ToString(),
                                FechaInicio = new DateTime(Anio, Mes, Dia),
                                strFechaInicio = miDataReader["FechaInicio"].ToString(),
                                PermiteInscipcionIndividual = Convert.ToInt32(miDataReader["PermiteInscripcionIndividual"]) == 1 ? true : false
                            },
                            detallePago = new DetallePago() {
                                Codigo = Convert.ToInt32(miDataReader["Cod_Pago"]),
                                estado = new Estado() { Codigo = Convert.ToInt32(miDataReader["nro_resultado_pago"]) }
                            },
                            lPersona = this.ListadoPersonaEquipo(Convert.ToInt32(miDataReader["Cod_Equipo"]))
                        });
                    }


                }
                /*DataTable dt = new DataTable();
                dt.Load(miDataReader);

                lEquipo = (from x in dt.AsEnumerable()
                           select new Equipo()
                           {
                               Codigo = x.Field<int>("Cod_Equipo"),
                               torneo = new Torneo()
                               {
                                   NombreTorneo = x.Field<string>("NombreTorneo"),
                                   strFechaInicio = x.Field<string>("FechaInicio"),
                                   PermiteInscipcionIndividual = x.Field<bool>("PermiteInscripcionIndividual") //== 1 ? true : false
                               },
                               lPersona = (from p in dt.AsEnumerable()
                                           where x.Field<int>("Cod_Equipo")
                                           select new Persona() {
                                               Codigo = x.Field<int>("Cod_Equipo"),
                                               Nombre = x.Field<string>("Nombre"),
                                               Apellido = x.Field<string>("Apellido")
                                           }).ToList(),
                           }).ToList();*/

                Conn.Close();
                return lEquipo;
                //return lEquipo.GroupBy(g => g.Codigo).Select(s => s.FirstOrDefault()).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> ListadoPersonaEquipo(int Cod_Equipo)
        {
            List<Persona> lPersona = new List<Persona>();
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Equipo",
                    Value = Cod_Equipo
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_PersonaXEquipo", lstParametro);
                while (miDataReader.Read())
                {
                    if (Cod_Equipo == Convert.ToInt32(miDataReader["Cod_Equipo"]))
                    {
                        lPersona.Add(new Persona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]),
                            Nombre = miDataReader["Nombre"].ToString(),
                            Apellido = miDataReader["Apellido"].ToString(),
                            EsCapitan = Convert.ToInt32(miDataReader["EsCapitan"]) == 1 ? true : false
                        });
                    }
                }
                Conn.Close();
                return lPersona;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public List<Persona> ListadoPersonaEquipoPartido(int Cod_Equipo, int Cod_Partido)
        {
            List<Persona> lPersona = new List<Persona>();
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Equipo",
                    Value = Cod_Equipo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Partido",
                    Value = Cod_Partido
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_PersonaXEquipoPartido", lstParametro);
                while (miDataReader.Read())
                {
                    lPersona.Add(new Persona()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]),
                        Nombre = miDataReader["Nombre"].ToString(),
                        Apellido = miDataReader["Apellido"].ToString(),
                        EsCapitan = Convert.ToInt32(miDataReader["EsCapitan"]) == 1 ? true : false,
                        EsSuplente = Convert.ToInt32(miDataReader["EsSuplente"]) == 1 ? true : false,
                        UrlFoto = miDataReader["UrlFoto"].ToString(),
                        Gol = Convert.ToInt32(miDataReader["Gol"]),
                        Pais = new Pais() {
                            Nombre = miDataReader["NombrePais"].ToString(),
                            UrlImagen = miDataReader["UrlImagenPais"].ToString()
                        },
                        HandicapPersona = new HandicapPersona()
                        {
                            NroHandicap = Convert.ToInt32(miDataReader["NroHandicap"])
                        },
                        Incidencia = new Incidencia()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Incidencia"]),
                        }
                    });
                }
                Conn.Close();
                return lPersona;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ActualizarInscripcionTorneoAutomatico()
        {
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_activaInscripcionTorneo");

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarCodigoProducto(ProductoTorneo obj)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Nombre",
                    Value = obj.Nombre
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_ins_ProductoTorneo", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                    result.CodigoResultado = Convert.ToInt32(miDataReader["CodigoProducto"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Zona> BuscarZonaTorneo(int Cod_Torneo, bool CargarEquipo, int Cod_Zona)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Zona> lZona = new List<Zona>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = Cod_Torneo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Zona",
                    Value = Cod_Zona
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_ZonaTorneo", lstParametro);

                while (miDataReader.Read())
                {
                    lZona.Add(new Zona()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Zona"]),
                        Torneo = new Torneo()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Torneo"]),
                            NombreTorneo = miDataReader["NombreTorneo"].ToString(),
                            EsDisponibleZona = Convert.ToInt32(miDataReader["EsDisponibleZona"]) == 1 ? true : false,
                            EsDisponiblePartidoLlave = Convert.ToInt32(miDataReader["EsDisponibleParidoLlave"]) == 1 ? true : false,
                        },
                        TipoZona = new TipoZona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_TipoZona"]),
                            CantidadEquipo = Convert.ToInt32(miDataReader["Cant_Equipo"])
                        },
                        LetraZona = miDataReader["Caracter"].ToString(),
                        lEquipo = CargarEquipo ? this.BuscarEquipoZona(Convert.ToInt32(miDataReader["Cod_Zona"]), 0, 0) : new List<Equipo>(),
                        lPartido = CargarEquipo ? this.BuscarPartidoZona(Convert.ToInt32(miDataReader["Cod_Zona"]), 0, 0, 0) : new List<Partido>(),
                    });
                }
                Conn.Close();
                return lZona;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarZonaTorneo(Torneo obj)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = obj.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@CantZona",
                    Value = obj.CantZona
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_ins_Zona", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<TipoZona> BuscarTipoZonaTorneo()
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<TipoZona> lTipoZona = new List<TipoZona>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_TipoZona");

                while (miDataReader.Read())
                {
                    lTipoZona.Add(new TipoZona()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_TipoZona"]),
                        Nombre = Convert.ToString(miDataReader["Nombre"]),
                        CantidadEquipo = Convert.ToInt32(miDataReader["Cant_Equipo"])
                    });
                }
                Conn.Close();
                return lTipoZona;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Equipo> BuscarEquipoZonaTorneo(int Cod_Equipo, string Nombre, int Cod_Torneo, int Cod_Zona)
        {

            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Equipo> lEquipo = new List<Equipo>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Equipo",
                    Value = Cod_Equipo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Nombre",
                    Value = Nombre
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = Cod_Torneo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Zona",
                    Value = Cod_Zona
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_equipoZonaTorneo", lstParametro);

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["FechaInicio"].ToString()) ? miDataReader["FechaInicio"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]);

                    lEquipo.Add(new Equipo()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Equipo"]),
                        NombreEquipo = miDataReader["NombreEquipo"].ToString(),
                        torneo = new Torneo()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Torneo"]),
                            NombreTorneo = miDataReader["NombreTorneo"].ToString(),
                            FechaInicio = new DateTime(Anio, Mes, Dia),
                            strFechaInicio = miDataReader["FechaInicio"].ToString(),
                            PermiteInscipcionIndividual = Convert.ToInt32(miDataReader["PermiteInscripcionIndividual"]) == 1 ? true : false,
                        },
                        club = new Club()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Club"]),
                            NombreClub = miDataReader["NombreClub"].ToString()
                        },
                        detallePago = new DetallePago()
                        {
                            strFechaPago = miDataReader["FechaPago"].ToString(),
                            strImporte = miDataReader["Importe"].ToString(),
                            estado = new Estado()
                            {
                                Codigo = Convert.ToInt32(miDataReader["EstadoPago"]),
                            },
                            personaAlta = new Persona()
                            {
                                Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]),
                                Nombre = miDataReader["NombrePersona"].ToString(),
                                Apellido = miDataReader["ApellidoPersona"].ToString(),
                                JugadorID = miDataReader["JugadorID"].ToString(),
                            }
                        },
                        lPersona = new TorneoAD().ListadoPersonaEquipo(Convert.ToInt32(miDataReader["Cod_Equipo"]))
                    });

                }

                Conn.Close();
                return lEquipo.GroupBy(g => g.Codigo).Select(s => s.FirstOrDefault()).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarEquipoZona(Zona obj)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            DataTable tablaEquipo = new DataTable();
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Zona",
                    Value = obj.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoZona",
                    Value = obj.TipoZona.Codigo
                });

                /*-------------------------------------------------------------------------------------------------------*/
                tablaEquipo.Columns.Add("Codigo", typeof(int));
                foreach (var item in obj.lEquipo)
                {
                    tablaEquipo.Rows.Add(item.Codigo);
                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Structured,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@lstEquipo",
                    Value = tablaEquipo
                });
                /*-------------------------------------------------------------------------------------------------------*/

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@CantidadEquipo",
                    Value = obj.lEquipo.Count
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_ins_EquipoZona", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Equipo> BuscarEquipoZona(int Cod_Zona, int Cod_Torneo, int Cod_Equipo)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            DataTable tablaEquipo = new DataTable();
            List<Equipo> lEquipo = new List<Equipo>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Zona",
                    Value = Cod_Zona
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = Cod_Torneo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Equipo",
                    Value = Cod_Equipo
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_equipoZona", lstParametro);

                while (miDataReader.Read())
                {
                    lEquipo.Add(new Equipo()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Equipo"]),
                        NombreEquipo = miDataReader["NombreEquipo"].ToString(),
                        TotalGolZona = Convert.ToInt32(miDataReader["GolTotal"]),
                        PartidosGanados = Convert.ToInt32(miDataReader["CantPartidoGanado"]),
                        torneo = new Torneo()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Torneo"])
                        },
                        club = new Club()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Club"]),
                            NombreClub = miDataReader["NombreClub"].ToString(),
                            UrlImagen = miDataReader["UrlImagen"].ToString()
                        },
                        Zona = new Zona()
                        {
                            TipoZona = new TipoZona()
                            {
                                Codigo = Convert.ToInt32(miDataReader["Cod_TipoZona"]),
                                Nombre = miDataReader["NombreTipoZona"].ToString(),
                                CantidadEquipo = Convert.ToInt32(miDataReader["Cant_Equipo"]),
                            }
                        },
                        lPersona = this.ListadoPersonaEquipoPartido(Convert.ToInt32(miDataReader["Cod_Equipo"]), 0)
                    });

                }

                Conn.Close();
                return lEquipo;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Equipo> BuscarEquipoPartido(int Cod_Zona, int Cod_Torneo, int Cod_Partido)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            DataTable tablaEquipo = new DataTable();
            List<Equipo> lEquipo = new List<Equipo>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Zona",
                    Value = Cod_Zona
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = Cod_Torneo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Partido",
                    Value = Cod_Partido
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_equipoZonaPartido", lstParametro);

                while (miDataReader.Read())
                {
                    lEquipo.Add(new Equipo()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Equipo"]),
                        NombreEquipo = miDataReader["NombreEquipo"].ToString(),
                        torneo = new Torneo()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Torneo"])
                        }
                    });

                }

                Conn.Close();
                return lEquipo;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Equipo> BuscarEquipoPartidoSegundaFase(int Cod_Zona, int Cod_Torneo)

        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            DataTable tablaEquipo = new DataTable();
            List<Equipo> lEquipo = new List<Equipo>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Zona",
                    Value = Cod_Zona
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = Cod_Torneo
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_equipoZonaPartidoSegundaFace", lstParametro);

                while (miDataReader.Read())
                {
                    lEquipo.Add(new Equipo()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Equipo"]),
                        NombreEquipo = miDataReader["NombreEquipo"].ToString(),
                        torneo = new Torneo()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Torneo"])
                        },
                        EsGanador = Convert.ToInt32(miDataReader["EsGanador"]) == 1 ? true : false
                    });

                }

                Conn.Close();
                return lEquipo;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Partido> BuscarPartidoZona(int Cod_Zona, int Cod_Torneo, int Cod_Instancia, int Cod_Partido)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            DataTable tablaEquipo = new DataTable();
            List<Partido> lPartido = new List<Partido>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Zona",
                    Value = Cod_Zona
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = Cod_Torneo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Instancia",
                    Value = Cod_Instancia
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Partido",
                    Value = Cod_Partido
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_PartidoZona", lstParametro);

                while (miDataReader.Read())
                {
                    lPartido.Add(new Partido()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Partido"]),
                        EquipoLocal = new Equipo()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Equipo_Local"]),
                            NombreEquipo = miDataReader["EquipoLocal"].ToString(),
                            //ResultadoEquipo = Convert.ToInt32(miDataReader["ResultadoLocal"]),
                            club = new Club()
                            {
                                NombreClub = miDataReader["ClubLocal"].ToString(),
                                UrlImagen = miDataReader["UrlImagenClubEquipoLocal"].ToString(),
                            },
                            lPersona = this.ListadoPersonaEquipoPartido(Convert.ToInt32(miDataReader["Cod_Equipo_Local"]), Convert.ToInt32(miDataReader["Cod_Partido"]))
                        },
                        EquipoVisitante = new Equipo()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Equipo_Visitante"]),
                            NombreEquipo = miDataReader["EquipoVisitante"].ToString(),
                            //ResultadoEquipo = Convert.ToInt32(miDataReader["ResultadoVisitante"]),
                            club = new Club()
                            {
                                NombreClub = miDataReader["ClubVisitante"].ToString(),
                                UrlImagen = miDataReader["UrlImagenClubEquipoVisitante"].ToString(),
                            },
                            lPersona = this.ListadoPersonaEquipoPartido(Convert.ToInt32(miDataReader["Cod_Equipo_Visitante"]), Convert.ToInt32(miDataReader["Cod_Partido"]))
                        },
                        FechaPartido = Convert.ToDateTime(miDataReader["Fec_Partido"]),
                        Intancia = new Instancia()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Instancia"])
                        },
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        Zona = new Zona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Zona"])
                        },
                        Referee = new Referee()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Referee"])
                        },
                        Veedor = new Persona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Veedor"])
                        },
                        Copa = new Copa()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Copa"])
                        },
                        Cancha = Convert.ToInt32(miDataReader["Cancha"]),
                        Observacion = miDataReader["Observacion"].ToString()
                    });

                }

                Conn.Close();
                return lPartido;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarPartido(Partido obj, int Cod_Usuario)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            DataTable tablaEquipo = new DataTable();
            ResultadoTransaccion result = new ResultadoTransaccion();
            string query = "sp_ins_partido";

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros

                if (obj.Codigo > 0)
                {
                    query = "sp_upd_partido";
                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_Partido",
                        Value = obj.Codigo
                    });
                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = obj.Torneo.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_EquipoLocal",
                    Value = obj.EquipoLocal.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_EquipoVisitante",
                    Value = obj.EquipoVisitante.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@ResuldatoEquipoLocal",
                    Value = obj.EquipoLocal.ResultadoEquipo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@ResuldatoEquipoVisitante",
                    Value = obj.EquipoVisitante.ResultadoEquipo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.DateTime,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@FechaPartido",
                    Value = obj.FechaPartido
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Instancia",
                    Value = obj.Intancia.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Referee",
                    Value = obj.Referee.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Veedor",
                    Value = obj.Veedor.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Copa",
                    Value = obj.Copa != null ? obj.Copa.Codigo : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Zona",
                    Value = obj.Zona != null ? obj.Zona.Codigo : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cancha",
                    Value = obj.Cancha
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Observacion",
                    Value = String.IsNullOrEmpty(obj.Observacion) ? "" : obj.Observacion
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Usuario
                });

                /*-------------------------------------------------------------------------------------------------------*/
                tablaEquipo.Columns.Add("Codigo", typeof(int));
                tablaEquipo.Columns.Add("Gol", typeof(int));
                tablaEquipo.Columns.Add("Cod_Incidencia", typeof(int));
                if (obj.lPersonasGol != null)
                {
                    foreach (var item in obj.lPersonasGol)
                    {
                        int CodIncidencia = (obj.lPersonasIncidencia != null ? obj.lPersonasIncidencia.Find(f => f.Codigo == item.Codigo).Incidencia.Codigo : 0);
                        tablaEquipo.Rows.Add(item.Codigo, item.Gol, CodIncidencia);
                    }
                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Structured,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@lstEquipoResultado",
                    Value = tablaEquipo
                });
                /*-------------------------------------------------------------------------------------------------------*/

                if (obj.Codigo > 0)
                {

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_EquipoSiguientePartido",
                        Value = obj.EquipoDeSigPartido != null ? obj.EquipoDeSigPartido.Codigo : 0
                    });

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Orden",
                        Value = obj.NroOrdenSiguientePartido
                    });
                }

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, query, lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Incidencia> BuscarIncidencia()
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Incidencia> lIncidencia = new List<Incidencia>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_incidencia");

                while (miDataReader.Read())
                {
                    lIncidencia.Add(new Incidencia()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Incidencia"]),
                        Nombre = Convert.ToString(miDataReader["Nombre"])
                    });
                }
                Conn.Close();
                return lIncidencia;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Copa> BuscarCopaTorneo(int Cod_Torneo, int Cod_Copa, bool CargarPartidoZona)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Copa> lCopa = new List<Copa>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = Cod_Torneo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Copa",
                    Value = Cod_Copa
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_Copa", lstParametro);

                while (miDataReader.Read())
                {
                    lCopa.Add(new Copa()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Copa"]),
                        Nombre = Convert.ToString(miDataReader["Nombre"]),
                        Torneo = new Torneo()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Torneo"]),
                            EsDisponiblePartidoLlave = Convert.ToInt32(miDataReader["EsDisponiblePartidoLlave"]) == 1? true:false
                        },
                        EsPrincipal = Convert.ToInt32(miDataReader["EsPrincipal"]) == 1 ? true : false,
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        lPartido = this.BuscarPartidoInstancia(Cod_Torneo, 0, 0, (Convert.ToInt32(miDataReader["EsPrincipal"]) == 1 ? 1 : 0)),
                        lZona = this.BuscarZonaTorneo(Cod_Torneo, false, 0)
                    });
                }
                Conn.Close();
                return lCopa;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Equipo> BuscarEquipoPartidoInstancia(int Cod_Partido)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Partido> lPartido = new List<Partido>();
            bool SoloGanadores = false;
            List<Equipo> lEquipo = new List<Equipo>();
            Partido partido = new Partido();
            try
            {

                partido = this.BuscarPartidoInstancia(0, 0, Cod_Partido, 2).FirstOrDefault();
                SoloGanadores = partido.Copa.EsPrincipal;
                try
                {
                    SqlConnection Conn = ExtensionMethods.ObtConnection();
                    #region parametros
                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_Copa",
                        Value = 0 //partido.Copa.Codigo
                    });

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_Torneo",
                        Value = partido.Torneo.Codigo
                    });
                    #endregion

                    miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_EquipoGanadorZona", lstParametro);

                    while (miDataReader.Read())
                    {
                        lPartido.Add(new Partido()
                        {
                            Zona = new Zona()
                            {
                                Codigo = Convert.ToInt32(miDataReader["Cod_Zona"]),
                                TipoZona = new TipoZona()
                                {
                                    Codigo = Convert.ToInt32(miDataReader["Cod_TipoZona"]),
                                    CantidadEquipo = Convert.ToInt32(miDataReader["Cant_Equipo"])
                                }
                            },
                            Torneo = new Torneo()
                            {
                                Codigo = Convert.ToInt32(miDataReader["Cod_Torneo"]),
                                Copa = new Copa()
                                {
                                    Codigo = Convert.ToInt32(miDataReader["Cod_Copa"]),
                                }
                            },
                            EquipoGanador = new Equipo()
                            {
                                Codigo = Convert.ToInt32(miDataReader["Cod_EquipoGanador"]),
                                NombreEquipo = Convert.ToString(miDataReader["EquipoGanador"]),
                                ResultadoEquipoGol = Convert.ToInt32(miDataReader["GolGanador"]),
                            },
                            EquipoPerdedor = new Equipo()
                            {
                                Codigo = Convert.ToInt32(miDataReader["Cod_EquipoPerdedor"]),
                                NombreEquipo = Convert.ToString(miDataReader["EquipoPerdedor"]),
                                ResultadoEquipoGol = Convert.ToInt32(miDataReader["GolPerdedor"]),
                            }
                        });
                    }
                    Conn.Close();

                    char c = 'A';
                    List<Partido> lPar = new List<Partido>();
                    Equipo e;
                    int Gol = 0;
                    foreach (var item in lPartido.GroupBy(g => g.Zona.Codigo).Select(s => s.FirstOrDefault()))
                    {
                        e = new Equipo();
                        Gol = 0;
                        if (item.Zona.TipoZona.Codigo == ConfiguracionAPP.TipoZona_4Equipo)
                        {
                            if (SoloGanadores)
                            {
                                e = lPartido.Where(x => x.Zona.Codigo == item.Zona.Codigo).ToList()[2].EquipoGanador;
                            }
                            else
                            {
                                e = lPartido.Where(x => x.Zona.Codigo == item.Zona.Codigo).ToList()[3].EquipoGanador;
                            }
                            e.Zona = item.Zona;
                            lEquipo.Add(e);
                        }
                        else
                        {
                            lPar = lPartido.Where(x => x.Zona.Codigo == item.Zona.Codigo).OrderBy(o => o.EquipoGanador.ResultadoEquipoGol).ToList();

                            if (lPar.Where(x => x.EquipoGanador.Codigo == lPar[0].EquipoGanador.Codigo).Count() == 1 && lPar.Where(x => x.EquipoGanador.Codigo == lPar[1].EquipoGanador.Codigo).Count() == 1 && lPar.Where(x => x.EquipoGanador.Codigo == lPar[2].EquipoGanador.Codigo).Count() == 1)
                            {
                                if (lPar[0].EquipoGanador.ResultadoEquipoGol == lPar[1].EquipoGanador.ResultadoEquipoGol && lPar[2].EquipoGanador.ResultadoEquipoGol == lPar[0].EquipoGanador.ResultadoEquipoGol)
                                {
                                    if (SoloGanadores)
                                    {
                                        e = lPar[0].EquipoGanador;
                                        e.Zona = item.Zona;
                                        lEquipo.Add(e);
                                    }
                                    else
                                    {
                                        e = lPar[1].EquipoGanador;
                                        e.Zona = item.Zona;
                                        lEquipo.Add(e);
                                    }
                                    /*
                                    e = lPar[2].EquipoGanador;
                                    e.Zona = item.Zona;
                                    lEquipo.Add(e);

                                    SOLUCION TEMPORAL

                                    */
                                }
                                else
                                {

                                    if (SoloGanadores)
                                    {
                                        /*foreach (var part in lPar)
                                        {
                                            if (part.EquipoGanador.ResultadoEquipoGol > Gol)
                                            {
                                                e = part.EquipoGanador;
                                                Gol = part.EquipoGanador.ResultadoEquipoGol;
                                            }
                                        }*/
                                        foreach (var part in lPar.OrderBy(o => o.EquipoGanador.ResultadoEquipoGol))
                                        {
                                            if (part.EquipoGanador.ResultadoEquipoGol >= Gol)
                                            {
                                                e = part.EquipoGanador;
                                                Gol = part.EquipoGanador.ResultadoEquipoGol;
                                            }
                                        }
                                    }
                                    else
                                    {

                                        /*for (int i = 0; i < lPar.Count; i++)
                                        {
                                            if (lPar[i].EquipoGanador.ResultadoEquipoGol > Gol && i < lPar.Count) // CHEQUEAR LOGUICA
                                            {
                                                e = lPar[i].EquipoGanador;
                                                Gol = lPar[i].EquipoGanador.ResultadoEquipoGol;
                                            }
                                        }*/

                                        foreach (var part in lPar.OrderBy(o => o.EquipoGanador.ResultadoEquipoGol))
                                        {
                                            if (part.EquipoGanador.ResultadoEquipoGol > Gol && part.EquipoGanador.Codigo != lPar.OrderBy(o => o.EquipoGanador.ResultadoEquipoGol).LastOrDefault().EquipoGanador.Codigo)
                                            {
                                                e = part.EquipoGanador;
                                                Gol = part.EquipoGanador.ResultadoEquipoGol;
                                            }
                                        }
                                    }

                                    e.Zona = item.Zona;
                                    lEquipo.Add(e);
                                }
                            }
                            else
                            {
                                if (SoloGanadores)
                                {
                                    if (lPar.Where(x => x.EquipoGanador.Codigo == lPar[0].EquipoGanador.Codigo).Count() > 1 && lPar.Where(x => x.EquipoGanador.Codigo == lPar[1].EquipoGanador.Codigo).Count() > 1)
                                    {
                                        if (lPar[0].EquipoGanador.ResultadoEquipoGol > lPar[1].EquipoGanador.ResultadoEquipoGol || lPar[0].EquipoGanador.ResultadoEquipoGol == lPar[1].EquipoGanador.ResultadoEquipoGol)
                                        {
                                            e = lPar[0].EquipoGanador;
                                            Gol = lPar[0].EquipoGanador.ResultadoEquipoGol;
                                        }
                                        else if (lPar[0].EquipoGanador.ResultadoEquipoGol < lPar[1].EquipoGanador.ResultadoEquipoGol)
                                        {
                                            e = lPar[1].EquipoGanador;
                                            Gol = lPar[1].EquipoGanador.ResultadoEquipoGol;
                                        }
                                    }
                                    else if (lPar.Where(x => x.EquipoGanador.Codigo == lPar[0].EquipoGanador.Codigo).Count() > 1 && lPar.Where(x => x.EquipoGanador.Codigo == lPar[2].EquipoGanador.Codigo).Count() > 1)
                                    {
                                        if (lPar[0].EquipoGanador.ResultadoEquipoGol > lPar[2].EquipoGanador.ResultadoEquipoGol || lPar[0].EquipoGanador.ResultadoEquipoGol == lPar[2].EquipoGanador.ResultadoEquipoGol)
                                        {
                                            e = lPar[0].EquipoGanador;
                                            Gol = lPar[0].EquipoGanador.ResultadoEquipoGol;
                                        }
                                        else if (lPar[0].EquipoGanador.ResultadoEquipoGol < lPar[2].EquipoGanador.ResultadoEquipoGol)
                                        {
                                            e = lPar[2].EquipoGanador;
                                            Gol = lPar[2].EquipoGanador.ResultadoEquipoGol;
                                        }
                                    }
                                    else if (lPar.Where(x => x.EquipoGanador.Codigo == lPar[1].EquipoGanador.Codigo).Count() > 1 && lPar.Where(x => x.EquipoGanador.Codigo == lPar[2].EquipoGanador.Codigo).Count() > 1)
                                    {
                                        if (lPar[1].EquipoGanador.ResultadoEquipoGol > lPar[2].EquipoGanador.ResultadoEquipoGol || lPar[1].EquipoGanador.ResultadoEquipoGol == lPar[2].EquipoGanador.ResultadoEquipoGol)
                                        {
                                            e = lPar[1].EquipoGanador;
                                            Gol = lPar[1].EquipoGanador.ResultadoEquipoGol;
                                        }
                                        else if (lPar[1].EquipoGanador.ResultadoEquipoGol < lPar[2].EquipoGanador.ResultadoEquipoGol)
                                        {
                                            e = lPar[2].EquipoGanador;
                                            Gol = lPar[2].EquipoGanador.ResultadoEquipoGol;
                                        }
                                    }

                                }
                                else
                                {
                                    //lPar = lPar.GroupBy(g => g.EquipoGanador.Codigo).ToList();
                                    if (lPar[0].EquipoGanador.Codigo == lPar[1].EquipoGanador.Codigo && lPar[1].EquipoGanador.Codigo != lPar[2].EquipoGanador.Codigo)
                                    {
                                        e = lPar[2].EquipoGanador;
                                        Gol = lPar[2].EquipoGanador.ResultadoEquipoGol;
                                    }
                                    else if (lPar[0].EquipoGanador.Codigo != lPar[1].EquipoGanador.Codigo && lPar[1].EquipoGanador.Codigo == lPar[2].EquipoGanador.Codigo)
                                    {
                                        e = lPar[0].EquipoGanador;
                                        Gol = lPar[0].EquipoGanador.ResultadoEquipoGol;
                                    }
                                    else
                                    {
                                        e = lPar[1].EquipoGanador;
                                        Gol = lPar[1].EquipoGanador.ResultadoEquipoGol;
                                    }
                                }

                                e.Zona = item.Zona;
                                lEquipo.Add(e);
                            }

                        }
                        lEquipo.LastOrDefault().Zona.LetraZona = c.ToString();
                        c++;
                    }
                    lEquipo = lEquipo.Where(x => x.Zona.Codigo == partido.ZonaLocal.Codigo || x.Zona.Codigo == partido.ZonaVisitante.Codigo).ToList();
                    return lEquipo;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public ResultadoTransaccion GuardarCopa(List<Copa> obj, int Cod_Usuario)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            DataTable tablaEquipoLlave1 = new DataTable();
            DataTable tablaEquipoLlave2 = new DataTable();
            ResultadoTransaccion result = new ResultadoTransaccion();
            string query = "sp_ins_copa";

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NombreCopa1",
                    Value = obj[0].Nombre
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NombreCopa2",
                    Value = obj[1].Nombre
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = obj[0].Torneo.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@EsActivo",
                    Value = obj[0].EsActivo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario_Alta",
                    Value = Cod_Usuario
                });


                /*-------------------------------------------------------------------------------------------------------*/
                tablaEquipoLlave1.Columns.Add("CodZonaLocal", typeof(int));
                tablaEquipoLlave1.Columns.Add("CodZonaVisitante", typeof(int));

                foreach (var item in obj[0].lPartido)
                {
                    tablaEquipoLlave1.Rows.Add(item.ZonaLocal.Codigo, item.ZonaVisitante.Codigo);
                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Structured,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@lstPartidoLlave1",
                    Value = tablaEquipoLlave1
                });
                /*------------------------------------*/
                tablaEquipoLlave2.Columns.Add("CodZonaLocal", typeof(int));
                tablaEquipoLlave2.Columns.Add("CodZonaVisitante", typeof(int));

                foreach (var item in obj[1].lPartido)
                {
                    tablaEquipoLlave2.Rows.Add(item.ZonaLocal.Codigo, item.ZonaVisitante.Codigo);
                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Structured,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@lstPartidoLlave2",
                    Value = tablaEquipoLlave2
                });
                /*-------------------------------------------------------------------------------------------------------*/

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, query, lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Partido> BuscarPartidoInstancia(int Cod_Torneo, int Cod_Instancia, int Cod_Partido, int SoloCopaPrincipal)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            DataTable tablaEquipo = new DataTable();
            List<Partido> lPartido = new List<Partido>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = Cod_Torneo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Instancia",
                    Value = Cod_Instancia
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Partido",
                    Value = Cod_Partido
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@SoloCopaPrincipal",
                    Value = SoloCopaPrincipal
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_partidos_instancia", lstParametro);

                while (miDataReader.Read())
                {
                    lPartido.Add(new Partido()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Partido"]),
                        NroOrdenSiguientePartido = Convert.ToInt32(miDataReader["Orden"]),
                        EquipoLocal = new Equipo()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Equipo_Local"]),
                            NombreEquipo = miDataReader["EquipoLocal"].ToString(),
                            ResultadoEquipoGol = Convert.ToInt32(miDataReader["ResultadoLocal"]),
                            club = new Club()
                            {
                                NombreClub = miDataReader["ClubLocal"].ToString(),
                                UrlImagen = miDataReader["UrlImagenClubEquipoLocal"].ToString(),
                            },
                            lPersona = this.ListadoPersonaEquipoPartido(Convert.ToInt32(miDataReader["Cod_Equipo_Local"]), Convert.ToInt32(miDataReader["Cod_Partido"]))
                        },
                        EquipoVisitante = new Equipo()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Equipo_Visitante"]),
                            NombreEquipo = miDataReader["EquipoVisitante"].ToString(),
                            ResultadoEquipoGol = Convert.ToInt32(miDataReader["ResultadoVisitante"]),
                            club = new Club()
                            {
                                NombreClub = miDataReader["ClubVisitante"].ToString(),
                                UrlImagen = miDataReader["UrlImagenClubEquipoVisitante"].ToString(),
                            },
                            lPersona = this.ListadoPersonaEquipoPartido(Convert.ToInt32(miDataReader["Cod_Equipo_Visitante"]), Convert.ToInt32(miDataReader["Cod_Partido"]))
                        },
                        FechaPartido = Convert.ToDateTime(miDataReader["Fec_Partido"]),
                        Intancia = new Instancia()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Instancia"])
                        },
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        Zona = new Zona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Zona"])
                        },
                        Referee = new Referee()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Referee"])
                        },
                        Veedor = new Persona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Veedor"])
                        },
                        Copa = new Copa()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Copa"]),
                            EsPrincipal = Convert.ToInt32(miDataReader["EsPrincipal"]) == 1 ? true : false,
                        },
                        Cancha = Convert.ToInt32(miDataReader["Cancha"]),
                        Observacion = miDataReader["Observacion"].ToString(),
                        ZonaLocal = new Zona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_ZonaLocal"]),
                            LetraZona = miDataReader["CaracterZonaLocal"].ToString()
                        },
                        ZonaVisitante = new Zona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_ZonaVisitante"]),
                            LetraZona = miDataReader["CaracterZonaVisitante"].ToString()
                        },
                        Torneo = new Torneo()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Torneo"]),
                            EsDisponiblePartidoLlave = Convert.ToInt32(miDataReader["EsDisponiblePartidoLlave"]) == 1 ? true : false,
                        }
                    });

                }

                Conn.Close();
                return lPartido;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarPartidoInstancia(Partido obj, int Cod_Usuario)

        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            DataTable tablaEquipo = new DataTable();
            ResultadoTransaccion result = new ResultadoTransaccion();
            string query = "sp_upd_PartidoInstancia";

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Partido",
                    Value = obj.Codigo
                });


                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = obj.Torneo.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_EquipoLocal",
                    Value = obj.EquipoLocal.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_EquipoVisitante",
                    Value = obj.EquipoVisitante.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@ResuldatoEquipoLocal",
                    Value = obj.EquipoLocal.ResultadoEquipo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@ResuldatoEquipoVisitante",
                    Value = obj.EquipoVisitante.ResultadoEquipo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.DateTime,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@FechaPartido",
                    Value = obj.FechaPartido
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Instancia",
                    Value = obj.Intancia.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Referee",
                    Value = obj.Referee.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Veedor",
                    Value = obj.Veedor.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Copa",
                    Value = obj.Copa != null ? obj.Copa.Codigo : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Zona",
                    Value = obj.Zona != null ? obj.Zona.Codigo : 0
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cancha",
                    Value = obj.Cancha
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Observacion",
                    Value = String.IsNullOrEmpty(obj.Observacion) ? "" : obj.Observacion
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Usuario
                });

                /*-------------------------------------------------------------------------------------------------------*/
                tablaEquipo.Columns.Add("Codigo", typeof(int));
                tablaEquipo.Columns.Add("Gol", typeof(int));
                tablaEquipo.Columns.Add("Cod_Incidencia", typeof(int));
                if (obj.lPersonasGol != null)
                {
                    foreach (var item in obj.lPersonasGol)
                    {
                        int CodIncidencia = obj.lPersonasIncidencia.Find(f => f.Codigo == item.Codigo).Incidencia.Codigo;
                        tablaEquipo.Rows.Add(item.Codigo, item.Gol, CodIncidencia);
                    }
                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Structured,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@lstEquipoResultado",
                    Value = tablaEquipo
                });
                /*-------------------------------------------------------------------------------------------------------*/

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, query, lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion DisponibleZona(int Cod_Torneo, bool Disponible)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = Cod_Torneo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Estado",
                    Value = Disponible
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_DisponibleZonaTorneo", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion DisponibleLlaves(int Cod_Torneo, bool Disponible)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = Cod_Torneo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Estado",
                    Value = Disponible
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_DisponibleLlavesTorneo", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion DisponibleParidoLlaves(int Cod_Torneo, bool Disponible)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = Cod_Torneo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Estado",
                    Value = Disponible
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_DisponibleParidoLlavesTorneo", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
