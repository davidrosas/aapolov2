﻿using AAPolo.Data.Utilidades;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Data
{
    public class TemporadaAD
    {
        public List<PuntoXClub> BuscarTemporadaPuntajeXClub(int Cod_Canje, int Cod_Club, int Cod_Temporada, int Cod_CategoriaAccion)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<PuntoXClub> lPuntoXClub = new List<PuntoXClub>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Canje",
                    Value = Cod_Canje
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Club",
                    Value = Cod_Club
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Temporada",
                    Value = Cod_Temporada
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_CategoriaAccion",
                    Value = Cod_CategoriaAccion
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_CanjePuntosXClub", lstParametro);

                while (miDataReader.Read())
                {
                    lPuntoXClub.Add(new PuntoXClub()
                    {
                        Cod_Canje = Convert.ToInt32(miDataReader["Cod_Canje"]),
                        Codigo = Convert.ToInt32(miDataReader["Cod_Club"]),
                        NombreClub = Convert.ToString(miDataReader["NombreClub"]),
                        CategoriaAccion = new CategoriaAccion() {
                            Codigo = Convert.ToInt32(miDataReader["Cod_CategoriaAccion"]),
                            Nombre = Convert.ToString(miDataReader["NombreCategoria"]),
                        },
                        Temporada = new Temporada() {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Temporada"]),
                            Nombre = Convert.ToString(miDataReader["NombreTemporada"]),
                        },
                        Puntos = Convert.ToInt32(miDataReader["Puntos"]),
                        Accion = Convert.ToString(miDataReader["Accion"]),
                        Premio = Convert.ToString(miDataReader["Premio"]),
                        UrlImagenEvidencia = Convert.ToString(miDataReader["UrlImagenEvidencia"]),
                        AnioTemporada = Convert.ToInt32(miDataReader["AnioTemporada"]),
                        EsActivoCanje = Convert.ToBoolean(miDataReader["EsActivo"]),

                    });
                }

                Conn.Close();
                return lPuntoXClub;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<CategoriaAccion> BuscarCategoriaAccion()
        {
            SqlDataReader miDataReader;
            List<CategoriaAccion> lCategoriaAccion = new List<CategoriaAccion>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();


                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_CategoriaAccion");

                while (miDataReader.Read())
                {
                    lCategoriaAccion.Add(new CategoriaAccion()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_CategoriaAccion"]),
                        Nombre = miDataReader["Nombre"].ToString()
                    });
                }

                Conn.Close();
                return lCategoriaAccion;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Temporada> BuscarTemporada()
        {
            SqlDataReader miDataReader;
            List<Temporada> lTemporada = new List<Temporada>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();


                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_Temporada");

                while (miDataReader.Read())
                {
                    lTemporada.Add(new Temporada()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Temporada"]),
                        Nombre = miDataReader["Nombre"].ToString()
                    });
                }

                Conn.Close();
                return lTemporada;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarTemporada(PuntoXClub obj, int Cod_Usuario)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros

                string query = "sp_ins_CanjePuntosXClub";
                if (obj.Cod_Canje > 0)
                {
                    query = "sp_upd_CanjePuntosXClub";

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_Canje",
                        Value = obj.Cod_Canje
                    });
                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Club",
                    Value = obj.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_CategoriaAccion",
                    Value = obj.CategoriaAccion.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Temporada",
                    Value = obj.Temporada.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Puntos",
                    Value = obj.Puntos
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@txt_accion",
                    Value = obj.Accion
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@txt_premio",
                    Value = obj.Premio
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Url_ImagenEvidencia",
                    Value = obj.UrlImagenEvidencia
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@AnioTemporada",
                    Value = obj.AnioTemporada
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Usuario
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, query, lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoTemporada(int Codigo, int EstadoTemporada)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Canje",
                    Value = Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@EstadoTemporada",
                    Value = EstadoTemporada
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_EstadoTemporada", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<PuntoXClub> BuscarTemporadaPuntajeXClubPublico(int Cod_Canje, int Cod_Club, int Cod_Temporada, int Cod_CategoriaAccion)

        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<PuntoXClub> lPuntoXClub = new List<PuntoXClub>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Canje",
                    Value = Cod_Canje
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Club",
                    Value = Cod_Club
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Temporada",
                    Value = Cod_Temporada
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_CategoriaAccion",
                    Value = Cod_CategoriaAccion
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_CanjePuntosXClubPublico", lstParametro);

                while (miDataReader.Read())
                {
                    lPuntoXClub.Add(new PuntoXClub()
                    {
                        Cod_Canje = Convert.ToInt32(miDataReader["Cod_Canje"]),
                        Codigo = Convert.ToInt32(miDataReader["Cod_Club"]),
                        NombreClub = Convert.ToString(miDataReader["NombreClub"]),
                        UrlImagen = Convert.ToString(miDataReader["UrlImagenClub"]),
                        CategoriaAccion = new CategoriaAccion()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_CategoriaAccion"]),
                            Nombre = Convert.ToString(miDataReader["NombreCategoria"]),
                        },
                        Temporada = new Temporada()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Temporada"]),
                            Nombre = Convert.ToString(miDataReader["NombreTemporada"]),
                        },
                        Puntos = Convert.ToInt32(miDataReader["Puntos"]),
                        Accion = Convert.ToString(miDataReader["Accion"]),
                        Premio = Convert.ToString(miDataReader["Premio"]),
                        UrlImagenEvidencia = Convert.ToString(miDataReader["UrlImagenEvidencia"]),
                        AnioTemporada = Convert.ToInt32(miDataReader["AnioTemporada"]),
                        EsActivoCanje = Convert.ToBoolean(miDataReader["EsActivo"]),

                    });
                }

                Conn.Close();
                return lPuntoXClub;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
