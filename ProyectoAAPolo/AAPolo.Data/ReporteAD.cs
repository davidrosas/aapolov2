﻿using AAPolo.Data.Utilidades;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Data
{
    public class ReporteAD
    {
        public List<Club> BuscarReporteClub(int Cod_Persona, int AnioEnCurso, int Cod_Club)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Club> lClub = new List<Club>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region PArametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Persona",
                    Value = Cod_Persona
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@AnioEnCurso",
                    Value = AnioEnCurso
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Club",
                    Value = Cod_Club
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_PagosClubReporte", lstParametro);

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["FechaVencimiento"].ToString()) ? miDataReader["FechaVencimiento"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]);

                    string FechaPago = !String.IsNullOrEmpty(miDataReader["FechaPago"].ToString()) ? miDataReader["FechaPago"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int DiaPago = Convert.ToInt32(FechaPago.Split('/')[0]),
                        MesPago = Convert.ToInt32(FechaPago.Split('/')[1]),
                        AnioPago = Convert.ToInt32(FechaPago.Split('/')[2]);

                    lClub.Add(new Club()
                    {
                        detallePago = new DetallePago()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_HistorialPago"]),
                            Detalle = miDataReader["Detalle"].ToString(),
                            strImporte = miDataReader["Importe"].ToString(),
                            PagadoEsteAnio = Convert.ToInt32(miDataReader["PagadoEsteAnio"]) == 1 ? true : false,
                            strFechaPago = miDataReader["FechaPago"].ToString(),
                            estado = new Estado() { Codigo = Convert.ToInt32(miDataReader["Cod_EstadoPago"]) }
                        },
                        Codigo = Convert.ToInt32(miDataReader["Cod_Club"]),
                        NombreClub = miDataReader["NombreClub"].ToString(),
                        AfiliacionVenciada = Convert.ToInt32(miDataReader["Vencido"]) == 1 ? true : false,
                        FechaPagoAfilicion = new DateTime(AnioPago, MesPago, DiaPago),
                        strFechaPagoAfilicion = miDataReader["FechaPago"].ToString(),
                        Categoria = new CategoriaClub()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_CategoriaClub"]),
                            NombreCategoria = miDataReader["NombreCategoriaClub"].ToString()
                        },
                        PersonaAdmin = new Persona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_UsuarioAdm"]),
                            Nombre = miDataReader["NombreAdm"].ToString(),
                            Apellido = miDataReader["ApellidoAdm"].ToString(),
                        },
                    });

                }

                Conn.Close();
                return lClub.GroupBy(x => x.Codigo).Select(s => s.FirstOrDefault()).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ReporteEstadisticaHandicap BuscarReporteEstadisticaHandicap()
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ReporteEstadisticaHandicap EstadisticaHandicap = new ReporteEstadisticaHandicap();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_Estadistica_Reporte_handicap_jugador", lstParametro);
                while (miDataReader.Read())
                {
                    EstadisticaHandicap.TotalJugadores = Convert.ToInt32(miDataReader["TotalJugadores"]);
                    EstadisticaHandicap.TotalPagaronEsteAnio = Convert.ToInt32(miDataReader["PagoEsteAnio"]);
                }
                Conn.Close();
                return EstadisticaHandicap;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscarReportePagoHandicap(int Cod_Persona, string Cadena, int Cod_TipoJugador, int Cod_Club, int Nro_Handicap)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Persona> lper = new List<Persona>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Persona
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoJugador",
                    Value = Cod_TipoJugador
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NroHandicap",
                    Value = Nro_Handicap
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NombreDocumento",
                    Value = Cadena
                });
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Club",
                    Value = Cod_Club
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_reporte_handicap_jugador", lstParametro);
                int Dia, Mes, Anio;
                string Fecha;
                while (miDataReader.Read())
                {
                    Fecha = miDataReader["FechaNacimiento"].ToString();
                    Dia = Convert.ToInt32(Fecha.Split('/')[0]);
                    Mes = Convert.ToInt32(Fecha.Split('/')[1]);
                    Anio = Convert.ToInt32(Fecha.Split('/')[2]);

                    lper.Add(new Persona()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]),
                        Nombre = miDataReader["Nombre"].ToString(),
                        Apellido = miDataReader["Apellido"].ToString(),
                        DocumentoIdentidad = miDataReader["DocumentoIdentidad"].ToString(),
                        strFechaNacimiento = Fecha,
                        FechaNacimiento = new DateTime(Anio, Mes, Dia),
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        TipoDocumento = new TipoDocumento()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_TipoDocumento"]),
                        },
                        Genero = new Genero()
                        {
                            Nombre = miDataReader["NombreGenero"].ToString()
                        },
                        Pais = new Pais()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Pais"]),
                            Nombre = miDataReader["Nacionalidad"].ToString()
                        },
                        TipoJugador = new TipoJugador
                        {
                            Nombre = miDataReader["NombreTipoJugador"].ToString()
                        },
                        HandicapPersona = new HandicapPersona()
                        {
                            NroHandicap = Convert.ToInt32(miDataReader["NroHandicap"]),
                            //Nombre = miDataReader["CategoriaHandicap"].ToString(),
                            Categoria = new CategoriaHandicap() { Nombre = miDataReader["CategoriaHandicap"].ToString() },
                            EsPrincipal = true,
                            strFechaPago = miDataReader["fechaPago"].ToString(),
                            EsVencido = Convert.ToInt32(miDataReader["Vencido"]) == 1 ? true : false,
                            strImporte = miDataReader["Importe"].ToString(),
                            PagoEsteAnio = Convert.ToInt32(miDataReader["PagoEsteAnio"]) == 1 ? true : false,
                            detallePago = new DetallePago()
                            {
                                Codigo = Convert.ToInt32(miDataReader["Cod_Pago"]),
                                Detalle = miDataReader["Detalle"].ToString(),
                                strImporte = miDataReader["Importe"].ToString(),
                                collection_id = miDataReader["collection_id"].ToString(),
                                estado = new Estado()
                                {
                                    Codigo = Convert.ToInt32(miDataReader["EstadoPago"]),
                                }
                            },
                            vigencia = new Vigencia()
                            {
                                Nombre = miDataReader["Vigencia"].ToString()
                            }
                        },
                        clubUsuarioPrincipal = new ClubUsuario()
                        {
                            NombreClub = miDataReader["NombreClub"].ToString(),
                        },
                    });
                }

                Conn.Close();
                return lper.GroupBy(g => g.Codigo).Select(s => s.FirstOrDefault()).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Equipo> BuscarReporteEquipo(int Cod_Equipo, int Cod_Torneo, string Cadena, int Cod_EstadoPago)
        {

            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Equipo> lEquipo = new List<Equipo>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Equipo",
                    Value = Cod_Equipo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = Cod_Torneo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NombreEquipo",
                    Value = Cadena
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_EstadoPago",
                    Value = Cod_EstadoPago
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_equipoReporte", lstParametro);

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["FechaInicio"].ToString()) ? miDataReader["FechaInicio"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]);

                    lEquipo.Add(new Equipo()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Equipo"]),
                        NombreEquipo = miDataReader["NombreEquipo"].ToString(),
                        torneo = new Torneo()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Torneo"]),
                            NombreTorneo = miDataReader["NombreTorneo"].ToString(),
                            FechaInicio = new DateTime(Anio, Mes, Dia),
                            strFechaInicio = miDataReader["FechaInicio"].ToString(),
                            PermiteInscipcionIndividual = Convert.ToInt32(miDataReader["PermiteInscripcionIndividual"]) == 1 ? true : false,
                        },
                        club = new Club()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Club"]),
                            NombreClub = miDataReader["NombreClub"].ToString()
                        },
                        detallePago = new DetallePago()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Pago"]),
                            Detalle = miDataReader["Detalle"].ToString(),
                            strFechaPago = miDataReader["FechaPago"].ToString(),
                            strImporte = miDataReader["Importe"].ToString(),
                            estado = new Estado()
                            {
                                Codigo = Convert.ToInt32(miDataReader["EstadoPago"]),
                            },
                            personaAlta = new Persona()
                            {
                                Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]),
                                Nombre = miDataReader["NombrePersona"].ToString(),
                                Apellido = miDataReader["ApellidoPersona"].ToString(),
                                JugadorID = miDataReader["JugadorID"].ToString(),
                            }
                        },
                        lPersona = new TorneoAD().ListadoPersonaEquipo(Convert.ToInt32(miDataReader["Cod_Equipo"]))
                    });

                }

                Conn.Close();
                //return lEquipo.GroupBy(g => g.Codigo).Select(s => s.FirstOrDefault()).ToList();
                return lEquipo;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<MedioDePago> BuscarMedioDePago()
        {
            SqlDataReader miDataReader;
            List<MedioDePago> lMediaDePago = new List<MedioDePago>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_MedioDePago");

                while (miDataReader.Read())
                {
                    lMediaDePago.Add(new MedioDePago()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Medio"]),
                        Nombre = miDataReader["Nombre"].ToString()
                    });

                }

                Conn.Close();
                return lMediaDePago;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ConfirmarPagoHandicap(Persona obj, int Cod_UsuarioTransaccion)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();
            string query = "sp_upd_PagoHandicapManual";

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Url_Imagen",
                    Value = obj.HandicapPersona.detallePago.Url_Imagen
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Medio",
                    Value = obj.HandicapPersona.detallePago.medioDePago.Codigo
                });

                if (obj.HandicapPersona.detallePago.Codigo > 0)
                {
                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_Pago",
                        Value = obj.HandicapPersona.detallePago.Codigo
                    });
                }
                else
                {
                    query = "sp_ins_PagoHandicapManual";
                    
                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_Handicap",
                        Value = obj.HandicapPersona.Codigo
                    });

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@EsPase",
                        Value = obj.HandicapPersona.EsPase
                    });

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_UsuarioTransaccion",
                        Value = Cod_UsuarioTransaccion
                    });

                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoDocumento",
                    Value = obj.TipoDocumento.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NroDocumento",
                    Value = obj.DocumentoIdentidad
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Pais ",
                    Value = obj.Pais.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.DateTime,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@FechaNacimiento",
                    Value = obj.FechaNacimiento
                });


                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Club",
                    Value = obj.clubUsuarioPrincipal.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = obj.Codigo
                });
                
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NroComprobante",
                    Value = obj.HandicapPersona.detallePago.collection_id
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, query, lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();

                try
                {
                    if (result.EsSatisfactorio && obj.HandicapPersona != null && obj.HandicapPersona.detallePago != null && !String.IsNullOrEmpty(obj.HandicapPersona.detallePago.Url_Imagen))
                    {
                        result.MensajeError = new PagoAD().NotificarNuevosJugadoresParaAdministradoresDeClub(obj.HandicapPersona.detallePago.Url_Imagen);
                    }
                }
                catch (Exception e)
                {
                    result.MensajeError = e.Message;
                }

                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ConfirmarPagoClub(Club obj, int Cod_UsuarioTransaccion)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();
            string query = "sp_upd_PagoClubManual";

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Url_Imagen",
                    Value = obj.detallePago.Url_Imagen
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Medio",
                    Value = obj.detallePago.medioDePago.Codigo
                });

                if (obj.detallePago.Codigo > 0)
                {
                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_Pago",
                        Value = obj.detallePago.Codigo
                    });
                }
                else
                {
                    query = "sp_ins_PagoClubManual";

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.VarChar,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@NroComprobante",
                        Value = obj.detallePago.collection_id
                    });

                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_UsuarioTransaccion",
                    Value = Cod_UsuarioTransaccion
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Club",
                    Value = obj.Codigo
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, query, lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ConfirmarPagoEquipo(Equipo obj, int Cod_UsuarioTransaccion)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();
            string query = "sp_upd_PagoEquipoManual";

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros

                if (obj.detallePago.Codigo > 0)
                {
                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_Pago",
                        Value = obj.detallePago.Codigo
                    });
                }
                else
                {
                    query = "sp_ins_PagoEquipoManual";
                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Url_Imagen",
                    Value = obj.detallePago.Url_Imagen
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Medio",
                    Value = obj.detallePago.medioDePago.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_UsuarioTransaccion",
                    Value = Cod_UsuarioTransaccion
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Equipo",
                    Value = obj.Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@NroComprobante",
                    Value = obj.detallePago.collection_id
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, query, lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion DeclinarPagoEquipo(int Cod_Pago)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();
            string query = "sp_upd_DeclinarPagoEquipoManual";

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Pago",
                    Value = Cod_Pago
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, query, lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #region descargables
        public List<reporteCuentaClub> GetReporteClub()
        {

            SqlDataReader miDataReader;
            List<reporteCuentaClub> lReporteCuentaClub = new List<reporteCuentaClub>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_reporte_cuenta_clubes");

                while (miDataReader.Read())
                {
                    lReporteCuentaClub.Add(new reporteCuentaClub()
                    {
                        cod_club = Convert.ToInt32(miDataReader["cod_club"]),
                        Club = Convert.ToString(miDataReader["Club"]),
                        cod_categoria_club = Convert.ToInt32(miDataReader["cod_categoria_club"]),
                        CategoriaClub = Convert.ToString(miDataReader["CategoriaClub"]),
                        UsuarioAdmin = Convert.ToInt32(miDataReader["UsuarioAdmin"]),
                        NombreAdmin = Convert.ToString(miDataReader["NombreAdmin"]),
                        ApellidoAdmin = Convert.ToString(miDataReader["ApellidoAdmin"]),
                        sn_pago = Convert.ToInt32(miDataReader["sn_pago"]),
                        importe = Convert.ToDecimal(miDataReader["importe"]),
                        fec_ult_pago = Convert.ToDateTime(miDataReader["fec_ult_pago"]),
                        ImporteCat = Convert.ToDecimal(miDataReader["ImporteCat"]),
                        // TipoPago = Convert.ToString(miDataReader["TipoPago"]),
                        txt_url_comprobante = Convert.ToString(miDataReader["txt_url_comprobante"]),
                        ClubID = Convert.ToString(miDataReader["ClubID"])

                    });

                }

                Conn.Close();
                return lReporteCuentaClub;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public List<reporteCuentaClub> GetReporteDatosClub()
        {

            SqlDataReader miDataReader;
            List<reporteCuentaClub> lReporteCuentaClub = new List<reporteCuentaClub>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_reporte_clubes");

                while (miDataReader.Read())
                {
                    lReporteCuentaClub.Add(new reporteCuentaClub()
                    {
                        //cod_club = Convert.ToInt32(miDataReader["cod_club"]),
                        Club = Convert.ToString(miDataReader["Club"]),
                        //cod_categoria_club = Convert.ToInt32(miDataReader["cod_categoria_club"]),
                        CategoriaClub = Convert.ToString(miDataReader["CategoriaClub"]),
                        //UsuarioAdmin = Convert.ToInt32(miDataReader["UsuarioAdmin"]),
                        NombreAdmin = Convert.ToString(miDataReader["NombreAdmin"]),
                        ApellidoAdmin = Convert.ToString(miDataReader["ApellidoAdmin"]),
                        //sn_pago = Convert.ToInt32(miDataReader["sn_pago"]),
                        //importe = Convert.ToDecimal(miDataReader["importe"]),
                        //fec_ult_pago = Convert.ToDateTime(miDataReader["fec_ult_pago"]),
                        //ImporteCat = Convert.ToDecimal(miDataReader["ImporteCat"]),
                        // TipoPago = Convert.ToString(miDataReader["TipoPago"]),
                        //txt_url_comprobante = Convert.ToString(miDataReader["txt_url_comprobante"]),
                        ClubID = Convert.ToString(miDataReader["ClubID"]),
                        //-----------------------------------------------------------------------------------------------------------
                        NroCanchas = Convert.ToInt32(miDataReader["NroCanchas"]),
                        Presidente = Convert.ToString(miDataReader["Presidente"]),
                        VicePresidente = Convert.ToString(miDataReader["VicePresidente"]),
                        FecAlta = Convert.ToString(miDataReader["FecAlta"]),
                        Vocal1 = Convert.ToString(miDataReader["Vocal1"]),
                        Vocal2 = Convert.ToString(miDataReader["Vocal2"]),
                        Vocal3 = Convert.ToString(miDataReader["Vocal3"]),
                        Tesorero = Convert.ToString(miDataReader["Tesorero"]),
                        FecFundacion = Convert.ToString(miDataReader["FecFundacion"]),
                        Historia = Convert.ToString(miDataReader["Historia"]),
                        CantSocios = Convert.ToInt32(miDataReader["CantSocios"]),
                        BoolActivo = Convert.ToString(miDataReader["BoolActivo"]),
                        Mail = Convert.ToString(miDataReader["Mail"]),
                        Teléfono = Convert.ToString(miDataReader["Teléfono"]),
                        TeléfonoClub = Convert.ToString(miDataReader["TeléfonoClub"]),
                        Calle = Convert.ToString(miDataReader["Calle"]),
                        Numero = Convert.ToString(miDataReader["Numero"]),
                        Pais = Convert.ToString(miDataReader["Pais"]),
                        Provincia = Convert.ToString(miDataReader["Provincia"]),
                        Ciudad = Convert.ToString(miDataReader["Ciudad"]),
                        Departamento = Convert.ToString(miDataReader["Departamento"]),
                        CodPostal = Convert.ToString(miDataReader["CodPostal"]),
                        Facebook = Convert.ToString(miDataReader["Facebook"]),
                        Twitter = Convert.ToString(miDataReader["Twitter"]),
                        Instagram = Convert.ToString(miDataReader["Instagram"])
                    });

                }

                Conn.Close();
                return lReporteCuentaClub;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public List<reporteCuentaJugadores> GetReporteCuentaJugadores()
        {
            SqlDataReader miDataReader;
            List<reporteCuentaJugadores> lReporteCuentaJugadores = new List<reporteCuentaJugadores>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_reporte_cuenta_jugadores");

                while (miDataReader.Read())
                {
                    lReporteCuentaJugadores.Add(new reporteCuentaJugadores()
                    {
                        cod_usuario = Convert.ToInt32(miDataReader["cod_usuario"]),
                        Nombre = Convert.ToString(miDataReader["Nombre"]),
                        Apellido = Convert.ToString(miDataReader["Apellido"]),
                        cod_tipo_jugador = Convert.ToInt32(miDataReader["cod_tipo_jugador"]),
                        TipoJugador = Convert.ToString(miDataReader["TipoJugador"]),
                        //cod_handicap = Convert.ToInt32(miDataReader["cod_handicap"]),
                        Handicap = Convert.ToString(miDataReader["Handicap"]),
                        CategoriaHandicap = Convert.ToString(miDataReader["CategoriaHandicap"]),
                        cod_club = Convert.ToInt32(miDataReader["cod_club"]),
                        Club = Convert.ToString(miDataReader["Club"]),
                        Vigencia = Convert.ToString(miDataReader["Vigencia"]),
                        sn_pago = Convert.ToInt32(miDataReader["sn_pago"]),
                        importe = Convert.ToDecimal(miDataReader["importe"]),
                        fec_ult_pago = Convert.ToDateTime(miDataReader["fec_ult_pago"]),
                        Pais = Convert.ToString(miDataReader["Pais"]),
                        //PrecioHandicap = Convert.ToDecimal(miDataReader["PrecioHandicap"]),
                        NroHandicap = Convert.ToInt32(miDataReader["NroHandicap"]),
                        Documento = Convert.ToString(miDataReader["Documento"]),
                        cod_historial_pago = Convert.ToInt32(miDataReader["cod_historial_pago"]),
                        txt_url_comprobante = Convert.ToString(miDataReader["txt_url_comprobante"]),
                        JugadorID = Convert.ToString(miDataReader["JugadorID"]),
                        FechaAlta = Convert.ToDateTime(miDataReader["FechaAlta"]),
                        TipoPago = Convert.ToString(miDataReader["TipoPago"]),
                        cod_mercado_pago = Convert.ToString(miDataReader["cod_mercado_pago"]),

                    });

                }

                Conn.Close();
                return lReporteCuentaJugadores;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public List<reporteCuentaJugadores> GetReporteAllCuentasCorrientesJugadores()
        {
            SqlDataReader miDataReader;
            List<reporteCuentaJugadores> lReporteCuentaJugadores = new List<reporteCuentaJugadores>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_reporte_all_cuenta_jugadores");

                while (miDataReader.Read())
                {
                    lReporteCuentaJugadores.Add(new reporteCuentaJugadores()
                    {
                        cod_usuario = Convert.ToInt32(miDataReader["cod_usuario"]),
                        Nombre = Convert.ToString(miDataReader["Nombre"]),
                        Apellido = Convert.ToString(miDataReader["Apellido"]),
                        cod_tipo_jugador = Convert.ToInt32(miDataReader["cod_tipo_jugador"]),
                        TipoJugador = Convert.ToString(miDataReader["TipoJugador"]),
                        cod_handicap = Convert.ToInt32(miDataReader["cod_handicap"]),
                        Handicap = Convert.ToString(miDataReader["Handicap"]),
                        CategoriaHandicap = Convert.ToString(miDataReader["CategoriaHandicap"]),
                        cod_club = Convert.ToInt32(miDataReader["cod_club"]),
                        Club = Convert.ToString(miDataReader["Club"]),
                        sn_pago = Convert.ToInt32(miDataReader["sn_pago"]),
                        importe = Convert.ToDecimal(miDataReader["importe"]),
                        fec_ult_pago = Convert.ToDateTime(miDataReader["fec_ult_pago"]),
                        Pais = Convert.ToString(miDataReader["Pais"]),
                        PrecioHandicap = Convert.ToDecimal(miDataReader["PrecioHandicap"]),
                        NroHandicap = Convert.ToInt32(miDataReader["NroHandicap"]),
                        Documento = Convert.ToString(miDataReader["Documento"]),
                        cod_historial_pago = Convert.ToInt32(miDataReader["cod_historial_pago"]),
                        txt_url_comprobante = Convert.ToString(miDataReader["txt_url_comprobante"]),
                        JugadorID = Convert.ToString(miDataReader["JugadorID"]),
                        FechaAlta = Convert.ToDateTime(miDataReader["FechaAlta"]),
                        TipoPago = Convert.ToString(miDataReader["TipoPago"]),
                        cod_mercado_pago = Convert.ToString(miDataReader["cod_mercado_pago"]),

                    });

                }

                Conn.Close();
                return lReporteCuentaJugadores;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public List<reporteCuentaTorneo> GetReporteInscripcionesEquiposExcel()
        {
            SqlDataReader miDataReader;
            List<reporteCuentaTorneo> lReporteCuentaTorneo = new List<reporteCuentaTorneo>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_reporte_equipos_inscriptos");

                while (miDataReader.Read())
                {
                    lReporteCuentaTorneo.Add(new reporteCuentaTorneo()
                    {
                        cod_torneo = Convert.ToInt32(miDataReader["cod_torneo"]),
                        cod_equipo = Convert.ToInt32(miDataReader["cod_equipo"]),
                        cod_club = Convert.ToInt32(miDataReader["cod_club"]),
                        cod_usuario = Convert.ToInt32(miDataReader["cod_usuario"]),
                        Torneo = Convert.ToString(miDataReader["Torneo"]),
                        Equipo = Convert.ToString(miDataReader["Equipo"]),
                        Club = Convert.ToString(miDataReader["Club"]),
                        NombreJugador = Convert.ToString(miDataReader["NombreJugador"]),
                        ApellidoJugador = Convert.ToString(miDataReader["ApellidoJugador"]),
                        Email = Convert.ToString(miDataReader["Email"]),
                        Telefono = Convert.ToString(miDataReader["Telefono"]),
                        JugadorID = Convert.ToString(miDataReader["JugadorID"]),
                        cod_historial_pago = Convert.ToInt32(miDataReader["cod_historial_pago"]),
                        txt_url_comprobante = Convert.ToString(miDataReader["txt_url_comprobante"]),
                        sn_pago = Convert.ToInt32(miDataReader["sn_pago"]),
                        importe = Convert.ToDecimal(miDataReader["importe"]),
                        CantTorneos = Convert.ToInt32(miDataReader["CantTorneos"]),
                        fec_ult_pago = Convert.ToDateTime(miDataReader["fec_ult_pago"])
                    });

                }

                Conn.Close();
                return lReporteCuentaTorneo;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public List<reporteJugadorDTO> GetReporteJugadores()
        {
            SqlDataReader miDataReader;
            List<reporteJugadorDTO> lReporteJugadorDTO = new List<reporteJugadorDTO>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_reporte_jugadores");

                while (miDataReader.Read())
                {
                    lReporteJugadorDTO.Add(new reporteJugadorDTO()
                    {
                        JugadorID = Convert.ToString(miDataReader["JugadorID"]),
                        Nombre = Convert.ToString(miDataReader["Nombre"]),
                        Apellido = Convert.ToString(miDataReader["Apellido"]),
                        TipoDoc = Convert.ToString(miDataReader["TipoDoc"]),
                        Documento = Convert.ToString(miDataReader["Documento"]),
                        TipoJugador = Convert.ToString(miDataReader["TipoJugador"]),
                        FecNac = Convert.ToString(miDataReader["FecNac"]),
                        Nacionalidad = Convert.ToString(miDataReader["Nacionalidad"]),
                        Genero = Convert.ToString(miDataReader["Genero"]),
                        Activo = Convert.ToString(miDataReader["Activo"]),
                        Mail = Convert.ToString(miDataReader["Mail"]),
                        Telefono = Convert.ToString(miDataReader["Telefono"]),
                        Club = Convert.ToString(miDataReader["Club"]),
                        ClubRepublica = Convert.ToString(miDataReader["ClubRepublica"]),
                        ClubSec1 = Convert.ToString(miDataReader["ClubSec1"]),
                        ClubSec2 = Convert.ToString(miDataReader["ClubSec2"]),
                        ClubSec3 = Convert.ToString(miDataReader["ClubSec3"]),
                        ClubSec4 = Convert.ToString(miDataReader["ClubSec4"]),
                        TipoHandicapPrincipal = Convert.ToString(miDataReader["TipoHandicapPrincipal"]),
                        CategoriaHandicapPrincipal = Convert.ToString(miDataReader["CategoriaHandicapPrincipal"]),
                        HandicapPrincipal = Convert.ToString(miDataReader["HandicapPrincipal"]),
                        TipoHandicapSecundario = Convert.ToString(miDataReader["TipoHandicapSecundario"]),
                        CategoriaHandicapSecundario = Convert.ToString(miDataReader["CategoriaHandicapSecundario"]),
                        HandicapSecundario = Convert.ToString(miDataReader["HandicapSecundario"]),
                        Domicilio = Convert.ToString(miDataReader["Domicilio"]),
                        TipoDomicilio = Convert.ToString(miDataReader["TipoDomicilio"]),
                        PaisDomicilio = Convert.ToString(miDataReader["PaisDomicilio"]),
                        ProvinciaDomicilio = Convert.ToString(miDataReader["ProvinciaDomicilio"]),
                        MunicipioDomicilio = Convert.ToString(miDataReader["MunicipioDomicilio"]),
                        NroDomicilio = Convert.ToString(miDataReader["NroDomicilio"]),
                        Piso = Convert.ToString(miDataReader["Piso"]),
                        Dpto = Convert.ToString(miDataReader["Dpto"]),
                        CodPostal = Convert.ToString(miDataReader["CodPostal"]),
                        Veedor = Convert.ToString(miDataReader["Veedor"]),
                        AdminClub = Convert.ToString(miDataReader["AdminClub"]),
                        Instagram = Convert.ToString(miDataReader["Instagram"]),
                        Facebook = Convert.ToString(miDataReader["Facebook"]),
                        Twitter = Convert.ToString(miDataReader["Twitter"]),
                        cod_historial_pago = Convert.ToString(miDataReader["cod_historial_pago"]),
                        fec_alta = Convert.ToString(miDataReader["fec_alta"]),
                        TipoPago = Convert.ToString(miDataReader["TipoPago"]),
                        txt_url_comprobante = Convert.ToString(miDataReader["txt_url_comprobante"]),
                        sn_pago = Convert.ToInt32(miDataReader["sn_pago"]),
                        importe = Convert.ToDecimal(miDataReader["importe"]),
                        fec_ult_pago = Convert.ToString(miDataReader["fec_ult_pago"]),
                        FechaAlta = Convert.ToString(miDataReader["FechaAlta"]),

                    });

                }

                Conn.Close();
                return lReporteJugadorDTO;
            }
            catch (Exception e)
            {
                throw e;
            }

        }


        public List<reporteTorneo> GetReporteInscripcionesTorneosExcel(int cod_torneo)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<temporalTorneo> lTemporalTorneo = new List<temporalTorneo>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region PArametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@cod_torneo",
                    Value = cod_torneo
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_reporte_inscripciones_torneos_equipos", lstParametro);

                while (miDataReader.Read())
                {
                    lTemporalTorneo.Add(new temporalTorneo()
                    {

                        cod_torneo = Convert.ToInt32(miDataReader["cod_torneo"]),
                        Torneo = Convert.ToString(miDataReader["Torneo"]),
                        cod_equipo = Convert.ToInt32(miDataReader["cod_equipo"]),
                        Equipo = Convert.ToString(miDataReader["Equipo"]),
                        cod_club = Convert.ToInt32(miDataReader["cod_club"]),
                        Club = Convert.ToString(miDataReader["Club"]),
                        cod_usuario = Convert.ToInt32(miDataReader["cod_usuario"]),
                        NombreJugador = Convert.ToString(miDataReader["NombreJugador"]),
                        ApellidoJugador = Convert.ToString(miDataReader["ApellidoJugador"]),
                        bool_Capitan = Convert.ToInt32(miDataReader["bool_Capitan"]),
                        bool_suplente = Convert.ToInt32(miDataReader["bool_suplente"]),
                        fec_nacimiento = Convert.ToString(miDataReader["fec_nacimiento"]),
                        cod_historial_pago = Convert.ToInt32(miDataReader["cod_historial_pago"]),
                        txt_url_comprobante = Convert.ToString(miDataReader["txt_url_comprobante"]),
                        sn_pago = Convert.ToInt32(miDataReader["sn_pago"]),
                        importe = Convert.ToDecimal(miDataReader["importe"]),
                        fec_ult_pago = Convert.ToDateTime(miDataReader["fec_ult_pago"]),
                        Sexo = Convert.ToString(miDataReader["Sexo"]),
                        Email = Convert.ToString(miDataReader["Email"]),
                        Telefono = Convert.ToString(miDataReader["Telefono"]),
                        NroHandicapPP = Convert.ToInt32(miDataReader["NroHandicapPP"]),
                        NroHandicapSec = Convert.ToInt32(miDataReader["NroHandicapSec"]),

                    });

                }

                Conn.Close();
                return getReporteTorneo(lTemporalTorneo);
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public List<reporteTorneo> getReporteTorneo(List<temporalTorneo> miObj)
        {
            List<reporteTorneo> miResultado = new List<reporteTorneo>();

            var misEquipos = miObj.Select(x => x.cod_equipo).Distinct();
            var datosTorneo = new ttorneo { cod_torneo = miObj.First().cod_torneo, txt_desc = miObj.First().Torneo };
            reporteTorneo miRepo;
            EquipoUserReporte miJugador;
            foreach (var i in misEquipos)
            {
                miRepo = new reporteTorneo();
                var miListaDeJugadores = new List<EquipoUserReporte>();
                miRepo.Torneo = datosTorneo;
                miRepo.Equipo = new tequipo { cod_equipo = i, txt_desc = miObj.Where(x => x.cod_equipo == i).First().Equipo };
                if (miObj.Where(x => x.cod_equipo == i).First().cod_club != null)
                {
                    miRepo.Club = new tclub { cod_club = (int)miObj.Where(x => x.cod_equipo == i).First().cod_club, txt_desc = miObj.Where(x => x.cod_equipo == i).First().Club };
                }
                else
                {
                    miRepo.Club = new tclub { txt_desc = miObj.Where(x => x.cod_equipo == i).First().Club };
                }

                foreach (var miEquipo in miObj.Where(x => x.cod_equipo == i))
                {
                    miJugador = new EquipoUserReporte();
                    miJugador.cod_usuario = miEquipo.cod_usuario;
                    miJugador.txt_nombre = miEquipo.NombreJugador;
                    miJugador.txt_apellido = miEquipo.ApellidoJugador;
                    miJugador.bool_suplente = miEquipo.bool_suplente;
                    miJugador.bool_Capitan = miEquipo.bool_Capitan;
                    miJugador.sexo = miEquipo.Sexo;
                    miJugador.email = miEquipo.Email;
                    miJugador.telefono = miEquipo.Telefono;
                    miJugador.handicap_principal = miEquipo.NroHandicapPP;
                    miJugador.handicap_secundario = miEquipo.NroHandicapSec;
                    miJugador.fec_nacimiento = miEquipo.fec_nacimiento;
                    miListaDeJugadores.Add(miJugador);
                }
                miRepo.Usuarios = miListaDeJugadores;
                miRepo.sn_pago = miObj.Where(x => x.cod_equipo == i).First().sn_pago;
                miRepo.importe = miObj.Where(x => x.cod_equipo == i).First().importe;
                miRepo.fec_ult_pago = miObj.Where(x => x.cod_equipo == i).First().fec_ult_pago;
                miRepo.cod_historial_pago = miObj.Where(x => x.cod_equipo == i).First().cod_historial_pago;
                miRepo.txt_url_comprobante = miObj.Where(x => x.cod_equipo == i).First().txt_url_comprobante;
                miResultado.Add(miRepo);
            }

            return miResultado;
        }

        public class temporalTorneo
        {
            public int cod_torneo { get; set; }
            public string Torneo { get; set; }
            public int cod_equipo { get; set; }
            public string Equipo { get; set; }
            public int? cod_club { get; set; }
            public string Club { get; set; }
            public int cod_usuario { get; set; }
            public string NombreJugador { get; set; }
            public string ApellidoJugador { get; set; }
            public int? bool_Capitan { get; set; }
            public int? bool_suplente { get; set; }
            public string fec_nacimiento { get; set; }
            public int? cod_historial_pago { get; set; }
            public string txt_url_comprobante { get; set; }
            public int? sn_pago { get; set; }
            public decimal? importe { get; set; }
            public DateTime? fec_ult_pago { get; set; }
            public string Sexo { get; set; }
            public string Email { get; set; }
            public string Telefono { get; set; }
            public int? NroHandicapPP { get; set; }
            public int? NroHandicapSec { get; set; }

        }
        #endregion

    }
}
