﻿using AAPolo.Data.Utilidades;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Data
{
    public class NotificacionAD
    {
        public List<Notificacion> BuscarNotificacion(int Cod_Notificacion, string Titulo)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Notificacion> lNotificacion = new List<Notificacion>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Notificacion",
                    Value = Cod_Notificacion
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Titulo",
                    Value = String.IsNullOrEmpty(Titulo) ? "" : Titulo
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_Notificacion", lstParametro);

                while (miDataReader.Read())
                {
                    lNotificacion.Add(new Notificacion()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Notificacion"]),
                        Titulo = miDataReader["Titulo"].ToString(),
                        Mensaje = miDataReader["Mensaje"].ToString(),
                        FechaHoraEnvio = Convert.ToDateTime(miDataReader["FechaEnvio"]),
                        EsEnviado = Convert.ToInt16(miDataReader["EsEnviado"]) == 1 ? true : false,
                        FechaEnviado = Convert.ToInt16(miDataReader["EsEnviado"]) == 1 ? Convert.ToDateTime(miDataReader["FechaEnviado"]) : DateTime.MinValue,
                        PersonaAlta = new Persona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_UsuarioAlta"]),
                        },
                        HandicapDesde = Convert.ToInt32(miDataReader["HandicapDesde"]),
                        HandicapHasta = Convert.ToInt32(miDataReader["HandicapHasta"]),
                        lRol = this.BuscarRolNotificacion(Convert.ToInt32(miDataReader["Cod_Notificacion"]))
                    });
                    Tuple<List<TipoJugador>, List<CategoriaHandicap>> tListados = this.BuscarTipoJugadorNotificacion(Convert.ToInt32(miDataReader["Cod_Notificacion"]));
                    lNotificacion.LastOrDefault().lTipoJugador = tListados.Item1;
                    lNotificacion.LastOrDefault().lCategoriaHandicap = tListados.Item2;

                }

                Conn.Close();
                return lNotificacion;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Rol> BuscarRolNotificacion(int Codigo)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Rol> lRol = new List<Rol>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Notificacion",
                    Value = Codigo
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_RolNotificacion", lstParametro);

                while (miDataReader.Read())
                {
                    lRol.Add(new Rol()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Rol"]),
                        NombreRol = miDataReader["NombreRol"].ToString(),
                    });

                }

                Conn.Close();
                return lRol;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private Tuple<List<TipoJugador>, List<CategoriaHandicap>> BuscarTipoJugadorNotificacion(int Codigo)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<TipoJugador> lTipoJugador = new List<TipoJugador>();
            List<CategoriaHandicap> lCategoriaHandicap = new List<CategoriaHandicap>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Notificacion",
                    Value = Codigo
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_TipoJugadorNotificacion", lstParametro);

                while (miDataReader.Read())
                {
                    lTipoJugador.Add(new TipoJugador()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Tipo_Jugador"]),
                        Nombre = miDataReader["TipoJugador"].ToString()
                    });

                    lCategoriaHandicap.Add(new CategoriaHandicap()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Categoria_Handicap"]),
                    });
                }

                Conn.Close();
                return new Tuple<List<TipoJugador>, List<CategoriaHandicap>>(lTipoJugador.GroupBy(g => g.Codigo).Select(s => s.FirstOrDefault()).ToList(), lCategoriaHandicap.GroupBy(g => g.Codigo).Select(s => s.FirstOrDefault()).ToList());
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarNotificacion(Notificacion obj)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            DataTable tablaRol = new DataTable();
            DataTable tablaTipoJugador = new DataTable();
            DataTable tablaCategoriaHandicap = new DataTable();
            ResultadoTransaccion result = new ResultadoTransaccion();
            string query = "sp_ins_notificacion";
            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros

                if (obj.Codigo > 0)
                {
                    query = "sp_upd_notificacion";
                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_Notificacion",
                        Value = obj.Codigo
                    });
                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.DateTime,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@FechaEnvio",
                    Value = obj.FechaHoraEnvio
                });
                //----------------------------------------------------------------------------------------------------------------------------------------------
                tablaRol.Columns.Add("Codigo", typeof(int));
                foreach (var item in obj.lRol)
                {
                    tablaRol.Rows.Add(item.Codigo);
                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Structured,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@lstRol",
                    Value = tablaRol
                });
                //----------------------------------------------------------------------------------------------------------------------------------------------
                tablaTipoJugador.Columns.Add("Codigo", typeof(int));
                if (obj.lTipoJugador != null)
                {
                    foreach (var item in obj.lTipoJugador)
                    {
                        tablaTipoJugador.Rows.Add(item.Codigo);
                    }
                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Structured,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@lstTipoJugador",
                    Value = tablaTipoJugador
                });
                //----------------------------------------------------------------------------------------------------------------------------------------------
                tablaCategoriaHandicap.Columns.Add("Codigo", typeof(int));
                if (obj.lCategoriaHandicap != null)
                {
                    foreach (var item in obj.lCategoriaHandicap)
                    {
                        tablaCategoriaHandicap.Rows.Add(item.Codigo);
                    }
                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Structured,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@lstCategoriaHandicap",
                    Value = tablaCategoriaHandicap
                });
                //----------------------------------------------------------------------------------------------------------------------------------------------
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@HandicapDesde",
                    Value = obj.HandicapDesde
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@HandicapHasta",
                    Value = obj.HandicapHasta
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Titulo",
                    Value = obj.Titulo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Mensaje",
                    Value = obj.Mensaje
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_UsuarioAlta",
                    Value = obj.PersonaAlta.Codigo
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, query, lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                    result.CodigoResultado = Convert.ToInt32(miDataReader["Cod_Notificacion"]);
                }

                Conn.Close();

                if (result.EsSatisfactorio)
                {
                    try
                    {
                        int UtcPais = Convert.ToInt32(new ExtensionUtils().obtenerTagDelWebConfig("UtcArgentina"));
                        new TareasScheduler().CrearTarea(
                            obj.FechaHoraEnvio,
                            UtcPais,
                            new ExtensionUtils().obtenerTagDelWebConfig("PathNotificacionesExe"),
                            "AAPolo_Notificaciones" + result.CodigoResultado,
                            "Notificaciones programadas",
                            "Notificaciones programadas " + DateTime.Now.ToString("yyyy/MM/ddHHmm"),
                            new ExtensionUtils().obtenerTagDelWebConfig("UsuarioAdmin"),
                            new ExtensionUtils().obtenerTagDelWebConfig("PasswordAdmin"),
                            "PT10M");

                        /*DateTime HoraUtc = obj.FechaHoraEnvio.ToUniversalTime();

                        DateTime HoraUtcConAddHour = obj.FechaHoraEnvio.ToUniversalTime().AddHours(UtcPais);// (TimeZone.CurrentTimeZone.ToUniversalTime(FechaEjecucion)).AddHours(UtcPais);
                                                                                            //FechaEjecucion = TimeZone.CurrentTimeZone.ToLocalTime(FechaEjecucion);

                        if (DateTime.Compare(obj.FechaHoraEnvio, DateTime.Now.AddMinutes(2)) == 0 || DateTime.Compare(obj.FechaHoraEnvio, DateTime.Now.AddMinutes(2)) < 0)
                        {
                            obj.FechaHoraEnvio = DateTime.Now.AddMinutes(4);
                        }
                        result.Mensaje += " - Hora original de ejecucion: " + obj.FechaHoraEnvio + 
                                          " - Hora de ejecucion utc: " + HoraUtc + 
                                          " - Hora utc pais: " + HoraUtcConAddHour + 
                                          " - UtcPais: " + UtcPais;*/
                    }
                    catch (Exception e)
                    {
                        throw;
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<NotificacionUsuario> BuscarNotificacionAEnviar(int Cod_Notificacion)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<NotificacionUsuario> lNotificacionUsuario = new List<NotificacionUsuario>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Notificacion",
                    Value = Cod_Notificacion
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_NotificacionAEnviar", lstParametro);

                while (miDataReader.Read())
                {
                    lNotificacionUsuario.Add(new NotificacionUsuario()
                    {
                        Notificacion = new Notificacion()
                        {
                            Codigo = Convert.ToInt32(miDataReader["CodNotificacion"]),
                            Titulo = miDataReader["Titulo"].ToString(),
                            Mensaje = miDataReader["Mensaje"].ToString(),
                        },
                        PersonaDestinatario = new Persona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["CodUsuario"]),
                            IdRegistroMobile = miDataReader["IdRegistro"].ToString(),
                            PlataformaMobile = miDataReader["PlataformaMobile"].ToString()
                        },
                    });
                }

                Conn.Close();
                return lNotificacionUsuario;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<NotificacionUsuario> BuscarNotificacionUsuario(int Cod_Usuario, int Indice)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<NotificacionUsuario> lNotificacionUsuario = new List<NotificacionUsuario>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Usuario
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Indice",
                    Value = Indice
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_NotificacionUsuario", lstParametro);

                while (miDataReader.Read())
                {
                    lNotificacionUsuario.Add(new NotificacionUsuario()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Notificacion_Usuario"]),
                        EsLeido = Convert.ToInt32(miDataReader["EsLeido"]) == 1 ? true : false,
                        FechaEntrega = Convert.ToDateTime(miDataReader["fec_entrega"]),
                        TotalNoLeido = Convert.ToInt32(miDataReader["TotalNoLeido"]),
                        Indice = Convert.ToInt32(miDataReader["Indice"]),
                        Notificacion = new Notificacion()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Notificacion"]),
                            Titulo = miDataReader["Titulo"].ToString(),
                            Mensaje = miDataReader["Mensaje"].ToString()
                        }
                    });
                }

                Conn.Close();
                return lNotificacionUsuario;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ActualizarNotificacionLeida(int Cod_NotificacionUsuario)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Notificacion_Usuario",
                    Value = Cod_NotificacionUsuario
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_NotificacionLeida", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }


    }
}
