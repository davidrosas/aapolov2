﻿using AAPolo.Data.Utilidades;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Data
{
    public class NoticiaAD
    {
        public List<Noticia> BuscarNoticia(int Cod_Noticia, int Cod_TipoNoticia, string Titulo, bool CargarTags, bool CargarImg)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Noticia> lNoticia = new List<Noticia>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Noticia",
                    Value = Cod_Noticia
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoNoticia",
                    Value = Cod_TipoNoticia
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Titulo",
                    Value = Titulo
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_noticias", lstParametro);

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["FechaPublicacion"].ToString()) ? miDataReader["FechaPublicacion"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]);
                    lNoticia.Add(new Noticia()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Noticia"]),
                        strFechaPublicacion = miDataReader["FechaPublicacion"].ToString(),
                        FechaPublicacion = new DateTime(Anio, Mes, Dia),
                        Titulo = miDataReader["Titulo"].ToString(),
                        Copete = miDataReader["Copete"].ToString(),
                        CuerpoNoticia = miDataReader["CuerpoNoticia"].ToString(),
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        EsDestacado = Convert.ToInt32(miDataReader["EsDestacado"]) == 1 ? true : false,
                        TipoNoticia = new TipoNoticia()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Tipo_Noticia"]),
                            Nombre = miDataReader["NombreTipoNoticia"].ToString(),
                        },
                        lTags = CargarTags ? this.BuscarEtiquetaNoticia(Convert.ToInt32(miDataReader["Cod_Noticia"])) : new List<Tag>(),
                        lAdjunto = CargarImg ? this.BuscarAdjuntoNoticia(Convert.ToInt32(miDataReader["Cod_Noticia"])) : new List<Adjunto>(),
                    });
                }
                Conn.Close();
                return lNoticia;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Noticia> BuscarNoticiaDestacadaHome()
        {
            SqlDataReader miDataReader;
            List<Noticia> lNoticia = new List<Noticia>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_NoticiaDestacadaHome");

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["FechaPublicacion"].ToString()) ? miDataReader["FechaPublicacion"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]);
                    lNoticia.Add(new Noticia()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Noticia"]),
                        strFechaPublicacion = miDataReader["FechaPublicacion"].ToString(),
                        FechaPublicacion = new DateTime(Anio, Mes, Dia),
                        Titulo = miDataReader["Titulo"].ToString(),
                        Copete = miDataReader["Copete"].ToString(),
                        CuerpoNoticia = miDataReader["CuerpoNoticia"].ToString(),
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        EsDestacado = Convert.ToInt32(miDataReader["EsDestacado"]) == 1 ? true : false,
                        TipoNoticia = new TipoNoticia()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Tipo_Noticia"]),
                            Nombre = miDataReader["NombreTipoNoticia"].ToString(),
                        },
                        lTags = this.BuscarEtiquetaNoticia(Convert.ToInt32(miDataReader["Cod_Noticia"])),
                        lAdjunto = this.BuscarAdjuntoNoticia(Convert.ToInt32(miDataReader["Cod_Noticia"]))
                    });
                }
                Conn.Close();
                return lNoticia;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Adjunto> BuscarAdjuntoNoticia(int Codigo) {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Adjunto> lAdjunto = new List<Adjunto>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Noticia",
                    Value = Codigo
                });
                
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_adjuntoNoticias", lstParametro);

                while (miDataReader.Read())
                {
                    lAdjunto.Add(new Adjunto()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Noticia_Archivo"]),
                        tipoAdjunto = new TipoAdjunto() { Codigo = Convert.ToInt32(miDataReader["Cod_Tipo_Archivo"]) },
                        EsActivo = Convert.ToInt32(miDataReader["sn_activo"]) == 1 ? true : false,
                        UrlAdjunto = miDataReader["url_archivo"].ToString(),
                        Descripcion = miDataReader["txt_desc"].ToString(),
                        Orden = Convert.ToInt32(miDataReader["orden"])
                    });
                }
                Conn.Close();
                return lAdjunto;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Tag> BuscarEtiquetaNoticia(int Codigo)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Tag> lTag = new List<Tag>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Noticia",
                    Value = Codigo
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_EtiquetaNoticias", lstParametro);

                while (miDataReader.Read())
                {
                    lTag.Add(new Tag()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Etiqueta"]),
                        Nombre = miDataReader["NombreEtiqueta"].ToString()
                    });
                }
                Conn.Close();
                return lTag;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<TipoNoticia> BuscarTipoNoticia()
        {
            SqlDataReader miDataReader;
            List<TipoNoticia> lTipoNoticia = new List<TipoNoticia>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_tipo_noticia");

                while (miDataReader.Read())
                {
                    lTipoNoticia.Add(new TipoNoticia()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Tipo_Noticia"]),
                        Nombre = miDataReader["Nombre"].ToString(),

                    });
                }
                Conn.Close();
                return lTipoNoticia;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Etiqueta> BuscarEtiqueta()
        {
            SqlDataReader miDataReader;
            List<Etiqueta> lEtiqueta = new List<Etiqueta>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_etiqueta");

                while (miDataReader.Read())
                {
                    lEtiqueta.Add(new Etiqueta()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Etiqueta"]),
                        Nombre = miDataReader["Nombre"].ToString(),

                    });
                }
                Conn.Close();
                return lEtiqueta;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarNoticia(Noticia obj)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();
            DataTable tablaCodigoEtiqueta = new DataTable();
            DataTable tablaImagen = new DataTable();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.DateTime,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@FechaPublicacion",
                    Value = obj.FechaPublicacion
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoNoticia",
                    Value = obj.TipoNoticia.Codigo
                });

                /*-------------------------------------------------------------------------------------------------------*/
                tablaCodigoEtiqueta.Columns.Add("Codigo", typeof(int));
                foreach (var item in obj.lTags)
                {
                    tablaCodigoEtiqueta.Rows.Add(item.Codigo);
                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Structured,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@lstCodEtiqueta",
                    Value = tablaCodigoEtiqueta
                });
                /*-------------------------------------------------------------------------------------------------------*/

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Titulo",
                    Value = obj.Titulo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Copete",
                    Value = obj.Copete
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cuerpo",
                    Value = obj.CuerpoNoticia
                });

                /*-------------------------------------------------------------------------------------------------------*/
                tablaImagen.Columns.Add("Cod_Imagen", typeof(int));
                tablaImagen.Columns.Add("Cod_TipoArchivo", typeof(int));
                tablaImagen.Columns.Add("UrlImagen", typeof(string));
                tablaImagen.Columns.Add("Descripcion", typeof(string));
                tablaImagen.Columns.Add("Orden", typeof(int));
                tablaImagen.Columns.Add("EsActivo", typeof(int));

                foreach (var item in obj.lAdjunto)
                {
                    if(item.UrlAdjunto != null && item.UrlAdjunto!="")
                        tablaImagen.Rows.Add(0, item.tipoAdjunto.Codigo, item.UrlAdjunto, String.IsNullOrEmpty(item.Descripcion)?"": item.Descripcion, item.Orden, item.EsActivo);
                }

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Structured,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@lstAdjunto",
                    Value = tablaImagen
                });
                /*-------------------------------------------------------------------------------------------------------*/

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@EsDestacado",
                    Value = obj.EsDestacado
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_UsuarioAdmin",
                    Value = obj.PersonaAdmin.Codigo
                });

                string query = "sp_ins_noticia";
                if (obj.Codigo > 0)
                {
                    query = "sp_upd_noticia";

                    lstParametro.Add(new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Input,
                        ParameterName = "@Cod_Noticia",
                        Value = obj.Codigo
                    });

                }

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, query, lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoNoticia(int Codigo, int EstadoNoticia)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Noticia",
                    Value = Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@EstadoNoticia",
                    Value = EstadoNoticia
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_EstadoNoticia", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoNoticiaDestacado(int Codigo, int EstadoNoticiaDestacado)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Noticia",
                    Value = Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@EstadoNoticiaDestacado",
                    Value = EstadoNoticiaDestacado
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_EstadoNoticiaDestacado", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
