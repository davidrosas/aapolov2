﻿using AAPolo.Data.Utilidades;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Data
{
    public class RolAD
    {
        public List<Rol> BuscarRolVisible()
        {
            SqlDataReader miDataReader;
            List<Rol> lRol = new List<Rol>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_RolVisible");

                while (miDataReader.Read())
                {
                    lRol.Add(new Rol()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Rol"]),
                        NombreRol = miDataReader["Rol"].ToString(),
                    });
                }

                Conn.Close();
                return lRol;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
