﻿using AAPolo.Data.Utilidades;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Data
{
    public class RefereeAD
    {
        public List<TipoReferee> BuscaTipoReferee()
        {
            SqlDataReader miDataReader;
            List<TipoReferee> lTipoReferee = new List<TipoReferee>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_TipoReferee");

                while (miDataReader.Read())
                {
                    lTipoReferee.Add(new TipoReferee()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Tipo_Referee"]),
                        Nombre = Convert.ToString(miDataReader["Tipo"])
                    });
                }

                Conn.Close();
                return lTipoReferee;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Referee> BuscarPartidoEvaluacionReferee(int Cod_Usuario, int Cod_Torneo, bool SoloPartidoEfectuado)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Referee> lReferee = new List<Referee>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Usuario
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Torneo",
                    Value = Cod_Torneo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@SoloPartidoEfectuado",
                    Value = SoloPartidoEfectuado
                });
                #endregion

                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_PartidoReferee", lstParametro);

                while (miDataReader.Read())
                {
                    lReferee.Add(new Referee()
                    {
                        Codigo = Convert.ToInt32(miDataReader["CodigoReferee"]),
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        Persona = new Persona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["CodReferee"]),
                            Nombre = miDataReader["NombreReferee"].ToString(),
                            Apellido = miDataReader["ApellidoReferee"].ToString(),
                            TipoReferee = new TipoReferee()
                            {
                                Codigo = Convert.ToInt32(miDataReader["CodCategReferee"]),
                                Nombre = miDataReader["CategoriaReferee"].ToString()
                            },
                            EsReferee = true
                        },
                        Evaluacion = new Evaluacion()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Codigo"]),
                            Veedor = new Persona()
                            {
                                Codigo = Convert.ToInt32(miDataReader["CodVeedor"]),
                                Nombre = miDataReader["NombreVeedor"].ToString(),
                                Apellido = miDataReader["ApellidoVeedor"].ToString(),
                            },
                            EquipoLocal = new Equipo() { Codigo = Convert.ToInt32(miDataReader["Cod_Equipo_Local"]), NombreEquipo = miDataReader["Local"].ToString() },
                            EquipoVisitante = new Equipo() { Codigo = Convert.ToInt32(miDataReader["Cod_Equipo_Visitante"]), NombreEquipo = miDataReader["Visitante"].ToString() },
                            PartidoEvaluado = new Partido() {
                                Codigo = Convert.ToInt32(miDataReader["Cod_Partido"]),
                                EquipoLocal = new Equipo() {
                                    Codigo = Convert.ToInt32(miDataReader["Cod_Equipo_Local"]),
                                    NombreEquipo = miDataReader["Local"].ToString(),
                                    ResultadoEquipoGol = Convert.ToInt32(miDataReader["nro_resultado_equipo_uno"])
                                },
                                EquipoVisitante = new Equipo() {
                                    Codigo = Convert.ToInt32(miDataReader["Cod_Equipo_Visitante"]),
                                    NombreEquipo = miDataReader["Visitante"].ToString(),
                                    ResultadoEquipoGol = Convert.ToInt32(miDataReader["nro_resultado_equipo_dos"])
                                },
                            },
                            Torneo = new Torneo()
                            {
                                Codigo = Convert.ToInt32(miDataReader["CodTorneo"]),
                                NombreTorneo = miDataReader["Torneo"].ToString()
                            },
                            ZonaPartido = new ZonaPartido
                            {
                                Codigo = Convert.ToInt32(miDataReader["CodZona"]),
                                Nombre = miDataReader["ZonaPartido"].ToString()
                            },
                            Instancia = new Instancia
                            {
                                Codigo = Convert.ToInt32(miDataReader["CodInstancia"]),
                                Nombre = miDataReader["Instancia"].ToString()
                            },
                            Nivel = new NivelPolo
                            {
                                Codigo = Convert.ToInt32(miDataReader["CodNivel"]),
                                Nombre = miDataReader["Nivel"].ToString()
                            },
                            FechaEvaluacion = Convert.ToDateTime(miDataReader["FecEvaluacion"]),
                            FechaPartido = Convert.ToDateTime(miDataReader["FecPartido"]),
                            Puntaje = Convert.ToDecimal(miDataReader["Puntaje"])
                        }
                    });
                }

                Conn.Close();
                return lReferee;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Referee> BuscarEvaluacionReferee(int Cod_Usuario)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Referee> lReferee = new List<Referee>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario_Polo",
                    Value = Cod_Usuario
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_evaluaciones_referee", lstParametro);

                while (miDataReader.Read())
                {
                    lReferee.Add(new Referee()
                    {
                        Codigo = Convert.ToInt32(miDataReader["CodigoReferee"]),
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        Persona = new Persona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["CodReferee"]),
                            Nombre = miDataReader["NombreReferee"].ToString(),
                            Apellido = miDataReader["ApellidoReferee"].ToString(),
                            TipoReferee = new TipoReferee()
                            {
                                Codigo = Convert.ToInt32(miDataReader["CodCategReferee"]),
                                Nombre = miDataReader["CategoriaReferee"].ToString()
                            },
                            EsReferee = true
                        },
                        Evaluacion = new Evaluacion()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Codigo"]),
                            Veedor = new Persona()
                            {
                                Codigo = Convert.ToInt32(miDataReader["CodVeedor"]),
                                Nombre = miDataReader["NombreVeedor"].ToString(),
                                Apellido = miDataReader["ApellidoVeedor"].ToString(),
                            },
                            EquipoLocal = new Equipo() { NombreEquipo = miDataReader["Local"].ToString() },
                            EquipoVisitante = new Equipo() { NombreEquipo = miDataReader["Visitante"].ToString() },
                            Torneo = new Torneo()
                            {
                                Codigo = Convert.ToInt32(miDataReader["CodTorneo"]),
                                NombreTorneo = miDataReader["Torneo"].ToString()
                            },
                            ZonaPartido = new ZonaPartido
                            {
                                Codigo = Convert.ToInt32(miDataReader["CodZona"]),
                                Nombre = miDataReader["ZonaPartido"].ToString()
                            },
                            Instancia = new Instancia
                            {
                                Codigo = Convert.ToInt32(miDataReader["CodInstancia"]),
                                Nombre = miDataReader["Instancia"].ToString()
                            },
                            Nivel = new NivelPolo
                            {
                                Codigo = Convert.ToInt32(miDataReader["CodNivel"]),
                                Nombre = miDataReader["Nivel"].ToString()
                            },
                            FechaEvaluacion = Convert.ToDateTime(miDataReader["FecEvaluacion"]),
                            FechaPartido = Convert.ToDateTime(miDataReader["FecPartido"]),
                            Puntaje = Convert.ToDecimal(miDataReader["Puntaje"])
                        }
                    });
                }

                Conn.Close();
                return lReferee;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Evaluacion BuscarEvaluacionReferee(int Cod_Usuario, int Cod_Evaluacion)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            Evaluacion detEvaluacion = new Evaluacion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@cod_usuario_polo",
                    Value = Cod_Usuario
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@cod_evaluacion",
                    Value = Cod_Evaluacion
                });

                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_detalle_evaluacion_referee", lstParametro);

                if (miDataReader.Read())
                {
                    detEvaluacion = new Evaluacion()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Codigo"]),
                        Veedor = new Persona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["CodVeedor"]),
                            Nombre = miDataReader["NombreVeedor"].ToString(),
                            Apellido = miDataReader["ApellidoVeedor"].ToString(),
                        },
                        EquipoLocal = new Equipo() { NombreEquipo = miDataReader["Local"].ToString() },
                        EquipoVisitante = new Equipo() { NombreEquipo = miDataReader["Visitante"].ToString() },
                        PartidoEvaluado = new Partido {
                            EquipoLocal = new Equipo() { NombreEquipo = miDataReader["Local"].ToString() },
                            EquipoVisitante = new Equipo() { NombreEquipo = miDataReader["Visitante"].ToString() },
                            FechaPartido = Convert.ToDateTime(miDataReader["FecPartido"]),
                            Veedor = new Persona()
                            {
                                Codigo = Convert.ToInt32(miDataReader["CodVeedor"]),
                                Nombre = miDataReader["NombreVeedor"].ToString(),
                                Apellido = miDataReader["ApellidoVeedor"].ToString(),
                            },
                            Referee = new Referee() {
                                Codigo = Convert.ToInt32(miDataReader["Cod_Referee"]),
                                Persona = new Persona() {
                                    Nombre = miDataReader["NombreReferee"].ToString(),
                                    Apellido = miDataReader["ApellidoReferee"].ToString(),
                                }
                            },
                            Intancia = new Instancia
                            {
                                Codigo = Convert.ToInt32(miDataReader["CodInstancia"]),
                                Nombre = miDataReader["Instancia"].ToString()
                            }
                        },
                        Torneo = new Torneo()
                        {
                            Codigo = Convert.ToInt32(miDataReader["CodTorneo"]),
                            NombreTorneo = miDataReader["Torneo"].ToString()
                        },
                        ZonaPartido = new ZonaPartido
                        {
                            Codigo = Convert.ToInt32(miDataReader["CodZona"]),
                            Nombre = miDataReader["ZonaPartido"].ToString()
                        },
                        Instancia = new Instancia
                        {
                            Codigo = Convert.ToInt32(miDataReader["CodInstancia"]),
                            Nombre = miDataReader["Instancia"].ToString()
                        },
                        Nivel = new NivelPolo
                        {
                            Codigo = Convert.ToInt32(miDataReader["CodNivel"]),
                            Nombre = miDataReader["Nivel"].ToString()
                        },
                        FechaEvaluacion = Convert.ToDateTime(miDataReader["FecEvaluacion"]),
                        FechaPartido = Convert.ToDateTime(miDataReader["FecPartido"]),
                        Puntaje = Convert.ToDecimal(miDataReader["Puntaje"]),
                        Comentario = miDataReader["Comentario"].ToString()

                    };

                    detEvaluacion.lstPregunta = new List<PreguntasEvaluacion>();
                    miDataReader.NextResult();
                    while (miDataReader.Read())
                    {
                        detEvaluacion.lstPregunta.Add( new PreguntasEvaluacion() {
                            Codigo = Convert.ToInt32(miDataReader["Codigo"]),
                            Pregunta = miDataReader["Descripcion"].ToString(),
                            Puntaje = Convert.ToDecimal(miDataReader["Puntaje"]),
                            EsActivo = Convert.ToInt32(miDataReader["Sn_Activo"]) == 0 ? false : true
                        });
                    }

                }

                Conn.Close();
                return detEvaluacion;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Referee> BuscaReferee(string DocumentoNombre, int Cod_tipoReferee, int Cod_Usuario, int Cod_Referee)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Referee> lReferee = new List<Referee>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Usuario
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@DocumentoNombre",
                    Value = DocumentoNombre
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_tipoReferee",
                    Value = Cod_tipoReferee
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Referee",
                    Value = Cod_Referee
                });
                
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_Referee", lstParametro);

                while (miDataReader.Read())
                {
                    string fec = !String.IsNullOrEmpty(miDataReader["FechaNacimiento"].ToString()) ? miDataReader["FechaNacimiento"].ToString() : DateTime.MinValue.ToString("dd/MM/yyyy");
                    int Dia = Convert.ToInt32(fec.Split('/')[0]),
                        Mes = Convert.ToInt32(fec.Split('/')[1]),
                        Anio = Convert.ToInt32(fec.Split('/')[2]);

                    lReferee.Add(new Referee()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Referee"]),
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        Persona = new Persona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]),
                            DocumentoIdentidad = miDataReader["DocumentoIdentidad"].ToString(),
                            Nombre = miDataReader["Nombre"].ToString(),
                            Apellido = miDataReader["Apellido"].ToString(),
                            FechaNacimiento = new DateTime(Anio, Mes, Dia),
                            TipoReferee = new TipoReferee() { Nombre = miDataReader["NombreTipoReferee"].ToString() },
                            EsReferee = true
                        }
                    });
                }

                Conn.Close();
                return lReferee;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscarPosibleReferee(int Cod_Usuario, string DocumentoNombre)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Persona> lPersona = new List<Persona>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Usuario
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@DocumentoNombre",
                    Value = DocumentoNombre
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_get_UsuarioPosibleReferee", lstParametro);

                while (miDataReader.Read())
                {
                    lPersona.Add(new Persona()
                    {
                        Codigo = Convert.ToInt32(miDataReader["Cod_Usuario"]),
                        DocumentoIdentidad = miDataReader["DocumentoIdentidad"].ToString(),
                        Nombre = miDataReader["Nombre"].ToString(),
                        Apellido = miDataReader["Apellido"].ToString(),
                        EsActivo = Convert.ToInt32(miDataReader["EsActivo"]) == 1 ? true : false,
                        TipoDocumento = new TipoDocumento() { Nombre = miDataReader["TipoDocumento"].ToString() },
                        clubUsuarioPrincipal = new ClubUsuario() { NombreClub = miDataReader["NombreClub"].ToString() }
                    });
                }

                Conn.Close();
                return lPersona;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Referee> RankingReferee(string Nombre, int Cod_TipoReferee, int Cod_Usuario)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            List<Referee> lReferee = new List<Referee>();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoReferee",
                    Value = Cod_TipoReferee
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Nombre",
                    Value = Nombre
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Usuario
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_Get_RankingReferees", lstParametro);

                while (miDataReader.Read())
                {
                    lReferee.Add(new Referee()
                    {
                        Persona = new Persona()
                        {
                            Codigo = Convert.ToInt32(miDataReader["Codigo"]),
                            DocumentoIdentidad = miDataReader["Documento"].ToString(),
                            Nombre = miDataReader["Nombre"].ToString(),
                            Apellido = miDataReader["Apellido"].ToString(),
                            UrlFoto = miDataReader["UrlImagen"].ToString(),
                            EsActivo = Convert.ToInt32(miDataReader["BoolActivo"]) == 1 ? true : false,
                            TipoReferee = new TipoReferee()
                            {
                                Codigo = Convert.ToInt32(miDataReader["CodTipoReferee"]),
                                Nombre = miDataReader["TipoReferee"].ToString()
                            }
                        },
                        CantidadEvaluados = Convert.ToInt32(miDataReader["CantEvaluados"]),
                        PromedioPuntaje = Convert.ToInt32(miDataReader["PromedioPuntaje"]),
                        SumaPuntaje = Convert.ToInt32(miDataReader["SumaPuntaje"]),
                        Ranking = Convert.ToInt32(miDataReader["Ranking"]),
                    });
                }

                Conn.Close();
                return lReferee;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarReferee(int Cod_Usuario, int Cod_TipoReferee)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region Parametros

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Usuario",
                    Value = Cod_Usuario
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_TipoReferee",
                    Value = Cod_TipoReferee
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_ins_referee", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoReferee(int Codigo, int EstadoReferee)
        {
            List<SqlParameter> lstParametro = new List<SqlParameter>();
            SqlDataReader miDataReader;
            ResultadoTransaccion result = new ResultadoTransaccion();

            try
            {
                SqlConnection Conn = ExtensionMethods.ObtConnection();
                #region parametros
                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@Cod_Referee",
                    Value = Codigo
                });

                lstParametro.Add(new SqlParameter()
                {
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Input,
                    ParameterName = "@EstadoReferee",
                    Value = EstadoReferee
                });
                #endregion
                miDataReader = ExtensionMethods.ObtDataReader(Conn, "sp_upd_EstadoReferee", lstParametro);

                while (miDataReader.Read())
                {
                    result.Mensaje += miDataReader["msj"].ToString();
                    result.EsSatisfactorio = Convert.ToBoolean(miDataReader["EsSatisfactorio"]);
                }

                Conn.Close();
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
