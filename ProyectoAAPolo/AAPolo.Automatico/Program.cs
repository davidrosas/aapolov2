﻿using AAPolo.Aplication;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Automatico
{
    class Program
    {
        static void Main(string[] args)
        {
            ResultadoTransaccion resul = new ResultadoTransaccion()
            {
                EsSatisfactorio = false,
                Mensaje = "Disculpe, los procesos no se ejecutaron correctamente."
            };
            Console.WriteLine("Inciando procesos de actualización de registros AAPOLO {0}", DateTime.Now);
            try
            {
                resul = new TorneoSM().ActualizarInscripcionTorneoAutomatico();
                Console.WriteLine("Actualización de torneos finalizada {0}. Estado: {1}, mensaje: {2}", DateTime.Now, resul.EsSatisfactorio, resul.Mensaje);
            }
            catch (Exception e)
            {
                Console.WriteLine("Actualización de torneos finalizada con errores {0}. Mensaje: {1}", DateTime.Now, e.Message);
            }

        }
    }
}
