﻿using AAPolo.Aplication;
using AAPolo.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.AutomaticoEnvioDeDatosFacturacionA_piTANGO
{
    class Program
    {
        public static string obtenerTagDelWebConfig(string pNombreTag)
        {
            var AppSettings = ConfigurationManager.AppSettings;
            string key = AppSettings[pNombreTag];
            return key;
        }

        static void Main(string[] args)
        {
            try
            {
                bool enviarJson;
                string AccessToken = obtenerTagDelWebConfig("AccessToken");
                string result = "";
                /*using (var client = new WebClient())
                {
                    client.Headers.Add("Content-Type", "application/json");
                    client.Headers.Add("AccessToken", AccessToken);
                    client.Encoding = Encoding.UTF8;
                    Stream _stream = client.OpenWrite("https://tiendas.axoft.com/api/v2/Aperture/dummy", "POST");
                }*/

                int CodPago = 14401;
                List<Order> lOrden = new PagoSM().BuscarPagosNoFacturados(CodPago);
                int i = 1;
                foreach (Order item in lOrden)
                {
                    Console.Write("{0} - ", i);
                    i++;
                    enviarJson = true;
                    if (item == null)
                    {
                        Console.WriteLine("El json no es un objeto orden válido, desea enviarlo de todas formas?");
                        enviarJson = false;
                    }

                    if (enviarJson)
                    {
                        result = "";
                        try
                        {
                            using (var client = new WebClient())
                            {
                                client.Headers.Add("Content-Type", "application/json");
                                client.Headers.Add("AccessToken", AccessToken);
                                client.Encoding = Encoding.UTF8;
                                result = client.UploadString("https://tiendas.axoft.com/api/v2/Aperture/order", "POST", JsonConvert.SerializeObject(item, Formatting.Indented));

                                TangoResultadoTransaccion tangoResul = JsonConvert.DeserializeObject<TangoResultadoTransaccion>(result);
                                if (tangoResul.isOk && tangoResul.Status == 0)
                                {
                                    new PagoSM().ActualizarRegistroTransaccionFacturacion(Convert.ToInt32(item.OrderID));
                                    Console.WriteLine("{0} - {1} - {2}", item.OrderID, item.OrderItems.First().ProductCode, item.PaidTotal);
                                }
                                else
                                {
                                    Console.WriteLine(JsonConvert.SerializeObject(item, Formatting.Indented));
                                }
                            }
                        }
                        catch (WebException Wex)
                        {

                            string resp = new StreamReader(Wex.Response.GetResponseStream()).ReadToEnd();
                            Console.WriteLine(resp);

                        }
                        catch (Exception exc)
                        {
                            Console.WriteLine(exc.ToString());
                        }

                        Console.WriteLine(result);
                    }

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Console.ReadLine();
            Console.ReadLine();

        }
    }
}
