﻿using AAPolo.Business;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Aplication
{
    public class TorneoSM
    {

        public List<ProductoTorneo> BuscarProductoTorneo(bool SoloActivo)
        {
            try
            {
                return new TorneoRN().BuscarProductoTorneo(SoloActivo);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<Torneo> BuscarTorneo(int Cod_Torneo, int Cod_CategoriaTorneo, int Cod_CategoriaHandicap, bool SoloActivo)
        {
            try
            {
                return new TorneoRN().BuscarTorneo(Cod_Torneo, Cod_CategoriaTorneo, Cod_CategoriaHandicap, SoloActivo);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Torneo> BuscarTorneoHistorico(int Cod_Torneo, string NombreTorneo, int Cod_CategoriaTorneo, int Cod_CategoriaHandicap, bool SoloActivo)
        {
            try
            {
                return new TorneoRN().BuscarTorneoHistorico(Cod_Torneo, NombreTorneo, Cod_CategoriaTorneo, Cod_CategoriaHandicap, SoloActivo);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<List<Torneo>> BuscarTorneoAsync(int Cod_Torneo, int Cod_CategoriaTorneo, int Cod_CategoriaHandicap, bool SoloActivo)
        {
            try
            {
                return await Task.Run(() =>
                {
                    return new TorneoRN().BuscarTorneo(Cod_Torneo, Cod_CategoriaTorneo, Cod_CategoriaHandicap, SoloActivo);
                });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Torneo> BuscarTorneoInscipcionActivo(int Cod_Torneo, int Cod_CategoriaTorneo)
        {
            try
            {
                return new TorneoRN().BuscarTorneoInscipcionActivo(Cod_Torneo, Cod_CategoriaTorneo);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<List<Torneo>> BuscarTorneoInscipcionActivoAsync(int Cod_Torneo, int Cod_CategoriaTorneo)
        {
            try
            {
                return await Task.Run(() =>
                {
                    return new TorneoRN().BuscarTorneoInscipcionActivo(Cod_Torneo, Cod_CategoriaTorneo);
                });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<TipoTorneo> BuscarTipoTorneo()
        {
            try
            {
                return new TorneoRN().BuscarTipoTorneo();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<CategoriaHandicap> BuscarCategoriaHandicapTorneo()
        {
            try
            {
                return new TorneoRN().BuscarCategoriaHandicapTorneo();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarTorneo(Torneo obj)
        {
            try
            {
                return new TorneoRN().GuardarTorneo(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoTorneo(int Codigo, int EstadoTorneo)
        {
            try
            {
                return new TorneoRN().CambiarEstadoTorneo(Codigo, EstadoTorneo);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Equipo> BuscarEquipo(int Cod_Equipo, int Cod_Persona, int Cod_Torneo)
        {
            try
            {
                return new TorneoRN().BuscarEquipo(Cod_Equipo, Cod_Persona, Cod_Torneo);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ActualizarInscripcionTorneoAutomatico()
        {
            try
            {
                return new TorneoRN().ActualizarInscripcionTorneoAutomatico();

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<ResultadoTransaccion> ActualizarInscripcionTorneoAutomaticoAsync()
        {
            try
            {
                return await Task.Run(() =>
                {
                    return new TorneoRN().ActualizarInscripcionTorneoAutomatico();
                });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarCodigoProducto(ProductoTorneo obj)
        {
            try
            {
                return new TorneoRN().GuardarCodigoProducto(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Zona> BuscarZonaTorneo(int Cod_Torneo, bool CargarEquipo, int Cod_Zona)
        {
            try
            {
                return new TorneoRN().BuscarZonaTorneo(Cod_Torneo, CargarEquipo, Cod_Zona);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<List<Zona>> BuscarZonaTorneoDisponibleAsync(int Cod_Torneo, bool CargarEquipo, int Cod_Zona)
        {
            try
            {
                return await Task.Run(() =>
                {
                    return new TorneoRN().BuscarZonaTorneo(Cod_Torneo, CargarEquipo, Cod_Zona);
                });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarZonaTorneo(Torneo obj)
        {
            try
            {
                return new TorneoRN().GuardarZonaTorneo(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<TipoZona> BuscarTipoZonaTorneo()
        {
            try
            {
                return new TorneoRN().BuscarTipoZonaTorneo();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Equipo> BuscarEquipoZonaTorneo(int Cod_Equipo, string Nombre, int Cod_Torneo, int Cod_Zona)
        {
            try
            {
                return new TorneoRN().BuscarEquipoZonaTorneo(Cod_Equipo, Nombre, Cod_Torneo, Cod_Zona);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarEquipoZona(Zona obj)
        {
            try
            {
                return new TorneoRN().GuardarEquipoZona(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Equipo> BuscarEquipoZona(int Cod_Zona, int Cod_Torneo, int Cod_Equipo)
        {

            try
            {
                return new TorneoRN().BuscarEquipoZona(Cod_Zona, Cod_Torneo, Cod_Equipo);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Equipo> BuscarEquipoPartido(int Cod_Zona, int Cod_Torneo, int Cod_Partido)
        {
            try
            {
                return new TorneoRN().BuscarEquipoPartido(Cod_Zona, Cod_Torneo, Cod_Partido);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Equipo> BuscarEquipoPartidoSegundaFase(int Cod_Zona, int Cod_Torneo)
        {
            try
            {
                return new TorneoRN().BuscarEquipoPartidoSegundaFase(Cod_Zona, Cod_Torneo);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Partido> BuscarPartidoZona(int Cod_Zona, int Cod_Torneo, int Cod_Instancia, int Cod_Partido)
        {
            try
            {
                return new TorneoRN().BuscarPartidoZona(Cod_Zona, Cod_Torneo, Cod_Instancia, Cod_Partido);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultadoTransaccion GuardarPartido(Partido obj, int Cod_Usuario)
        {
            try
            {
                return new TorneoRN().GuardarPartido(obj, Cod_Usuario);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Incidencia> BuscarIncidencia()
        {
            try
            {
                return new TorneoRN().BuscarIncidencia();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Copa> BuscarCopaTorneo(int Cod_Torneo, int Cod_Copa, bool CargarPartidoZona)
        {
            try
            {
                return new TorneoRN().BuscarCopaTorneo(Cod_Torneo, Cod_Copa, CargarPartidoZona);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<Copa>> BuscarCopaTorneoDisponibleAsync(int Cod_Torneo, int Cod_Copa, bool CargarPartidoZona)
        {
            try
            {
                return await Task.Run(() =>
                {
                    return new TorneoRN().BuscarCopaTorneo(Cod_Torneo, Cod_Copa, CargarPartidoZona);
                });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Equipo> BuscarEquipoPartidoInstancia(int Cod_Partido)
        {
            try
            {
                return new TorneoRN().BuscarEquipoPartidoInstancia(Cod_Partido);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultadoTransaccion GuardarCopa(List<Copa> obj, int Cod_Usuario)
        {
            try
            {
                return new TorneoRN().GuardarCopa(obj, Cod_Usuario);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Partido> BuscarPartidoInstancia(int Cod_Torneo, int Cod_Instancia, int Cod_Partido, int SoloCopaPrincipal)
        {
            try
            {
                return new TorneoRN().BuscarPartidoInstancia(Cod_Torneo, Cod_Instancia, Cod_Partido, SoloCopaPrincipal);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultadoTransaccion GuardarPartidoInstancia(Partido obj, int Cod_Usuario)
        {
            try
            {
                return new TorneoRN().GuardarPartidoInstancia(obj, Cod_Usuario);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Persona> ListadoPersonaEquipoPartido(int Cod_Equipo, int Cod_Partido)
        {
            try
            {
                return new TorneoRN().ListadoPersonaEquipoPartido(Cod_Equipo, Cod_Partido);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion DisponibleZona(int Cod_Torneo, bool Disponible)
        {
            try
            {
                return new TorneoRN().DisponibleZona(Cod_Torneo, Disponible);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion DisponibleLlaves(int Cod_Torneo, bool Disponible)
        {
            try
            {
                return new TorneoRN().DisponibleLlaves(Cod_Torneo, Disponible);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public ResultadoTransaccion DisponibleParidoLlaves(int Cod_Torneo, bool Disponible)
        {
            try
            {
                return new TorneoRN().DisponibleParidoLlaves(Cod_Torneo, Disponible);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
