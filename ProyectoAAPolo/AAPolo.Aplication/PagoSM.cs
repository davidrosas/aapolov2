﻿using AAPolo.Business;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Aplication
{
    public class PagoSM
    {
        public List<Vigencia> BuscarVigencia()
        {
            try
            {
                return new PagoRN().BuscarVigencia();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<CategoriaHandicap> BuscarCategoriaHandicap(int Cod_TipoJugador)
        {
            try
            {
                return new PagoRN().BuscarCategoriaHandicap(Cod_TipoJugador);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Handicap> BuscarCostoHandicap(int Cod_TipoJugador, int Cod_Vigencia, int Cod_CategoriaHandicap)
        {
            try
            {
                return new PagoRN().BuscarCostoHandicap(Cod_TipoJugador, Cod_Vigencia, Cod_CategoriaHandicap);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ProcesarPagoHandicap(List<Persona> lPersona, string idReference, int Cod_UsuarioTramite)
        {
            try
            {
                return new PagoRN().ProcesarPagoHandicap(lPersona, idReference, Cod_UsuarioTramite);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ProcesarPagoHandicapPase(List<Persona> lPersona, string idReference, int Cod_UsuarioTramite)
        {
            try
            {
                return new PagoRN().ProcesarPagoHandicapPase(lPersona, idReference, Cod_UsuarioTramite);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion PagoAfiliacionClub(int Cod_Club, int Cod_Persona, string ReferenciaExterna)
        {
            try
            {
                return new PagoRN().PagoAfiliacionClub(Cod_Club, Cod_Persona, ReferenciaExterna);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarInscripcionEquipo(Equipo obj, string idReference, int Cod_Persona)
        {
            try
            {
                return new PagoRN().GuardarInscripcionEquipo(obj, idReference, Cod_Persona);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
        public ResultadoTransaccion ActualizarTransacionPagoHandicap(string external_reference, int Cod_Estado, string collection_id) {
            try
            {
                return new PagoRN().ActualizarTransacionPagoHandicap(external_reference, Cod_Estado, collection_id);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ActualizarTransacionPagoInscripcionEquipo(string external_reference, int Cod_Estado, string collection_id) {
            try
            {
                return new PagoRN().ActualizarTransacionPagoInscripcionEquipo(external_reference, Cod_Estado, collection_id);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ActualizarTransaccionHandicap(string external_reference, int Cod_Estado, string collection_id, int Cod_Persona)
        {
            try
            {
                return new PagoRN().ActualizarTransaccionHandicap(external_reference, Cod_Estado, collection_id, Cod_Persona);
            }
            catch (Exception e)
            {
                throw e;
            }
        }        

        public List<Pago> BuscarPago(int Cod_Persona) {
            try
            {
                return new PagoRN().BuscarPago(Cod_Persona);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Handicap> BuscarHandicapTransicion(int Cod_Handicap)
        {
            try
            {
                return new PagoRN().BuscarHandicapTransicion(Cod_Handicap);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<DetallePago> BuscarPagosEnEspera() {
            try
            {
                return new PagoRN().BuscarPagosEnEspera();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Order> BuscarPagosNoFacturados(int CodPago)
        {
            try
            {
                return new PagoRN().BuscarPagosNoFacturados(CodPago);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultadoTransaccion ActualizarRegistroTransaccionFacturacion(int Cod_Pago) {
            try
            {
                return new PagoRN().ActualizarRegistroTransaccionFacturacion(Cod_Pago);
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
