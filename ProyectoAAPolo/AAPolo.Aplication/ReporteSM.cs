﻿using AAPolo.Business;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Aplication
{
    public class ReporteSM
    {
        public List<Club> BuscarReporteClub(int Cod_Persona, int AnioEnCurso, int Cod_Club)
        {
            try
            {
                return new ReporteRN().BuscarReporteClub(Cod_Persona, AnioEnCurso, Cod_Club);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public List<reporteCuentaClub> GetReporteDatosClub()
        {
            try
            {
                return new ReporteRN().GetReporteDatosClub();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscarReportePagoHandicap(int Cod_Persona, string Cadena, int Cod_TipoJugador, int Cod_Club, int Nro_Handicap)
        {
            try
            {
                return new ReporteRN().BuscarReportePagoHandicap(Cod_Persona, Cadena, Cod_TipoJugador, Cod_Club, Nro_Handicap);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Equipo> BuscarReporteEquipo(int Cod_Equipo, int Cod_Torneo, string Cadena, int Cod_EstadoPago)
        {
            try
            {
                return new ReporteRN().BuscarReporteEquipo(Cod_Equipo, Cod_Torneo, Cadena, Cod_EstadoPago);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<MedioDePago> BuscarMedioDePago()
        {
            try
            {
                return new ReporteRN().BuscarMedioDePago();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ConfirmarPagoHandicap(Persona obj, int Cod_UsuarioTransaccion) {
            try
            {
                return new ReporteRN().ConfirmarPagoHandicap(obj, Cod_UsuarioTransaccion);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ConfirmarPagoClub(Club obj, int Cod_UsuarioTransaccion)
        {
            try
            {
                return new ReporteRN().ConfirmarPagoClub(obj, Cod_UsuarioTransaccion);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ConfirmarPagoEquipo(Equipo obj, int Cod_UsuarioTransaccion)
        {
            try
            {
                return new ReporteRN().ConfirmarPagoEquipo(obj, Cod_UsuarioTransaccion);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion DeclinarPagoEquipo(int Cod_Pago)
        {
            try
            {
                return new ReporteRN().DeclinarPagoEquipo(Cod_Pago);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ReporteEstadisticaHandicap BuscarReporteEstadisticaHandicap()
        {
            try
            {
                return new ReporteRN().BuscarReporteEstadisticaHandicap();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #region descargables
        public List<reporteCuentaClub> GetReporteClub()
        {
            try
            {
                return new ReporteRN().GetReporteClub();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<reporteCuentaJugadores> GetReporteCuentaJugadores()
        {
            try
            {
                return new ReporteRN().GetReporteCuentaJugadores();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<reporteCuentaJugadores> GetReporteAllCuentasCorrientesJugadores()
        {
            try
            {
                return new ReporteRN().GetReporteAllCuentasCorrientesJugadores();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<reporteCuentaTorneo> GetReporteInscripcionesEquiposExcel()
        {
            try
            {
                return new ReporteRN().GetReporteInscripcionesEquiposExcel();
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public List<reporteTorneo> GetReporteInscripcionesTorneosExcel(int cod_torneo)
        {
            try
            {
                return new ReporteRN().GetReporteInscripcionesTorneosExcel(cod_torneo);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<reporteJugadorDTO> GetReporteJugadores()
        {

            try
            {
                return new ReporteRN().GetReporteJugadores();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

    }
}
