﻿using AAPolo.Business;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Aplication
{
    public class NoticiaSM
    {
        public List<Noticia> BuscarNoticia(int Cod_Noticia, int Cod_TipoNoticia, string Titulo, bool CargarTags, bool CargarImg)
        {
            try
            {
                return new NoticiaRN().BuscarNoticia(Cod_Noticia, Cod_TipoNoticia, Titulo, CargarTags, CargarImg);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Noticia> BuscarNoticiaDestacadaHome()
        {
            try
            {
                return new NoticiaRN().BuscarNoticiaDestacadaHome();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Etiqueta> BuscarEtiqueta()
        {
            try
            {
                return new NoticiaRN().BuscarEtiqueta();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<TipoNoticia> BuscarTipoNoticia()
        {
            try
            {
                return new NoticiaRN().BuscarTipoNoticia();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultadoTransaccion GuardarNoticia(Noticia obj) {
            try
            {
                return new NoticiaRN().GuardarNoticia(obj);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultadoTransaccion CambiarEstadoNoticia(int Codigo, int EstadoNoticia)
        {
            try
            {
                return new NoticiaRN().CambiarEstadoNoticia(Codigo, EstadoNoticia);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoNoticiaDestacado(int Codigo, int EstadoNoticiaDestacado)
        {
            try
            {
                return new NoticiaRN().CambiarEstadoNoticiaDestacado(Codigo, EstadoNoticiaDestacado);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
