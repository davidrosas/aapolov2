﻿using AAPolo.Business;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Aplication
{
    public class ClubSM
    {
        public List<Club> BuscaClub(int Cod_CategoriaClub, int Cod_Club, string NombreClub, int Cod_UsuarioAdmin)
        {
            try
            {
                return new ClubRN().BuscaClub(Cod_CategoriaClub, Cod_Club, NombreClub, Cod_UsuarioAdmin);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<List<Club>> BuscaClubAsync(int Cod_CategoriaClub, int Cod_Club, string NombreClub, int Cod_UsuarioAdmin)
        {
            try
            {
                return await Task.Run(() =>
                {
                    return new ClubRN().BuscaClub(Cod_CategoriaClub, Cod_Club, NombreClub, Cod_UsuarioAdmin);
                });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Club> BuscarClubActivo(int Cod_CategoriaClub, int Cod_Club, string NombreClub, int Cod_UsuarioAdmin, int Cod_Provincia)
        {
            try
            {
                return new ClubRN().BuscarClubActivo(Cod_CategoriaClub, Cod_Club, NombreClub, Cod_UsuarioAdmin, Cod_Provincia);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarImagenClub(string urlImg, int Cod_Club)
        {
            try
            {
                return new ClubRN().GuardarImagenClub(urlImg, Cod_Club);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion RechazarJugadorClubPrincipal(int Cod_Persona)
        {
            try
            {
                return new ClubRN().RechazarJugadorClubPrincipal(Cod_Persona);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<CategoriaClub> BuscarCategoriaClub()
        {
            try
            {
                return new ClubRN().BuscarCategoriaClub();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarClub(Club obj)
        {
            try
            {
                return new ClubRN().GuardarClub(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarDatosClub(Club obj)
        {
            try
            {
                return new ClubRN().GuardarDatosClub(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoClub(int Codigo, int EstadoClub)
        {
            try
            {
                return new ClubRN().CambiarEstadoClub(Codigo, EstadoClub);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoDestacadoClub(int Codigo, int EstadoDestacadoClub)
        {
            try
            {
                return new ClubRN().CambiarEstadoDestacadoClub(Codigo, EstadoDestacadoClub);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
