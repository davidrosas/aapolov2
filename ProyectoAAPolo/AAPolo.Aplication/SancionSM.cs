﻿using AAPolo.Business;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Aplication
{
    public class SancionSM
    {
        public List<Sancion> BuscarSancion(int Cod_Sancion, int Cod_Usuario, string CodigoArticulo, string Documento_NombreUsuario)
        {
            try
            {
                return new SancionRN().BuscarSancion(Cod_Sancion, Cod_Usuario, CodigoArticulo, Documento_NombreUsuario);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Sancion> BuscarSancionActiva()
        {
            try
            {
                return new SancionRN().BuscarSancionActiva();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
        public ResultadoTransaccion GuardarSancion(Sancion obj){
            try
            {
                return new SancionRN().GuardarSancion(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoSancion(int Codigo, int EstadoSancion)
        {
            try
            {
                return new SancionRN().CambiarEstadoSancion(Codigo, EstadoSancion);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
    }
}
