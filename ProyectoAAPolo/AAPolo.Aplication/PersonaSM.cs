﻿using AAPolo.Business;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Aplication
{
    public class PersonaSM
    {
        public Persona BuscarUsuarioLogin(Persona per)
        {
            try
            {
                return new PersonaRN().BuscarUsuarioLogin(per);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Persona BuscarUsuarioLoginPasswordEncriptado(Persona per)
        {
            try
            {
                return new PersonaRN().BuscarUsuarioLoginPasswordEncriptado(per);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Persona BuscarUsuarioSidepro(Persona p)
        {
            try
            {
                return new PersonaRN().BuscarUsuarioSidepro(p);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion AceptaTerminosCondiciones(int Cod_Persona, string telefono, string email)
        {
            try
            {
                return new PersonaRN().AceptaTerminosCondiciones(Cod_Persona, telefono, email);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GurdarImgPerfil(string urlImg, int Cod_Persona)
        {
            try
            {
                return new PersonaRN().GurdarImgPerfil(urlImg, Cod_Persona);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GurdarImgCertificado(string urlImg, int Cod_Persona)
        {
            try
            {
                return new PersonaRN().GurdarImgCertificado(urlImg, Cod_Persona);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Persona BuscarPersonaPorClaveVerificacion(string ClaveVerificacion)
        {
            try
            {
                return new PersonaRN().BuscarPersonaPorClaveVerificacion(ClaveVerificacion);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambioDePassword(string DocumentoIdentidad)
        {
            try
            {
                return new PersonaRN().CambioDePassword(DocumentoIdentidad);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarPasswordJugador(Persona obj)
        {
            try
            {
                return new PersonaRN().CambiarPasswordJugador(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscaJugador(Persona obj)
        {
            try
            {
                return new PersonaRN().BuscaJugador(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<List<Persona>> BuscaJugadorAsync(Persona obj)
        {
            try
            {
                return await Task.Run(() =>
                {
                    return new PersonaRN().BuscaJugador(obj);
                });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarDatosUsuario(Persona obj)
        {
            try
            {
                return new PersonaRN().GuardarDatosUsuario(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarMiembro(Miembro obj)
        {
            try
            {
                return new PersonaRN().GuardarMiembro(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoJugador(int Codigo, int EstadoJugador)
        {
            try
            {
                return new PersonaRN().CambiarEstadoJugador(Codigo, EstadoJugador);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoDestacadoJugador(int Codigo, int EstadoDestacadoJugador)
        {
            try
            {
                return new PersonaRN().CambiarEstadoDestacadoJugador(Codigo, EstadoDestacadoJugador);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscaJugadorResumen(int Cod_Persona, string Cadena, int Cod_TipoJugador, int Cod_Club, int Nro_Handicap, int Cod_Genero)
        {
            try
            {
                return new PersonaRN().BuscaJugadorResumen(Cod_Persona, Cadena, Cod_TipoJugador, Cod_Club, Nro_Handicap, Cod_Genero);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscaJugadorDetalleResumen(int Cod_Persona, string Cadena, int Cod_TipoJugador, int Cod_Club, int Nro_Handicap, int Cod_Genero)
        {
            try
            {
                return new PersonaRN().BuscaJugadorDetalleResumen(Cod_Persona, Cadena, Cod_TipoJugador, Cod_Club, Nro_Handicap, Cod_Genero);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscaJugadorResumenPerfilPublico(int Cod_Persona, string Cadena, int Cod_TipoJugador, int Cod_Club, int Nro_Handicap, int Cod_Genero)
        {
            try
            {
                return new PersonaRN().BuscaJugadorResumenPerfilPublico(Cod_Persona, Cadena, Cod_TipoJugador, Cod_Club, Nro_Handicap, Cod_Genero);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscaJugadorPagoHandicap(int Cod_Persona, string Cadena, int Cod_TipoJugador, int Cod_Club, int Nro_Handicap, int Cod_Genero = 0, bool EsPaseHandicap = false)
        {
            try
            {
                return new PersonaRN().BuscaJugadorPagoHandicap(Cod_Persona, Cadena, Cod_TipoJugador, Cod_Club, Nro_Handicap, EsPaseHandicap, Cod_Genero);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<List<Persona>> BuscaJugadorPagoHandicapAsync(int Cod_Persona, string Cadena, int Cod_TipoJugador, int Cod_Club, int Nro_Handicap, int Cod_Genero = 0, bool EsPaseHandicap = false)
        {
            try
            {
                return await Task.Run(() =>
                {
                    return new PersonaRN().BuscaJugadorPagoHandicap(Cod_Persona, Cadena, Cod_TipoJugador, Cod_Club, Nro_Handicap, EsPaseHandicap, Cod_Genero);
                });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscaJugadorInscripcionTorneo(int Cod_Persona, string Cadena, int Cod_TipoJugador, int Cod_Club, int Nro_Handicap, int Cod_Torneo, int Cod_Genero = 0, bool EsPaseHandicap = false)
        {
            try
            {
                return new PersonaRN().BuscaJugadorInscripcionTorneo(Cod_Persona, Cadena, Cod_TipoJugador, Cod_Club, Nro_Handicap, Cod_Torneo, Cod_Genero, EsPaseHandicap);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Genero> BuscarGenero()
        {
            try
            {
                return new PersonaRN().BuscarGenero();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<TipoDocumento> BuscarTipoDocumento()
        {
            try
            {
                return new PersonaRN().BuscarTipoDocumento();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Pais> BuscarPais()
        {
            try
            {
                return new PersonaRN().BuscarPais();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Provincia> BuscarProvincia(int Cod_Pais)
        {
            try
            {
                return new PersonaRN().BuscarProvincia(Cod_Pais);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Municipio> BuscarMunicipio(int Cod_Provincia)
        {
            try
            {
                return new PersonaRN().BuscarMunicipio(Cod_Provincia);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<TipoJugador> BuscarTipoJugador()
        {
            try
            {
                return new PersonaRN().BuscarTipoJugador();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarJugador(Persona obj, int Cod_Usuario_Alta)
        {
            try
            {
                return new PersonaRN().GuardarJugador(obj, Cod_Usuario_Alta);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarMember(Persona obj)
        {
            try
            {
                return new PersonaRN().GuardarMember(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        

        public ResultadoTransaccion ActualizarClubPrincipalUsuario(int Cod_Persona, int Cod_Club)
        {
            try
            {
                return new PersonaRN().ActualizarClubPrincipalUsuario(Cod_Persona, Cod_Club);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscaVeedor(string DocumentoNombre, int Cod_Usuario)
        {
            try
            {
                return new PersonaRN().BuscaVeedor(DocumentoNombre, Cod_Usuario);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
