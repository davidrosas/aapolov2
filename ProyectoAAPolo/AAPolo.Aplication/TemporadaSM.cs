﻿using AAPolo.Business;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Aplication
{
    public class TemporadaSM
    {
        public List<PuntoXClub> BuscarTemporadaPuntajeXClub(int Cod_Canje, int Cod_Club, int Cod_Temporada, int Cod_CategoriaAccion) {
            try
            {
                return new TemporadaRN().BuscarTemporadaPuntajeXClub(Cod_Canje, Cod_Club, Cod_Temporada, Cod_CategoriaAccion);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CategoriaAccion> BuscarCategoriaAccion()
        {
            try
            {
                return new TemporadaRN().BuscarCategoriaAccion();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Temporada> BuscarTemporada()
        {
            try
            {
                return new TemporadaRN().BuscarTemporada();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultadoTransaccion GuardarTemporada(PuntoXClub obj, int Cod_Usuario) {
            try
            {
                return new TemporadaRN().GuardarTemporada(obj, Cod_Usuario);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultadoTransaccion CambiarEstadoTemporada(int Codigo, int EstadoTemporada)
        {
            try
            {
                return new TemporadaRN().CambiarEstadoTemporada(Codigo, EstadoTemporada);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<PuntoXClub> BuscarTemporadaPuntajeXClubPublico(int Cod_Canje, int Cod_Club, int Cod_Temporada, int Cod_CategoriaAccion)
        {
            try
            {
                return new TemporadaRN().BuscarTemporadaPuntajeXClubPublico(Cod_Canje, Cod_Club, Cod_Temporada, Cod_CategoriaAccion);
            }
            catch (Exception)
            {
                throw;
            }
        }
        
    }
}
