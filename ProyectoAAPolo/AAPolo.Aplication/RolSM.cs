﻿using AAPolo.Business;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Aplication
{
    public class RolSM
    {
        public List<Rol> BuscarRolVisible()
        {
            try
            {
                return new RolRN().BuscarRolVisible();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
