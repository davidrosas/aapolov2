﻿using AAPolo.Business;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Aplication
{
    public class BeneficioSM
    {
        public List<CategoriaBeneficio> BuscarCategoriaBeneficio()
        {
            try
            {
                return new BeneficioRN().BuscarCategoriaBeneficio();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<TipoBeneficiario> BuscarTipoBeneficiario()
        {
            try
            {
                return new BeneficioRN().BuscarTipoBeneficiario();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Beneficio> BuscarBeneficio(int Cod_Beneficio, string Nombre, int Cod_CategoriaBeneficio, int Cod_TipoBeneficiario, bool CargarPrecios)
        {
            try
            {
                return new BeneficioRN().BuscarBeneficio(Cod_Beneficio, Nombre, Cod_CategoriaBeneficio, Cod_TipoBeneficiario, CargarPrecios);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Beneficio> BuscarBeneficioDestacadoActivo(int Cod_Beneficio, string Nombre, int Cod_CategoriaBeneficio, int Cod_TipoBeneficiario, bool CargarPrecios)
        {
            try
            {
                return new BeneficioRN().BuscarBeneficioDestacadoActivo(Cod_Beneficio, Nombre, Cod_CategoriaBeneficio, Cod_TipoBeneficiario, CargarPrecios);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Beneficio> BuscarBeneficioActivo(int Cod_Beneficio, string NombreContenido, int Cod_CategoriaBeneficio, int Cod_TipoBeneficiario, bool CargarPrecios)
        {
            try
            {
                return new BeneficioRN().BuscarBeneficioActivo(Cod_Beneficio, NombreContenido, Cod_CategoriaBeneficio, Cod_TipoBeneficiario, CargarPrecios);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultadoTransaccion GuardarBeneficio(Beneficio obj){
            try
            {
                return new BeneficioRN().GuardarBeneficio(obj);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultadoTransaccion CambiarEstadoBeneficio(int Codigo, int EstadoBeneficio)
        {
            try
            {
                return new BeneficioRN().CambiarEstadoBeneficio(Codigo, EstadoBeneficio);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoDestacadoBeneficio(int Codigo, int EstadoDestacadoBeneficio)
        {
            try
            {
                return new BeneficioRN().CambiarEstadoDestacadoBeneficio(Codigo, EstadoDestacadoBeneficio);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public EstadisticasCommunity BuscarEstadisticasCommunity()
        {
            try
            {
                return new BeneficioRN().BuscarEstadisticasCommunity();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Miembro> BuscarMiembro(int Cod_TipoDocumento, string Documento)
        {
            try
            {
                return new BeneficioRN().BuscarMiembro(Cod_TipoDocumento, Documento);
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
