﻿using AAPolo.Business;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Aplication
{
    public class NotificacionSM
    {
        public List<Notificacion> BuscarNotificacion(int Cod_Notificacion, string Titulo)
        {
            try
            {
                return new NotificacionRN().BuscarNotificacion(Cod_Notificacion, Titulo);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarNotificacion(Notificacion obj) {
            try
            {
                return new NotificacionRN().GuardarNotificacion(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<NotificacionUsuario> BuscarNotificacionAEnviar(int Cod_Notificacion)
        {
            try
            {
                return new NotificacionRN().BuscarNotificacionAEnviar(Cod_Notificacion);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<NotificacionUsuario> BuscarNotificacionUsuario(int Cod_Usuario, int Indice)
        {
            try
            {
                return new NotificacionRN().BuscarNotificacionUsuario(Cod_Usuario, Indice);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ActualizarNotificacionLeida(int Cod_NotificacionUsuario)
        {
            try
            {
                return new NotificacionRN().ActualizarNotificacionLeida(Cod_NotificacionUsuario);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
