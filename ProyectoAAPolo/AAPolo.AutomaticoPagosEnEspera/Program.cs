﻿using AAPolo.Aplication;
using AAPolo.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mercadopago;
using System.Configuration;
using System.Threading;

namespace AAPolo.AutomaticoPagosEnEspera
{
    class Program
    {
        public static string obtenerTagDelWebConfig(string pNombreTag)
        {
            var AppSettings = ConfigurationManager.AppSettings;
            string key = AppSettings[pNombreTag];
            return key;
        }

        static void Main(string[] args)
        {
            ResultadoTransaccion result = new ResultadoTransaccion();
            string Estado = "";
            Console.WriteLine("Inciando procesos de actualización de registros de pago. AAPOLO {0}", DateTime.Now);
            try
            {
                List<DetallePago> lPago = new PagoSM().BuscarPagosEnEspera();
                try
                {
                    string MercadoPagoClientID = obtenerTagDelWebConfig("MercadoPagoClientID"),
                        MercadoPagoClientSecret = obtenerTagDelWebConfig("MercadoPagoClientSecret");

                    MP mp = new MP(MercadoPagoClientID, MercadoPagoClientSecret);

                    int i = 0, iCancelado = 0, iAprobado = 0, iPendiente = 0;
                    List<string> lReferenciaAprobada = new List<string>();
                    List<string> lReferenciaPendiente = new List<string>();

                    foreach (var item in lPago)
                    {
                        int Estatus = 0;
                        string collection_id = item.collection_id;
                        Estado = "No se encontro registro en mercado pago.";
                        Dictionary<String, String> filters2 = new Dictionary<String, String>();
                        filters2.Add("external_reference", item.ReferenciaExterna);

                        // Search payment data according to filters
                        Hashtable searchResult2 = ((Hashtable)(mp.searchPayment(filters2, 0, 10)["response"]));
                        ArrayList misPagosRecibidos = (ArrayList)searchResult2["results"];

                        i++;
                        Console.WriteLine("{0}º - Referencia externa: {1} - Fecha transaccion: {2}\n", i, item.ReferenciaExterna, item.strFechaTransaccion);
                        
                        if (((TimeSpan)(DateTime.Now - item.FechaTransaccion)).Hours > 1 && (misPagosRecibidos == null || misPagosRecibidos.Count == 0))
                        {
                            Console.WriteLine(Estado);
                            Estatus = 3;
                            
                        }
                        else if (((TimeSpan)(DateTime.Now - item.FechaTransaccion)).Days > 30 && ((misPagosRecibidos == null || misPagosRecibidos.Count == 0) || !((Hashtable)misPagosRecibidos[0])["status"].ToString().Equals("approved")))
                        {
                            Estatus = 3;
                            Estado = "Cancelado por pasar más de 30 días.";
                        }
                        else
                        {
                            foreach (Hashtable miPago in misPagosRecibidos)
                            {
                                Estatus = 0;
                                collection_id = miPago["id"].ToString();
                                Console.WriteLine("Pago nro {0}. \n", i);
                                Console.WriteLine("Estatus de pago: {2} - collector_id: {0} - Referencia externa: {1}. - MismoCollection: {3} - Descripcion: {4}", miPago["id"].ToString(), miPago["external_reference"].ToString(), miPago["status"].ToString(), (miPago["id"].ToString().Equals(item.collection_id) ? "sí" : (String.IsNullOrEmpty(item.collection_id) ? "Sin collectionID en sistema" : "No concuerda")), miPago["description"].ToString());

                                if (miPago["status"].ToString().Equals("approved"))
                                {
                                    iAprobado++;
                                    lReferenciaAprobada.Add(miPago["external_reference"].ToString());
                                    Console.WriteLine("Aprobado - ReferenciaExterna: {0} - Collection_id: {1} \n", miPago["external_reference"].ToString(), miPago["id"].ToString());
                                    Estado = "Aprobado.";
                                    Estatus = 1;
                                }
                                else if (miPago["status"].ToString().Equals("cancelled"))
                                {
                                    iCancelado++;
                                    Console.WriteLine("Cancelado - ReferenciaExterna: {0} - Collection_id: {1} \n", miPago["external_reference"].ToString(), miPago["id"].ToString());
                                    Estado = "Cancelado.";
                                    Estatus = 3;
                                }
                                else if (miPago["status"].ToString().Equals("pending"))
                                {
                                    iPendiente++;
                                    lReferenciaPendiente.Add(miPago["external_reference"].ToString());
                                    if (miPago["id"].ToString() != item.collection_id)
                                    {
                                        Estatus = 2;
                                    }

                                    Console.WriteLine("Pendiente - ReferenciaExterna: {0} - Collection_id: {1} \n", miPago["external_reference"].ToString(), miPago["id"].ToString());
                                    Estado = "Pendiente.";
                                }
                                else
                                {
                                    Estado = miPago["status"].ToString();
                                }

                                /*if (!miPago["status"].ToString().Equals("approved") && !miPago["status"].ToString().Equals("cancelled") && !miPago["status"].ToString().Equals("pending")) {
                                    foreach (var miKey in miPago.Keys)
                                    {
                                        Console.WriteLine("{0}: {1}.", miKey.ToString(), miPago[miKey.ToString()]);
                                    }
                                }*/
                            }

                        }

                        /*if (Estatus > 0)
                        {
                            if (item.TipoPago.Codigo == ConfiguracionAPP.TipoPago_Handicap)
                            {
                                result = new PagoSM().ActualizarTransaccionHandicap(item.ReferenciaExterna, Estatus, collection_id, 0);
                            }
                            else if (item.TipoPago.Codigo == ConfiguracionAPP.TipoPago_Club)
                            {
                                result = new PagoSM().ActualizarTransacionPagoHandicap(item.ReferenciaExterna, Estatus, collection_id);
                            }
                            else if (item.TipoPago.Codigo == ConfiguracionAPP.TipoPago_Torneo)
                            {
                                result = new PagoSM().ActualizarTransacionPagoInscripcionEquipo(item.ReferenciaExterna, Estatus, collection_id);
                            }
                        }*/

                        Console.WriteLine("{0} - {1} - {2}", (String.IsNullOrEmpty(result.Mensaje) ? "Sin resultado" : result.Mensaje), item.ReferenciaExterna, Estado);

                    }

                    Console.WriteLine("Actualizacion de pagos en espera. {0} - Total de registros: {1} - Total cancelado: {2} - Total Aprobado: {3} - Total pendiente: {4}\n", DateTime.Now, i, iCancelado, iAprobado, iPendiente);
                    /*Console.WriteLine("Aprobada -------------------------------------------------");
                    foreach (var item in lReferenciaAprobada)
                    {
                        Console.WriteLine("Aprobado Referencia {0} ", item);
                    }
                    Console.WriteLine("Pendiente -------------------------------------------------");
                    foreach (var item in lReferenciaPendiente)
                    {
                        Console.WriteLine("Pendiente Referencia {0} ", item);
                    }*/
                }
                catch (Exception e)
                {
                    Console.WriteLine("Finalización de servicio de actualizacion de pagos en espera finalizada con errores {0}. Mensaje: {1} \n", DateTime.Now, e.Message);
                }

                Console.WriteLine("Finalización de servicio de actualizacion de pagos en espera. {0} \n", DateTime.Now);
            }
            catch (Exception e)
            {
                Console.WriteLine("Finalización de servicio de actualizacion de pagos en espera finalizada con errores {0}. Mensaje: {1} \n", DateTime.Now, e.Message);
            }

            int milisegundos = 12000;
            Thread.Sleep(milisegundos);
            Console.WriteLine("......");
            Console.ReadLine();
            //Console.ReadLine();

        }
    }
}
