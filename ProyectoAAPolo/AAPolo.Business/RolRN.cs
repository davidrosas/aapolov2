﻿using AAPolo.Data;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Business
{
    public class RolRN
    {
        public List<Rol> BuscarRolVisible()
        {
            try
            {
                return new RolAD().BuscarRolVisible();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
