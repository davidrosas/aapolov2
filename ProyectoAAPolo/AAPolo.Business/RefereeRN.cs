﻿using AAPolo.Data;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Business
{
    public class RefereeRN
    {
        public List<Referee> BuscaReferee(string DocumentoNombre, int Cod_tipoReferee, int Cod_Usuario, int Cod_Referee)
        {
            try
            {
                return new RefereeAD().BuscaReferee(DocumentoNombre, Cod_tipoReferee, Cod_Usuario, Cod_Referee);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Referee> BuscarPartidoEvaluacionReferee(int Cod_Usuario, int Cod_Torneo, bool SoloPartidoEfectuado)
        {
            try
            {
                return new RefereeAD().BuscarPartidoEvaluacionReferee(Cod_Usuario, Cod_Torneo, SoloPartidoEfectuado);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Referee> BuscarEvaluacionReferee(int Cod_Usuario)
        {
            try
            {
                return new RefereeAD().BuscarEvaluacionReferee(Cod_Usuario);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Evaluacion BuscarEvaluacionReferee(int Cod_Usuario, int Cod_Evaluacion)
        {
            try
            {
                return new RefereeAD().BuscarEvaluacionReferee(Cod_Usuario, Cod_Evaluacion);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<TipoReferee> BuscaTipoReferee()
        {
            try
            {
                return new RefereeAD().BuscaTipoReferee();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscarPosibleReferee(int Cod_Usuario, string DocumentoNombre)
        {
            try
            {
                return new RefereeAD().BuscarPosibleReferee(Cod_Usuario, DocumentoNombre);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Referee> RankingReferee(string Nombre, int Cod_TipoReferee, int Cod_Usuario)
        {
            try
            {
                return new RefereeAD().RankingReferee(Nombre, Cod_TipoReferee, Cod_Usuario);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarReferee(int Cod_Usuario, int Cod_TipoReferee)
        {
            try
            {
                return new RefereeAD().GuardarReferee(Cod_Usuario, Cod_TipoReferee);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoReferee(int Codigo, int EstadoReferee)
        {
            try
            {
                return new RefereeAD().CambiarEstadoReferee(Codigo, EstadoReferee);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
