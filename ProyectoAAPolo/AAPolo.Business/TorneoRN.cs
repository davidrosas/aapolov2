﻿using AAPolo.Data;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Business
{
    public class TorneoRN
    {
        public List<ProductoTorneo> BuscarProductoTorneo(bool SoloActivo)
        {
            try
            {
                return new TorneoAD().BuscarProductoTorneo(SoloActivo);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Torneo> BuscarTorneo(int Cod_Torneo, int Cod_CategoriaTorneo, int Cod_CategoriaHandicap, bool SoloActivo)
        {
            try
            {
                return new TorneoAD().BuscarTorneo(Cod_Torneo, Cod_CategoriaTorneo, Cod_CategoriaHandicap, SoloActivo);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Torneo> BuscarTorneoHistorico(int Cod_Torneo, string NombreTorneo, int Cod_CategoriaTorneo, int Cod_CategoriaHandicap, bool SoloActivo)
        {
            try
            {
                return new TorneoAD().BuscarTorneoHistorico(Cod_Torneo, NombreTorneo, Cod_CategoriaTorneo, Cod_CategoriaHandicap, SoloActivo);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Torneo> BuscarTorneoInscipcionActivo(int Cod_Torneo, int Cod_CategoriaTorneo)
        {
            try
            {
                return new TorneoAD().BuscarTorneoInscipcionActivo(Cod_Torneo, Cod_CategoriaTorneo);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<TipoTorneo> BuscarTipoTorneo()
        {
            try
            {
                return new TorneoAD().BuscarTipoTorneo();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<CategoriaHandicap> BuscarCategoriaHandicapTorneo()
        {
            try
            {
                return new TorneoAD().BuscarCategoriaHandicapTorneo();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarTorneo(Torneo obj)
        {
            try
            {
                return new TorneoAD().GuardarTorneo(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoTorneo(int Codigo, int EstadoTorneo)
        {
            try
            {
                return new TorneoAD().CambiarEstadoTorneo(Codigo, EstadoTorneo);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Equipo> BuscarEquipo(int Cod_Equipo, int Cod_Persona, int Cod_Torneo)
        {
            try
            {
                return new TorneoAD().BuscarEquipo(Cod_Equipo, Cod_Persona, Cod_Torneo);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ActualizarInscripcionTorneoAutomatico()
        {
            try
            {
                return new TorneoAD().ActualizarInscripcionTorneoAutomatico();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarCodigoProducto(ProductoTorneo obj)
        {
            try
            {
                return new TorneoAD().GuardarCodigoProducto(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Zona> BuscarZonaTorneo(int Cod_Torneo, bool CargarEquipo, int Cod_Zona)
        {
            try
            {
                return new TorneoAD().BuscarZonaTorneo(Cod_Torneo, CargarEquipo, Cod_Zona);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarZonaTorneo(Torneo obj)
        {
            try
            {
                return new TorneoAD().GuardarZonaTorneo(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<TipoZona> BuscarTipoZonaTorneo()
        {
            try
            {
                return new TorneoAD().BuscarTipoZonaTorneo();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Equipo> BuscarEquipoZonaTorneo(int Cod_Equipo, string Nombre, int Cod_Torneo, int Cod_Zona)
        {
            try
            {
                return new TorneoAD().BuscarEquipoZonaTorneo(Cod_Equipo, Nombre, Cod_Torneo, Cod_Zona);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarEquipoZona(Zona obj)
        {
            try
            {
                return new TorneoAD().GuardarEquipoZona(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Equipo> BuscarEquipoZona(int Cod_Zona, int Cod_Torneo, int Cod_Equipo)
        {

            try
            {
                return new TorneoAD().BuscarEquipoZona(Cod_Zona, Cod_Torneo, Cod_Equipo);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Equipo> BuscarEquipoPartido(int Cod_Zona, int Cod_Torneo, int Cod_Partido)
        {
            try
            {
                return new TorneoAD().BuscarEquipoPartido(Cod_Zona, Cod_Torneo, Cod_Partido);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Equipo> BuscarEquipoPartidoSegundaFase(int Cod_Zona, int Cod_Torneo)
        {
            try
            {
                return new TorneoAD().BuscarEquipoPartidoSegundaFase(Cod_Zona, Cod_Torneo);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Partido> BuscarPartidoZona(int Cod_Zona, int Cod_Torneo, int Cod_Instancia, int Cod_Partido)
        {
            try
            {
                return new TorneoAD().BuscarPartidoZona(Cod_Zona, Cod_Torneo, Cod_Instancia, Cod_Partido);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultadoTransaccion GuardarPartido(Partido obj, int Cod_Usuario)
        {
            try
            {
                return new TorneoAD().GuardarPartido(obj, Cod_Usuario);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Incidencia> BuscarIncidencia()
        {
            try
            {
                return new TorneoAD().BuscarIncidencia();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Copa> BuscarCopaTorneo(int Cod_Torneo, int Cod_Copa, bool CargarPartidoZona)
        {
            try
            {
                return new TorneoAD().BuscarCopaTorneo(Cod_Torneo, Cod_Copa, CargarPartidoZona);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Equipo> BuscarEquipoPartidoInstancia(int Cod_Partido)
        {
            try
            {
                return new TorneoAD().BuscarEquipoPartidoInstancia(Cod_Partido);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultadoTransaccion GuardarCopa(List<Copa> obj, int Cod_Usuario)
        {
            try
            {
                return new TorneoAD().GuardarCopa(obj, Cod_Usuario);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Partido> BuscarPartidoInstancia(int Cod_Torneo, int Cod_Instancia, int Cod_Partido, int SoloCopaPrincipal)
        {
            try
            {
                return new TorneoAD().BuscarPartidoInstancia(Cod_Torneo, Cod_Instancia, Cod_Partido, SoloCopaPrincipal);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultadoTransaccion GuardarPartidoInstancia(Partido obj, int Cod_Usuario)
        {
            try
            {
                return new TorneoAD().GuardarPartidoInstancia(obj, Cod_Usuario);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Persona> ListadoPersonaEquipoPartido(int Cod_Equipo, int Cod_Partido)
        {
            try
            {
                return new TorneoAD().ListadoPersonaEquipoPartido(Cod_Equipo, Cod_Partido);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion DisponibleZona(int Cod_Torneo, bool Disponible)
        {
            try
            {
                return new TorneoAD().DisponibleZona(Cod_Torneo, Disponible);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion DisponibleLlaves(int Cod_Torneo, bool Disponible)
        {
            try
            {
                return new TorneoAD().DisponibleLlaves(Cod_Torneo, Disponible);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public ResultadoTransaccion DisponibleParidoLlaves(int Cod_Torneo, bool Disponible)
        {
            try
            {
                return new TorneoAD().DisponibleParidoLlaves(Cod_Torneo, Disponible);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
