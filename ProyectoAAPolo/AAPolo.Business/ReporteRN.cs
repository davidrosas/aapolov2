﻿using AAPolo.Data;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Business
{
    public class ReporteRN
    {
        public List<Club> BuscarReporteClub(int Cod_Persona, int AnioEnCurso, int Cod_Club)
        {
            try
            {
                return new ReporteAD().BuscarReporteClub(Cod_Persona, AnioEnCurso, Cod_Club);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscarReportePagoHandicap(int Cod_Persona, string Cadena, int Cod_TipoJugador, int Cod_Club, int Nro_Handicap)
        {
            try
            {
                return new ReporteAD().BuscarReportePagoHandicap(Cod_Persona, Cadena, Cod_TipoJugador, Cod_Club, Nro_Handicap);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Equipo> BuscarReporteEquipo(int Cod_Equipo, int Cod_Torneo, string Cadena, int Cod_EstadoPago)
        {
            try
            {
                return new ReporteAD().BuscarReporteEquipo(Cod_Equipo, Cod_Torneo, Cadena, Cod_EstadoPago);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<MedioDePago> BuscarMedioDePago()
        {
            try
            {
                return new ReporteAD().BuscarMedioDePago();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ConfirmarPagoHandicap(Persona obj, int Cod_UsuarioTransaccion)
        {
            try
            {
                return new ReporteAD().ConfirmarPagoHandicap(obj, Cod_UsuarioTransaccion);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ConfirmarPagoClub(Club obj, int Cod_UsuarioTransaccion)
        {
            try
            {
                return new ReporteAD().ConfirmarPagoClub(obj, Cod_UsuarioTransaccion);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ConfirmarPagoEquipo(Equipo obj, int Cod_UsuarioTransaccion)
        {
            try
            {
                return new ReporteAD().ConfirmarPagoEquipo(obj, Cod_UsuarioTransaccion);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion DeclinarPagoEquipo(int Cod_Pago)
        {
            try
            {
                return new ReporteAD().DeclinarPagoEquipo(Cod_Pago);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ReporteEstadisticaHandicap BuscarReporteEstadisticaHandicap()
        {
            try
            {
                return new ReporteAD().BuscarReporteEstadisticaHandicap();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #region descargables
        public List<reporteCuentaClub> GetReporteClub()
        {
            try
            {
                return new ReporteAD().GetReporteClub();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<reporteCuentaClub> GetReporteDatosClub()
        {
            try
            {
                return new ReporteAD().GetReporteDatosClub();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<reporteCuentaJugadores> GetReporteCuentaJugadores()
        {
            try
            {
                return new ReporteAD().GetReporteCuentaJugadores();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<reporteCuentaJugadores> GetReporteAllCuentasCorrientesJugadores()
        {
            try
            {
                return new ReporteAD().GetReporteAllCuentasCorrientesJugadores();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<reporteCuentaTorneo> GetReporteInscripcionesEquiposExcel()
        {
            try
            {
                return new ReporteAD().GetReporteInscripcionesEquiposExcel();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<reporteTorneo> GetReporteInscripcionesTorneosExcel(int cod_torneo)
        {
            try
            {
                return new ReporteAD().GetReporteInscripcionesTorneosExcel(cod_torneo);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<reporteJugadorDTO> GetReporteJugadores()
        {

            try
            {
                return new ReporteAD().GetReporteJugadores();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion
    }
}
