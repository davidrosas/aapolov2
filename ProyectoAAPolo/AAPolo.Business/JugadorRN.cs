﻿using AAPolo.Data;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Business
{
    public class JugadorRN
    {

        public List<JugadorHandicap> BuscarJugadorHandicap(int Cod_TipoJugador, int Cod_Handicap, int Cod_CategoriaHandicap)
        {
            try
            {
                return new JugadorAD().BuscarJugadorHandicap(Cod_TipoJugador, Cod_Handicap, Cod_CategoriaHandicap);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
