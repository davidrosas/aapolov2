﻿using AAPolo.Data;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Business
{
    public class PagoRN
    {
        public List<Vigencia> BuscarVigencia()
        {
            try
            {
                return new PagoAD().BuscarVigencia();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<CategoriaHandicap> BuscarCategoriaHandicap(int Cod_TipoJugador)
        {
            try
            {
                return new PagoAD().BuscarCategoriaHandicap(Cod_TipoJugador);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Handicap> BuscarCostoHandicap(int Cod_TipoJugador, int Cod_Vigencia, int Cod_CategoriaHandicap)
        {
            try
            {
                return new PagoAD().BuscarCostoHandicap(Cod_TipoJugador, Cod_Vigencia, Cod_CategoriaHandicap);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ProcesarPagoHandicap(List<Persona> lPersona, string idReference, int Cod_UsuarioTramite)
        {
            try
            {
                return new PagoAD().ProcesarPagoHandicap(lPersona, idReference, Cod_UsuarioTramite);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ProcesarPagoHandicapPase(List<Persona> lPersona, string idReference, int Cod_UsuarioTramite)
        {
            try
            {
                return new PagoAD().ProcesarPagoHandicapPase(lPersona, idReference, Cod_UsuarioTramite);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion PagoAfiliacionClub(int Cod_Club, int Cod_Persona, string ReferenciaExterna)
        {
            try
            {
                return new PagoAD().PagoAfiliacionClub(Cod_Club, Cod_Persona, ReferenciaExterna);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarInscripcionEquipo(Equipo obj, string idReference, int Cod_Persona)
        {
            try
            {
                return new PagoAD().GuardarInscripcionEquipo(obj, idReference, Cod_Persona);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ActualizarTransacionPagoHandicap(string external_reference, int Cod_Estado, string collection_id)
        {
            try
            {
                return new PagoAD().ActualizarTransacionPagoHandicap(external_reference, Cod_Estado, collection_id);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ActualizarTransacionPagoInscripcionEquipo(string external_reference, int Cod_Estado, string collection_id)
        {
            try
            {
                return new PagoAD().ActualizarTransacionPagoInscripcionEquipo(external_reference, Cod_Estado, collection_id);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ActualizarTransaccionHandicap(string external_reference, int Cod_Estado, string collection_id, int Cod_Persona)
        {
            try
            {
                return new PagoAD().ActualizarTransaccionHandicap(external_reference, Cod_Estado, collection_id, Cod_Persona);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Pago> BuscarPago(int Cod_Persona)
        {
            try
            {
                return new PagoAD().BuscarPago(Cod_Persona);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Handicap> BuscarHandicapTransicion(int Cod_Handicap)
        {
            try
            {
                return new PagoAD().BuscarHandicapTransicion(Cod_Handicap);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<DetallePago> BuscarPagosEnEspera()
        {
            try
            {
                return new PagoAD().BuscarPagosEnEspera();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Order> BuscarPagosNoFacturados(int CodPago)
        {
            try
            {
                return new PagoAD().BuscarPagosNoFacturados(CodPago);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultadoTransaccion ActualizarRegistroTransaccionFacturacion(int Cod_Pago)
        {
            try
            {
                return new PagoAD().ActualizarRegistroTransaccionFacturacion(Cod_Pago);
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
