﻿using AAPolo.Data;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Business
{
    public class PersonaRN
    {

        public Persona BuscarUsuarioLogin(Persona per)
        {
            try
            {
                return new PersonaAD().BuscarUsuarioLogin(per);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Persona BuscarUsuarioLoginPasswordEncriptado(Persona per)
        {
            try
            {
                return new PersonaAD().BuscarUsuarioLoginPasswordEncriptado(per);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Persona BuscarUsuarioSidepro(Persona p)
        {
            try
            {
                return new PersonaAD().BuscarUsuarioSidepro(p);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion AceptaTerminosCondiciones(int Cod_Persona, string telefono, string email)
        {
            try
            {
                return new PersonaAD().AceptaTerminosCondiciones(Cod_Persona, telefono, email);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GurdarImgPerfil(string urlImg, int Cod_Persona)
        {
            try
            {
                return new PersonaAD().GurdarImgPerfil(urlImg, Cod_Persona);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GurdarImgCertificado(string urlImg, int Cod_Persona)
        {
            try
            {
                return new PersonaAD().GurdarImgCertificado(urlImg, Cod_Persona);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Persona BuscarPersonaPorClaveVerificacion(string ClaveVerificacion)
        {
            try
            {
                return new PersonaAD().BuscarPersonaPorClaveVerificacion(ClaveVerificacion);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambioDePassword(string DocumentoIdentidad)
        {
            try
            {
                return new PersonaAD().CambioDePassword(DocumentoIdentidad);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarPasswordJugador(Persona obj)
        {
            try
            {
                return new PersonaAD().CambiarPasswordJugador(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscaJugador(Persona obj)
        {
            try
            {
                return new PersonaAD().BuscaJugador(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarDatosUsuario(Persona obj)
        {
            try
            {
                return new PersonaAD().GuardarDatosUsuario(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarMiembro(Miembro obj)
        {
            try
            {
                return new PersonaAD().GuardarMiembro(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoJugador(int Codigo, int EstadoJugador)
        {
            try
            {
                return new PersonaAD().CambiarEstadoJugador(Codigo, EstadoJugador);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoDestacadoJugador(int Codigo, int EstadoDestacadoJugador)
        {
            try
            {
                return new PersonaAD().CambiarEstadoDestacadoJugador(Codigo, EstadoDestacadoJugador);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscaJugadorResumen(int Cod_Persona, string Cadena, int Cod_TipoJugador, int Cod_Club, int Nro_Handicap, int Cod_Genero)
        {
            try
            {
                return new PersonaAD().BuscaJugadorResumen(Cod_Persona, Cadena, Cod_TipoJugador, Cod_Club, Nro_Handicap, Cod_Genero);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscaJugadorDetalleResumen(int Cod_Persona, string Cadena, int Cod_TipoJugador, int Cod_Club, int Nro_Handicap, int Cod_Genero) {
            try
            {
                return new PersonaAD().BuscaJugadorDetalleResumen(Cod_Persona, Cadena, Cod_TipoJugador, Cod_Club, Nro_Handicap, Cod_Genero);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscaJugadorResumenPerfilPublico(int Cod_Persona, string Cadena, int Cod_TipoJugador, int Cod_Club, int Nro_Handicap, int Cod_Genero)
        {
            try
            {
                return new PersonaAD().BuscaJugadorResumenPerfilPublico(Cod_Persona, Cadena, Cod_TipoJugador, Cod_Club, Nro_Handicap, Cod_Genero);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
        public List<Persona> BuscaJugadorPagoHandicap(int Cod_Persona, string Cadena, int Cod_TipoJugador, int Cod_Club, int Nro_Handicap, bool EsPaseHandicap, int Cod_Genero)
        {
            try
            {
                return new PersonaAD().BuscaJugadorPagoHandicap(Cod_Persona, Cadena, Cod_TipoJugador, Cod_Club, Nro_Handicap, EsPaseHandicap, Cod_Genero);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscaJugadorInscripcionTorneo(int Cod_Persona, string Cadena, int Cod_TipoJugador, int Cod_Club, int Nro_Handicap, int Cod_Torneo, int Cod_Genero = 0, bool EsPaseHandicap = false)
        {
            try
            {
                return new PersonaAD().BuscaJugadorInscripcionTorneo(Cod_Persona, Cadena, Cod_TipoJugador, Cod_Club, Nro_Handicap, Cod_Torneo, Cod_Genero, EsPaseHandicap);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Genero> BuscarGenero()
        {
            try
            {
                return new PersonaAD().BuscarGenero();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<TipoDocumento> BuscarTipoDocumento()
        {
            try
            {
                return new PersonaAD().BuscarTipoDocumento();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Pais> BuscarPais()
        {
            try
            {
                return new PersonaAD().BuscarPais();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Provincia> BuscarProvincia(int Cod_Pais)
        {
            try
            {
                return new PersonaAD().BuscarProvincia(Cod_Pais);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Municipio> BuscarMunicipio(int Cod_Provincia)
        {
            try
            {
                return new PersonaAD().BuscarMunicipio(Cod_Provincia);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<TipoJugador> BuscarTipoJugador()
        {
            try
            {
                return new PersonaAD().BuscarTipoJugador();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarJugador(Persona obj, int Cod_Usuario_Alta)
        {
            try
            {
                return new PersonaAD().GuardarJugador(obj, Cod_Usuario_Alta);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarMember(Persona obj)
        {
            try
            {
                return new PersonaAD().GuardarMember(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ActualizarClubPrincipalUsuario(int Cod_Persona, int Cod_Club)
        {
            try
            {
                return new PersonaAD().ActualizarClubPrincipalUsuario(Cod_Persona, Cod_Club);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Persona> BuscaVeedor(string DocumentoNombre, int Cod_Usuario)
        {
            try
            {
                return new PersonaAD().BuscaVeedor(DocumentoNombre, Cod_Usuario);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


    }
}
