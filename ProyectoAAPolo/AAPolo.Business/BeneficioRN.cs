﻿using AAPolo.Data;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Business
{
    public class BeneficioRN
    {
        public List<CategoriaBeneficio> BuscarCategoriaBeneficio()
        {
            try
            {
                return new BeneficioAD().BuscarCategoriaBeneficio();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<TipoBeneficiario> BuscarTipoBeneficiario()
        {
            try
            {
                return new BeneficioAD().BuscarTipoBeneficiario();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Beneficio> BuscarBeneficio(int Cod_Beneficio, string Nombre, int Cod_CategoriaBeneficio, int Cod_TipoBeneficiario, bool CargarPrecios)
        {
            try
            {
                return new BeneficioAD().BuscarBeneficio(Cod_Beneficio, Nombre, Cod_CategoriaBeneficio, Cod_TipoBeneficiario, CargarPrecios);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Beneficio> BuscarBeneficioDestacadoActivo(int Cod_Beneficio, string Nombre, int Cod_CategoriaBeneficio, int Cod_TipoBeneficiario, bool CargarPrecios)
        {
            try
            {
                return new BeneficioAD().BuscarBeneficioDestacadoActivo(Cod_Beneficio, Nombre, Cod_CategoriaBeneficio, Cod_TipoBeneficiario, CargarPrecios);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Beneficio> BuscarBeneficioActivo(int Cod_Beneficio, string NombreContenido, int Cod_CategoriaBeneficio, int Cod_TipoBeneficiario, bool CargarPrecios)
        {
            try
            {
                return new BeneficioAD().BuscarBeneficioActivo(Cod_Beneficio, NombreContenido, Cod_CategoriaBeneficio, Cod_TipoBeneficiario, CargarPrecios);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultadoTransaccion GuardarBeneficio(Beneficio obj)
        {
            try
            {
                return new BeneficioAD().GuardarBeneficio(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoBeneficio(int Codigo, int EstadoBeneficio)
        {
            try
            {
                return new BeneficioAD().CambiarEstadoBeneficio(Codigo, EstadoBeneficio);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoDestacadoBeneficio(int Codigo, int EstadoDestacadoBeneficio)
        {
            try
            {
                return new BeneficioAD().CambiarEstadoDestacadoBeneficio(Codigo, EstadoDestacadoBeneficio);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public EstadisticasCommunity BuscarEstadisticasCommunity()
        {
            try
            {
                return new BeneficioAD().BuscarEstadisticasCommunity();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Miembro> BuscarMiembro(int Cod_TipoDocumento, string Documento) {
            try
            {
                return new BeneficioAD().BuscarMiembro(Cod_TipoDocumento, Documento);
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
