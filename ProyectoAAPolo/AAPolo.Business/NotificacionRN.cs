﻿using AAPolo.Data;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Business
{
    public class NotificacionRN
    {
        public List<Notificacion> BuscarNotificacion(int Cod_Notificacion, string Titulo)
        {
            try
            {
                return new NotificacionAD().BuscarNotificacion(Cod_Notificacion, Titulo);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarNotificacion(Notificacion obj)
        {
            try
            {
                return new NotificacionAD().GuardarNotificacion(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<NotificacionUsuario> BuscarNotificacionAEnviar(int Cod_Notificacion)
        {
            try
            {
                return new NotificacionAD().BuscarNotificacionAEnviar(Cod_Notificacion);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<NotificacionUsuario> BuscarNotificacionUsuario(int Cod_Usuario, int Indice)
        {
            try
            {
                return new NotificacionAD().BuscarNotificacionUsuario(Cod_Usuario, Indice);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion ActualizarNotificacionLeida(int Cod_NotificacionUsuario)
        {
            try
            {
                return new NotificacionAD().ActualizarNotificacionLeida(Cod_NotificacionUsuario);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
