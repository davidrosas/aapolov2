﻿using AAPolo.Data;
using AAPolo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AAPolo.Business
{
    public class ClubRN
    {

        public List<Club> BuscaClub(int Cod_CategoriaClub, int Cod_Club, string NombreClub, int Cod_UsuarioAdmin) {

            try
            {
                return new ClubAD().BuscaClub(Cod_CategoriaClub, Cod_Club, NombreClub, Cod_UsuarioAdmin);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Club> BuscarClubActivo(int Cod_CategoriaClub, int Cod_Club, string NombreClub, int Cod_UsuarioAdmin, int Cod_Provincia)
        {
            try
            {
                return new ClubAD().BuscarClubActivo(Cod_CategoriaClub, Cod_Club, NombreClub, Cod_UsuarioAdmin, Cod_Provincia);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarImagenClub(string urlImg, int Cod_Club)
        {
            try
            {
                return new ClubAD().GuardarImagenClub(urlImg, Cod_Club);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion RechazarJugadorClubPrincipal(int Cod_Persona)
        {
            try
            {
                return new ClubAD().RechazarJugadorClubPrincipal(Cod_Persona);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<CategoriaClub> BuscarCategoriaClub()
        {
            try
            {
                return new ClubAD().BuscarCategoriaClub();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarClub(Club obj)
        {
            try
            {
                return new ClubAD().GuardarClub(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion GuardarDatosClub(Club obj)
        {
            try
            {
                return new ClubAD().GuardarDatosClub(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoClub(int Codigo, int EstadoClub)
        {
            try
            {
                return new ClubAD().CambiarEstadoClub(Codigo, EstadoClub);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultadoTransaccion CambiarEstadoDestacadoClub(int Codigo, int EstadoDestacadoClub)
        {
            try
            {
                return new ClubAD().CambiarEstadoDestacadoClub(Codigo, EstadoDestacadoClub);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
